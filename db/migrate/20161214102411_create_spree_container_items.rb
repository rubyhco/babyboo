class CreateSpreeContainerItems < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_container_items do |t|
      t.integer :container_id
      t.integer :variant_id
      t.integer :quantity
      t.integer :items
      t.integer :total_items
      t.string :status
      t.decimal :import_cost
      t.string :pack
      t.integer :qty_send
      t.timestamps
    end
  end
end
