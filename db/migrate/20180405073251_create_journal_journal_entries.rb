class CreateJournalJournalEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_journal_entries do |t|
      t.text :description
      t.string :number
      t.datetime :date

      t.timestamps
    end
  end
end
