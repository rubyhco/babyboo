class AddDescriptionToSpreeLostItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_lost_items, :description, :string
  end
end
