class CreateSpreePages < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_pages do |t|
      t.string :name
      t.string :permalink
      t.text :content
      t.timestamps
    end
  end
end
