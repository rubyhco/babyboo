class AddJournalTransactionToPayments < ActiveRecord::Migration[5.0]
  def change
  	add_reference :spree_payments, :journal_transaction, index: true, foreign_key: true
    add_reference :spree_invoice_payments, :journal_transaction, index: true, foreign_key: true
    add_reference :spree_containers, :journal_transaction, index: true, foreign_key: true
    add_reference :journal_invoices, :journal_transaction, index: true, foreign_key: true
    add_reference :journal_bills, :journal_transaction, index: true, foreign_key: true
  end
end
