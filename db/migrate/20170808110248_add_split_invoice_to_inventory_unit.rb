class AddSplitInvoiceToInventoryUnit < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_inventory_units, :split_invoice, :boolean, default: false
  end
end
