class AddSupplierIdToSpreeContainers < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_containers, :supplier_id, :integer
  end
end
