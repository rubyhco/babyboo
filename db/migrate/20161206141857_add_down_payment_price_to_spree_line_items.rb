class AddDownPaymentPriceToSpreeLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_line_items, :down_payment_price, :decimal
  end
end
