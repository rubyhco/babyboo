class ChangeDescriptionTypeDataToText < ActiveRecord::Migration[5.0]
  def change
    change_column :spree_stores, :description, :text
  end
end
