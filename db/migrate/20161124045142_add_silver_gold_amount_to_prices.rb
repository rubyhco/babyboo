class AddSilverGoldAmountToPrices < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_prices, :silver_amount, :decimal, precision: 8, scale: 2
  	add_column :spree_prices, :gold_amount, :decimal, precision: 8, scale: 2
  end
end
