class AddLockToSpreeOrders < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_orders, :lock, :boolean, default: false
  end
end
