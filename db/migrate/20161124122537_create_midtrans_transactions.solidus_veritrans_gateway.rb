# This migration comes from solidus_veritrans_gateway (originally 20161124111451)
class CreateMidtransTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :midtrans_transactions do |t|
      t.string :payment_method_id
      t.string :issuer_id
      t.string :transaction_id
      t.string :payment_url
      t.timestamps
    end
  end
end
