class AddIsOpenToSpreeTaxons < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_taxons, :is_open, :boolean, default: true
  end
end
