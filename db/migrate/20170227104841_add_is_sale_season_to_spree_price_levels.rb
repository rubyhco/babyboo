class AddIsSaleSeasonToSpreePriceLevels < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_price_levels, :is_sale_season, :boolean, default: false
  end
end
