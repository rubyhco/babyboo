class AddOriginatorTypeToJournalItem < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_journal_items, :originator_type, :string
  end
end
