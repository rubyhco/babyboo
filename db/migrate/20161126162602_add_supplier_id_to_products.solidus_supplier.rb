# This migration comes from solidus_supplier (originally 20161126161314)
class AddSupplierIdToProducts < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_products, :supplier_id, :integer
  end
end
