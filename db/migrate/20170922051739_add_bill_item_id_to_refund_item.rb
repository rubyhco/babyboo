class AddBillItemIdToRefundItem < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_refund_purchase_items, :bill_item_id, :integer
  end
end
