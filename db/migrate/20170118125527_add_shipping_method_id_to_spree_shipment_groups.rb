class AddShippingMethodIdToSpreeShipmentGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_shipment_groups, :shipping_method_id, :integer
  end
end
