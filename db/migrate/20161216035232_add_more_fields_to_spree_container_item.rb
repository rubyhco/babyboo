class AddMoreFieldsToSpreeContainerItem < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_container_items, :purchase_order_id, :integer
    add_column :spree_container_items, :weight, :decimal, precision: 8, scale: 2
    add_column :spree_container_items, :qty_received, :integer
  end
end
