class AddExpectedShippedToInventoryUnit < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_inventory_units, :expected_shipped, :integer, default: 0
  end
end
