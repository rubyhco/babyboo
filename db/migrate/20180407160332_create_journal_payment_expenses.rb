class CreateJournalPaymentExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_payment_expenses do |t|
    	t.string :number
    	t.string :state
      t.integer :account_id
      t.text :description
      t.decimal :amount
      t.integer :transaction_id
      t.references :originator, polymorphic: true, index: {:name => "index_journal_payment_expenses_on_originator"}
      
      t.timestamps
    end
  end
end
