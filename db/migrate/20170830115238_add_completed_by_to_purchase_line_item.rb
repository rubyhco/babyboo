class AddCompletedByToPurchaseLineItem < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_purchase_line_items, :completed_by, :integer
  end
end
