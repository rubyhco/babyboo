class AddInvoiceIdToSpreeShipments < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_shipments, :invoice_id, :integer
  end
end
