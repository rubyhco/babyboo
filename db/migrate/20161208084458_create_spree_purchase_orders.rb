class CreateSpreePurchaseOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_purchase_orders do |t|
      t.decimal :total
      t.string :payment_state
      t.integer :user_id
      t.integer :supplier_id
      t.datetime :completed_at
      t.string :shipping_state
      t.string :notes
      t.timestamps
    end
  end
end
