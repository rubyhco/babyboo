class AddResponseCodetoJournalInvoicePayment < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_invoice_payments, :response_code, :string
  end
end
