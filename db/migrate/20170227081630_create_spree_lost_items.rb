class CreateSpreeLostItems < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_lost_items do |t|
      t.string :number
      t.timestamps
    end
  end
end
