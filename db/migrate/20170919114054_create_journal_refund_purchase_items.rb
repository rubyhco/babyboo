class CreateJournalRefundPurchaseItems < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_refund_purchase_items do |t|
      t.integer :variant_id
      t.integer :qty
      t.string :originator_type
      t.integer :originator_id

      t.timestamps
    end
  end
end
