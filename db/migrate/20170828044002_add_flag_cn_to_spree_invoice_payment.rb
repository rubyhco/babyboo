class AddFlagCnToSpreeInvoicePayment < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_invoice_payments, :is_cn, :boolean, default: false
  end
end
