class AddSplashScreenToNews < ActiveRecord::Migration[5.0]
  def change
    add_attachment :spree_news, :image
    add_column :spree_news, :for_splash_screen, :boolean, default: false
    add_column :spree_news, :description, :text
  end
end
