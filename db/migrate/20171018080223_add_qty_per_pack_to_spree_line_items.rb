class AddQtyPerPackToSpreeLineItems < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_line_items, :qty_per_pack, :integer
  end
end
