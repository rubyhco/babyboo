class AddOriginatorIdToJournalJournalItems < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_journal_items, :originator_id, :integer
  end
end
