class AddNamePhonePreferContactToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_users, :name, :string
    add_column :spree_users, :phone, :string
    add_column :spree_users, :prefer_contact, :string
    add_index :spree_users, :phone, unique: true
  end
end
