class AddDropshipToSpreeOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_orders, :dropship, :boolean, default: false
    add_column :spree_orders, :dropship_name, :string
    add_column :spree_orders, :dropship_phone, :string
  end
end
