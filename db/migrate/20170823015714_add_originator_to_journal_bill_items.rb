class AddOriginatorToJournalBillItems < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_bill_items, :originator_id, :integer
    add_column :journal_bill_items, :originator_type, :string
  end
end
