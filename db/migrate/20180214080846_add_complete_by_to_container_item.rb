class AddCompleteByToContainerItem < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_container_items, :completed_by, :integer
  end
end
