class AddEtaToSpreeTaxons < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_taxons, :eta, :string
  end
end
