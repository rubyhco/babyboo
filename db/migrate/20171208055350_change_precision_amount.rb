class ChangePrecisionAmount < ActiveRecord::Migration[5.0]
  def change
    change_column :spree_purchase_line_items, :cost_price, :decimal, precision: 10, scale: 2
    change_column :spree_purchase_line_items, :total, :decimal, precision: 10, scale: 2
    change_column :spree_purchase_orders, :total, :decimal, precision: 12, scale: 2
  end
end
