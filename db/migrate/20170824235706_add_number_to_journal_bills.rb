class AddNumberToJournalBills < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_bills, :number, :string
  end
end
