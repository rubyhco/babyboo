class CreateSpreeTestimonials < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_testimonials do |t|
      t.string :name
      t.text :testimonial

      t.timestamps
    end
  end
end
