class AddActiveUserTousers < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_users, :active_user, :boolean, default: false
  end
end
