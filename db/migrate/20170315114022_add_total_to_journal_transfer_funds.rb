class AddTotalToJournalTransferFunds < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_transfer_funds, :total, :decimal
    add_column :journal_transfer_funds, :amount_used, :decimal
  end
end
