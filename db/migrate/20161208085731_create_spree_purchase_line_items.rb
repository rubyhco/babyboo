class CreateSpreePurchaseLineItems < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_purchase_line_items do |t|
      t.string :variant_id
      t.integer :quantity
      t.integer :items
      t.decimal :cost_price
      t.decimal :total
      t.string :currency
      t.integer :purchase_order_id
      t.timestamps
    end
  end
end
