class AddDefaultKursToSpreeStores < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_stores, :default_kurs, :decimal
  end
end
