class ChangeLengthDecimalShippingRates < ActiveRecord::Migration[5.0]
  def change
  	change_column :spree_shipping_rates, :cost, :decimal, :precision => 15, :scale => 2
  end
end
