class AddShipmentGroupIdToSpreeShipments < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_shipments, :shipment_group_id,:integer
  end
end
