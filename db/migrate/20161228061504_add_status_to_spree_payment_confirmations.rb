class AddStatusToSpreePaymentConfirmations < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_payment_confirmations, :status, :string
  end
end
