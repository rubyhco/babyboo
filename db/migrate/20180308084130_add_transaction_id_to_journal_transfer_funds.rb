class AddTransactionIdToJournalTransferFunds < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_transfer_funds, :income_transaction_id, :int
  	add_column :journal_transfer_funds, :expense_transaction_id, :int
  end
end
