class AddDropshipToInvoice < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_invoices, :dropship, :boolean, default: false
    add_column :spree_invoices, :dropship_name, :string
    add_column :spree_invoices, :dropship_phone, :string
  end
end
