class AddClosedDateToSpreeTaxons < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_taxons, :closed_at, :datetime
  end
end
