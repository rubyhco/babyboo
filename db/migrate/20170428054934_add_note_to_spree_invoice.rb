class AddNoteToSpreeInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_invoices, :note, :text
  end
end
