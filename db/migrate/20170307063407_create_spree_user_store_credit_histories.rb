class CreateSpreeUserStoreCreditHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_user_store_credit_histories do |t|
      t.integer :user_id
      t.decimal :amount
      t.string :memo
      t.string :action
      t.integer :store_credit_event_id
      t.timestamps
    end
  end
end
