# This migration comes from solidus_supplier (originally 20161126161017)
class CreateSpreeSuppliers < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_suppliers do |t|
      t.string :name
      t.string :address
      t.string :phone
      t.string :email
      t.timestamps
    end
  end
end
