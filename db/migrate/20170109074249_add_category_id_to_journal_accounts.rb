class AddCategoryIdToJournalAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_accounts, :category_id, :integer
  end
end
