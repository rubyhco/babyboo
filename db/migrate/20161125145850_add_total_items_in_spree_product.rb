class AddTotalItemsInSpreeProduct < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_products, :total_items, :integer 
  end
end
