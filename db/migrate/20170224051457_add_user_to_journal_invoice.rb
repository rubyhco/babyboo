class AddUserToJournalInvoice < ActiveRecord::Migration[5.0]
  def change
    add_reference :journal_invoices, :spree_user, index: true, foreign_key: true
  end
end	