class CreateJournalJournalItems < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_journal_items do |t|
      t.integer :account_id
      t.text :description
      t.decimal :debit
      t.decimal :credit

      t.timestamps
    end
  end
end
