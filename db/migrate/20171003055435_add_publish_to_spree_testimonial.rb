class AddPublishToSpreeTestimonial < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_testimonials, :publish, :boolean, default: false
  end
end
