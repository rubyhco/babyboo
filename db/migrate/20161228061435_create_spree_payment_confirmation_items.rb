class CreateSpreePaymentConfirmationItems < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_payment_confirmation_items do |t|
      t.integer :payment_confirmation_id
      t.integer :order_id
      t.integer :invoice_id

      t.timestamps
    end
  end
end
