class AddAdjustmentTotalToSpreeInvoiceItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_invoice_items, :adjustment_total, :float
  end
end
