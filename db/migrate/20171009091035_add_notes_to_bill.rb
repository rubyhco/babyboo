class AddNotesToBill < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_bills, :notes, :text
  end
end
