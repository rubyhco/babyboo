class AddStateToJournalBills < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_bills, :state, :string
  end
end
