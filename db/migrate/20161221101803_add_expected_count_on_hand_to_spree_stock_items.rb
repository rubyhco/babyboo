class AddExpectedCountOnHandToSpreeStockItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_stock_items, :expected_count_on_hand, :integer, default: 0
  end
end
