# This migration comes from solidus_banner (originally 20161205080603)
class AddPublishToBanners < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_banners, :publish, :boolean
  end
end
