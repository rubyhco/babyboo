class AddFlagJournalToTransaction < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_transactions, :is_journal_item, :boolean, default: false
  end
end
