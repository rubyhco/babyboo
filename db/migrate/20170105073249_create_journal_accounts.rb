class CreateJournalAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_accounts do |t|
      t.string :name
      t.boolean :is_payment_account
      t.string :currency

      t.timestamps
    end
  end
end
