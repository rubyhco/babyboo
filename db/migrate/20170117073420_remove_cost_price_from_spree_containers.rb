class RemoveCostPriceFromSpreeContainers < ActiveRecord::Migration[5.0]
  def change
  	remove_column :spree_container_items, :import_cost, :float
  end
end
