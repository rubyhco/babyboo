class CreateJournalInvoiceItems < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_invoice_items do |t|
      t.integer :category
      t.text :description
      t.decimal :amount
      t.integer :invoice_id

      t.timestamps
    end
  end
end
