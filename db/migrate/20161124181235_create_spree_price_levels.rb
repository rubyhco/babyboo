class CreateSpreePriceLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_price_levels do |t|
      t.integer :silver_level, default: 10
      t.integer :gold_level, default: 20
      t.decimal :silver_member, precision: 15, scale: 2, default: 75000000.0, null: false
      t.decimal :gold_member, precision: 15, scale: 2, default: 500000000.0, null: false
      t.timestamps
    end
  end
end
