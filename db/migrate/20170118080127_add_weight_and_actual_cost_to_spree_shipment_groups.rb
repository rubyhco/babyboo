class AddWeightAndActualCostToSpreeShipmentGroups < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_shipment_groups, :weight, :decimal, precision: 8, scale: 2
  	add_column :spree_shipment_groups, :actual_cost, :decimal
  end
end
