class AddOriginatorToJournalBills < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_bills, :originator_id, :integer
  	add_column :journal_bills, :originator_type, :string
  end
end
