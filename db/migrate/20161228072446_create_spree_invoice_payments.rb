class CreateSpreeInvoicePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_invoice_payments do |t|
      t.integer :invoice_id
      t.decimal :amount
      t.integer :payment_method_id
      t.string :state
      t.string :number
      t.integer :payment_confirmation_id

      t.timestamps
    end
  end
end
