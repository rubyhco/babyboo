class AddPrefixToJournalCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_categories, :prefix, :string
  end
end
