class AddShipmentStatusToSpreePurchaseLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_purchase_line_items, :shipment_status, :string
  end
end
