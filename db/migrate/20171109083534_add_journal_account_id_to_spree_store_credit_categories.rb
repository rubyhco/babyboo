class AddJournalAccountIdToSpreeStoreCreditCategories < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_store_credit_categories, :journal_account_id, :integer
  end
end
