class CreateSpreeNews < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_news do |t|
      t.text :news
      t.boolean :status
      t.timestamps
    end
  end
end
