class AddCategoryIdToStoreCreditHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_user_store_credit_histories, :category_id, :integer
  end
end
