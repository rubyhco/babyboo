class CreateSpreeStockWarehouseMovements < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_stock_warehouse_movements do |t|
      t.references :stock_item
      t.integer :quantity
      t.text :description
      t.references :originator, polymorphic: true, index: {:name => "index_spree_stock_warehouse_movements_on_originator"}

      t.timestamps
    end
  end
end
