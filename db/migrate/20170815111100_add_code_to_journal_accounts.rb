class AddCodeToJournalAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_accounts, :code, :string
  end
end
