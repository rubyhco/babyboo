class AddUserIdToSpreeInvoicePayments < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_invoice_payments, :user_id, :integer
  end
end
