class AddRequiredToStoreCreditCategory < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_store_credit_categories, :required, :boolean, default: false
  end
end
