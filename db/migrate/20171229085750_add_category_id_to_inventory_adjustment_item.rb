class AddCategoryIdToInventoryAdjustmentItem < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_inventory_adjustment_items, :category_id, :integer
  end
end
