class CreateJournalInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_invoices do |t|
      t.string :vendor
      t.string :currency
      t.datetime :due_date
      t.decimal :total
      t.string :payment_state

      t.timestamps
    end
  end
end
