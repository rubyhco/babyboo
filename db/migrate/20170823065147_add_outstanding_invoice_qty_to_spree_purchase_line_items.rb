class AddOutstandingInvoiceQtyToSpreePurchaseLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_purchase_line_items, :outstanding_invoice_qty, :integer, default: 0
  end
end
