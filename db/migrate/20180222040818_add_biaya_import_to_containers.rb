class AddBiayaImportToContainers < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_containers, :biaya_import, :decimal, precision: 12, scale: 2, default: 0.0
  end
end
