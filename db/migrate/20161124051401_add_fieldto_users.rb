class AddFieldtoUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_users, :birthdate, :date
    add_column :spree_users, :howdoyou, :string
  end
end
