class AddTotalToRefundPurchaseItem < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_refund_purchase_items, :total, :decimal
    add_column :journal_refund_purchase_items, :cost_price, :decimal
  end
end
