class AddNumberToJournalInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_invoices, :number, :string
  end
end
