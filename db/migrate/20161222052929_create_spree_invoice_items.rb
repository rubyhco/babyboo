class CreateSpreeInvoiceItems < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_invoice_items do |t|
      t.integer :invoice_id
      t.integer :inventory_unit_id

      t.timestamps
    end
  end
end
