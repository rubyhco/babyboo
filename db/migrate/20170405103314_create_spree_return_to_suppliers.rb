class CreateSpreeReturnToSuppliers < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_return_to_suppliers do |t|
      t.string :name
      t.string :status
      t.date :return_at
      t.decimal :total, precision: 10, scale: 2
      t.string :currency
      t.integer :supplier_id
      t.timestamps
    end
  end
end
