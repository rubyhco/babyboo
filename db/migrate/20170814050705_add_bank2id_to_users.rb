class AddBank2idToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_users, :bank2_id, :integer, default: 2
  end
end
