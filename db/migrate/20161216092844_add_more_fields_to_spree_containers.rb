class AddMoreFieldsToSpreeContainers < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_containers, :total_pcs, :integer
    add_column :spree_containers, :status, :string
  end
end
