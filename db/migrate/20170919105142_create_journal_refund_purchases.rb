class CreateJournalRefundPurchases < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_refund_purchases do |t|
      t.string :name
      t.integer :bill_id
      t.decimal :total
      t.string :number

      t.timestamps
    end
  end
end
