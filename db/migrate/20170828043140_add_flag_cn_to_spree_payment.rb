class AddFlagCnToSpreePayment < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_payments, :is_cn, :boolean, default: false
  end
end
