class AddInvoiceIdToSpreePayments < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_payments, :invoice_id, :integer
  end
end
