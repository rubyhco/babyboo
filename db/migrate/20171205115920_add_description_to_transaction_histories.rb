class AddDescriptionToTransactionHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_transaction_histories, :description, :string
  end
end
