class ChangeLengthDecimalStoreCredit < ActiveRecord::Migration[5.0]
  def change
  	change_column :spree_store_credits, :amount, :decimal, :precision => 15, :scale => 2
  	change_column :spree_store_credits, :amount_used, :decimal, :precision => 15, :scale => 2
  	change_column :spree_store_credits, :amount_authorized, :decimal, :precision => 15, :scale => 2
  	change_column :spree_store_credit_events, :amount, :decimal, :precision => 15, :scale => 2
  	change_column :spree_store_credit_events, :user_total_amount, :decimal, :precision => 15, :scale => 2
  end
end
