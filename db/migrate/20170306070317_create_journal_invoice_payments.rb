class CreateJournalInvoicePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_invoice_payments do |t|
      t.integer :invoice_id
      t.string :source_type
      t.integer :source_id
      t.decimal :amount
      t.integer :payment_method_id
      t.string :state
      t.string :number
      t.integer :payment_confirmation_id

      t.timestamps
    end
  end
end
