class ChangePrecisionAmountBill < ActiveRecord::Migration[5.0]
  def change
    change_column :journal_bill_items, :amount, :decimal, precision: 10, scale: 2
    change_column :journal_bill_payments, :amount, :decimal, precision: 12, scale: 2
    change_column :journal_bills, :total, :decimal, precision: 12, scale: 2
  end
end
