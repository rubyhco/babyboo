class AddSaleAmountToSpreePrices < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_prices, :sale_amount, :float, scale: 2
  end
end
