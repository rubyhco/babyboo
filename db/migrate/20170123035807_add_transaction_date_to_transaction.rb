class AddTransactionDateToTransaction < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_transactions, :transaction_date, :date
  end
end
