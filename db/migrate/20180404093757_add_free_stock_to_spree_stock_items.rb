class AddFreeStockToSpreeStockItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_stock_items, :free_stock, :integer, default: 0
  end
end
