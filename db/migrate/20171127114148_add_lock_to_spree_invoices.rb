class AddLockToSpreeInvoices < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_invoices, :lock, :boolean, default: false
  end
end
