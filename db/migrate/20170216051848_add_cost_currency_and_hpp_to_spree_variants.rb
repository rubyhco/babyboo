class AddCostCurrencyAndHppToSpreeVariants < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_variants, :kurs, :decimal, precision: 8,  scale: 2
  	add_column :spree_variants, :hpp, :decimal, precision: 8,  scale: 2
  end
end
