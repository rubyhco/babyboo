# This migration comes from solidus_bank_transfer (originally 20161126022716)
class AddBankTransferFieldsToPayments < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_payments, :bank_name, :string
    add_column :spree_payments, :account_no, :string
    add_column :spree_payments, :transaction_reference_no, :string
    add_column :spree_payments, :deposited_on, :date
  end
end
