class AddStatusToTransaction < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_transactions, :status, :string
  end
end
