class AddOriginatorToJournalInvoice < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_invoices, :originator_type, :string
  end
end
