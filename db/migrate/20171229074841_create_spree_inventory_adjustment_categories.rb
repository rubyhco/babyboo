class CreateSpreeInventoryAdjustmentCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_inventory_adjustment_categories do |t|
      t.string :name
      t.integer :journal_account_id

      t.timestamps
    end
  end
end
