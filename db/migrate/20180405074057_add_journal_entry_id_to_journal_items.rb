class AddJournalEntryIdToJournalItems < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_journal_items, :journal_entry_id, :integer
		add_index :journal_journal_items, :journal_entry_id
  end
end
