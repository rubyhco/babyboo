class AddTransactionDateToTransferFunds < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_transfer_funds, :transaction_date, :date
  end
end
