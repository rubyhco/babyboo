class AddPurchaseLineItemIdToSpreeContainerItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_container_items, :purchase_line_item_id, :integer
  end
end
