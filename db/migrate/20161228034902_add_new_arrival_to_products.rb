class AddNewArrivalToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_products, :is_new_arrival, :boolean
  end
end
