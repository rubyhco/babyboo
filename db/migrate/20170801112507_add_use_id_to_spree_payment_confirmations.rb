class AddUseIdToSpreePaymentConfirmations < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_payment_confirmations, :user_id, :integer
  end
end
