class AddPreorderAttributeToSpreeProducts < ActiveRecord::Migration[5.0]
  def up
    drop_table :spree_product_preorder_infos
    add_column :spree_products, :is_preorder, :boolean
    add_column :spree_products, :po_status, :string
    add_column :spree_products, :po_estimation, :string
    add_column :spree_products, :po_closed_at, :date
  end

  def down
    remove_column :spree_products , :is_preorder
    remove_column :spree_products , :po_status
    remove_column :spree_products , :po_estimation
    remove_column :spree_products , :po_closed_at

    create_table :spree_product_preorder_infos do |t|
      t.integer :product_id
      t.date :closed_at
      t.string :status
      t.string :estimation
    end
  end
end
