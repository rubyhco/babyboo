class AddOldTransactionTotalToUser < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_users, :old_transaction_total, :decimal, precision: 10, scale: 2, default: 0.0
  end
end
