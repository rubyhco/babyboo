class AddVariantIdTotalAmountQuantityShipmentIdToSpreeInvoiceItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_invoice_items, :variant_id, :integer
    add_column :spree_invoice_items, :total_amount, :decimal
    add_column :spree_invoice_items, :quantity, :integer
    add_column :spree_invoice_items, :shipment_id, :integer
    add_column :spree_invoice_items, :order_id, :integer
    add_column :spree_invoice_items, :shipping_method_id, :integer
    add_column :spree_invoice_items, :ship_address_id, :integer
  end
end
