class AddSupplierIdToJournalBills < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_bills, :supplier_id, :integer
  end
end
