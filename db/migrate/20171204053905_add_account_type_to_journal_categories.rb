class AddAccountTypeToJournalCategories < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_categories, :account_type, :string
  end
end
