# This migration comes from solidus_banner (originally 20161201050038)
class CreateSpreeBanners < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_banners do |t|
      t.string :name
      t.string :alt
      t.timestamps
    end
    add_attachment :spree_banners, :image
  end
end
