class ChangeTypeOfValueAtSpreeProductProperties < ActiveRecord::Migration[5.0]
  def change
  	change_column :spree_product_properties, :value, :text
  end
end
