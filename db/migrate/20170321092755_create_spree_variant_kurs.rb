class CreateSpreeVariantKurs < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_variant_kurs do |t|
      t.integer :variant_id
      t.integer :transfer_fund_id
      t.timestamps
    end
  end
end
