# This migration comes from solidus_export_csv (originally 20180212080514)
class CreateSpreeExports < ActiveRecord::Migration[4.2]
  def change
    create_table :spree_exports do |t|
    	t.string :export_model_name
      t.string :state
      t.datetime :completed_at
      t.datetime :failed_at
      t.integer :created_by
      t.timestamps
    end
  end
end
