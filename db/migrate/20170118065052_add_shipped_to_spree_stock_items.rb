class AddShippedToSpreeStockItems < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_stock_items, :shipped, :integer, default: 0
  end
end
