class CreateSpreeReturnToSupplierItems < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_return_to_supplier_items do |t|
      t.integer :variant_id
      t.integer :quantity
      t.decimal :cost_price, precision: 10, scale: 2
      t.string :cost_currency
      t.string :reason
      t.integer :return_to_supplier_id
      t.timestamps
    end
  end
end
