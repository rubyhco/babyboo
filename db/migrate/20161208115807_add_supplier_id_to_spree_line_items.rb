class AddSupplierIdToSpreeLineItems < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_line_items, :supplier_id, :integer
  end
end
