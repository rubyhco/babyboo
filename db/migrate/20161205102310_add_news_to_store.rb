class AddNewsToStore < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_stores, :news, :string
  	add_column :spree_stores, :description, :string
  end
end
