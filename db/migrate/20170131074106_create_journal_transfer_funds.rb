class CreateJournalTransferFunds < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_transfer_funds do |t|
      t.decimal :amount
      t.integer :from
      t.integer :to
      t.decimal :kurs

      t.timestamps
    end
  end
end
