class CreateSpreeInventoryAdjustmentItems < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_inventory_adjustment_items do |t|
      t.integer :inventory_adjustment_id
      t.integer :variant_id
      t.integer :theoritical_qty
      t.integer :real_qty
      t.text :notes

      t.timestamps
    end
  end
end
