class CreateJournalTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_transactions do |t|
      t.text :description
      t.decimal :amount
      t.integer :category_id
      t.integer :account_id
      t.boolean :is_verified
      t.boolean :is_expense

      t.timestamps
    end
  end
end
