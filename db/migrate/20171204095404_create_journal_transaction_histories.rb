class CreateJournalTransactionHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_transaction_histories do |t|
    	t.integer :journal_transaction_id
      t.string :originator_type
      t.integer :originator_id
      t.string :name
      t.timestamps
    end
  end
end
