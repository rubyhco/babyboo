class CreateSpreeContainers < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_containers do |t|
      t.datetime :delivery_date
      t.datetime :eta_date
      t.string :name
      t.string :delivery_orders
      t.integer :sack
      t.decimal :cbm
      t.timestamps
    end
  end
end
