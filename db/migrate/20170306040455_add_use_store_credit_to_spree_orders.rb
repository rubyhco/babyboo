class AddUseStoreCreditToSpreeOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_orders, :use_store_credit, :boolean
  end
end
