class AddShippedToSpreeInventoryUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_inventory_units, :shipped, :integer, default: 0
  end
end
