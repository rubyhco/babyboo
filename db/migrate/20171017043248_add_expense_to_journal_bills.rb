class AddExpenseToJournalBills < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_bills, :expenses, :boolean, default: false
  end
end
