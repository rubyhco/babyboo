class AddQuantityToJournalBillItems < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_bill_items, :quantity, :integer
  end
end
