class BankFieldsToSpreeUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_users, :bank_name, :string
    add_column :spree_users, :bank_account_name, :string
    add_column :spree_users, :bank_account_no, :string
    add_column :spree_users, :bank_branch, :string

    add_column :spree_cash_out_requests, :bank_name, :string
    add_column :spree_cash_out_requests, :bank_account_name, :string
    add_column :spree_cash_out_requests, :bank_account_no, :string
    add_column :spree_cash_out_requests, :bank_branch, :string

    add_column :spree_cash_out_requests, :user_id, :string
  end
end
