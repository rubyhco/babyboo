class CreateSpreePaymentConfirmations < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_payment_confirmations do |t|
      t.string :bank_destination
      t.string :bank_sender
      t.string :sender_name
      t.decimal :amount
      t.datetime :transaction_date
      t.string :note

      t.timestamps
    end
  end
end
