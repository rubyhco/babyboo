class DownPaymentTotalToSpreeOrders < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_orders, :down_payment_total, :decimal
  end
end
