class AddUsedAmountToTransaction < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_transactions, :used_amount, :decimal
  end
end
