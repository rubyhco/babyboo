class AddInvoiceIdToSpreeLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_line_items, :invoice_id, :integer
  end
end

