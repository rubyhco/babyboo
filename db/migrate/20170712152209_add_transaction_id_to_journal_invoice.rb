class AddTransactionIdToJournalInvoice < ActiveRecord::Migration[5.0]
  def change
    add_reference :journal_invoice_payments, :journal_transaction, index: true, foreign_key: true
  end
end
