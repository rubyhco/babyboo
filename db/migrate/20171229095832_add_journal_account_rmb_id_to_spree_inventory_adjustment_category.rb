class AddJournalAccountRmbIdToSpreeInventoryAdjustmentCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_inventory_adjustment_categories, :journal_account_rmb_id, :integer
  end
end
