class AddPaymentStateToSpreeContainers < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_containers, :payment_state, :string
  end
end
