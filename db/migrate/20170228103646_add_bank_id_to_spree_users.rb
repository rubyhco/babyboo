class AddBankIdToSpreeUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_users, :bank_id, :integer, default: 1
  end
end
