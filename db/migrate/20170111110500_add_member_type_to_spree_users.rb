class AddMemberTypeToSpreeUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_users, :member_type, :string
  end
end
