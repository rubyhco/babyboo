class AddDisableDpToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_users, :disable_dp, :boolean, default: false
  end
end
