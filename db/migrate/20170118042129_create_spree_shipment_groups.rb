class CreateSpreeShipmentGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_shipment_groups do |t|
      t.string :status
      t.float :total
      t.integer :user_id
      t.string :resi
      t.timestamps
    end
  end
end
