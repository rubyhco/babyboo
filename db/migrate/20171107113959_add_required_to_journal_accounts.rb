class AddRequiredToJournalAccounts < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_accounts, :required, :boolean, default: false
  end
end
