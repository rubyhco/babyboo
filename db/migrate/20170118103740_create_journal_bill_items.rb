class CreateJournalBillItems < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_bill_items do |t|
      t.integer :category
      t.text :description
      t.decimal :amount
      t.integer :bill_id

      t.timestamps
    end
  end
end
