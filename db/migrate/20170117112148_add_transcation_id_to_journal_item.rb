class AddTranscationIdToJournalItem < ActiveRecord::Migration[5.0]
  def change
  	add_column :journal_journal_items, :transaction_id, :int
  end
end
