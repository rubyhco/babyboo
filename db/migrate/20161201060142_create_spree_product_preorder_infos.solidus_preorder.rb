# This migration comes from solidus_preorder (originally 20161129133120)
class CreateSpreeProductPreorderInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_product_preorder_infos do |t|
      t.integer :product_id
      t.date :closed_at
      t.string :status
      t.string :estimation
    end
  end
end
