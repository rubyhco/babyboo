class AddStockGudangToStockItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_stock_items, :stock_warehouse, :integer, default: 0
  end
end
