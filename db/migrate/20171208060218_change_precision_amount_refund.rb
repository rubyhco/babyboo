class ChangePrecisionAmountRefund < ActiveRecord::Migration[5.0]
  def change
    change_column :journal_refund_purchases, :total, :decimal, precision: 10, scale: 2
    change_column :journal_refund_purchase_items, :total, :decimal, precision: 10, scale: 2
    change_column :journal_refund_purchase_items, :cost_price, :decimal, precision: 10, scale: 2
  end
end
