class CreateSpreeCashOutRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_cash_out_requests do |t|
      t.decimal :amount
      t.boolean :is_completed
      t.timestamps
    end
  end
end
