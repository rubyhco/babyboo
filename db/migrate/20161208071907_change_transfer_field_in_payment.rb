class ChangeTransferFieldInPayment < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_payments, :bank_store, :string
  	add_column :spree_payments, :transfer_amount, :decimal
  	add_column :spree_payments, :account_name, :string
  	add_column :spree_payments, :is_verified, :boolean, default: false
  end
end
