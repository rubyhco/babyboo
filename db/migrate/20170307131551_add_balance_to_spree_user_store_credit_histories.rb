class AddBalanceToSpreeUserStoreCreditHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_user_store_credit_histories, :balance, :decimal
  end
end
