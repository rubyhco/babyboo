class AddDpConfirmToSpreePurchaseLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_purchase_line_items, :dp_confirm, :integer
  end
end
