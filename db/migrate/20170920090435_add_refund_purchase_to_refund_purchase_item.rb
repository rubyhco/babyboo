class AddRefundPurchaseToRefundPurchaseItem < ActiveRecord::Migration[5.0]
  def change
    add_column :journal_refund_purchase_items, :refund_purchase_id, :integer
  end
end
