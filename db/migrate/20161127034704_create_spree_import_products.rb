class CreateSpreeImportProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_product_imports do |t|
      t.string :data_file_file_name
      t.string :data_file_content_type
      t.integer :data_file_file_size
      t.datetime :data_file_updated_at
      t.string :state
      t.text :product_ids
      t.datetime :completed_at
      t.datetime :failed_at
      t.text :error_message
      t.integer :created_by
      t.string :separatorChar
      t.string :encoding_csv
      t.string :quoteChar
      t.timestamps
    end
  end
end
