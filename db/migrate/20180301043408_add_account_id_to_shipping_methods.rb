class AddAccountIdToShippingMethods < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_shipping_methods, :account_id, :integer
  end
end
