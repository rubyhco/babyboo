class AddQuantityToSpreeReturnItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_return_items, :quantity, :integer
  end
end
