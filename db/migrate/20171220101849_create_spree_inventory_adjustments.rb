class CreateSpreeInventoryAdjustments < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_inventory_adjustments do |t|
      t.string :name
      t.datetime :inventory_date
      t.string :state

      t.timestamps
    end
  end
end
