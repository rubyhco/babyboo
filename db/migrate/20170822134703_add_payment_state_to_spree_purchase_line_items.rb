class AddPaymentStateToSpreePurchaseLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_purchase_line_items, :payment_state, :string
  end
end
