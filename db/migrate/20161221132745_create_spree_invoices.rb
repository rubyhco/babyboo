class CreateSpreeInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_invoices do |t|
      t.string :number
      t.integer :user_id
      t.decimal :total
      t.integer :ship_address_id
      t.string :status
      t.string :sprecial_instructions
      t.integer :created_by_id
      t.timestamps
    end
  end
end
