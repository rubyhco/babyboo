class CreateJournalBillPayments < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_bill_payments do |t|
      t.integer :bill_id
      t.string :source_type
      t.integer :source_id
      t.decimal :amount
      t.integer :journal_transaction_id

      t.timestamps
    end
  end
end
