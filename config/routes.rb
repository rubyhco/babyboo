Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  namespace :journal do
    resources :payment_store_credits do
      collection do
        put :use_store_credit_for_order
        put :use_store_credit_for_invoice
        put :use_store_credit_for_journal_invoice
      end
    end
    resources :store_credit_categories
    resources :purchase_invoices do
      member do
        get :add_items
        put :update_cart
        put :confirm
        get :paid
        get :refund
        put :refund_cart
        get :add_note
        put :update_note
        get :print
        get :reverse_to_draft
      end
    end
    resources :categories
    resources :accounts
    resources :reports do
      collection do
        get :trial_balance
        get :general_ledger
        get :balance_sheet
        get :transaction
        get :cash_and_bank
        get :profit_and_loss
        get :cash_flow_statement
      end
    end
    resources :transaction_rmbs
    resources :transactions do
      member do
        get :record_payment
        put :record_container
        put :record_order
        put :record_invoice
        put :record_purchase
        put :record_bill
        put :record_journal_invoice
        get :transfer
        put :transfer_fund
        put :add_store_credit
        get :history
        put :record_expense
        put :record_expense_transfer_fund
        put :record_income_transfer_fund
        get :add_counter_part
        put :counter_part
      end
      collection do
        put :use_store_credit_for_order
        put :use_store_credit_for_invoice
        get :show_import
        put :import
        get :all_payment_confirmed
      end
    end
    resources :transfer_funds
    resources :journal_items do
      collection do
        get :all_journal_entries
      end
    end
    resources :bills do
      member do 
        put :record_bill_rmb
        get :new_payment
        put :create_payment
      end
    end
    resources :invoices do
      member do
        get :record_invoice
        put :record_order_invoice
        put :record_invoice_cn
        get :reminder_email
        put :unpay
      end
    end
    resources :info_payments do 
      member do
        get :confirmed 
      end
    end
    resources :store_credits do
      collection do
        get :cash_out_new
        put :create_cash_out
        get :cash_out_request
        get :bill_paid
      end
    end
  end

  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being the default of "spree"
  mount Spree::Core::Engine, :at => '/'
  Spree::Core::Engine.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
    namespace :admin do
      get 'stock_items/card_stock', to: "stock_items#card_stock"
      get 'stock_items/:id/stock_movement', to: "stock_items#stock_movement", as: :stock_item_movement
      get 'stock_items/:id/stock_warehouse_movement', to: "stock_items#stock_warehouse_movement",  as: :stock_warehouse_movement
      get 'products/:id/view_buyer', to: "products#view_buyer", as: :view_buyer
      get 'products/:id/hide_show_product', to: "products#hide_show_product", as: :hide_show_product
      put 'orders/:id/payment_confirmation', to: "orders#payment_confirmation", as: :order_payment_confirmation
      get 'orders/:id/payment_confirmation_new', to: "orders#payment_confirmation_new", as: :order_payment_confirmation_new
      get 'orders/:id/lock_unlock', to: "orders#lock_unlock", as: :order_lock_unlock
      get 'orders/return_request', to: "orders#return_request", as: :customer_return_request
      get 'orders/show_inventory_unit', to: "orders#show_inventory_unit", as: :order_inventory_units
      put 'orders/:id/inventory_unit', to: "orders#backordered_inventory", as: :backordered_inventory_unit
      get 'orders/download_pending_po', to: "orders#download_pending_po", as: :download_pending_po
      get 'shipping_methods/:id/download_rates', to: "shipping_methods#download_rates", as: :download_rates
      resources :cash_out_requests do
        collection do
          get :cash_out_new
          put :create_cash_out
        end
      end
      resources :reports do
        collection do
          get :sales_ready_stock
          get :income_dp_preorder
          get :unpaid_invoice
          get :invoice_sales
          get :return_order
          get :store_credit
          get :lost_item
          get :dp_hangus
          get :cek_stock
          get :credit_note
        end
      end
      resources :return_to_suppliers do
        member do
          get :items
          put :return
          put :update_return
          put :confirm
          put :credit_to_supplier
          put :treat_as_expense
          put :ship
          put :create_invoice
        end
        collection do
          get :delete_item
        end
      end

      resources :inventory_adjustments do
        member do
          put :update_items
          put :add_item
          get :validate
        end
        collection do
          get :item_destroy
        end
      end

      resources :inventory_adjustment_categories
      # get 'reports/sales_ready_stock', to: "reports#sales_ready_stock"
      resources :price_levels
      resources :purchase_orders do
        member do
          get :items
          put :update_items
          get :payment
          get :shipment
          put :confirm
          put :add_item
          get :cart
          get :print
          get :reverse_to_pending
          get :update_after_change_item
        end
        collection do
          get :po_pending
          get :line_item_destroy
          get :all
          get :mark_completed
        end
      end
      resources :containers do
        member do
          get :new_item
          put :update_items
          put :receive_items
        end
        collection do
          get :selisih_barang
        end
      end
      resources :container_items do
        member do
          get :ready
          put :complete
          put :refund
          put :generate_po
          get :reverse
          get :delete_item
          put :treat_as_expense
        end
      end
      resources :invoices do
        member do
          get :items
          put :select_items
          get :confirm
          put :send_invoice
          get :completed
          put :dp_angus
          put :add_note
          put :flag_dropship
          get :reminder_email
          get :lock_unlock
          get :payment_confirmation_new
          put :payment_confirmation
        end
        collection do
          get :generate_invoice
        end
      end
      resources :invoice_items do
        member do
          patch :change_address
          put :add_adjustment
        end
      end

      resources :shipment_groups do
        member do
          put :update_inventory_unit
          get :ready
          get :print
          get :processing
          put :packed
          put :ship
        end
        collection do
          get :generate_shipment_group
        end
      end
      resources :news
      resources :testimonials do
        member do
          get :publish
        end
      end
      put 'new_address', to: "invoices#new_address"
    resources :shipments do
      member do
        put :ship
        put :mark_as_shipped
      end
    end
    end
    resources :testimonials
    resources :payment_confirmations
    resources :shipping_rates do
      collection do
        put :check_shipping_rate
        get :show_shipping_rate
      end
    end
    resources :cash_out_requests
    resources :return_authorization_users do 
      member do
        get :new_rma
      end
      collection do
        get :shipped_order
      end
    end
    get 'products/new_items', to: "products#new_items"
    get 'products/ready_stock', to: "products#ready_stock"
    get 'products/album', to: "products#album"
    get 'products/sale', to: "products#sale"
    put 'home/new_address', to: "home#new_address"
    get 'home/addresses', to: "home#addresses", as: :user_addresses
    get 'store_credits/:id', to: "home#store_credit"
    get 'invoice/:id', to: "home#invoice", as: :user_invoice
    get 'order/show/:id', to: "home#show_order", as: :user_order_show
    get 'invoice/:id/print', to: "home#print", as: :user_invoice_print
    get 'orders/:id/print', to: "orders#print", as: :orders_print
    get 'products/album/:id/show_album_image', to: "products#show_album_image", as: :show_album_product_image

    # buat bayar invoice pake store credit di my account
    get 'invoice/pay_with_sc/:id', to: "home#use_sc_for_invoice_po", as: :pay_sc_inv_po
    put 'invoice/pay_with_sc/:id', to: "home#pay_with_sc_invoice", as: :use_sc_inv_po

    # buat bayar order pake store credit di my account
    get 'order/pay_with_sc/:id', to: "home#use_sc_for_order", as: :pay_sc_order
    put 'order/pay_with_sc/:id', to: "home#pay_with_sc_order", as: :use_sc_order

    # buat bayar invoice_journal(credit) pake store credit di my account
    get 'credit/pay_with_sc/:id', to: "home#use_sc_for_credit", as: :pay_sc_credit
    put 'credit/pay_with_sc/:id', to: "home#pay_with_sc_credit", as: :use_sc_credit

    #ubah ekspedisi di invoice
    put 'invoice/ubah_ekspedisi/:id', to: "home#ubah_ekspedisi", as: :ubah_ekspedisi_user

  end
end
