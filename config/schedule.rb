# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

# every :day, at: '12:05am' do
#   rake 'brandedbabys:update_stock'
# end

# every :day, at: '2:30am' do
#   rake 'brandedbabys:generate_invoice'
# end

every :day, at: '11:00pm' do
  rake 'brandedbabys:set_member_type'
end

# running at 01:10am at Jakarta
every :day, at: '06:10pm' do
  # rake 'brandedbabys:generate_shipment_group'
end

# running at 10:00pm at Jakarta
every :day, at: '03:00pm' do
  rake 'brandedbabys:update_shipment_preorder'
end

# running at 03:00am at Jakarta
every :day, at: '8:00pm' do
  rake 'brandedbabys:send_payment_reminder'
end

# running at 01:00am at Jakarta
every :day, at: '06:00pm' do
  rake 'brandedbabys:auto_cancel'
end

# running at 02:00am at Jakarta
# every :day, at: '07:00pm' do
#   rake 'brandedbabys:auto_delete_album'
# end

# running at 10:00pm at Jakarta
every :day, at: '03:00pm' do
  rake 'brandedbabys:set_complete_shipment'
end 

# every :day, at: '12:01am' do
#   rake 'brandedbabys:set_preorder_album_open_flag'
# end

# running at 02:00am at Jakarta
every 7.days, at: '07:00pm' do
  rake 'brandedbabys:set_active_user'
end

# running at 00:00am at Jakarta
every 3.days, at: '05:00pm' do 
  rake 'brandedbabys:new_hide_products'
end

# running at 04:00am at Jakarta
# every :day, at: '09:00pm' do 
#   rake 'brandedbabys:auto_publish_product'
# end

every 15.minutes do 
  rake 'brandedbabys:remove_duplicate_inventory_units'
end