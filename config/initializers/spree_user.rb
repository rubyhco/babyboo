Spree::User.class_eval do
  has_many :invoices, class_name: "Spree::Invoice"

  def sales?
    self.role_users.any? { |ru| ru.role.name == 'sales' }
  end
  def products?
    self.role_users.any? { |ru| ru.role.name == 'products' }
  end
  def warehouse?
    self.role_users.any? { |ru| ru.role.name == 'warehouse' }
  end
  def finance?
    self.role_users.any? { |ru| ru.role.name == 'finance' }
  end
  def operations?
    self.role_users.any? { |ru| ru.role.name == 'operations' }
  end 
end