# Configure Solidus Preferences
# See http://docs.solidus.io/Spree/AppConfiguration.html for details

Spree.config do |config|
  # Without this preferences are loaded and persisted to the database. This
  # changes them to be stored in memory.
  # This will be the default in a future version.
  # This line resets all preferences! It should be the first line in the block
  config.use_static_preferences!

  # Core:

  # Default currency for new sites
  config.currency = "IDR"

  config.default_country_iso = "ID"
  # from address for transactional emails
  config.mails_from = "no-reply@brandedbabys.com"

  # Uncomment to stop tracking inventory levels in the application
  # config.track_inventory_levels = false

  # When set, product caches are only invalidated when they fall below or rise
  # above the inventory_cache_threshold that is set. Default is to invalidate cache on
  # any inventory changes.
  # config.inventory_cache_threshold = 3


  # Frontend:

  # Custom logo for the frontend
  # config.logo = "logo/solidus_logo.png"

  # Template to use when rendering layout
  # config.layout = "spree/layouts/spree_application"


  # Admin:

  # Custom logo for the admin
  config.admin_interface_logo = "branded-babys-logo-admin.png"
  config.allow_guest_checkout = false
  config.products_per_page = 16

  # Gateway credentials can be configured statically here and referenced from
  # the admin. They can also be fully configured from the admin.
  #
  # config.static_model_preferences.add(
  #   Spree::Gateway::StripeGateway,
  #   'stripe_env_credentials',
  #   secret_key: ENV['STRIPE_SECRET_KEY'],
  #   publishable_key: ENV['STRIPE_PUBLISHABLE_KEY'],
  #   server: Rails.env.production? ? 'production' : 'test',
  #   test_mode: !Rails.env.production?
  # )
end

Spree::Frontend::Config.configure do |config|
  config.use_static_preferences!

  config.locale = 'en'
end

Spree::Backend::Config.configure do |config|
  config.use_static_preferences!

  config.locale = 'en'
  
  config.menu_items << config.class::MenuItem.new(
    [:purchase_orders],
    'book',
    condition: -> { can?(:admin, Spree::PurchaseOrder) },
    partial: 'spree/admin/shared/purchase_order_sub_menu',
    url: :admin_purchase_orders_path
  )

  config.menu_items << config.class::MenuItem.new(
    [:containers],
    'truck',
    condition: -> { can?(:admin, Spree::Container) },
    partial: 'spree/admin/shared/container_sub_menu',
    url: :admin_containers_path
  )
  config.menu_items << config.class::MenuItem.new(
    [:invoices],
    'envelope',
    condition: -> { can?(:admin, Spree::Invoice) },
    url: :admin_invoices_path
  )
  config.menu_items << config.class::MenuItem.new(
    [:shipments],
    'send',
    condition: -> { can?(:admin, Spree::Shipment) },
    url: :admin_shipment_groups_path
  )
  config.menu_items << config.class::MenuItem.new(
    [:journal],
    'money',
    condition: -> { can?(:admin, Journal::Transaction) },
    url: "/journal/reports/cash_and_bank"
  )
end

Spree::Api::Config.configure do |config|
  config.use_static_preferences!

  config.requires_authentication = true
end

Spree.user_class = "Spree::LegacyUser"
# Spree.user_class = "Spree::User"

Spree::PermittedAttributes.user_attributes.push :birthdate, :howdoyou, :member_type, :name, :phone, :prefer_contact, :disable_dp, :bank_id, :bank2_id, :bank_name, :bank_account_name, :bank_account_no, :bank_branch, :active
Spree::PermittedAttributes.store_attributes.push :news, :description, :default_kurs
Spree::PermittedAttributes.product_attributes.push :is_new_arrival
Spree::PermittedAttributes.taxon_attributes.push :closed_at, :eta
Spree::PermittedAttributes.checkout_attributes.push :use_store_credit
Spree::PermittedAttributes.address_attributes.push :dropship, :dropship_name, :dropship_phone
Spree::Api::LineItemsController.line_item_options << :price
Spree::Api::LineItemsController.line_item_options << :qty_per_pack
Spree::Api::ApiHelpers.line_item_attributes.push :qty_per_pack, :price


Spree::Ability.register_ability(Spree::MultiVendorAbility)
Spree::PrintInvoice::Config.set(prawn_options: { page_layout: :portrait, page_size: "A4" })
Spree::Image.attachment_definitions[:attachment][:styles] = {
  mini: '48x48>',
  small: '200x200>',
  product: '450x450>',
  large: '600x600>'
}
Rails.application.config.spree.payment_methods << Spree::PaymentMethod::Invoice