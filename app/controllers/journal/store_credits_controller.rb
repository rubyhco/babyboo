class Journal::StoreCreditsController < Journal::BaseController
  def index
    @q = Spree::UserStoreCreditHistory.all.ransack((params[:q]))
    @histories = @q.result.includes(store_credit_event: :store_credit).order(id: :desc).page(params[:page])
    @users = Spree::User.where(id: Spree::StoreCredit.pluck(:created_by_id).uniq.compact)
  end

  def new
    @store_credit = Spree::StoreCredit.new
    render layout: false
  end

  def create
    params_sc = params[:store_credit]
    @store_credit = Spree::StoreCredit.new(user_id: params_sc[:user_id], amount: params_sc[:amount], category_id: params_sc[:category_id], currency: "IDR", created_by: whodunnit, memo: params_sc[:memo], action_originator: whodunnit)
    if @store_credit.save
      redirect_to journal_store_credits_path, notice: 'Store credit was successfully created.'
    else
      redirect_to journal_store_credits_path, notice: 'Failed create store credit.'
    end
  end

  def cash_out_new
    @cash_out_request = Spree::CashOutRequest.new
    user_id = Spree::UserStoreCreditHistory.having('SUM(amount) > 0').group(:user_id).pluck(:user_id)
    @users = Spree::User.where(id: user_id)
    render layout: false
  end

  def create_cash_out
    @cash_out_request = Spree::CashOutRequest.new(cash_out_request_params)
    if @cash_out_request.save
      flash[:success] = "Permintaan Cash Out akan di proses dalam waktu 3x24 jam."
      redirect_to request.referrer
    else
      redirect_to request.referrer, alert: "Sorry, something went wrong. Please try again!"
    end
  end

  def cash_out_request
    @q = Journal::Bill.cash_out_request.ransack((params[:q]))
    @bill_cash_out_reqs = @q.result.includes(:items).order(created_at: :desc).page(params[:page])
  end

  def bill_paid
    bill = Journal::Bill.find_by_id(params[:bill])
    bill.pay!
    bill.originator.update_columns(is_completed: true)
    Spree::BrandedBabysMailer.cash_out_request_paid(bill, bill.user, bill.originator).deliver!
    redirect_to cash_out_request_journal_store_credits_path, notice: "Cash Out Request Paid!"
  end

  private

  def whodunnit
    current_spree_user
  end

  def cash_out_request_params
    params.require(:cash_out_request).permit(:user_id, :amount, :bank_name, :bank_account_name, :bank_account_no, :bank_branch)
  end
end
