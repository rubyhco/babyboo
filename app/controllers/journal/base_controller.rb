require 'cancan'

class Journal::BaseController < ApplicationController
  layout 'journal'

  before_action :user_login?

  def user_login?
    if spree_current_user
      (spree_current_user.admin? || spree_current_user.operations?) ? true : redirect_to("/admin")
    else
      redirect_to("/admin")
    end
  end
end
