class Journal::TransferFundsController < Journal::BaseController
  before_action :set_journal_transfer_fund, only: [:show, :edit, :update, :destroy]

  # GET /journal/transfer_funds
  # GET /journal/transfer_funds.json
  def index
    @q = Journal::TransferFund.all.ransack((params[:q]))
    @journal_transfer_funds = @q.result.order(created_at: :desc).page(params[:page]).per(30)
  end

  # GET /journal/transfer_funds/1
  # GET /journal/transfer_funds/1.json
  def show
  end

  # GET /journal/transfer_funds/new
  def new
    @journal_transfer_fund = Journal::TransferFund.new
    render layout: false
  end

  # GET /journal/transfer_funds/1/edit
  def edit
    render layout: false
  end

  # POST /journal/transfer_funds
  # POST /journal/transfer_funds.json
  def create
    @journal_transfer_fund = Journal::TransferFund.new(journal_transfer_fund_params)
    if @journal_transfer_fund.save
      redirect_to journal_transfer_funds_path, notice: 'Transfer fund successfully created'
    else
      redirect_to journal_transfer_funds_path, alert: 'Failed'
    end

    # respond_to do |format|
    #   if @journal_transfer_fund.save
    #     format.html { redirect_to @journal_transfer_fund, notice: 'Transfer fund was successfully created.' }
    #     format.json { render :show, status: :created, location: @journal_transfer_fund }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @journal_transfer_fund.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /journal/transfer_funds/1
  # PATCH/PUT /journal/transfer_funds/1.json
  def update
    respond_to do |format|
      if @journal_transfer_fund.update(journal_transfer_fund_params)
        format.html { redirect_to @journal_transfer_fund, notice: 'Transfer fund was successfully updated.' }
        format.json { render :show, status: :ok, location: @journal_transfer_fund }
      else
        format.html { render :edit }
        format.json { render json: @journal_transfer_fund.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /journal/transfer_funds/1
  # DELETE /journal/transfer_funds/1.json
  def destroy
    @journal_transfer_fund.destroy
    respond_to do |format|
      format.html { redirect_to journal_transfer_funds_url, notice: 'Transfer fund was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_journal_transfer_fund
      @journal_transfer_fund = Journal::TransferFund.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def journal_transfer_fund_params
      params.require(:journal_transfer_fund).permit(:amount, :from, :to, :kurs, :transaction_date)
    end
end
