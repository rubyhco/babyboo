class Journal::AccountsController < Journal::BaseController
  before_action :set_journal_account, only: [:show, :edit, :update, :destroy]

  # GET /journal/accounts
  # GET /journal/accounts.json
  def index
    @journal_accounts = Journal::Account.all.order(:code)
  end

  # GET /journal/accounts/1
  # GET /journal/accounts/1.json
  def show
  end

  # GET /journal/accounts/new
  def new
    @journal_account = Journal::Account.new
    render layout: false
  end

  # GET /journal/accounts/1/edit
  def edit
    render layout: false
  end

  # POST /journal/accounts
  # POST /journal/accounts.json
  def create
    @journal_account = Journal::Account.new(journal_account_params)

    respond_to do |format|
      if @journal_account.save
        format.html { redirect_to @journal_account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @journal_account }
      else
        format.html { render :new }
        format.json { render json: @journal_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /journal/accounts/1
  # PATCH/PUT /journal/accounts/1.json
  def update
    respond_to do |format|
      if @journal_account.update(journal_account_params)
        format.html { redirect_to @journal_account, notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @journal_account }
      else
        format.html { render :edit }
        format.json { render json: @journal_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /journal/accounts/1
  # DELETE /journal/accounts/1.json
  def destroy
    @journal_account = Journal::Account.find(params[:id])
    if @journal_account.destroy!
      redirect_to journal_accounts_path, notice: "Account was successfully deleted."
    else
      redirect_to journal_accounts_path, notice: "Delete Account failed."
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_journal_account
      @journal_account = Journal::Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def journal_account_params
      params.require(:journal_account).permit(:name, :is_payment_account, :currency, :category_id)
    end
end
