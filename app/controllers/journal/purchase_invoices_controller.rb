class Journal::PurchaseInvoicesController < Journal::BaseController
  def index
    @q = Journal::Bill.where("supplier_id is not ? AND state != ?", nil, "pending").ransack((params[:q]))
    @bills = @q.result.includes(:supplier, :items).order(created_at: :desc).page(params[:page])
  end

  def new
    @bill = Journal::Bill.new
  end

  def create
    @bill = Journal::Bill.new(supplier_params)
    @bill.vendor = @bill.supplier.name
    @bill.user = current_spree_user
    if @bill.save
      redirect_to add_items_journal_purchase_invoice_path(@bill)
    else
      render :new
    end
  end

  def destroy
    @bill = Journal::Bill.find params[:id]
    if @bill.destroy!
      flash[:success] = "Puchase Invoice berhasil dihapus"
    else
      flash[:danger] = "Sorry, please try again"
    end
    redirect_to request.referrer
  end

  def add_items
    @bill = Journal::Bill.find params[:id]
    @q = Spree::PurchaseLineItem.joins(:purchase_order, :variant).where("spree_purchase_orders.supplier_id = ? AND spree_purchase_line_items.outstanding_invoice_qty > ?", @bill.supplier_id, 0).pending_payment.where.not(id: @bill.items.pluck(:originator_id)).order("spree_variants.sku ASC").ransack((params[:q]))
    @items = @q.result.includes(:variant).page(params[:page]).per(50)
  end

  def update_cart
    @bill = Journal::Bill.find params[:id]
    if @bill.currency == "RMB"
      inventory_id_journal_account = Journal::Account.find_by_code("1-10202").id
    else
      inventory_id_journal_account = Journal::Account.find_by_code("1-10201").id
    end
    params[:select_items].each do |x,y|
      next if y["'checked'"].nil?
      purchase_line_item = Spree::PurchaseLineItem.find x
      @bill.update_columns(currency: purchase_line_item.currency) if @bill.currency.nil?
      if @bill.items.where(originator_id: purchase_line_item.id).count > 0
        bill_item = @bill.items.where(originator_id: purchase_line_item.id).first
        bill_item.update_attributes(category: inventory_id_journal_account, quantity: y["'quantity'"].to_i, description: 
          "#{y["'quantity'"]} pcs x #{purchase_line_item.variant.sku}", amount: y["'cost_price'"].to_f, 
          originator: purchase_line_item)
        purchase_line_item.update_attributes(cost_price: y["'cost_price'"].to_f)
      else
        bill_item = @bill.items.create(category: inventory_id_journal_account, quantity: y["'quantity'"].to_i, description: 
          "#{y["'quantity'"]} pcs x #{purchase_line_item.variant.sku}", amount: y["'cost_price'"].to_f, 
          originator: purchase_line_item)
        purchase_line_item.update_attributes(cost_price: y["'cost_price'"].to_f)
      end
      if bill_item.quantity <= 0 
        bill_item.delete
      end
      @bill.draft! unless @bill.draft?
    end
    redirect_to request.referrer
  end

  def reverse_to_draft
    @bill = Journal::Bill.find params[:id]
    if @bill.reverse_to_draft
      redirect_to request.referrer, notice: "Succesfully reverse purchase invoice to draft"
    else
      redirect_to request.referrer, alert: "Failed reverse purchase invoice"
    end
  end

  def confirm
    @bill = Journal::Bill.find params[:id]
    @bill.confirm!
    redirect_to request.referrer, notice: "Purchase Invoice telah di validasi."
  end

  def show
    @bill = Journal::Bill.find params[:id]
    @q = @bill.items.joins(spree_purchase_line_item: :variant).order("spree_variants.sku ASC").ransack((params[:q]))
    @items = @q.result
    @payments = @bill.payments
    @refunds = @bill.refund_purchases
    @total = total_items
  end

  def paid
    bill = Journal::Bill.find_by_id(params[:id])
    bill.pay!
    redirect_to request.referrer, notice: "Purchase Invoice Terbayar"
  end

  def refund
    @bill = Journal::Bill.find params[:id]
    @q = @bill.items.ransack((params[:q]))
    @items = @q.result
    @payments = @bill.payments
  end

  def refund_cart
    @bill = Journal::Bill.find params[:id]
    refund = @bill.refund_purchases.create(name: "Purchase Invoice #{@bill.id} - #{@bill.vendor}")
    params[:select_items].each do |x,y|
      next if y["'checked'"].nil?
      bill_item = Journal::BillItem.find_by_id x
      purchase_line_item = bill_item.originator
      adjust_qty = y["'quantity'"].to_i/ purchase_line_item.variant.product.total_items
      purchase_line_item.update_attributes(quantity: purchase_line_item.quantity - adjust_qty)
      total_refund_item = purchase_line_item.cost_price * y["'quantity'"].to_i
      refund.items.create(variant_id:purchase_line_item.variant_id , qty:y["'quantity'"].to_i, originator: purchase_line_item, total: total_refund_item,cost_price:purchase_line_item.cost_price, bill_item_id: bill_item.id)
    end
    refund.create_journal
    redirect_to journal_purchase_invoice_path(@bill), notice: "Refund Invoice Berhasil"
  end

  def add_note
    @bill = Journal::Bill.find params[:id]
    render layout: false
  end

  def update_note
    @bill = Journal::Bill.find params[:id]
    if @bill.update_attributes(notes: params[:journal_bill][:notes])
      redirect_to request.referrer, notice: "Berhasil Update Notes"
    else
      redirect_to request.referrer, notice: "Gagal Update Notes"
    end
  end

  def print
    @bill = Journal::Bill.find params[:id]
    @items = @bill.items.joins(spree_purchase_line_item: :variant).order("spree_variants.sku ASC")
  end

  private

  def supplier_params
    params.require(:journal_bill).permit(:due_date, :supplier_id)
  end

  def total_items
    total_amount = 0
    @items.each do |item|
      total_amount += item.amount * item.quantity.to_i
    end
    return total_amount
  end
end
