class Journal::BillsController < Journal::BaseController
  before_action :set_journal_bill, only: [:show, :edit, :update, :destroy, :new_payment, :create_payment]

  def index
    @q = Journal::Bill.expenses.ransack((params[:q]))
    @bills = @q.result.includes(:items).order(created_at: :desc).page(params[:page])
  end

  def new
    @journal_bill = Journal::Bill.new
    render layout:false
  end

  def edit
    @journal_bill = Journal::Bill.find(params[:id])
    render layout: false
  end

  def new_payment
    @payment = @journal_bill.payments.new
    @payment_accounts = Journal::Account.where(category: 3, currency: @journal_bill.currency)
    render layout: false
  end

  def create_payment
    if @journal_bill.confirmed?
      payment = @journal_bill.payments.new(amount: params[:journal_bill_payment][:amount], source_type: "Journal::Account", 
        source_id: params[:journal_bill_payment][:source])

      if payment.save
        notice = "Berhasil menambahkan pembayaran"
        redirect_to request.referrer, notice: notice
      else
        alert = "Mohon ulangi kembali"
        redirect_to request.referrer, alert: alert
      end
    else
      alert = "Purchase invoice masih dalam bentuk draft. Silahkan confirm purchase invoice dan proses payment dapat diulangi kembali"
      redirect_to request.referrer, alert: alert
    end
  end

  def create
    Journal::Bill.transaction do
      param = params[:journal_bill]
      user = whodunnit
      bill = Journal::Bill.create(vendor: user.name, currency: param[:currency], total: param[:total], payment_state: "pending", spree_user_id: user.id, originator_type: "Journal::Bill", expenses: true)
      bill.items.create(category: param[:category], amount: param[:total], description: param[:description], bill_id: bill.id)
      bill.confirm!
    end
    redirect_to journal_bills_path, notice: "Expense Created."
  end

  def record_bill_rmb
    Journal::Bill.transaction do
      account_id = params[:record_bill][:from]
      journal_items = Journal::JournalItem.where(account_id: account_id)
      amount = journal_items.sum(:debit) - journal_items.sum(:credit)
      bill = Journal::Bill.find_by_id(params[:id])
      if amount >= bill.total
        bill.pay!
        bill.record_bill(account_id, whodunnit)
        redirect_to journal_bills_path, notice: "Bill successfully recorded"
      else
        redirect_to journal_bills_path, alert: "Amount Kurang "
      end
    end
  end

  def destroy
    @journal_bill = Journal::Bill.find(params[:id])
    if @journal_bill.destroy!
      redirect_to journal_bills_path, notice: "Expense was successfully deleted."
    else
      redirect_to journal_bills_path, alert: "Delete Expense failed."
    end
  end

  private

  def set_journal_bill
    @journal_bill = Journal::Bill.find(params[:id])
  end 

  def journal_bill_params
    params.require(:journal_bill).permit(:vendor, :currency, :due_date, :total, :payment_state, :spree_user_id, :category, :amount, :description, :originator_type)
  end

  def whodunnit
    spree_current_user
  end
end