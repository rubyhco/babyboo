class Journal::InvoicesController < Journal::BaseController
  before_action :set_journal_invoice, only: [:show, :edit, :update, :destroy, :record_invoice]
  
  def index
    @q = Journal::Invoice.ransack((params[:q]))
    @invoices = @q.result.includes(:items).order(created_at: :desc).page(params[:page])
  end

  def new
    @journal_invoice = Journal::Invoice.new
    render layout:false
  end

  def show

  end

  def edit
    @journal_invoice = Journal::Invoice.find(params[:id])
    render layout: false
  end

  def update
     @journal_invoice = Journal::Invoice.find(params[:id])
 
    if @journal_invoice.update(journal_invoice_params)
      flash[:success] = "Invoices was successfully updated."
      redirect_to journal_invoices_path
    else
      flash[:danger] = "Update invoice failed."
      redirect_to journal_invoices_path
    end
  end

  def unpay
    @journal_invoice = Journal::Invoice.find(params[:id])
    if @journal_invoice.unpay!
      flash[:success] = "Invoice berhasil ditandai tidak terbayar"
      redirect_to journal_invoices_path
    else
      flash[:danger] = "Operasi gagal. Harap ulangi kembali."
      redirect_to journal_invoices_path
    end
  end

  def create
    param = params[:journal_invoice]
    user = Spree::User.find_by_id params[:journal_invoice][:spree_user_id]
    journal_account = Journal::Account.find_by_id param[:category]
    invoice = Journal::Invoice.create(vendor: user.name, currency: journal_account.currency, due_date: param[:due_date], total: param[:total], payment_state: "pending", spree_user_id: user.id, originator_type: "Invoice::Order")
    invoice.items.create(category: param[:category], amount: param[:total], description: param[:description], invoice_id: invoice.id)
    redirect_to journal_invoices_path, notice: "Invoices Created."
  end

  def destroy
    @journal_invoice = Journal::Invoice.find(params[:id])
    if @journal_invoice.destroy
      flash[:success] = "Invoice was successfully deleted."
      redirect_to journal_invoices_path
    else
      flash[:danger] = "Delete Invoice failed."
      redirect_to journal_invoices_path
    end
  end

  def record_invoice
    @order_payments_invoice = Spree::Order.payment_state_is("balance_due").filter(params.slice(:name_user, :number)).page(params[:page]).per(30)
    @invoice_payments = Spree::Invoice.valid.where(:status => "pending_payment").filter(params.slice(:name_user)).page(params[:page]).per(30) 
  end

  def record_order_invoice
    if set_journal_invoice.total < total_order_amount
      redirect_to request.referrer, alert: "Amount Kurang"
    else
      params[:r].each do |order_id|
        order = Spree::Order.find_by_id order_id
        payment = order.payments.create({ :amount => order.outstanding_balance, :payment_method => Spree::PaymentMethod::BankTransfer.first, :is_cn => true})
        payment.started_processing!
        payment.pend!
        payment.complete!
        order.update!
      end
      redirect_to request.referrer, notice: "Invoice Order successfully recorded"
    end
  end

  def record_invoice_cn
    if set_journal_invoice.total < total_invoice_amount
      redirect_to request.referrer, alert: "Amount Kurang"
    else
      params[:r].each do |invoice_id|
        invoice = Spree::Invoice.find_by_id invoice_id
        payment_amount = invoice.outstanding_balance
        invoice_payment = Spree::InvoicePayment.create(:invoice_id => invoice.id, :amount => payment_amount, 
          :payment_method_id => Spree::PaymentMethod::BankTransfer.first.id, :state => "checkout", user_id: whodunnit.id)
        invoice_payment.update_attributes(state: "paid", journal_transaction_id: params[:id], is_cn: true)
        invoice.update!
        invoice.send_email_payment_confirmation(params[:id])
      end
      redirect_to request.referrer, notice: "Invoice successfully recorded from CN"
    end
  end

  def reminder_email
    @invoice = Journal::Invoice.find_by_id params[:id]
    if @invoice.send_email_invoice
      @invoice.send_sms_invoice
      flash[:success] = "Email & SMS sent"
    else
      flash[:error] = "Something went wrong! Please try again"
    end
    redirect_to request.referrer
  end

  private

  def set_journal_invoice
    @journal_invoice = Journal::Invoice.find(params[:id])
  end 

  def journal_invoice_params
    params.require(:journal_invoice).permit(:vendor, :currency, :due_date, :total, :payment_state, :spree_user_id, :category, :amount, :description, :originator_type)
  end

  def whodunnit
    current_spree_user
  end

  def total_order_amount
    total_order_amount = 0
    params[:r].each do |order_id|
      total_order_amount += Spree::Order.find_by_id(order_id).outstanding_balance
    end
    return total_order_amount.to_i
  end

  def total_invoice_amount
    total_invoice_amount = 0
    params[:r].each do |invoice_id|
      total_invoice_amount += Spree::Invoice.find_by_id(invoice_id).outstanding_balance
    end
    return total_invoice_amount.to_i
  end
end