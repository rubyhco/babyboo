class Journal::TransactionsController < Journal::BaseController
  before_action :set_journal_transaction, only: [:show, :edit, :update, :destroy, :record_payment, :record_order, :record_invoice, :transfer, :transfer_fund, :record_expense, :record_expense_transfer_fund, :record_income_transfer_fund]

  # GET /journal/transactions
  # GET /journal/transactions.json
  def index
    if current_spree_user.admin?
      @q = Journal::Transaction.not_journal.ransack((params[:q]))
    else
      @q = Journal::Transaction.not_journal.not_expense.ransack((params[:q]))
    end
    @journal_transactions = @q.result.order(created_at: :desc).page(params[:page])
    @income_transactions = Journal::Transaction.where("is_expense = ?", false)
    @expense_transactions = Journal::Transaction.where("is_expense = ?", true)
  end

  # GET /journal/transactions/1
  # GET /journal/transactions/1.json
  def show
  end

  # GET /journal/transactions/new
  def new
    @journal_transaction = Journal::Transaction.new
    @account = Journal::Account.where(is_payment_account: true)
    @category = Journal::Account.where(is_payment_account: false)
    render layout: false
  end

  # GET /journal/transactions/1/edit
  def edit
    render layout: false
  end

  # POST /journal/transactions
  # POST /journal/transactions.json
  def create
    @journal_transaction = Journal::Transaction.new(journal_transaction_params)

    if @journal_transaction.save
      redirect_to journal_transactions_path, notice: 'Transaction was successfully created.'
    else
      redirect_to journal_transactions_path, notice: 'Failed create transaction.'
    end
  end

  # PATCH/PUT /journal/transactions/1
  # PATCH/PUT /journal/transactions/1.json
  def update
    respond_to do |format|
      if @journal_transaction.update(journal_transaction_params)
        format.html { redirect_to @journal_transaction, notice: 'Transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @journal_transaction }
      else
        format.html { render :edit }
        format.json { render json: @journal_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /journal/transactions/1
  # DELETE /journal/transactions/1.json
  def destroy
    @journal_transaction.destroy
    respond_to do |format|
      format.html { redirect_to journal_transactions_url, notice: 'Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def show_import
    render layout: false
  end

  def import
    Journal::Transaction.import(params[:file], params[:account_id])
    redirect_to request.referrer, notice: "Transactions imported."
  end

  #delete journal transaction if record payment
  def delete_journal
    journal_user = Journal::JournalItem.where(transaction_id: params[:id])
    journal_user.delete_all
  end

  def use_store_credit_for_order
    order = Spree::Order.find_by_id(params[:order_id])
    order.add_store_credit_payments_from_admin
    flash[:notice] = "Store credit berhasil digunakan"
    redirect_to request.referrer
  end

  def use_store_credit_for_invoice
    invoice = Spree::Invoice.find_by_id(params[:invoice_id])
    invoice.add_store_credit_payments
    flash[:notice] = "Store credit berhasil digunakan"
    redirect_to request.referrer
  end

  # record transaction order
  def record_order
    Journal::Transaction.transaction do
      transaction_amount = @journal_transaction.outstanding_balance
      total_used = @journal_transaction.used_amount.to_i
      params[:r].each do |order_id|
        break if transaction_amount <= 0
        order = Spree::Order.find_by_id order_id
        payment_amount = transaction_amount < order.outstanding_balance ? transaction_amount : order.outstanding_balance
        payment = order.payments.create(:amount => payment_amount, :payment_method => Spree::PaymentMethod::BankTransfer.first, user_id: whodunnit.id)
        payment.update_columns(journal_transaction_id: params[:id])
        @journal_transaction.histories.create(originator: payment, name: order.user.name, description: "Payment Order #{order.number}")
        payment.started_processing!
        payment.pend!
        payment.complete!
        order.update!
        order.send_email_payment_confirmation(params[:id])
        transaction_amount -= payment_amount
        total_used += payment_amount
      end
      used_amount_order(total_used)
      if @journal_transaction.outstanding_balance < total_order_amount
        redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, alert: "Outstanding Balance Transaction Kurang, beberapa transaksi tidak dapat diproses"
      else
        redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, notice: "Order successfully recorded" 
      end
    end
  end

  # record transaction invoice
  def record_invoice
    Journal::Transaction.transaction do
      transaction_amount = @journal_transaction.outstanding_balance
      total_used = @journal_transaction.used_amount.to_i
      params[:r].each do |invoice_id|
        break if transaction_amount <= 0
        invoice = Spree::Invoice.find_by_id invoice_id
        payment_amount = transaction_amount < invoice.outstanding_balance ? transaction_amount : invoice.outstanding_balance
        invoice_payment = Spree::InvoicePayment.create(:invoice_id => invoice.id, :amount => payment_amount, 
          :payment_method_id => Spree::PaymentMethod::BankTransfer.first.id, :state => "checkout", user_id: whodunnit.id)
        invoice_payment.update_attributes(state: "paid", journal_transaction_id: params[:id])
        @journal_transaction.histories.create(originator: invoice_payment, name: invoice.user.name, description: "Payment Invoice ID #{invoice.id}")
        invoice.update!
        invoice.send_email_payment_confirmation(params[:id])
        transaction_amount -= payment_amount
        total_used += payment_amount
      end
      used_amount_invoice(total_used)
      if @journal_transaction.outstanding_balance < total_invoice_amount
        redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, alert: "Outstanding Balance Transaction Kurang, beberapa transaksi tidak dapat diproses"
      else
      redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, notice: "Invoice successfully recorded"
      end 
    end
  end

  def record_payment
    @transaction = Journal::Transaction.find_by_id(params[:id])
    if @transaction.outstanding_balance <= 0
      if @transaction.is_expense? && @transaction.amount >=0
        redirect_to request.referrer, alert: "Amount harus bernilai negatif! Silahkan edit amount dan record kembali."
      else
        redirect_to journal_transactions_path, alert: "Seluruh Amount Telah Terpakai!"
      end
    else
      @order_payments = Spree::Order.payment_state_is("balance_due").filter(params.slice(:name_user, :number)).page(params[:page]).per(30)
      @invoice_payments = Spree::Invoice.valid.where(:status => "pending_payment").filter(params.slice(:name_user)).page(params[:page]).per(30)
      # @purchase_payments = Spree::PurchaseOrder.where(:payment_state => "pending").page(params[:page]).per(30)
      @bill_payments = Journal::Bill.where(payment_state: "pending", currency: "IDR").without_purchase_order.filter(params.slice(:name_user)).order(created_at: :desc).page(params[:page]).per(30)
      @journal_invoice_payments = Journal::Invoice.where(payment_state: "pending").filter(params.slice(:name_user)).page(params[:page]).per(30)
      @container_payments = Spree::Container.where(payment_state: "pending").filter(params.slice(:name_user)).page(params[:page]).per(30)
      @category_expense = Journal::Account.where(id: Journal::BillItem.all.pluck(:category).uniq) + Journal::Account.cash_and_bank
      @income_transfer_funds = Journal::TransferFund.only_income_reconcile.unreconcile_income.filter(params.slice(:number, :amount_is)).uniq.page(params[:page]).per(30)
      @payment_expense = Journal::PaymentExpense.pending.filter(params.slice(:name_user, :number, :amount_is, :category_is)).page(params[:page]).per(30)
      if @transaction.is_expense == false
        delete_journal
      end
    end
    # @record_payments = @order_payments + @invoice_payments
  end

  def add_counter_part
    @transaction = Journal::Transaction.find_by_id(params[:id])
    render layout:false
  end

  def counter_part
    @transaction = Journal::Transaction.find_by_id(params[:id])
    if @transaction.process_counter_part(params[:journal_transaction][:account_counter_part])
      redirect_to journal_transactions_path, notice: "Succesfully add counter part transaction."
    else
      redirect_to journal_transactions_path, notice: "Failed add counter part transaction."
    end
  end

  def history
    @transaction = Journal::Transaction.find(params[:id])
    @payments = @transaction.payments
    @invoice_payments = @transaction.invoice_payments
    @bills = @transaction.bills
    @journal_invoices = @transaction.journal_invoices
    @journal_invoice_payments = @transaction.journal_invoice_payments
    @containers = @transaction.containers
    @expenses = @transaction.bill_payments
    @income_tfunds = @transaction.income_transfer_funds
    @expense_tfunds = @transaction.expense_transfer_funds
    @storecredit = Spree::StoreCredit.where("memo like ?", "%amount from transaction id #{@transaction.id}%")
  end

  def all_payment_confirmed
    @q = Journal::TransactionHistory.ransack((params[:q]))
    @transaction_histories = @q.result.order(created_at: :desc).page(params[:page])
    @total = @q.result.joins(:journal_transaction).sum("journal_transactions.used_amount")
  end

  def record_bill
    transaction_amount = set_journal_transaction.outstanding_balance
    total_used = set_journal_transaction.used_amount.to_i
    params[:r].each do |bill_id|
      Journal::Transaction.transaction do
        bill = Journal::Bill.find_by_id bill_id
        payment_amount = transaction_amount < bill.total ? transaction_amount : bill.total
        bill.pay!
        bill.record_bill(@journal_transaction.account_id, whodunnit)
        bill.update_columns(journal_transaction_id: params[:id])
        set_journal_transaction.histories.create(originator: bill, name: bill.vendor, description: "Bill #{bill.number}")
        transaction_amount -= payment_amount
        total_used += payment_amount
        if bill.originator_type == "Spree::CashOutRequest"
          bill.originator.update_columns(is_completed: true)
        end
      end
    end
    used_amount_bill(total_used)
    if set_journal_transaction.outstanding_balance < total_bill_amount
      redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, alert: "Outstanding Balance Transaction Kurang, beberapa transaksi tidak dapat diproses"
    else
      redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, notice: "Bill successfully recorded"
    end
  end

  def record_expense
    Journal::Transaction.transaction do
      transaction_amount = @journal_transaction.outstanding_balance
      total_used = @journal_transaction.used_amount.to_i
      params[:r].each do |payment_id|
        break if transaction_amount <= 0
        payment = Journal::PaymentExpense.find_by_id payment_id
        payment_amount = transaction_amount < payment.amount ? transaction_amount : payment.amount
        payment.update_attributes(transaction_id: params[:id])
        @journal_transaction.histories.create(originator: payment.originator, name: payment.description, description: payment.description)
        transaction_amount -= payment_amount
        total_used += payment_amount
        payment.reconcile!
      end
      used_amount_bill(total_used)
      if @journal_transaction.outstanding_balance < total_expense_amount
        redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, alert: "Outstanding Balance Transaction Kurang, beberapa transaksi tidak dapat diproses"
      else
        redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, notice: "Expense successfully recorded" 
      end
    end
  end

  def record_expense_transfer_fund
    Journal::Transaction.transaction do
      transaction_amount = @journal_transaction.outstanding_balance
      total_used = @journal_transaction.used_amount.to_i
      params[:r].each do |tf_expense|
        break if transaction_amount <= 0
        payment = Journal::TransferFund.find_by_id tf_expense
        payment_amount = transaction_amount < payment.total ? transaction_amount : payment.total
        payment.update_columns(expense_transaction_id: params[:id])
        @journal_transaction.histories.create(originator: payment, name: "Expense Transfer Fund To #{Journal::Account.find_by_id(payment.to).name}", description: "Expense Transfer Fund #{payment.id}")
        transaction_amount -= payment_amount
        total_used += payment_amount
      end
      used_amount_bill(total_used)
      if @journal_transaction.outstanding_balance < total_expense_amount
        redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, alert: "Outstanding Balance Transaction Kurang, beberapa transaksi tidak dapat diproses"
      else
        redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, notice: "Expense Transfer Fund successfully recorded" 
      end
    end
  end

  def record_income_transfer_fund
    Journal::Transaction.transaction do
      transaction_amount = @journal_transaction.outstanding_balance
      total_used = @journal_transaction.used_amount.to_i
      params[:r].each do |tf_income|
        break if transaction_amount <= 0
        payment = Journal::TransferFund.find_by_id tf_income
        payment_amount = transaction_amount < payment.total ? transaction_amount : payment.total
        payment.update_columns(income_transaction_id: params[:id])
        @journal_transaction.histories.create(originator: payment, name: "Income Transfer Fund From #{Journal::Account.find_by_id(payment.from).name}", description: "Expense Transfer Fund #{payment.id}")
        transaction_amount -= payment_amount
        total_used += payment_amount
      end
      used_amount_bill(total_used)
      if @journal_transaction.outstanding_balance < total_expense_amount
        redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, alert: "Outstanding Balance Transaction Kurang, beberapa transaksi tidak dapat diproses"
      else
        redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, notice: "Income Transfer Fund successfully recorded" 
      end
    end
  end

  def record_journal_invoice
    transaction_amount = set_journal_transaction.outstanding_balance
    total_used = set_journal_transaction.used_amount.to_i
    params[:r].each do |invoice_id|
      invoice = Journal::Invoice.find_by_id invoice_id
      payment_amount = transaction_amount < invoice.outstanding_balance ? transaction_amount : invoice.outstanding_balance
      invoice_payment = Journal::InvoicePayment.create(:invoice_id => invoice.id, :amount => payment_amount, :payment_method_id => Spree::PaymentMethod::BankTransfer.first, :state => "checkout")
      invoice_payment.update_attributes(state: "paid", journal_transaction_id: params[:id])
      set_journal_transaction.histories.create(originator: invoice_payment, name: invoice.user.name, description: "Credit Note #{invoice.number}")
      invoice.update!
      transaction_amount -= payment_amount
      total_used += payment_amount
    end
    used_amount_journal_invoice(total_used)
    if set_journal_transaction.outstanding_balance < total_journal_invoice_amount
      redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, alert: "Outstanding Balance Transaction Kurang, beberapa transaksi tidak dapat diproses"
    else
      redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, notice: "Invoice successfully recorded"
    end
  end

  def record_container
    params[:r].each do |container_id|
      container = Spree::Container.find_by_id container_id
      container.pay!
      container.update_columns(journal_transaction_id: params[:id])
    end
    redirect_to request.referrer.present? ? request.referrer : journal_transactions_path, notice: "Container successfully recorded"
  end

  def transfer
    @trans = Journal::Transaction.find_by_id(params[:id])
  end

  def add_store_credit
    user_id = params[:add_store_credit][:user_id]
    transaction = Journal::Transaction.find_by_id(params[:id])
    outstanding_balance = transaction.outstanding_balance
    total_used_amount = transaction.used_amount.to_i + outstanding_balance
    transaction.update_columns(used_amount: total_used_amount)

    transaction.move_to_store_credit(user_id, outstanding_balance, whodunnit)

    transaction.update_status
    redirect_to journal_transactions_path, notice: "Succesfully Move Balance to Store Credit"
  end

  private

    def whodunnit
      current_spree_user
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_journal_transaction
      @journal_transaction = Journal::Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def journal_transaction_params
      params.require(:journal_transaction).permit(:description, :amount, :category_id, :account_id, :is_verified, :is_expense, :transaction_date, :status, :pemilik_rekening)
    end

    def total_order_amount
      total_order_amount = 0
      params[:r].each do |order_id|
        total_order_amount += Spree::Order.find_by_id(order_id).outstanding_balance
      end
      return total_order_amount.to_i
    end

    def total_expense_amount
      total_expense_amount = 0
      params[:r].each do |payment_id|
        total_expense_amount += Journal::PaymentExpense.find_by_id(payment_id).amount
      end
      return total_expense_amount.to_i
    end

    def used_amount_order(total_used)
      @transaction = Journal::Transaction.find_by_id(params[:id])
      @transaction.update_columns(used_amount: total_used)
      @transaction.update_status
    end

    def total_invoice_amount
      total_invoice_amount = 0
      params[:r].each do |invoice_id|
        total_invoice_amount += Spree::Invoice.find_by_id(invoice_id).outstanding_balance
      end
      return total_invoice_amount.to_i
    end

    def total_purchase_amount
      total_purchase_amount = 0
      params[:r].each do |purchase_id|
        total_purchase_amount += Spree::PurchaseOrder.find_by_id(purchase_id).total
      end
      return total_purchase_amount.to_i
    end

    def total_bill_amount
      total_bill_amount = 0
      params[:r].each do |bill_id|
        @bill = Journal::Bill.find_by_id(bill_id)
        total_bill_amount += @bill.items.first.amount
      end
      return total_bill_amount.to_i
    end

    def total_journal_invoice_amount
      total_journal_invoice_amount = 0
      params[:r].each do |journal_invoice_id|
        @journal_invoice = Journal::Invoice.find_by_id(journal_invoice_id)
        total_journal_invoice_amount += @journal_invoice.outstanding_balance
      end
      return total_journal_invoice_amount.to_i
    end

    def used_amount_invoice(total_used)
      @transaction = Journal::Transaction.find_by_id(params[:id])
      @transaction.update_columns(used_amount: total_used)
      @transaction.update_status
    end

    def used_amount_purchase(total_used)
      @transaction = Journal::Transaction.find_by_id(params[:id])
      @transaction.update_columns(used_amount: total_used)
      @transaction.update_status
    end

    def used_amount_bill(total_used)
      @transaction = Journal::Transaction.find_by_id(params[:id])
      @transaction.update_columns(used_amount: total_used)
      @transaction.update_status
    end

    def used_amount_journal_invoice(total_used)
      @transaction = Journal::Transaction.find_by_id(params[:id])
      @transaction.update_columns(used_amount: total_used)
      @transaction.update_status
    end
end
