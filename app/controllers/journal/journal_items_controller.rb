class Journal::JournalItemsController < Journal::BaseController
  def index
    @q = Journal::JournalItem.ransack((params[:q]))
    @journal_items = @q.result.order(created_at: :desc).page(params[:page]).per(30)
  end

  def all_journal_entries
    @q = Journal::JournalEntry.ransack((params[:q]))
    @journal_entries = @q.result.uniq.order(date: :desc).page(params[:page]).per(30)
  end

  def new
    @journal_entry = Journal::JournalEntry.new
  end

  def create
    debit = 0
    credit = 0
    params[:journal_journal_entry][:items_attributes].each do |key, value|
      debit += value[:debit].to_i
      credit += value[:credit].to_i
    end
    if debit != credit
      flash[:danger] = "Create Journal Item failed."
      redirect_to request.referrer, alert: 'Create Journal Item Failed! Debit & Credit should be balanced'
    else
      @journal_transaction_item = Journal::JournalEntry.new(journal_entry_params)
      if @journal_transaction_item.save
        flash[:success] = "Journal Item was successfully created."
        redirect_to journal_journal_items_path, notice: 'Journal Item was successfully created.'
      else
        flash[:danger] = "Create Journal Item failed."
        redirect_to journal_journal_items_path, notice: 'Create Journal Item Failed'
      end
    end
  end

  def edit
    @journal_item = Journal::JournalItem.find(params[:id])
    render layout: false
  end

  def update
    @journal_item = Journal::JournalItem.find(params[:id])
    if @journal_item.update_columns(account_id: params[:journal_journal_item][:account_id])
      redirect_to journal_journal_items_path, notice: "Account was successfully updated."
    else
      redirect_to journal_journal_items_path, alert: "Update account failed."
    end
  end

  private

  def journal_entry_params
      params.require(:journal_journal_entry).permit(
        :description,  
        :date,
        items_attributes: [
          :id,
          :account_id,
          :description,
          :debit,
          :credit,
          :currency,
          :originator_type,
          :originator_id,
          :_destroy
        ],
        )
    end

  def journal_item_params
    params.require(:journal_journal_item).permit(:account_id, :description, :debit, :credit, :currency, :originator_type, :originator_id)
  end

  def whodunnit
    spree_current_user
  end
end
