class Journal::ReportsController < Journal::BaseController
  
  def trial_balance
    @categories = Journal::Category.all
    @q =  Journal::JournalItem.ransack(params[:q])
    @items = @q.result
  end

  def cash_and_bank
    @cash_and_banks = Journal::Account.where(category_id: 3)
  end

  def general_ledger
    @q = Journal::JournalItem.ransack(params[:q])
    @generals = @q.result
    @general_ledger = @generals
    @total_debit = @generals.sum(:debit)
    @total_credit = @generals.sum(:credit)
    @accounts = Journal::Account.filter(params.slice(:currency_is))
  end

  def balance_sheet
    @q = Journal::JournalItem.ransack(params[:q])
    @items = @q.result.page(params[:page]).per(30)
    @account_assets = Journal::Category.find_by_prefix('1-100').accounts1
    @account_liability = Journal::Category.find_by_prefix('2-202').accounts1
    @account_equity = Journal::Category.find_by_prefix('3-302').accounts1
    @total_assets = total_assets
    @total_liability = total_liability
    @total_equity = total_assets-total_liability-total_equity
  end

  def profit_and_loss
    @q = Journal::JournalItem.ransack(params[:q])
    @items = @q.result.page(params[:page]).per(30)
    @primary_income = Journal::Category.find_by_prefix('4-402').accounts1
    @cost_of_sales = Journal::Category.find_by_prefix('5-504').accounts1
    @expenses = Journal::Category.find_by_prefix('6-603').accounts1
    @other_income = Journal::Category.find_by_prefix('7-700').accounts1
    @other_expenses = Journal::Category.find_by_prefix('9-900').accounts1

    @total_primary_income = total_primary_income
    @total_cost_of_sales = total_cost_of_sales
    @total_expenses = total_expenses
    @total_other_income = total_other_income
    @total_other_expenses = total_other_expenses
  end

  def cash_flow_statement
    @q = Journal::JournalItem.item_idr.ransack(params[:q])
    @items = @q.result

    @operating = Journal::Account.account_idr.where(category_id: Journal::Category.where(account_type: "operating").pluck(:id))
    @investing = Journal::Account.account_idr.where(category_id: Journal::Category.where(account_type: "investing").pluck(:id))
    @financing = Journal::Account.account_idr.where(category_id: Journal::Category.where(account_type: "financing").pluck(:id))

    items_beginning = Journal::JournalItem.where("created_at < ?", params[:q][:created_at_gteq])
    @beginning_cash = balance_account(Journal::Account.account_idr,items_beginning)
  end

  def transaction
    @q = Journal::JournalItem.ransack(params[:q])
    @trans = @q.result.order(created_at: :desc).page(params[:page]).per(30)
    @transac = @q.result
    @account = Journal::Account.find_by_id(params[:q][:account_id_eq])
    journal_item = Journal::JournalItem.all

    if !@trans.empty?
      @total_debit = @trans.sum(:debit)
      @total_credit = @trans.sum(:credit)
      @trans_before = @account.items.where("id < ?", @trans.first.id)
      @total_debit_before = @trans_before.sum(:debit)
      @total_credit_before = @trans_before.sum(:credit)

      if @account.prefix == 1
        @total_balance_before = @total_debit_before - @total_credit_before
      else
        @total_balance_before = @total_credit_before - @total_debit_before
      end
    end

    respond_to do |format|
      format.html
      format.csv { send_data @transac.to_csv(@account, journal_item) }
    end
  end

  private
    def total_assets
      total_assets = 0
      @account_assets.each do |s|
        @debit = @items.where(account_id: s.id).sum(:debit)
        @credit = @items.where(account_id: s.id).sum(:credit)
        @total = @debit - @credit
        total_assets += @total
      end
      return total_assets
    end

    def total_liability
      total_liability = 0
      @account_liability.each do |s|
        @debit = @items.where(account_id: s.id).sum(:debit)
        @credit = @items.where(account_id: s.id).sum(:credit)
        @total = @debit - @credit
        total_liability += @total
      end
      return total_liability
    end

    def total_equity
      total_equity = 0
      @account_equity.each do |s|
        @debit = @items.where(account_id: s.id).sum(:debit)
        @credit = @items.where(account_id: s.id).sum(:credit)
        @total = @debit - @credit
        total_equity += @total
      end
      return total_equity
    end

    def total_primary_income
      total_primary_income = 0
      @primary_income.each do |s|
        @debit = @items.where(account_id: s.id).sum(:debit)
        @credit = @items.where(account_id: s.id).sum(:credit)
        @total = @credit - @debit
        total_primary_income += @total
      end
      return total_primary_income
    end

    def total_cost_of_sales
      total_cost_of_sales = 0
      @cost_of_sales.each do |s|
        @debit = @items.where(account_id: s.id).sum(:debit)
        @credit = @items.where(account_id: s.id).sum(:credit)
        @total = @debit - @credit
        total_cost_of_sales += @total
      end
      return total_cost_of_sales
    end

    def total_expenses
      total_expenses = 0
      @expenses.each do |s|
        @debit = @items.where(account_id: s.id).sum(:debit)
        @credit = @items.where(account_id: s.id).sum(:credit)
        @total = @debit - @credit
        total_expenses += @total
      end
      return total_expenses
    end

    def total_other_income
      total_other_income = 0
      @other_income.each do |s|
        @debit = @items.where(account_id: s.id).sum(:debit)
        @credit = @items.where(account_id: s.id).sum(:credit)
        @total = @credit - @debit
        total_other_income += @total
      end
      return total_other_income
    end

    def total_other_expenses
      total_other_expenses = 0
      @other_expenses.each do |s|
        @debit = @items.where(account_id: s.id).sum(:debit)
        @credit = @items.where(account_id: s.id).sum(:credit)
        @total = @debit - @credit
        total_other_expenses += @total
      end
      return total_other_expenses
    end

    def balance_account(accounts, journal_items)
      total = 0
      accounts.each do |account|
        account_items = journal_items.where(account_id: account.id)
        if account.prefix == 1
          total += account_items.sum(:debit) - account_items.sum(:credit)
        else
          total += account_items.sum(:credit) - account_items.sum(:debit)
        end
        return total
      end
    end
end