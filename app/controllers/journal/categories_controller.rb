class Journal::CategoriesController < Journal::BaseController
	def index
    @categories = Journal::Category.all
	end

  def new
    @category = Journal::Category.new
    render layout: false
  end

  def edit
    @category = Journal::Category.find(params[:id])
    render layout: false
  end

  def create
    @category = Journal::Category.new(category_params)
    if @category.save
      flash[:success] = "Category was successfully created."
      redirect_to journal_categories_path, notice: 'Category was successfully created.'
    else
      flash[:danger] = "Create category failed."
      redirect_to journal_categories_path, notice: 'Create Category Failed'
    end
  end

  def update
  @category = Journal::Category.find(params[:id])
 
    if @category.update(category_params)
      flash[:success] = "Category was successfully updated."
      redirect_to journal_categories_path
    else
      flash[:danger] = "Update category failed."
      redirect_to journal_categories_path
    end
  end

  def show
    
  end


  def destroy
    @category = Journal::Category.find(params[:id])
    if @category.destroy
      flash[:success] = "Category was successfully deleted."
      redirect_to journal_categories_path
    else
      flash[:danger] = "Delete category failed."
      redirect_to journal_categories_path
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:journal_category).permit(:name)
    end
end

