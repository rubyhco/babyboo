class Journal::StoreCreditCategoriesController < Journal::BaseController
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  # GET /journal/accounts
  # GET /journal/accounts.json
  def index
    @categories = Spree::StoreCreditCategory.all
  end

  # GET /journal/accounts/1
  # GET /journal/accounts/1.json
  def show
  end

  # GET /journal/accounts/new
  def new
    @category = Spree::StoreCreditCategory.new
    render layout: false
  end

  # GET /journal/accounts/1/edit
  def edit
    render layout: false
  end

  # POST /journal/accounts
  # POST /journal/accounts.json
  def create
    @category = Spree::StoreCreditCategory.new(store_credit_categories_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to journal_store_credit_categories_path, notice: 'Category was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /journal/accounts/1
  # PATCH/PUT /journal/accounts/1.json
  def update
    respond_to do |format|
      if @category.update(store_credit_categories_params)
        format.html { redirect_to journal_store_credit_categories_path, notice: 'Category was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /journal/accounts/1
  # DELETE /journal/accounts/1.json
  def destroy
    @category = Spree::StoreCreditCategory.find(params[:id])
    if @category.destroy!
      redirect_to journal_store_credit_categories_path, notice: "Category was successfully deleted."
    else
      redirect_to journal_store_credit_categories_path, notice: "Delete Category failed."
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Spree::StoreCreditCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def store_credit_categories_params
      params.require(:store_credit_category).permit(:name, :journal_account_id)
    end
end
