class Journal::TransactionRmbsController < Journal::BaseController
  before_action :set_journal_transaction_rmb, only: [:show, :edit, :update, :destroy]

  # GET /journal/transaction_rmbs
  # GET /journal/transaction_rmbs.json
  def index
    @journal_transaction_rmbs = Journal::TransactionRmb.all
  end

  # GET /journal/transaction_rmbs/1
  # GET /journal/transaction_rmbs/1.json
  def show
  end

  # GET /journal/transaction_rmbs/new
  def new
    @journal_transaction_rmb = Journal::TransactionRmb.new
    render layout: false
  end

  # GET /journal/transaction_rmbs/1/edit
  def edit
    render layout: false

  end

  # POST /journal/transaction_rmbs
  # POST /journal/transaction_rmbs.json
  def create
    @journal_transaction_rmb = Journal::TransactionRmb.new(journal_transaction_rmb_params)

    respond_to do |format|
      if @journal_transaction_rmb.save
        format.html { redirect_to @journal_transaction_rmb, notice: 'Transaction rmb was successfully created.' }
        format.json { render :show, status: :created, location: @journal_transaction_rmb }
      else
        format.html { render :new }
        format.json { render json: @journal_transaction_rmb.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /journal/transaction_rmbs/1
  # PATCH/PUT /journal/transaction_rmbs/1.json
  def update
    respond_to do |format|
      if @journal_transaction_rmb.update(journal_transaction_rmb_params)
        format.html { redirect_to @journal_transaction_rmb, notice: 'Transaction rmb was successfully updated.' }
        format.json { render :show, status: :ok, location: @journal_transaction_rmb }
      else
        format.html { render :edit }
        format.json { render json: @journal_transaction_rmb.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /journal/transaction_rmbs/1
  # DELETE /journal/transaction_rmbs/1.json
  def destroy
    @journal_transaction_rmb.destroy
    respond_to do |format|
      format.html { redirect_to journal_transaction_rmbs_url, notice: 'Transaction rmb was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_journal_transaction_rmb
      @journal_transaction_rmb = Journal::TransactionRmb.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def journal_transaction_rmb_params
      params.require(:journal_transaction_rmb).permit(:description, :amount, :category_id, :account_id, :is_verified, :is_expense)
    end
end
