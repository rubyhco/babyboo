class Journal::InfoPaymentsController < Journal::BaseController

  def index
    @info_payments = Spree::PaymentConfirmation.filter(params.slice(:status_is, :sender_name_contain, :member_contain, :amount_is, :invoice_is, :order_is)).order(created_at: :desc).page(params[:page]).per(20)    
  end

  def confirmed
  	@payment_confirmation = Spree::PaymentConfirmation.find_by_id(params[:id])
  	if @payment_confirmation.confirmed!
      @payment_confirmation.send_sms
  		redirect_to request.referrer, notice: "Payment Confirmed"
  	else
  		redirect_to request.referrer, notice: "Something went wrong! Please try again"
  	end
  end

end