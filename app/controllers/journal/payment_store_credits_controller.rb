class Journal::PaymentStoreCreditsController < Journal::BaseController
  def index
    @order_payments = Spree::Order.payment_state_is("balance_due").filter(params.slice(:name_user, :number)).page(params[:page]).per(20)
    @invoice_payments = Spree::Invoice.valid.where(:status => "pending_payment").filter(params.slice(:name_user)).page(params[:page]).per(20)
    @journal_invoice_payments = Journal::Invoice.where(payment_state: "pending").filter(params.slice(:name_user)).page(params[:page]).per(20)
  end

  def use_store_credit_for_order
    order = Spree::Order.find_by_id(params[:order_id])
    order.add_store_credit_payments_from_admin
    flash[:notice] = "Store credit berhasil digunakan"
    redirect_to request.referrer
  end

  def use_store_credit_for_invoice
    invoice = Spree::Invoice.find_by_id(params[:invoice_id])
    invoice.add_store_credit_payments
    flash[:notice] = "Store credit berhasil digunakan"
    redirect_to request.referrer
  end

  def use_store_credit_for_journal_invoice
    journal_invoice = Journal::Invoice.find_by_id(params[:journal_invoice_id])
    journal_invoice.add_payment_store_credit
    flash[:notice] = "Store credit berhasil digunakan"
    redirect_to request.referrer
  end
end
