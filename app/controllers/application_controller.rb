class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery prepend: true

  before_action :set_locale
  before_filter :configure_permitted_parameters, if: :devise_controller?

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

protected

  def configure_permitted_parameters
    # devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:email, :password, :birthdate, :howdoyou) }
    # devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:email, :password, :birthdate, :howdoyou) }
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :password, :password_confirmation, :birthdate, :howdoyou, :name, :phone, :prefer_contact])
    devise_parameter_sanitizer.permit(:account_update, keys: [:email, :password, :password_confirmation, :birthdate, :howdoyou, :name, :phone, :prefer_contact])
  end

end
