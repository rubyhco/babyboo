Spree::Admin::ReportsController.class_eval do

  before_action :user_admin?, only: [:index]

  def user_admin?
    if spree_current_user
      spree_current_user.admin? ? true : redirect_to("/admin")
    else
      redirect_to("/admin")
    end
  end

  def index
    if params[:start_date].nil? && params[:end_date].nil?
      params[:start_date] = Time.now.beginning_of_month.strftime("%Y-%m-%d")
      params[:end_date] = Time.now.to_date.strftime("%Y-%m-%d")
    end
    @sales_pack_rs = paid_ready_stock.joins(:line_items).sum("spree_line_items.quantity")
    @sales_pack_inv = paid_invoices.joins(:items).sum("spree_invoice_items.quantity")
    @dp_pack = paid_preorder.joins(:line_items).sum("spree_line_items.quantity")
    @total_ready_stock = paid_ready_stock.sum(:item_total)
    @total_invoice = total_invoice
    @total_unpaid_invoices = total_unpaid_invoice.sum(:total)
    @total_dp = paid_preorder.sum(:payment_total)
    @total_hpp_invoices = total_hpp_invoices
    @total_hpp_rs = total_hpp_rs
    @total_credit_note = Journal::Invoice.pending.sum(&:outstanding_balance)
  end

  def sales_ready_stock
    if params[:start_date].nil? && params[:end_date].nil?
      params[:start_date] = Time.now.beginning_of_month.strftime("%Y-%m-%d")
      params[:end_date] = Time.now.to_date.strftime("%Y-%m-%d")
    end
    @paid_ready_stock = paid_ready_stock
    @items_sales = paid_ready_stock.page params[:page]

    respond_to do |format|
      format.html
      format.csv { send_data @paid_ready_stock.to_csv_rs }
    end
  end

  def cek_stock
    @variants = Spree::Variant.where(id: [3621, 3615, 3616, 3610, 3599, 3595, 3522, 3488, 3339, 3289, 3263, 3229, 3218, 3190, 3134, 3108, 3106, 3102, 3104, 3097, 3096, 1798, 1727, 1701, 1661, 1656, 1533, 1530, 1486, 1462, 1358, 1329, 1310, 1260, 520, 513, 514, 515, 507, 508, 509, 510, 511, 504, 505, 499, 500, 502, 503, 497, 498, 492, 494, 495, 488, 489, 491, 483, 485, 486, 480, 481, 482, 472, 473, 116])

    respond_to do |format|
      format.html
      format.csv { send_data @variants.cek_stock }
    end
  end

  def income_dp_preorder
    if params[:start_date].nil? && params[:end_date].nil?
      params[:start_date] = Time.now.beginning_of_month.strftime("%Y-%m-%d")
      params[:end_date] = Time.now.to_date.strftime("%Y-%m-%d")
    end
    @paid_preorder = paid_preorder
    @items_sales = paid_preorder.page params[:page]

    respond_to do |format|
      format.html
      format.csv { send_data @paid_preorder.to_csv_po }
    end
  end

  def credit_note
    @credit_note = Journal::Invoice.pending.order(created_at: :desc).page params[:page]
    @credit_note_csv = Journal::Invoice.pending
    respond_to do |format|
      format.html
      format.csv { send_data @credit_note_csv.to_csv }
    end
  end

  def unpaid_invoice
    if params[:start_date].nil? && params[:end_date].nil?
      params[:start_date] = Time.now.beginning_of_month.strftime("%Y-%m-%d")
      params[:end_date] = Time.now.to_date.strftime("%Y-%m-%d")
    end
    @unpaid = total_unpaid_invoice
    @unpaid_invoice = total_unpaid_invoice.page params[:page]

    respond_to do |format|
      format.html
      format.csv { send_data @unpaid.to_csv }
    end
  end

  def invoice_sales
    if params[:start_date].nil? && params[:end_date].nil?
      params[:start_date] = Time.now.beginning_of_month.strftime("%Y-%m-%d")
      params[:end_date] = Time.now.to_date.strftime("%Y-%m-%d")
    end
    @paid_invoices = paid_invoices
    @items_sales = paid_invoices.page params[:page]
    @total_invoice = total_invoice

    respond_to do |format|
      format.html
      format.csv { send_data @paid_invoices.to_csv }
    end
  end

  def return_order
    @search = Spree::ReturnAuthorization.all.ransack(params[:q])
    @return_order = @search.result.order(created_at: :desc).page params[:page]
  end

  def store_credit
    @store_credits = Spree::StoreCredit.all.filter(params.slice(:start_date, :end_date, :name_user)).page params[:page]
  end

  def lost_item
    @lost_items = Spree::StockMovement.lost_item.filter(params.slice(:start_date, :end_date)).order(created_at: :desc).page params[:page]
  end

  def dp_hangus
    @dp_hangus = Spree::InventoryUnit.dp_hangus.filter(params.slice(:start_date, :end_date, :order_number_contain)).page params[:page]
  end

  private

  def paid_ready_stock
    Spree::Order.ready_stock.paid.filter(params.slice(:start_date, :end_date))
  end

  def paid_preorder
    Spree::Order.preorder.paid.filter(params.slice(:start_date, :end_date))
  end

  def paid_invoices
    Spree::Invoice.paid.filter(params.slice(:start_date, :end_date))
  end

  def total_invoice
    total_invoice = 0
    paid_invoices.each do |a|
      total_invoice += a.items.sum(:total_amount)
    end
    return total_invoice
  end

  def total_hpp_invoices
    total_hpp = 0
    paid_invoices.each do |inv|
      inv.items.each do |item|
        total_hpp += item.total_pieces * item.variant.cost_price.to_i
      end
    end
    return total_hpp
  end

  def total_unpaid_invoice
    Spree::Invoice.valid.pending_payment.filter(params.slice(:start_date, :end_date, :name_user))
  end

  def total_hpp_rs
    total_hpp = 0
    paid_ready_stock.each do |order|
      order.line_items.each do |a|
        total_hpp += a.real_cost_price.to_i
      end
    end
    return total_hpp
  end
end