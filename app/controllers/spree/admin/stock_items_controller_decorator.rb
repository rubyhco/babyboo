Spree::Admin::StockItemsController.class_eval do
  def card_stock
    @search = Spree::StockItem.ransack(params[:q])
    @stock_items = @search.result.page(params[:page]).per(params[:per_page])
  end

  def stock_movement
    @stock_item = Spree::StockItem.find_by_id params[:id]
    @movements = @stock_item.stock_movements.page params[:page]  
  end

  def stock_warehouse_movement
    @stock_item = Spree::StockItem.find_by_id params[:id]
    @movements = @stock_item.stock_warehouse_movements.page params[:page]
  end

  private

 	def total_pack
 		total_pack = 0
    @search.result.joins(variant: :product).each do |stock|
      total_pack += stock.count_on_hand/stock.variant.product.total_items
    end
    return total_pack
 	end

 	def total_value
 		total_value = 0
    @search.result.joins(:variant).each do |stock|
      total_value += stock.count_on_hand.to_i * stock.variant.cost_price.to_f
    end
    return total_value
 	end
end