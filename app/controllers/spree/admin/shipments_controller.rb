module Spree
  module Admin
    class ShipmentsController < Spree::Admin::BaseController
      def index
        @ready_stocks = Spree::Shipment.joins(:order).where("spree_orders.preorder = ?", false)
        @preorder = Spree::Shipment.where("invoice_id IS NOT NULL")
        @shipments = (@ready_stocks + @preorder).sort_by(&:created_at).reverse!
        @paginatable_array = Kaminari.paginate_array(@shipments).page(params[:page]).per(20)
        # @shipments = Spree::Shipment.includes(:order).joins(:order).where("spree_orders.preorder = false OR spree_shipments.invoice_id IS NOT NULL").order(:created_at).page params[:page]
      end

      def show
        @shipment = Spree::Shipment.find_by_number params[:id]
        @order_or_invoice = @shipment.order.nil? ? @shipment.invoice : @shipment.order
        respond_with(@order_or_invoice) do |format|
          format.html
          format.pdf do
            render :layout => false , :template => "spree/admin/shipments/invoice.pdf.prawn"
          end
        end
      end
      
      def ship
        params[:shipments].each do |x,y|
          ship = Spree::Shipment.find_by_id x
          ship.update_attributes(tracking: y["tracking"])
          ship.suppress_mailer = (params[:send_mailer] == 'false')
          ship.ship!
        end
        flash[:success] = "Berhasil memperbarui Shipments"
        redirect_to admin_shipment_path
      end

      def mark_as_shipped
        @shipement = Spree::Shipment.find_by_id(params[:number])
        unless @shipment.shipped?
          @shipment.ship!
        end
        redirect_to admin_shipment_path
      end
    end
  end
end
