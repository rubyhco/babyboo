Spree::Admin::OrdersController.class_eval do

  def payment_confirmation_new
    @payment_confirmation = Spree::PaymentConfirmation.new
  end

  def payment_confirmation
    @payment_confirmation = Spree::PaymentConfirmation.new(payment_confirmation_params)
    if @payment_confirmation.save
      @payment_confirmation.contents.create!(order_id: Spree::Order.find_by_number(params[:id]).id)
      redirect_to admin_orders_path, notice: "Successfully created payment confirmation."
    else
      redirect_to admin_orders_path, alert: "Sorry, something went wrong. Please try again!"
    end
  end

  def lock_unlock
    order = Spree::Order.find_by_number(params[:id])
    if order.process_lock!
      notice =  order.lock? ? "Successfully lock order." : "Successfully unlock order."
      redirect_to admin_orders_path, notice: notice
    else
      alert = "Sorry, something went wrong. Please try again!"
      redirect_to admin_orders_path, alert: alert
    end
  end

  def approve
    @order.contents.approve(user: try_spree_current_user)
    flash[:success] = Spree.t(:order_approved)
    redirect_to(spree.cart_admin_order_url(@order))
  end

  def return_request
    @pending_return_items = Spree::ReturnItem.awaiting.page params[:page]
  end

  def show_inventory_unit
    @q = Spree::InventoryUnit.ransack((params[:q]))
    @inventory_units = @q.result.includes(:order).order(created_at: :desc).page(params[:page])
  end

  def backordered_inventory
    inventory_unit = Spree::InventoryUnit.find_by_id(params[:id])
    if inventory_unit.order.preorder? && inventory_unit.invoice_item.present?
      flash[:error] = "Item ini sudah ada di invoice. Silahkan hapus invoice terlebih dahulu dan silahkan mengulang proses mark as pending."
      redirect_to request.referrer
    else
      Spree::Order.transaction do
        if inventory_unit.on_hand?
          if inventory_unit.update_columns(state: "backordered")
            inventory_unit.shipment.update_columns(state: "pending")
            inventory_unit.order.update_columns(shipment_state: "backorder")
            inventory_unit.update_free_stock(inventory_unit.total_pieces)
            flash[:success] = "Successfully mark as pending inventory unit"
            redirect_to request.referrer
          else
            flash[:error] = "Mark as pending inventory unit failed"
            redirect_to request.referrer
          end
        else
          if inventory_unit.variant.stock_items.first.can_supply_qty?(inventory_unit.total_pieces)
            inventory_unit.update_columns(state: "on_hand")
            inventory_unit.shipment.update_columns(state: "ready") unless inventory_unit.shipment.inventory_units.pluck(:state).include?("backordered")
            inventory_unit.order.update_columns(shipment_state: "ready") unless inventory_unit.shipment.inventory_units.pluck(:state).include?("backordered")
            inventory_unit.update_free_stock(inventory_unit.total_pieces * -1)
            flash[:success] = "Successfully mark as ready inventory unit"
            redirect_to request.referrer
          else
            flash[:error] = "Mark as ready inventory unit failed, insufficient stock."
            redirect_to request.referrer
          end
        end
      end
    end
  end

  def download_pending_po
    if Spree::Order.delay.export_po_with_state(try_spree_current_user.email, "backordered")
      flash[:notice] = "Thanks, your export has been added to the queue for processing. You will receive an email confirming the export po order pending once it has completed."
    else
      flash[:error] = "Error export data."
    end
    redirect_to admin_orders_path
  end

  private

  def load_order
    @order ||= Spree::Order.includes(:adjustments).find_by_number!(params[:id])
    if @order.preorder? && @order.number.first == "R" && !@order.completed? && @order.confirm?
      @order.generate_order_number(prefix: "PO")
      @order.save
    end
    authorize! action, @order
  end

  def payment_confirmation_params
    params.require(:payment_confirmation).permit(:bank_destination, :bank_sender, :sender_name, :amount, :transaction_date,
      :note, :user_id)
  end
end

