module Spree
  module Admin
    class InventoryAdjustmentsController < ResourceController

      def index
        @q = Spree::InventoryAdjustment.ransack((params[:q]))
        @inventory_adjustments = @q.result.page(params[:page]).per(params[:per_page])
      end

      def new
        invoke_callbacks(:new_action, :before)
        respond_with(@object) do |format|
          format.html { render layout: !request.xhr? }
          if request.xhr?
            format.js   { render layout: false }
          end
        end
      end

      def create
        invoke_callbacks(:create, :before)
        @object.attributes = permitted_resource_params
        if @object.save
          invoke_callbacks(:create, :after)
          # flash[:success] = flash_message_for(@object, :successfully_created)
          respond_with(@object) do |format|
            format.html { redirect_to admin_inventory_adjustment_path(@object) }
            format.js   { render layout: false }
          end
        else
          invoke_callbacks(:create, :fails)
          respond_with(@object) do |format|
            format.html do
              flash.now[:error] = @object.errors.full_messages.join(", ")
              render_after_create_error
            end
            format.js { render layout: false }
          end
        end
      end

      def show
        @inventory_adjustment = @object
        @items = @object.items
      end

      def update_items
        update = Spree::InventoryAdjustment.transaction do
          @inventory_adjustment = Spree::InventoryAdjustment.find_by_id(params[:id])
          params[:select_items].each do |x,y|
            next if y["'checked'"].nil?
            item = Spree::InventoryAdjustmentItem.find x
            item.update_attributes(notes: y["'notes'"], real_qty: y["'real_qty'"].to_i, category_id: y["'category'"].to_i)
          end
        end
        if update
          @inventory_adjustment.in_progress! unless @inventory_adjustment.in_progress?
          flash[:success] = "Item successfully updated"
          redirect_to request.referrer
        else
          flash[:error] = "Update failed"
          redirect_to request.referrer
        end

      end

      def destroy
        if @object.destroy
          flash[:success] = "Inventory Adjustment was successfully deleted."
          redirect_to request.referrer
        else
          flash[:error] = "Delete Inventory Adjustment failed"
          redirect_to request.referrer
        end
      end

      def item_destroy
        item = Spree::InventoryAdjustmentItem.find_by_id(params[:item])
        if item.destroy
          flash[:success] = "Item was successfully deleted."
          redirect_to request.referrer
        else
          flash[:error] = "Delete Item failed"
          redirect_to request.referrer
        end
      end

      def add_item
        variant = Spree::Variant.find_by_id(params[:add_item][:variant_id])
        if @object.items.find_by_variant_id(variant.id).present?
          flash[:error] = "SKU telah ada didalam daftar"
          redirect_to request.referrer
        else
          create_item = @object.items.create(variant: variant, theoritical_qty: variant.stock_items.first.stock_warehouse)
          if create_item
            flash[:success] = "Add code item was successfully created."
            redirect_to request.referrer
          else
            flash[:error] = "Add code item failed."
            redirect_to request.referrer
          end
        end
      end

      def update
        invoke_callbacks(:update, :before)
        if @object.update_attributes(permitted_resource_params)
          invoke_callbacks(:update, :after)
          respond_with(@object) do |format|
            format.html do
              flash[:success] = flash_message_for(@object, :successfully_updated)
              redirect_to request.referrer
            end
            format.js { render layout: false }
          end
        else
          invoke_callbacks(:update, :fails)
          respond_with(@object) do |format|
            format.html do
              flash.now[:error] = @object.errors.full_messages.join(", ")
              render_after_update_error
            end
            format.js { render layout: false }
          end
        end
      end

      def validate
        if @object.validate!
          flash[:success] = "Inventory adjustment has been validated."
          redirect_to admin_inventory_adjustment_path(@object)
        else
          flash[:error] = "Failed validate inventory adjustment"
          redirect_to admin_inventory_adjustment_path(@object)          
        end
      end

    end
  end
end