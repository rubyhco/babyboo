module Spree
  module Admin
    class ReturnToSuppliersController < ResourceController

      def index
        @q = @return_to_suppliers.ransack((params[:q]))
        @return_to_suppliers = @q.result.page(params[:page]).per(params[:per_page])
      end

      def show
        
      end

      def items
        @supplier = @return_to_supplier.supplier
        @search = Spree::Variant.joins(:product).where("spree_products.supplier_id = ?", @supplier.id).ransack(params[:q])
        @items = @search.result.page(params[:page]).per(params[:per_page])
      end

      def show
        @items = @return_to_supplier.items.page(params[:page]).per(params[:per_page])
      end

      def return
        variant_id = Spree::Variant.find_by_sku(params[:item][:sku]).id
        @return_to_supplier.items.create!(variant_id: variant_id, quantity: params[:item][:quantity], 
          cost_price: params[:item][:cost_price].to_d, reason: params[:item][:reason], cost_currency: 
          params[:item][:cost_currency])
        flash[:notice] = "Item has been succesfully added."
        redirect_to request.referrer
      end

      def update_return
        return_item = Spree::ReturnToSupplierItem.find_by_id params[:item][:id]
        return_item.update_attributes(quantity: params[:item][:quantity], 
          cost_price: params[:item][:cost_price].to_d, reason: params[:item][:reason])
        flash[:notice] = "Item has been succesfully updated."
        redirect_to request.referrer
      end

      def confirm
        @return_to_supplier.confirm!
        flash[:notice] = "Return to Supplier has been succesfully confirmed."
        redirect_to request.referrer
      end

      def delete_item
        item = Spree::ReturnToSupplierItem.find_by_id(params[:item_id])
        if item.destroy
          flash[:notice] = "Item has been succesfully deleted."
        else
          flash[:alert] = "Delete item failed. Please try again."
        end
        redirect_to request.referrer
      end

      def credit_to_supplier
        supplier_deposit = Journal::Account.cash_and_bank.where("journal_accounts.name like ?", @return_to_supplier.supplier.name).first
        if supplier_deposit.present?
          Spree::ReturnToSupplierItem.transaction do
            total = 0
            total_selisih = 0
            @return_to_supplier.items.each do |item|
              if @return_to_supplier.currency == "RMB"
                total += (item.cost_price * item.variant.real_kurs * item.quantity)
              else
                total += (item.cost_price * item.quantity)
              end

              if item.cost_price != item.variant.cost_price
                if @return_to_supplier.currency == "RMB"
                  total_selisih += ((item.variant.cost_price - item.cost_price) * item.variant.real_kurs * item.quantity)
                else
                  total_selisih += ((item.variant.cost_price - item.cost_price) * item.quantity)
                end
              end
            end

            supplier_deposit_idr = Journal::Account.find_by_code("1-10003")
            inventory_idr = Journal::Account.find_by_code("1-10203")
            inventory_rmb = Journal::Account.find_by_code("1-10204")

            if @return_to_supplier.currency == "RMB"
              # journal RMB
              journal_entry_rmb_rmb = Journal::JournalEntry.create(description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", date: DateTime.now)
              journal_debit = Journal::JournalItem.create(account_id: supplier_deposit.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", debit: total, currency: supplier_deposit.currency, journal_entry_id: journal_entry_rmb_rmb.id)
              journal_credit = Journal::JournalItem.create(account_id: inventory_rmb.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", credit: total, currency: inventory_rmb.currency, journal_entry_id: journal_entry_rmb_rmb.id)

               # journal IDR
              journal_entry_rmb_idr = Journal::JournalEntry.create(description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", date: DateTime.now)
              journal_debit = Journal::JournalItem.create(account_id: supplier_deposit_idr.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", debit: total, currency: supplier_deposit_idr.currency, journal_entry_id: journal_entry_rmb_idr.id)
              journal_credit = Journal::JournalItem.create(account_id: inventory_idr.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", credit: total, currency: inventory_idr.currency, journal_entry_id: journal_entry_rmb_idr.id)
              
              # expense selisih
              if total_selisih > 0
                goods_reject = Journal::Account.find_by_code("6-60352")
                inventory = Journal::Account.find_by_code("1-10203")
                journal_entry_selisih = Journal::JournalEntry.create(description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", date: DateTime.now)
                journal_debit = Journal::JournalItem.create(account_id: goods_reject.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", debit: total_selisih, currency: goods_reject.currency, journal_entry_id: journal_entry_selisih.id)
                journal_credit = Journal::JournalItem.create(account_id: inventory.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", credit: total_selisih, currency: inventory.currency, journal_entry_id: journal_entry_selisih.id)
              end

            else
              # journal IDR
              journal_entry_idr = Journal::JournalEntry.create(description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", date: DateTime.now)
              journal_debit = Journal::JournalItem.create(account_id: supplier_deposit.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", debit: total, currency: supplier_deposit.currency, journal_entry_id: journal_entry_idr.id)
              journal_credit = Journal::JournalItem.create(account_id: inventory_idr.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", credit: total, currency: inventory_idr.currency, journal_entry_id: journal_entry_idr.id)
              
              # expense selisih
              if total_selisih > 0
                goods_reject = Journal::Account.find_by_code("6-60352")
                inventory = Journal::Account.find_by_code("1-10203")
                journal_entry_idr_selisih = Journal::JournalEntry.create(description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", date: DateTime.now)
                journal_debit = Journal::JournalItem.create(account_id: goods_reject.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", debit: total_selisih, currency: goods_reject.currency, journal_entry_id: journal_entry_idr_selisih.id)
                journal_credit = Journal::JournalItem.create(account_id: inventory.id, description: "Credit return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", credit: total_selisih, currency: inventory.currency, journal_entry_id: journal_entry_idr_selisih.id)
              end
            end
            
            @return_to_supplier.complete!
            flash[:notice] = "Return to Supplier has been succesfully completed."
            redirect_to request.referrer
          end
        else
          flash[:alert] = "Supplier Deposit #{@return_to_supplier.supplier.name} not found"
          redirect_to request.referrer
        end
      end

      def ship
        @return_to_supplier.update_attributes(return_at: Date.today)
        flash[:notice] = "Return to Supplier has been succesfully updated."
        redirect_to request.referrer
      end

      def treat_as_expense
        Spree::ReturnToSupplierItem.transaction do
          total = 0
          @return_to_supplier.items.each do |item|
            if @return_to_supplier.currency == "RMB"
              total += (item.cost_price * item.variant.real_kurs * item.quantity)
            else
              total += (item.cost_price * item.quantity)
            end

            if item.cost_price != item.variant.cost_price
              if @return_to_supplier.currency == "RMB"
                total += ((item.variant.cost_price - item.cost_price) * item.variant.real_kurs * item.quantity)
              else
                total += ((item.variant.cost_price - item.cost_price) * item.quantity)
              end
            end
          end

          # journal treat as expense
          goods_reject = Journal::Account.find_by_code("6-60352")
          inventory = Journal::Account.find_by_code("1-10203")
          journal_entry_treat_expense = Journal::JournalEntry.create(description: "Expense return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", date: DateTime.now)
          journal_debit = Journal::JournalItem.create(account_id: goods_reject.id, description: "Expense return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", debit: total, currency: goods_reject.currency, journal_entry_id: journal_entry_treat_expense.id)
          journal_credit = Journal::JournalItem.create(account_id: inventory.id, description: "Expense return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", credit: total, currency: inventory.currency, journal_entry_id: journal_entry_treat_expense.id)

          @return_to_supplier.complete!
          flash[:notice] = "Return to Supplier has been succesfully completed."
          redirect_to request.referrer
        end
      end

      def create_invoice
        Spree::ReturnToSupplierItem.transaction do
          total = 0
          total_selisih = 0
          @return_to_supplier.items.each do |item|
            if @return_to_supplier.currency == "RMB"
              total += (item.cost_price * item.variant.real_kurs * item.quantity)
            else
              total += (item.cost_price * item.quantity)
            end

            if item.cost_price != item.variant.cost_price
              if @return_to_supplier.currency == "RMB"
                total_selisih += ((item.variant.cost_price - item.cost_price) * item.variant.real_kurs * item.quantity)
              else
                total_selisih += ((item.variant.cost_price - item.cost_price) * item.quantity)
              end
            end
          end
          invoice = Journal::Invoice.new(vendor: "Return Supplier #{@return_to_supplier.supplier.name}", currency: @return_to_supplier.currency, due_date: DateTime.now, total: @return_to_supplier.total, payment_state: "pending", spree_user_id: @return_to_supplier.supplier_id, originator_type: "Spree::ReturnToSupplier")
          inventory = Journal::Account.find_by_code("1-10203").id
          invoice_item = invoice.items.new(category: inventory, amount: @return_to_supplier.total, description: "Return Supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", invoice_id: invoice.id)
          
          # expense selisih
          if total_selisih > 0
            goods_reject = Journal::Account.find_by_code("6-60352")
            inventory = Journal::Account.find_by_code("1-10203")
            journal_entry_expense_sel = Journal::JournalEntry.create(description: "Invoice return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", date: DateTime.now)
            journal_debit = Journal::JournalItem.create(account_id: goods_reject.id, description: "Invoice return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", debit: total_selisih, currency: goods_reject.currency, journal_entry_id: journal_entry_expense_sel.id)
            journal_credit = Journal::JournalItem.create(account_id: inventory.id, description: "Invoice return to supplier #{@return_to_supplier.supplier.name} ID #{@return_to_supplier.id}", credit: total_selisih, currency: inventory.currency, journal_entry_id: journal_entry_expense_sel)
          end
          if invoice.save && invoice_item.save
            @return_to_supplier.complete!
            flash[:notice] = "Invoice has been successfully created and return to Supplier has been succesfully completed."
            redirect_to request.referrer
          else
            flash[:alert] = "Sorry something went wrong. Please try again!"
            redirect_to request.referrer
          end
        end
      end

      private

      def location_after_save
        items_admin_return_to_supplier_path(@return_to_supplier)
      end
    end
  end
end
