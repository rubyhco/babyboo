module Spree
  module Admin
    class NewsController < ResourceController
    	def index
    		@news = Spree::News.all
    	end
    end
  end
end