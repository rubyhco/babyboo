module Spree
  module Admin
    class ContainersController < ResourceController
      def index
        @containers = @containers.filter(params.slice(:sku_contain, :id_is, :name_user)).order(created_at: :desc).page params[:page]  
      end

      def show
        @purchase_order_lines = @container.items.filter(params.slice(:sku_contain))
      end

      def create
        invoke_callbacks(:create, :before)
        @object.attributes = permitted_resource_params
        if @object.save
          invoke_callbacks(:create, :after)
          flash[:success] = flash_message_for(@object, :successfully_created)
          respond_with(@object) do |format|
            format.html { redirect_to new_item_admin_container_path(@object) }
            format.js   { render layout: false }
          end
        else
          invoke_callbacks(:create, :fails)
          respond_with(@object) do |format|
            format.html do
              @suppliers = Spree::Supplier.all
              flash.now[:error] = @object.errors.full_messages.join(", ")
              render_after_create_error
            end
            format.js { render layout: false }
          end
        end
      end

      def new_item
        @search = Spree::PurchaseLineItem.joins(:purchase_order).where("spree_purchase_orders.supplier_id = ? AND 
          spree_purchase_orders.payment_state != ? AND spree_purchase_orders.payment_state != ? AND
          spree_purchase_line_items.shipment_status != ?", @container.supplier_id, "pending", "cart", "completed").ransack(params[:q]) 
        @purchase_order_lines = @search.result.page(params[:page]).per(params[:per_page])
      end

      def update_items
        params[:container_items].each do |x, y|
          next if y["pack"].blank? && y["qty_send"].blank?
          item = Spree::ContainerItem.create!(variant_id: x, qty_send: y[:qty_send], pack: y[:pack], 
            container_id: params[:id], purchase_line_item_id: y[:purchase_line_item_id])
          item.ship!
        end
        flash[:success] = "Berhasil memasukan produk ke kontainer"
        redirect_to request.referrer
      end

      def receive_items
        success = Spree::Container.transaction do 
          params[:container_items].each do |x,y|
            next if y["qty_received"].blank? && y["weight"].blank?
            item = Spree::ContainerItem.shipped.find_by_id x
            next if item.nil?
            item.update_attributes(qty_received: y["qty_received"], weight: y["weight"])
            item.receive!
          end
          @container.receive! if @container.pending?
        end
        if success
          flash[:success] = "Berhasil memperbarui kontainer"
        else
          flash[:alert] = "Gagal memperbarui kontainer"
        end
        redirect_to admin_container_path(@object)
      end

      def selisih_barang
        @search = Spree::ContainerItem.ransack(params[:q]) 
        @items = @search.result.received.where("total_items != qty_received").order(:created_at).page params[:page]
      end

      private

      def whodunnit
        spree_current_user
      end
    end
  end
end
