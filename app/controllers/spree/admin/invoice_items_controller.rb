module Spree
  module Admin
    class InvoiceItemsController < ResourceController
      def change_address
        @object.update_attributes(invoice_item_params)
        redirect_to request.referrer
      end

      def add_adjustment
        @object.update_attributes(invoice_item_adjustment_params)
        redirect_to request.referrer
      end

      private

      def invoice_item_params
        params.require(:invoice_item).permit(:ship_address_id)
      end

      def invoice_item_adjustment_params
        params.require(:invoice_item).permit(:adjustment_total)
      end
    end
  end
end
