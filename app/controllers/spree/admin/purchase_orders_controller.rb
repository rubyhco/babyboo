module Spree
  module Admin
    class PurchaseOrdersController < ResourceController

      def all
        @search = Spree::PurchaseLineItem.ransack(params[:q]) 
        @purchase_order_lines = @search.result.joins(:purchase_order).where("spree_purchase_orders.payment_state != ?", "cart").page(params[:page]).per(params[:per_page])
      end

      def index
        @purchase_orders = Spree::PurchaseOrder.filter(params.slice(:start_date, :end_date, :supplier_id_is, :shipment_is, :payment_is, :sku_contain, :id_is)).where("spree_purchase_orders.payment_state != ?", "cart").order("created_at DESC").page params[:page]
      end

      def new
        @suppliers = Spree::Supplier.all
        invoke_callbacks(:new_action, :before)
        respond_with(@object) do |format|
          format.html { render layout: !request.xhr? }
          if request.xhr?
            format.js   { render layout: false }
          end
        end
      end

      def cart
        @items = Spree::PurchaseLineItem.where("purchase_order_id = ? AND quantity > ?", @purchase_order.id, 0).includes(:variant).order("spree_variants.sku ASC")
      end

      def create
        invoke_callbacks(:create, :before)
        @object.attributes = permitted_resource_params
        if @object.save
          invoke_callbacks(:create, :after)
          # flash[:success] = flash_message_for(@object, :successfully_created)
          respond_with(@object) do |format|
            format.html { redirect_to items_admin_purchase_order_path(@object) }
            format.js   { render layout: false }
          end
        else
          invoke_callbacks(:create, :fails)
          respond_with(@object) do |format|
            format.html do
              @suppliers = Spree::Supplier.all
              flash.now[:error] = @object.errors.full_messages.join(", ")
              render_after_create_error
            end
            format.js { render layout: false }
          end
        end
      end

      def items
        @user = spree_current_user
        @suppliers = Spree::Supplier.all
        @items = @object.items.includes(:variant).filter(params.slice(:name_contain, :sku_contain, :zero_cost_price)).order("spree_variants.sku ASC")
        @items_purchase = @object.items.filter(params.slice(:name_contain, :sku_contain, :zero_cost_price))
        @products = @object.supplier.products.all
        @variants = Spree::Variant.where(product_id: @products.pluck(:id)).where.not(id: @items_purchase.pluck(:variant_id))
      end

      def confirm
        @object.confirm!
        flash[:success] = "Purchase Order has been confirmed."
        redirect_to cart_admin_purchase_order_path(@object)
      end

      def update_items
        unless @object.confirmed?
          params[:item_quantity].each do |key, value|
            next if value.to_i <= 0
            line = Spree::PurchaseLineItem.find_by_id key
            if line
              line.quantity = value
              line.save
            end
          end
        end
        params[:cost_price].each do |key, value|
          next if value.to_i <= 0
          line = Spree::PurchaseLineItem.find_by_id key
          if line
            line.cost_price = value
            line.save
            variant = line.variant
            if variant
              variant.cost_price = value
              variant.save
            end
          end
        end
        flash[:success] = "Item has been updated"
        @object.pending! if @object.cart?
        @object.recalculate if @object.confirmed?
        redirect_to items_admin_purchase_order_path(@object)
      end

      def po_pending
        @purchase_orders = Spree::PurchaseOrder.shipment_pending.order(:created_at).page params[:page]
      end

      def add_item
        variant = Spree::Variant.find_by_id(params[:add_item][:variant_id])
        create_item = @object.create_item(variant)
        if create_item
          flash[:success] = "Items Purchase Order was successfully created."
          redirect_to request.referrer
        else
          flash[:danger] = "Add Items Purchase Order failed."
          redirect_to request.referrer
        end
      end

      def line_item_destroy
        item = Spree::PurchaseLineItem.find_by_id(params[:item])
        po = Spree::PurchaseOrder.find_by_id(params[:po])
        if item.destroy
          po.recalculate
          flash[:success] = "Item was successfully deleted."
          redirect_to request.referrer
        else
          flash[:danger] = "Delete Purchase Item failed"
          redirect_to request.referrer
        end
      end

      def print
        @purchase_order = @object
        @items = Spree::PurchaseLineItem.where("purchase_order_id = ? AND quantity > ?", @purchase_order.id, 0).includes(:variant).order("spree_variants.sku ASC")
      end

      def mark_completed
        line_item = Spree::PurchaseLineItem.find_by_id(params[:line_item_id])
        if line_item.mark_completed(whodunnit)
          flash[:success] = "Items Purchase Order was successfully completed."
          redirect_to request.referrer
        else
          flash[:danger] = "Sorry something went wrong"
          redirect_to request.referrer
        end
      end

      def reverse_to_pending
        if @object.reverse_po_to_pending
          flash[:success] = "Successfully reverse status to pending"
          redirect_to request.referrer
        else
          flash[:danger] = "Sorry something went wrong"
          redirect_to request.referrer
        end
      end

      def update_after_change_item
        if @object.items.collect{|x| x.update_after_change_item}
          flash[:success] = "Successfully update total items"
          redirect_to request.referrer
        else
          flash[:danger] = "Sorry something went wrong"
          redirect_to request.referrer
        end
      end

      private

      def items_params
        params.require(:purchase_order).permit(items_attributes: [:quantity])
      end

      def whodunnit
        spree_current_user
      end

    end
  end
end
