module Spree
  module Admin
    class TestimonialsController < ResourceController
        before_action :set_testimonial, only: [:show, :edit, :update, :destroy, :publish]
        
        def index
          @testimonials = Spree::Testimonial.all.order(created_at: :desc).page params[:page]
        end

        def publish
          if @testimonial.publish == true 
            @testimonial.publish = false
          else
            @testimonial.publish = true
          end
          if @testimonial.save
            flash[:success] = "Testimonial was successfully updated."
            redirect_to request.referrer
          else
            flash[:danger] = "Update Testimonial failed."
            redirect_to request.referrer
          end
        end

        def destroy
          if @testimonial.destroy!
            flash[:success] = "Testimonial was successfully deleted."
            redirect_to request.referrer
          else
            flash[:danger] = "Delete Testimonial failed."
            redirect_to request.referrer
          end
        end

        private

        def set_testimonial
          @testimonial = Spree::Testimonial.find(params[:id])
        end
    end
  end
end