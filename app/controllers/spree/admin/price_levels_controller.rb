module Spree
  module Admin
    class PriceLevelsController < ResourceController
      before_action :set_price_level

      def index
        
      end

      def update
        set_rental_price if params[:price_level][:is_sale_season]
        if @price_level.update_attributes(price_level_params)
          flash[:success] = "Successfully updated the price level"
          redirect_to admin_price_levels_path
        else
          render :edit
        end
      end

      private

      def price_level_params
        params.require(:price_level).permit(:silver_level, :gold_level, :silver_member, :gold_member, :is_sale_season)
      end

      def set_price_level
        @price_level = Spree::PriceLevel.last
      end

      def set_rental_price
        taxon_ids = params[:price_level][:category_ids].reject { |c| c.empty? }
        taxon_ids.each do |taxon_id|
          Spree::Product.joins(:taxons).where("spree_taxons.id = ?", taxon_id).each do |product|
            product.update_attributes(sale_price: product.gold_price)
          end
        end
      end
    end
  end
end
