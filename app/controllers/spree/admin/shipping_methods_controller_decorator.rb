Spree::Admin::ShippingMethodsController.class_eval do
  
  def download_rates
    sm = Spree::ShippingMethod.find(params[:id])
    if sm.delay.export_rates_by_calc(try_spree_current_user.email, sm.calculator.type)
      flash[:notice] = "Thanks, your export has been added to the queue for processing. You will receive an email confirming the export shipping method rates once it has completed."
    else
      flash[:error] = "Error export data."
    end
    redirect_to admin_orders_path
  end
end