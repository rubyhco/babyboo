Spree::Admin::ProductsController.class_eval do
  
  def index
    session[:return_to] = request.url
    @all_product = @search.result
    respond_with(@collection) do |format|
      format.html
      format.csv { send_data @all_product.download_csv }
    end
  end
  
  def view_buyer
    variant = @product.master.id
    @inventory_units = Spree::InventoryUnit.joins(:order).where("spree_orders.completed_at IS NOT NULL").where("spree_inventory_units.variant_id = ?", variant).page params[:page]
    @total_pack_coh = Spree::Variant.find_by_id(variant).stock_items.first.count_on_hand.abs/@product.total_items
  end

  def hide_show_product
    if @product.available_on.nil?
      @product.update_columns(available_on: Time.now)
      notice = "Product berhasil ditampilkan"
    else
      @product.update_columns(available_on: nil)
      notice = "Product berhasil disembunyikan"
    end
    redirect_to request.referrer, notice: notice
  end

  private

  def collection
    return @collection if @collection.present?
    params[:q] ||= {}
    params[:q][:deleted_at_null] ||= "1"
    params[:q][:available_on_null] ||= "1"

    params[:q][:s] ||= "created_at DESC"
    @collection = super
    @collection = @collection.with_deleted if params[:q].delete(:deleted_at_null) == '0'
    @collection = @collection.out_of_stock if params[:q][:stock_items_count_on_hand_eq] == '0'
    @collection = @collection.preorder_pending if params[:q][:stock_items_count_on_hand_lt] == '0'
    @collection = @collection.product_types_for(params[:q][:is_preorder_true]) if params[:q][:is_preorder_true].present?
    @collection = @collection.hidden_product if params[:q][:available_on_null] == "0"
    # @search needs to be defined as this is passed to search_form_for
    @search = @collection.ransack(params[:q])
    @collection = @search.result.
          distinct_by_product_ids(params[:q][:s]).
          includes(product_includes).
          page(params[:page]).
          per(30)

    @collection
  end
end