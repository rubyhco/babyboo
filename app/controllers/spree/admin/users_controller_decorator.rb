Spree::Admin::UsersController.class_eval do
	def index
		@all_user = Spree.user_class.ransack(params[:q]).result
	  respond_with(@collection) do |format|
	    format.html
	    format.csv { send_data @all_user.download_csv }
	  end
	end
end