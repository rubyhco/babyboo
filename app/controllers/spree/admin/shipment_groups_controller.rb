module Spree
  module Admin
    class ShipmentGroupsController < ResourceController
      def index
        @shipment_groups = @shipment_groups.order(created_at: :desc).filter(params.slice(:status_contain, :ekspedisi_is, :actual_cost, :name_contain, :ship_id_is)).page params[:page]
      end

      def update
        invoke_callbacks(:update, :before)
        if @object.update_attributes(permitted_resource_params)
          invoke_callbacks(:update, :after)
          respond_with(@object) do |format|
            format.html do
              flash[:success] = flash_message_for(@object, :successfully_updated)
              redirect_to request.referrer
            end
            format.js { render layout: false }
          end
        else
          invoke_callbacks(:update, :fails)
          respond_with(@object) do |format|
            format.html do
              flash.now[:error] = @object.errors.full_messages.join(", ")
              render_after_update_error
            end
            format.js { render layout: false }
          end
        end
      end

      def show
        @shipments = @shipment_group.shipments
        @shipment = @shipments.first
      end

      def print
        @shipments = @shipment_group.shipments
        @shipment = @shipments.first
        @hide_prices = @shipment.dropship?
        @ship_address = @shipment.get_shipping_address
        if @hide_prices
          order_dropship = @shipment.order.nil? ? @shipment.invoice.items.first.order : @shipment.order
          @dropship_name = order_dropship.dropship_name
          @dropship_phone = order_dropship.dropship_phone
        end
      end

      def update_inventory_unit
        inventory = Spree::InventoryUnit.find_by_id params[:inventory_unit_id]
        inventory.shipped = params[:shipped_stock]
        inventory.save!
        redirect_to request.referrer
      end

      def packed
        @shipment_group.packed!
        flash[:notice] = "Successfully pack this shipment"
        redirect_to request.referrer
      end

      def ship
        @shipment_group.ship!
        flash[:notice] = "Successfully shipped this shipment"
        redirect_to request.referrer
      end

      def ready
        @shipment_group.ready_to_pack!
        redirect_to request.referrer
      end

      def processing
        @shipment_group.processing!
        redirect_to request.referrer
      end

      def generate_shipment_group
        ShipmentGroup.generate_shipment_group
        flash[:notice] = "Successfully generate shipment group"
        if !request.referrer.nil?
          redirect_to request.referrer
        else
          redirect_to admin_shipment_groups_path
        end
      end
    end
  end
end