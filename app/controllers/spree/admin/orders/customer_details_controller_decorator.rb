Spree::Admin::Orders::CustomerDetailsController.class_eval do
 
  def update
    if @order.contents.update_cart(order_params)

      if should_associate_user?
        requested_user = Spree.user_class.find(params[:user_id])
        @order.associate_user!(requested_user, @order.email.blank?)
      end
      @order.update_attributes(dropship_params) unless dropship_params.blank? || dropship_params.nil?
      unless @order.completed?
        @order.next
        @order.refresh_shipment_rates
      end

      flash[:success] = Spree.t('customer_details_updated')
      redirect_to edit_admin_order_url(@order)
    else
      render action: :edit
    end
       
  end   

  private

  def order_params
    params.require(:order).permit(
      :email,
      :use_billing,
      bill_address_attributes: permitted_address_attributes,
      ship_address_attributes: permitted_address_attributes
    )
  end
  def dropship_params
    params.permit(:dropship, :dropship_name, :dropship_phone, :notes)
  end
end