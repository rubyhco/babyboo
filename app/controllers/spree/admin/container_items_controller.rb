module Spree
  module Admin
    class ContainerItemsController < ResourceController
      def ready
        product = @object.variant.product
        if product.update_attributes(po_status: "READY") && @object.update_stock_item
          redirect_to request.referrer, success: "Product is now ready to ship!"
        else
          redirect_to request.referrer, alert: "Sorry, unable to update product po status"
        end
      end

      def complete
        if @object.shortage? && @object.purchase_line_item.journal_bill_items.present? 
          if !@object.purchase_line_item.journal_bill_items.last.bill.confirmed?
            flash[:danger] = "Item telah ada di draft purchase invoice, hapus item terlebih dahulu dan ulangi proses mark as complete."
          else
            if @object.manual_completed(whodunnit)
              flash[:success] = "Items Completed!"
            else
              flash[:danger] = "Failed mark as complete!"
            end
          end
        else
          if @object.manual_completed(whodunnit)
            flash[:success] = "Items Completed!"
          else
            flash[:danger] = "Failed mark as complete!"
          end
        end
        redirect_to request.referrer
      end

      def refund
        @object.add_my_credit(whodunnit)
        flash[:success] = "Berhasil menambah kembali deposit di Supplier"
        redirect_to request.referrer
      end

      def treat_as_expense
        @object.treat_as_expense(whodunnit)
        flash[:success] = "Berhasil menandakan sebagai Expense"
        redirect_to request.referrer
      end

      def generate_po
        @object.generate_purchase_order
        flash[:success] = "Berhasil membuat Purchase Order"
        redirect_to request.referrer
      end

      def reverse
        @object.reverse_to_shipped
        flash[:success] = "Berhasil mengembalikan status item menjadi shipped."
        redirect_to request.referrer
      end

      def delete_item
        if @object.delete_item
          flash[:success] = "Berhasil menghapus item"
        else
          flash[:danger] = "Gagal menghapus item"
        end
        redirect_to request.referrer
      end

      private

      def whodunnit
        spree_current_user
      end
    end
  end
end
