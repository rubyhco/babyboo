module Spree
  module Admin
    class InvoicesController < ResourceController
      before_action :load_order_items, only: [:new, :create, :items]

      def index
        @invoices = @invoices.filter(params.slice(:name_contain, :payment_is, :id_is, :lock_is)).order(created_at: :desc).page params[:page]  
      end

      def new
        invoke_callbacks(:new_action, :before)
        respond_with(@object) do |format|
          format.html { render layout: !request.xhr? }
          if request.xhr?
            format.js   { render layout: false }
          end
        end
      end

      def destroy
        Spree::Invoice.transaction do
          Spree::InvoiceItem.where(invoice_id: @object.id).delete_all
          Spree::Shipment.where(invoice_id: @object.id).delete_all
          if @object.delete
            invoke_callbacks(:destroy, :after)
            flash[:success] = flash_message_for(@object, :successfully_removed)
            respond_with(@object) do |format|
              format.html { redirect_to location_after_destroy }
              format.js   { render partial: "spree/admin/shared/destroy" }
            end
          else
            invoke_callbacks(:destroy, :fails)
            respond_with(@object) do |format|
              format.html { redirect_to location_after_destroy }
            end
          end
        end
      end

      def create
        invoke_callbacks(:create, :before)
        @object.attributes = permitted_resource_params
        @object.created_by_id = spree_current_user.id
        if @object.save
          invoke_callbacks(:create, :after)
          flash[:success] = flash_message_for(@object, :successfully_created)
          respond_with(@object) do |format|
            format.html { redirect_to items_admin_invoice_path(@object) }
            format.js   { render layout: false }
          end
        else
          invoke_callbacks(:create, :fails)
          respond_with(@object) do |format|
            format.html do
              flash.now[:error] = @object.errors.full_messages.join(", ")
              render_after_create_error
            end
            format.js { render layout: false }
          end
        end
      end

      def items
        @order_items = @order_items.where("spree_orders.user_id = ?", @object.user_id)
      end

      def select_items
        params[:select_items_2].each do |key, value|
          next if !params[:select_items].include?(key)
          @object.items.create(inventory_unit_id: key, quantity: value)
        end
        redirect_to confirm_admin_invoice_path(@object)
      end

      def confirm
        if !@object.pending_payment?
          flash[:error] = "Tidak diperkenankan mengubah pesanan yang telah terbayar"
          redirect_to admin_invoice_path(@object) 
        end

        @invoice_items = @object.items
        @shipping_addresses = @invoice_items.collect{|x|x.ship_address_id}
        @shipping_addresses = @shipping_addresses.uniq
        if !@invoice_items.empty?
          @user_address = Spree::UserAddress.where(user_id: @invoice_items.first.invoice.user_id)
        else
          flash[:error] = "Sorry, item tidak tersedia. Mohon ulang / dihapus kembali."
          redirect_to request.referrer
        end
      end

      def new_address
        invoice_item = Spree::InvoiceItem.find_by_id params[:invoice_id]
        user = invoice_item.invoice.user
        if address = user.addresses.create!(address_params)
          invoice_item.update_attributes(ship_address_id: address.id)
          flash[:success] = "Successfully create & change shipping address"
          redirect_to request.referrer
        else
          flash[:error] = "Sorry, something is not right. Please try again."
          redirect_to request.referrer
        end
      end

      def send_invoice
        if @object.create_shipment(params[:shipment][:courier])
          flash[:success] = "Successfully send invoice"
          redirect_to admin_invoice_path(@object)
        else
          flash[:error] = @object.errors.full_messages
          redirect_to request.referrer
        end
      end

      def flag_dropship
        invoice_dropship = @object.update_attributes(dropship: params[:dropship][:dropship], dropship_name: params[:dropship][:dropship_name], dropship_phone: params[:dropship][:dropship_phone])
        if invoice_dropship
          flash[:success] = "Successfully save dropship to invoice"
          redirect_to admin_invoice_path(@object)
        else
          flash[:error] = @object.errors.full_messages
          redirect_to request.referrer
        end
      end

      def dp_angus
        invoice = Spree::Invoice.find_by_id params[:id]
        if invoice.cancel!(whodunnit)
          flash[:success] = "Pesanan telah dibatalkan - DP Hangus"
        else
          flash[:error] = "Something went wrong! Please try again"
        end
        redirect_to request.referrer
      end

      def add_note
        invoice = Spree::Invoice.find_by_id params[:id]
        notes = params[:update_note][:note]
        invoice.update_attributes(note: notes)
        redirect_to request.referrer, notice: 'Notes was successfully saved.'
      end

      def show
        @invoice_items = @object.items
        @shipping_addresses = @invoice_items.collect{|x|x.ship_address_id}
        @shipping_addresses = @shipping_addresses.uniq
      end

      def reminder_email
        @invoice = Spree::Invoice.find_by_id params[:id]
        if @invoice.send_email_shipment_ready
          @invoice.reminder_sms
          flash[:success] = "Email & SMS sent"
        else
          flash[:error] = "Something went wrong! Please try again"
        end
        redirect_to request.referrer
      end

      def completed
        
      end

      def generate_invoice
        Spree::Invoice.generate_invoice
        flash[:notice] = "Successfully generate invoice"
        # Rake::Task['brandedbabys:generate_shipment_group'].invoke
        redirect_to request.referrer
      end

      def lock_unlock
        invoice = Spree::Invoice.find_by_id(params[:id])
        if invoice.process_lock!
          notice =  invoice.lock? ? "Successfully lock invoice." : "Successfully unlock invoice."
          redirect_to request.referrer, notice: notice
        else
          alert = "Sorry, something went wrong. Please try again!"
          redirect_to request.referrer, alert: alert
        end
      end

      def payment_confirmation_new
        @payment_confirmation = Spree::PaymentConfirmation.new
      end

      def payment_confirmation
        @payment_confirmation = Spree::PaymentConfirmation.new(payment_confirmation_params)
        if @payment_confirmation.save
          @payment_confirmation.contents.create!(invoice_id: Spree::Invoice.find_by_id(params[:id]).id)
          redirect_to admin_invoices_path, notice: "Successfully created payment confirmation."
        else
          redirect_to admin_invoices_path, alert: "Sorry, something went wrong. Please try again!"
        end
      end

      private

      def whodunnit
        try_spree_current_user.try(:email)
      end

      def load_order_items
        @order_items = Spree::InventoryUnit.ready_to_invoice
        @customers = Spree::Order.po_ready.select(:user_id).distinct
      end

      def address_params
        params.require(:address).permit(:firstname, :lastname, :address1, :address2, :country_id, :state_id, :city, 
          :zipcode, :phone, :note, :dropship, :dropship_name, :dropship_phone)
      end

      def payment_confirmation_params
        params.require(:payment_confirmation).permit(:bank_destination, :bank_sender, :sender_name, :amount, :transaction_date,
          :note, :user_id)
      end
    end
  end
end
