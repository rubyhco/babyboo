module Spree
  module Admin
    class CashOutRequestsController < ResourceController
      def cash_out_new
        @user = Spree::User.find(params[:user_id])
        @cash_out_request = Spree::CashOutRequest.new
      end

      def create_cash_out
        @user = Spree::User.find(params[:format])
        @cash_out_request = @user.cash_out_requests.new(cash_out_request_params)
        if @cash_out_request.save
          flash[:success] = "Permintaan Cash Out akan di proses dalam waktu 3x24 jam."
          redirect_to admin_user_store_credits_path(@user)
        else
          redirect_to root_path, alert: "Sorry, something went wrong. Please try again!"
        end
      end

      private

      def cash_out_request_params
        params.require(:cash_out_request).permit(:amount, :bank_name, :bank_account_name, :bank_account_no, :bank_branch)
      end
    end
  end
end
