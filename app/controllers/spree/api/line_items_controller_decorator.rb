Spree::Api::LineItemsController.class_eval do
	def update
	  @line_item = find_line_item
	  if @order.contents.update_cart(line_items_attributes)
	    @line_item.reload
	    respond_with(@line_item, default_template: :show)
	  else
	    invalid_resource!(@line_item)
	  end
	end

	def line_items_attributes
    { line_items_attributes: 
    	{
        id: params[:id],
        quantity: params[:line_item][:quantity],
        options: options_params || {}
    	} 
  	}
  end

  def options_params
  	{
  		price: params[:line_item][:options][:price],
  		qty_per_pack: params[:line_item][:options][:qty_per_pack]
  	}
  end
end