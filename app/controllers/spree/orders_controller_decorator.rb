Spree::OrdersController.class_eval do
  def populate
    @order   = current_order(create_order_if_necessary: true)
    variant  = Spree::Variant.find(params[:variant_id])
    quantity = params[:quantity].to_i

    # 2,147,483,647 is crazy. See issue https://github.com/spree/spree/issues/2695.
    if !quantity.between?(1, 2_147_483_647)
      @order.errors.add(:base, Spree.t(:please_enter_reasonable_quantity))
    end

    begin
      @line_item = @order.contents.add(variant, quantity)
    rescue ActiveRecord::RecordInvalid => e
      @order.errors.add(:base, e.record.errors.full_messages.join(", "))
    end

    respond_with(@order) do |format|
      format.html do
        if @order.errors.any?
          flash[:error] = @order.errors.full_messages.join(", ")
          redirect_to request.referrer
          return
        else
          redirect_to request.referrer
        end
      end
    end
  end

  def print
    @order = Spree::Order.find_by_number params[:id]
    @shipment = @order.shipments.first
    # @hide_prices = @shipment.dropship?
    @ship_address = @shipment.get_shipping_address
    render layout: false
    # if @hide_prices
    #   order_dropship = @shipment.order.nil? ? @shipment.invoice.items.first.order : @shipment.order
    #   @dropship_name = order_dropship.dropship_name
    #   @dropship_phone = order_dropship.dropship_phone
    # end
  end

  private

  def assign_order
    @order = current_order
    unless @order
      flash[:error] = Spree.t(:order_not_found)
      redirect_to(root_path) && return
    end
    
    if @order.preorder? && @order.number.first == "R" && !@order.completed?
      @order.generate_order_number(prefix: "PO")
      @order.save
    else !@order.preorder? && @order.number.first == "P" && !@order.completed?
      @order.generate_order_number(prefix: "R")
      @order.save
    end
  end
end