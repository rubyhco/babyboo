Spree::HomeController.class_eval do
  def new_address
    if spree_current_user.addresses.create!(address_params)
      redirect_to request.referrer, success: "New address has been successfully created!"
    else
      redirect_to request.referrer, success: "Sorry, something went wrong. Please try again or contact our customer service"
    end
  end

  def addresses
    if spree_user_signed_in?
      @user = spree_current_user
    else
      redirect_to spree_login_path
    end
  end

  def store_credit
    @store_credit = Spree::StoreCredit.find_by_id params[:format]
    @store_credit_events = @store_credit.store_credit_events.chronological
  end

  def invoice
    @invoice = Spree::Invoice.find_by_id(params[:id])
    @invoice_items = @invoice.items
    @shipping_addresses = @invoice_items.collect{|x|x.ship_address_id}
    @shipping_addresses = @shipping_addresses.uniq
    render layout: false
  end

  def show_order
    @order = Spree::Order.find_by_number!(params[:id])
    render layout: false
  end

  def print
    @invoice = Spree::Invoice.find_by_id params[:id]
    @shipment = @invoice.items.first.shipment
    @ship_address = @shipment.get_shipping_address
    render layout: false
  end

  # use store credit for pay invoice po 
  def pay_with_sc_invoice
    invoice = Spree::Invoice.find_by_id(params[:id])
    invoice.add_store_credit_payment_with_value(params[:pay_invoice_po][:amount].to_i)
    flash[:notice] = "Store credit berhasil digunakan untuk pembayaran Invoice #{invoice.id}"
    redirect_to request.referrer
  end

  def use_sc_for_invoice_po
    @invoice = Spree::Invoice.find_by_id params[:id]
    render layout: false
  end

  # use store credit for pay order ready stock / po
  def pay_with_sc_order
    order = Spree::Order.find_by_number(params[:id])
    order.add_store_credit_payments_from_user(params[:pay_order][:amount].to_i)
    flash[:notice] = "Store credit berhasil digunakan untuk pembayaran Order #{order.number}"
    redirect_to request.referrer
  end

  def use_sc_for_order
    @order = Spree::Order.find_by_number params[:id]
    render layout: false
  end

  # use store credit for pay invoice_journal credit 
  def pay_with_sc_credit
    invoice = Journal::Invoice.find_by_id(params[:id])
    invoice.add_payment_store_credit_with_value(params[:pay_credit][:amount].to_i)
    flash[:notice] = "Store credit berhasil digunakan untuk pembayaran Credit #{invoice.id}"
    redirect_to request.referrer
  end

  def use_sc_for_credit
    @invoice = Journal::Invoice.find_by_id params[:id]
    render layout: false
  end

  def ubah_ekspedisi
    invoice = Spree::Invoice.find_by_id(params[:id])
    if invoice.create_shipment(params[:shipment][:courier])
      redirect_to request.referrer, notice: "Successfully Change Shipping Method"
    else
      flash[:error] = invoice.errors.full_messages
      redirect_to request.referrer, notice: "Something Went Wrong"
    end 
  end

  private

  def address_params
    params.require(:address).permit(:firstname, :lastname, :address1, :address2, :zipcode, :state_id, :city, :country_id,
      :phone)
  end
end