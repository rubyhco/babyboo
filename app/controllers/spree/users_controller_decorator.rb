Spree::UsersController.class_eval do
  def show
    if spree_current_user
      @orders = spree_current_user.orders.complete.order('completed_at desc')
    else
      flash[:alert] = "Mohon login terlebih dahulu"
      redirect_to spree_login_path
    end
  end
end