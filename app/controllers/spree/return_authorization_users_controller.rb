class Spree::ReturnAuthorizationUsersController < Spree::StoreController
  
  def shipped_order
    order_id_shipped_iu = Spree::InventoryUnit.shipped.joins(:order).where("spree_orders.user_id = ?", current_user).pluck(:order_id).uniq
    @orders = Spree::Order.where(id: order_id_shipped_iu)
  end
  
  def index
    @rma = Spree::ReturnAuthorization.joins(:order).where("spree_orders.user_id = ?", current_user)
  end

  def new_rma
    order = Spree::Order.find_by_number(params[:id])
    @iu_shipped = Spree::InventoryUnit.where(order_id: order.id).shipped
    @return_authorization = Spree::ReturnItem.new
  end

  def new
    
  end

  def create
    order = Spree::Order.find_by_number(params[:order_number])
    return_authorization = Spree::ReturnAuthorization.create!(order: order, memo: params[:memo], stock_location: Spree::StockLocation.first)
    return_reasons = params[:return_reason]
    return_quantity = params[:quantity_return]
    params[:select_items].each do |return_iu|
      iu = Spree::InventoryUnit.find_by_id(return_iu)
      reimbursed_store_credit = Spree::ReimbursementType.find_by_name("store credit")
      return_reason_select = return_reasons.select {|r|r == return_iu}
      return_reason = Spree::ReturnReason.find_by_id(return_reason_select[return_iu])
      return_qty_select = return_quantity.select {|r|r == return_iu}
      return_authorization.return_items.create(inventory_unit: iu, amount: iu.line_item.price, preferred_reimbursement_type_id: reimbursed_store_credit.id, return_reason: return_reason, quantity: return_qty_select[return_iu])
    end
    redirect_to account_path(spree_current_user), notice: "Return request successfully created."
  end

  private

  def current_user
    spree_current_user
  end
end
