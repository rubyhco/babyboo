Spree::ProductsController.class_eval do
  def new_items
    @searcher = build_searcher(params.merge(include_images: true))
    @products = @searcher.retrieve_products.where(is_new_arrival: true).order('created_at DESC')
  end

  def ready_stock
    @searcher = build_searcher(params.merge(include_images: true))
    @products = @searcher.retrieve_products.where(is_preorder: false).order('created_at DESC')
  end

  def sale
    @searcher = build_searcher(params.merge(include_images: true))
    @products = @searcher.retrieve_products.where("available_on is not null").joins(:prices).where("spree_prices.sale_amount > 0").order('created_at DESC')
  end

  def album
    if params[:type] == "preorder"
      taxonomy = Spree::Taxonomy.find_by_name("Preorder")
    else
      taxonomy = Spree::Taxonomy.find_by_name("Ready Stock")
    end

    if params[:t]
      taxons = taxonomy.taxons.where(name: params[:t])
      if taxons.count > 0 
        @categories = taxons.first.children.order('position ASC').page(params[:page]).per(16)
      else
        flash[:error] = "Sorry, your chosen taxonomy is not found"
        taxon = taxonomy.root
        redirect_to spree.nested_taxons_path(taxon.permalink)
      end
    else
      taxon = taxonomy.root
      redirect_to spree.nested_taxons_path(taxon.permalink)
    end
  end

  def show_album_image
    @taxon = Spree::Taxon.find_by_permalink(params[:id])
    render layout: false
  end

  def show
    @variants = @product.
      variants_including_master.
      display_includes.
      with_prices(current_pricing_options).
      includes([:option_values, :images])

    @product_properties = @product.product_properties.includes(:property)
    @taxon = Spree::Taxon.find(params[:taxon_id]) if params[:taxon_id]
    render layout: false
  end
end