module Spree
  class CashOutRequestsController < Spree::StoreController
    def index
    end

    def new
      @cash_out_request = Spree::CashOutRequest.new
      render layout: false
    end

    def create
      @cash_out_request = spree_current_user.cash_out_requests.new(cash_out_request_params)
      if @cash_out_request.save
        flash[:success] = "Permintaan Anda akan segera kami proses dalam waktu 3x24 jam."
        redirect_to request.referrer
      else
        redirect_to root_path, alert: "Sorry, something went wrong. Please try again!"
      end
    end

    private

    def cash_out_request_params
      params.require(:cash_out_request).permit(:amount, :bank_name, :bank_account_name, :bank_account_no, :bank_branch)
    end
  end
end
