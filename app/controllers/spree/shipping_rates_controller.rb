module Spree
  class ShippingRatesController < Spree::StoreController
    def check_shipping_rate
      shipping_rates = Spree::ShippingRatesPerMethod.where(state_id: params[:provinces2], city_id: params[:city][:state_id]).first
      # price = shipping_rates.price * params[:berat].to_i
      redirect_to show_shipping_rate_shipping_rates_path(shipping_method_id: params[:name_cargo], state_id: params[:provinces2], city_id: params[:city][:state_id], min_weight: params[:berat])
    end

    def show_shipping_rate
      @shipping_rates = Spree::ShippingRatesPerMethod.where(city_id: params[:city_id], state_id: params[:state_id])
      @weight = params[:min_weight].to_d.ceil
      @cargo_shipping_rates = Spree::CargoShippingRatesPerMethod.where(city_id: params[:city_id], state_id: params[:state_id])
      @shipping_details = Spree::ShippingRatesPerMethod.where(city_id: params[:city_id], state_id: params[:state_id])
    end
  end
end
