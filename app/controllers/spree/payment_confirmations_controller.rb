class Spree::PaymentConfirmationsController < Spree::StoreController
  before_action :load_resources
  
  def index
    
  end

  def new
    if current_spree_user
      @payment_confirmation = Spree::PaymentConfirmation.new
      render layout: false
    else
      redirect_to spree_login_path, notice: "Please login to continue"
    end
  end

  def create
    @payment_confirmation = Spree::PaymentConfirmation.new(payment_confirmation_params)
    if @payment_confirmation.save
      create_payment_confirmation_association(@payment_confirmation)
      redirect_to payment_confirmation_path(@payment_confirmation)
    else
      redirect_to root_path, alert: "Sorry, something went wrong. Please try again!"
    end
  end

  def show
    
  end

  private

  def create_payment_confirmation_association(payment_confirmation)
    if !params[:order].nil?
      params[:order].each do |order_id|
        payment_confirmation.contents.create!(order_id: order_id)
      end
    end
    if !params[:invoice].nil?
      params[:invoice].each do |invoice_id|
        payment_confirmation.contents.create!(invoice_id: invoice_id)
      end
    end
  end

  def load_resources
    if current_spree_user
      orders = current_spree_user.orders.payment_state_is("balance_due") 
      @ready_stock_orders = orders.where("preorder != ?", 1)
      @preorders = orders.where(preorder: true)
      @invoices = Spree::Invoice.where(user_id: current_spree_user.id).valid.pending_payment
    end
  end

  def payment_confirmation_params
    params.require(:payment_confirmation).permit(:bank_destination, :bank_sender, :sender_name, :amount, :transaction_date,
      :note, :user_id)
  end
end
