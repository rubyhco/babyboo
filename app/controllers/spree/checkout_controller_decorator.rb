Spree::CheckoutController.class_eval do
  def update
    if Spree::OrderUpdateAttributes.new(@order, update_params, request_env: request.headers.env).apply
      @order.temporary_address = !params[:save_user_address]
      success = if @order.state == 'confirm'
        @order.complete
      else
        @order.update_attributes(dropship_params) unless dropship_params.blank? || dropship_params.nil?
        @order.next
      end

      if !success
        flash[:error] = @order.errors.full_messages.join("\n")
        redirect_to(checkout_state_path(@order.state)) && return
      end

      if @order.completed?
        @current_order = nil
        flash.notice = Spree.t(:order_processed_successfully)
        flash['order_completed'] = true
        redirect_to completion_route
      else
        redirect_to checkout_state_path(@order.state)
      end
    else
      render :edit
    end
  end

  private

  def dropship_params
    params.permit(:dropship, :dropship_name, :dropship_phone, :notes)
  end
end