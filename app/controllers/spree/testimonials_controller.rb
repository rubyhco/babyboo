module Spree
  class TestimonialsController < Spree::StoreController
    def index
      @spree_testimonials = Spree::Testimonial.all
    end
    def show
      @testimonial = Spree::Testimonial.find(params[:id])
    end
    def new
      @spree_testimonial = Spree::Testimonial.new
    end

    def create
      @spree_testimonial = Spree::Testimonial.new(spree_testimonial_params)
      
      if @spree_testimonial.save
        redirect_to request.referrer, notice: 'Testimonial was successfully created.'
      else
        redirect_to request.referrer, alert: 'Failed'
      end
    end

    private
    def spree_testimonial_params
      params.require(:testimonial).permit(:name, :testimonial)
    end
  end
end