Deface::Override.new(
  virtual_path: 'spree/admin/variants/_form',
  name: 'add_cost_currency_on_form_admin_variant',
  insert_after: '[data-hook="cost_price"]',
  text: <<-ERB
    <div class="field" data-hook="cost_currency">
      <%= f.label :cost_currency %>
      <%= f.text_field :cost_currency, :value => @variant.cost_currency, :class => 'fullwidth' %>
    </div>
  ERB
)