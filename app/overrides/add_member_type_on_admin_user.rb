Deface::Override.new(
  virtual_path: 'spree/admin/users/_form',
  name: 'add_member_type_on_admin_user',
  original: 'ffdd7ab1b3ad3fffbfb214275f68f410d6be3f49',
  insert_after: '[data-hook="admin_user_form_roles"]',
  text: <<-ERB
    <div data-hook="admin_user_form_member_type" class="field">
      <%= f.label :member_type, "Member Type" %><br>
      <%= f.select :member_type, ["Default", "Silver", "Gold"], {}, { class: 'select2 fullwidth' } %>
    </div>
  ERB
)

      