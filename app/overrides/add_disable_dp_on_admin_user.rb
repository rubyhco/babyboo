Deface::Override.new(
  virtual_path: 'spree/admin/users/_form',
  name: 'add_disable_dp_on_admin_user',
  insert_after: '[data-hook="admin_user_form_roles"]',
  text: <<-ERB
    <div data-hook="admin_user_form_disable_dp" class="field">
      <%= f.label :disable_dp, "Disable DP" %><br>
      <%= f.check_box :disable_dp, { class: 'select2 fullwidth' } %>
    </div>
  ERB
)

      