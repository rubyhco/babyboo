Deface::Override.new(
  virtual_path: 'spree/admin/products/index',
  name: 'add_filter_for_out_of_stock_on_admin_product',
  original: '9cb911f4f6d7b10dc4d9c67be0e65e05ff39a426',
  insert_after: '[data-hook="admin_products_index_search"]',
  text: <<-ERB
    <div class="row">
      <div class="col-xs-4">
        <div class="product-search-field-stock field checkbox" style="margin-top: -60px;">
          <label>
            <%= f.check_box :stock_items_count_on_hand_eq, {:checked => params[:q][:stock_items_count_on_hand_eq] == '0'}, '0', '1' %>
            <%= "Out of Stock" %>
          </label>
        </div>
      </div>
      <div class="col-xs-4">
        <div class="field checkbox" style="margin-top: -60px;">
          <label>
            <%= f.check_box :stock_items_count_on_hand_lt, {:checked => params[:q][:stock_items_count_on_hand_lt] == '0'}, '0', '1' %>
            <%= "Stock Preorder Pending " %>
          </label>
        </div>
      </div>
    </div>
  ERB
)      