Deface::Override.new(
  virtual_path: 'spree/admin/taxons/_form',
  name: 'add_closed_at_to_admin_taxons_form',
  insert_after: '[data-hook="admin_inside_taxon_form"]',
  text: <<-ERB
    <div class="row">
      <div class="twelve columns alpha omega">
        <%= f.field_container :closed_at do %>
          <%= f.label :closed_at, 'Closed at' %>
          <%= f.text_field :closed_at, :value => datepicker_field_value(@taxon.closed_at), :class => 'datepicker fullwidth' %>
        <% end %>
      </end>
    </div>
    <div class="row">
      <div class="twelve columns alpha omega">
        <%= f.field_container :eta do %>
          <%= f.label :eta, 'ETA' %>
          <%= f.text_field :eta,:class => 'fullwidth' %>
        <% end %>
      </end>
    </div>
  ERB
)