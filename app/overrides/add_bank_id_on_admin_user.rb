Deface::Override.new(
  virtual_path: 'spree/admin/users/_form',
  name: 'add_bank_id_on_admin_user',
  insert_bottom: '[data-hook="admin_user_form_password_fields"]',
  partial: "spree/admin/users/bank"
)

      