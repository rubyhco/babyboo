Deface::Override.new(
  virtual_path: 'spree/admin/users/_form',
  name: 'add_name_phone_prefer_contact_on_admin_user',
  insert_after: '[data-hook="admin_user_form_roles"]',
  text: <<-ERB
    <div data-hook="admin_user_form_name" class="field">
      <%= f.label :name%><br>
      <%= f.text_field :name, { class: 'input form-control' } %>
    </div>
    <div data-hook="admin_user_form_old_total_transaction" class="field">
      <%= f.label :old_transaction_total, "Total Old Transaction"%><br>
      <%= f.text_field :old_transaction_total, { class: 'input form-control' } %>
    </div>
    <div data-hook="admin_user_form_phone" class="field">
      <%= f.label :phone, "Phone" %><br>
      <%= f.text_field :phone, { class: 'input form-control' } %>
    </div>
    <div data-hook="admin_user_form_prefer_contact" class="field">
      <%= f.label :prefer_contact, "Prefer Contact"%><br>
      <%= f.text_field :prefer_contact, { class: 'input form-control' } %>
    </div>
    <div data-hook="admin_user_form_prefer_contact" class="field">
      <%= f.label :birthdate, "Birth Date"%><br>
      <%= f.date_field :birthdate, { class: 'input form-control' } %>
    </div>
    <div data-hook="admin_user_form_howdoyou" class="field">
      <%= f.label :howdoyou, "How Do You Know Us?"%><br>
      <%= f.text_field :howdoyou, { class: 'input form-control' } %>
    </div>
  ERB
)

      