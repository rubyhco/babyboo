Deface::Override.new(
  virtual_path: 'spree/admin/shared/_configuration_menu',
  name: 'add_page_to_admin_store_settings',
  insert_bottom: '[data-hook="admin_configurations_sidebar_menu"]',
  text: <<-ERB
    <% if can?(:edit, Spree::Store) %>
      <%= settings_tab_item plural_resource_name(Spree::Page), admin_pages_path %>
    <% end %>
  ERB
)