Deface::Override.new(
  virtual_path: 'spree/admin/shipping_methods/_form',
  name: 'add_account_on_shipping_method_admin',
  insert_after: '[data-hook="admin_shipping_method_form_internal_name_field"]',
  text: <<-ERB
    <div data-hook="admin_shipping_method_form_account_debit_id" class="col-xs-5">
		  <%= f.field_container :account_id do %>
		    <%= f.label :account_id %><br />
		    <%= f.select :account_id, Journal::Account.all.map { |tc| [tc.name, tc.id] }, {include_blank: true}, class: "select2 fullwidth" %>
		    <%= error_message_on :shipping_method, :account_id %>
		  <% end %>
		</div>
  ERB
)