Deface::Override.new(
  virtual_path: 'spree/admin/products/new',
  name: 'add_new_arrival_flag_on_admin_new_product',
  original: '18891a8751dee517d952d4964ab2ba07cb24b3c4',
  insert_after: '[data-hook="new_product_attrs"]',
  text: <<-ERB
  <div class="clearfix"></div>
    <div data-hook="new_arrival_flag">
      <div class="field">
        <%= f.check_box :is_new_arrival %>
        <%= f.label :is_new_arrival, 'NEW ARRIVAL' %>
      </div>
    </div>
  <div class="clearfix"></div>
  ERB
)