Deface::Override.new(
  virtual_path: 'spree/admin/products/_form',
  name: 'add_sale_price_to_admin_product_form',
  original: '44d9f8df4ec221e3f010c7354524718a176105be',
  insert_after: '[data-hook="admin_product_form_price"]',
  text: <<-ERB
    <%= f.field_container :sale_price do %>
      <%= f.label :sale_price, 'Sale price' %>
      <%= f.text_field :sale_price, :value => number_to_currency(@product.sale_price, :unit => '') %>
      <%= f.error_message_on :sale_price %>
    <% end %>
  ERB
)