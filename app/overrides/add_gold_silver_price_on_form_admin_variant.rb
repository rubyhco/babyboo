Deface::Override.new(
  virtual_path: 'spree/admin/variants/_form',
  name: 'add_gold_silver_price_on_form_admin_variant',
  insert_after: '[data-hook="price"]',
  text: <<-ERB
    <div class="field" data-hook="silver_price">
      <%= f.label :silver_price %>
      <%= f.text_field :silver_price, :value => number_to_currency(@variant.silver_price, :unit => ''), :class => 'fullwidth' %>
    </div>
    <div class="field" data-hook="gold_price">
      <%= f.label :gold_price %>
      <%= f.text_field :gold_price, :value => number_to_currency(@variant.gold_price, :unit => ''), :class => 'fullwidth' %>
    </div>
    <div class="field" data-hook="sale_price">
      <%= f.label :sale_price %>
      <%= f.text_field :sale_price, :value => number_to_currency(@variant.sale_price, :unit => ''), :class => 'fullwidth' %>
    </div>
  ERB
)