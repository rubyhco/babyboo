Spree::OrderMailer.class_eval do
  def preorder_is_ready(invoice)
    @invoice = invoice
    @ship_address = @invoice.items.first.ship_address
    @shipment = @invoice.items.first.shipment
    @total_adjustment = @invoice.items.sum(:adjustment_total)
    subject = "Your preorder product is ready!"

    mail(to: @invoice.user.email, from: "no-reply@brandedbabys.com", subject: subject)
  end

  def payment_confirmed(order, transaction_id)
    @order = order
    @store = @order.store
    @transaction_id = transaction_id
    subject = "Pembayaran ##{order.number} telah diterima"

    mail(to: @order.email, from: from_address(@store), subject: subject)
  end

  def invoice_payment_confirmed(invoice, transaction_id)
    @invoice = invoice
    @transaction_id = transaction_id
    @ship_address = @invoice.items.first.ship_address
    @shipment = @invoice.items.first.shipment
    subject = "Pembayaran Invoice PO ##{@invoice.id} telah diterima"

    mail(to: @invoice.user.email, from: "no-reply@brandedbabys.com", subject: subject)
  end

  def export_results(user, csv, error_message = nil)
    @user = user
    attachments['export_data.csv'] = {mime_type: 'text/csv', content: csv}
    @error_message = error_message
    from=Spree::Config.mails_from
    mail(:to => @user.email, :from => from, :subject => "Export Data #{error_message.nil? ? "Success" : "Failure"}")
  end

  def export_shipping_method(user, csv, error_message = nil)
    @user = user
    attachments['shipping_method.csv'] = {mime_type: 'text/csv', content: csv}
    @error_message = error_message
    from=Spree::Config.mails_from
    mail(:to => @user.email, :from => from, :subject => "Export Shipping Method #{error_message.nil? ? "Success" : "Failure"}")
  end
end