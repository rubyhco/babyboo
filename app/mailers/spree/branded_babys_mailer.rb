module Spree
  class BrandedBabysMailer < BaseMailer
    def add_store_credit_from_no_stock(user, inventory_unit, amount)
      subject = "Store Credit (#{amount}) Added to Your Account"
      @user = user
      @inventory_unit = inventory_unit
      @amount = amount
      mail(to: user.email, from: "no-reply@brandedbabys.com", subject: subject)
    end

    def cash_out_request_paid(bill, user, cash_out_request)
    	subject = "Cash Out Request (ID #{cash_out_request.id}) Already Paid"
    	@bill = bill
    	@user = user
    	@cash_out_request = cash_out_request
    	mail(to: user.email, from: "no-reply@brandedbabys.com", subject: subject)
    end

    def kurang_ongkir(total, user, id_shipment)
      subject = "Kurang Ongkir Shipment (ID #{id_shipment})"
      @total = total
      @user = user
      @id_shipment = id_shipment
      mail(to: user.email, from: "no-reply@brandedbabys.com", subject: subject)
    end

    def journal_invoice(invoice)
      subject = "Credit #{invoice.id} menunggu pembayaran"
      @invoice = invoice
      @user = invoice.user
      mail(to: @user.email, from: "no-reply@brandedbabys.com", subject: subject)
    end
  end
end