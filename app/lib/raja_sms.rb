require 'httparty'

class RajaSms
  include HTTParty
  base_uri '162.211.84.203'

  def send_sms(phone, message)
    if Rails.env.production?
      begin
        self.class.get("http://162.211.84.203/sms/smsmasking.php?username=Len-Len&key=440cafb7eeb13dfa7c681519af7d9871&number=#{phone}&message=#{message}")
        puts "#{phone}: #{message}"
      rescue
      end
    end
  end

  def cek_saldo
    self.class.get("http://162.211.84.203/sms/smssaldo.php?username=Len-Len&key=440cafb7eeb13dfa7c681519af7d9871")
  end
end