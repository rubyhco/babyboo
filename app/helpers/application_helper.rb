module ApplicationHelper
  include ActiveSupport::NumberHelper


  def news
    @news = Spree::News.where(status: 1, for_splash_screen: 0).last
  end

  def splash_screen
    @splash_screen = Spree::News.where("status = ? AND for_splash_screen = ?", true, true).last
  end

  def desc
    @desc = Spree::Store.where(id: 1)
  end

  def sidebar_news
    @news = Spree::News.all
  end

  def show_taxonomies
    @taxonomies = Spree::Taxonomy.includes(root: :children)
  end

  def testimonial
    @spree_testimonial = Spree::Testimonial.new
  end

  def testimonials
    @spree_testimonial = Spree::Testimonial.published
  end

  def banks
  	@banks = Spree::Bank.where(active: 1)
  end

  def selected_address_for(order)
    if order.ship_address_id.nil?
      if spree_current_user.addresses.empty?
        address = nil
      else
        address = spree_current_user.addresses.last
      end
    else
      address = order.ship_address_id
    end

    return address
  end

  def is_bank_transfer?(order)
  	if order.payments.valid.last
  		return true if order.payments.valid.last.payment_method.type == "Spree::PaymentMethod::BankTransfer"
  	end
  end

  def number_to_multi_currency(number, currency)
    if currency == "IDR"
      number_to_currency(number, :unit => "Rp ", :negative_format => "(%u%n)",  :separator => ",", :delimiter => ",")
    else
      number_to_currency(number, :unit => "RMB ", :negative_format => "(%u%n)",  :separator => ",", :delimiter => ",")
    end
  end

  def number_to_rmb(number)
    number_to_currency(number, :unit => "RMB ", :negative_format => "(%u%n)",  :separator => ",", :delimiter => ",")
  end

  def number_to_idr(number)
    number_to_currency(number, :unit => "Rp ", :negative_format => "(%u%n)",  :separator => ",", :delimiter => ",")
  end

  def human_state(state)
    case state
    when "address"
      "Shipping Address"
    when "delivery"
      "Shipping Method"
    when "payment"
      "Payment Method"
    when "confirm"
      "Your Order"
    end
  end
end
