module Spree::Admin::InvoicesHelper
  def total_amount(items, shipment)
    items.sum(:total_amount) + shipment.try(:cost).to_i
  end
end
