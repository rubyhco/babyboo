Spree::Admin::StoreCreditEventsHelper.class_eval do
  self.originator_links = {
    Spree::Payment.to_s => {
      new_tab: true,
      href_type: :payment,
      translation_key: 'admin.store_credits.payment_originator'
    },
    Spree::Refund.to_s => {
      new_tab: true,
      href_type: :payments,
      translation_key: 'admin.store_credits.refund_originator'
    },
    Spree::InvoicePayment.to_s => {
      new_tab: true,
      href_type: :invoice_payment,
      translation_key: 'admin.store_credits.invoice_payment'
    },
    Spree::CashOutRequest.to_s => {
      new_tab: true,
      href_type: :cash_out_request,
      translation_key: 'admin.store_credits.cash_out_request'
    },
    Journal::InvoicePayment.to_s => {
      new_tab: true,
      href_type: :invoice_payment,
      translation_key: 'admin.store_credits.invoice_journal_payment'
    },
    Journal::Transaction.to_s => {
      new_tab: true,
      href_type: :journal_transaction,
      translation_key: 'admin.store_credits.journal_transaction'
    },
    Spree::Order.to_s => {
      new_tab: true,
      href_type: :order,
      translation_key: 'admin.store_credits.order_originator'
    },
    Spree::ShipmentGroup.to_s => {
      new_tab: true,
      href_type: :shipment_group,
      translation_key: 'admin.store_credits.shipment_group'
    },
    Spree::InvoiceItem.to_s => {
      new_tab: true,
      href_type: :invoice_item,
      translation_key: 'admin.store_credits.invoice_item'
    },
    Spree::LineItem.to_s => {
      new_tab: true,
      translation_key: 'admin.store_credits.line_item'
    }
  }
end