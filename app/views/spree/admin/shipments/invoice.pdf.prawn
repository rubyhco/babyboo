if @order_or_invoice.class.name == "Spree::Order"
  @hide_prices = @order_or_invoice.dropship?
end

render :partial => "spree/admin/shared/print"
