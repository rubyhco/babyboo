data = []

if @order_or_invoice.class.name == "Spree::Invoice"
  line_items = Spree::InvoiceItem.where(shipment_id: @shipment.id)
else
  line_items = @order_or_invoice.line_items
end

if @hide_prices
  @column_widths = { 0 => 100, 1 => 165, 2 => 75, 3 => 75 }
  @align = { 0 => :left, 1 => :left, 2 => :right, 3 => :right }
  data << [Spree.t(:sku), Spree.t(:item_description), Spree.t(:options), Spree.t(:qty)]
else
  @column_widths = { 0 => 75, 1 => 100, 2 => 75, 3 => 50, 4 => 75, 5 => 60 }
  @align = { 0 => :left, 1 => :left, 2 => :left, 3 => :right, 4 => :right, 5 => :right}
  data << [Spree.t(:sku), Spree.t(:item_description), "Items per Pack", Spree.t(:price), "Pack Qty", Spree.t(:total)]
end

line_items.each do |item|
  row = [ item.variant.product.sku, item.variant.product.name]
  row << item.qty_per_pack
  row << (@order_or_invoice.class.name == "Spree::Invoice" ? (number_to_currency item.single_price) : item.single_display_amount.to_s) unless @hide_prices
  row << item.quantity
  row << (@order_or_invoice.class.name == "Spree::Invoice" ? (number_to_currency item.total_amount)  : item.display_total.to_s) unless @hide_prices
  data << row
end

extra_row_count = 0

unless @hide_prices
  extra_row_count += 1
  data << [""] * 5
  data << [nil, nil, nil, nil, Spree.t(:subtotal), if @order_or_invoice.class.name == "Spree::Order" then
    @order_or_invoice.display_item_total.to_s else number_to_currency @order_or_invoice.subtotal end]

  if @order_or_invoice.class.name == "Spree::Order"
    @order_or_invoice.all_adjustments.eligible.each do |adjustment|
      extra_row_count += 1
      data << [nil, nil, nil, nil, adjustment.label, adjustment.display_amount.to_s]
    end
  end

  @order_or_invoice.shipments.each do |shipment|
    extra_row_count += 1
    data << [nil, nil, nil, nil, if @order_or_invoice.class.name == "Spree::Order" then 
      shipment.shipping_method.name else shipment.get_shipping_method.name end, shipment.display_cost.to_s]
  end

  data << [nil, nil, nil, nil, Spree.t(:total), if @order_or_invoice.class.name == "Spree::Order" then 
    @order_or_invoice.display_total.to_s else number_to_currency @order_or_invoice.total end]
end

move_down(120)
table(data, :width => @column_widths.values.compact.sum, :column_widths => @column_widths) do
  cells.border_width = 0.5

  row(0).borders = [:bottom]
  row(0).font_style = :bold

  last_column = data[0].length - 1
  row(0).columns(0..last_column).borders = [:top, :right, :bottom, :left]
  row(0).columns(0..last_column).border_widths = [0.5, 0, 0.5, 0.5]

  row(0).column(last_column).border_widths = [0.5, 0.5, 0.5, 0.5]

  if extra_row_count > 0
    extra_rows = row((-2-extra_row_count)..-2)
    extra_rows.columns(0..5).borders = []
    extra_rows.column(4).font_style = :bold

    row(-1).columns(0..5).borders = []
    row(-1).column(4).font_style = :bold
  end
end
