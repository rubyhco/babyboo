# require 'prawn/layout'

shipment = @order.shipments.first
ship_address = shipment.get_shipping_address

@font_face = Spree::PrintInvoice::Config[:print_invoice_font_face]

font @font_face
font_size 8

fill_color "000000"

# Address Stuff

def shipper_info
  if @hide_prices
    info = "#{@order.preorder? ? "Preorder" : "Ready Stock"}\n"
    info += "#{@order.dropship_name}\n"
    info += "#{@order.dropship_phone}"
  else
    info = "BRANDEDBABY WHOLESELLER\n"
    info += "www.brandedbabys.com\n"
    info += "Tel. 087882905449\n"
    info += @order_or_invoice.class.name == "Spree::Order" ? "#{@order_or_invoice.completed_at.to_date}" : ""
  end
end

def address_info(address)
  info = %Q{
    #{address.first_name} #{address.last_name}
    #{address.address1}
  }
  info += "#{address.address2}\n" if address.address2.present?
  state = address.state ? address.state.abbr : ""
  info += "#{address.zipcode} #{address.city} #{state}\n"
  info += "#{address.country.name}\n"
  info += "#{address.phone}\n"
  info.strip
end

if @order.dropship?
  data = [
    ["INVOICE #{@order.number}", Spree.t(:shipping_address)],
    [shipper_info, address_info(ship_address) + "\n\n via #{shipment.get_shipping_method.name} "]
  ]
else
  data = [
    ["INVOICE #{@order.number}", Spree.t(:shipping_address)],
    [shipper_info + "\n\n Notes: #{shipment.invoice_id.nil? ? shipment.order.notes : shipment.invoice.note} #{shipment.dropship? ? "(Dropship)": ""}", address_info(ship_address) + "\n\n via #{shipment.get_shipping_method.name} "]
  ]
end

move_down 10
table(data, :width => 500) do
  row(0).font_style = :bold
  row(0).padding_bottom = 0
  row(0).column(0).size = 14
  row(0).column(0).width = 230

  row(1).padding_top = 0

  # Billing address header
  row(0).column(0).borders = [:top, :right, :bottom, :left]
  row(0).column(0).border_widths = [0, 0, 0, 0]

  # Shipping address header
  row(0).column(1).borders = [:top, :right, :bottom, :left]
  row(0).column(1).border_widths = [0, 0, 0, 0]

  # Bill address information
  row(1).column(0).borders = [:top, :right, :bottom, :left]
  row(1).column(0).border_widths = [0, 0, 0, 0]

  # Ship address information
  row(1).column(1).borders = [:top, :right, :bottom, :left]
  row(1).column(1).border_widths = [0, 0, 0, 0]

end

data = []

if @order.preorder?
  @column_widths = { 0 =>50 , 1 => 125, 2 => 75, 3 => 50, 4 => 50, 5 => 75, 6 => 60 }
  @align = { 0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :right, 5 => :right, 6 => :right}
  data << [Spree.t(:sku), Spree.t(:item_description), "Items per Pack", Spree.t(:price), "Pack Qty", Spree.t(:total), "DP"]
else
  if @hide_prices
    @column_widths = { 0 =>100 , 1 => 200, 2 => 125, 3 => 100}
    @align = { 0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :right}
    data << [Spree.t(:sku), Spree.t(:item_description), "Items per Pack", "Pack Qty"]
  else
    @column_widths = { 0 =>75 , 1 => 150, 2 => 75, 3 => 75, 4 => 75, 5 => 75}
    @align = { 0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :right, 5 => :right, 6 => :right}
    data << [Spree.t(:sku), Spree.t(:item_description), "Items per Pack", Spree.t(:price), "Pack Qty", Spree.t(:total)]
  end
end

@order.line_items.each do |item|
  row = [item.variant.product.sku, item.variant.product.name]
  row << item.qty_per_pack
  row << item.single_display_amount.to_s unless @hide_prices
  row << item.quantity
  row << item.display_total.to_s unless @hide_prices
  if @order.preorder?
    row << number_to_currency(item.down_payment_price*item.quantity)
  end
  data << row
end

extra_row_count = 0

unless @hide_prices
  extra_row_count += 1
  data << [""] * 5
  data << [nil, nil, nil, nil, Spree.t(:subtotal), number_to_currency(@order.item_total)]
  if shipment.order.adjustment_total < 0
    extra_row_count += 1
    data << [nil, nil, nil, nil, "Diskon", number_to_currency(shipment.order.adjustment_total.to_s)]
  end
  extra_row_count += 1
  data << [nil, nil, nil, nil, "Shipping Cost", number_to_currency(shipment.order.shipment_total)]
  extra_row_count += 1
  data << [nil, nil, nil, nil, Spree.t(:total), number_to_currency(shipment.order.total)]
  if @order.preorder?
    extra_row_count += 1
    data << [nil, nil, nil, nil, "Total DP", number_to_currency(@order.down_payment_total)]
  end
end

move_down(25)

table(data, :width => @column_widths.values.compact.sum, :column_widths => @column_widths) do
  cells.border_width = 0.5

  row(0).borders = [:bottom]
  row(0).font_style = :bold

  last_column = data[0].length - 1
  row(0).columns(0..last_column).borders = [:top, :right, :bottom, :left]
  row(0).columns(0..last_column).border_widths = [0.5, 0, 0.5, 0.5]

  row(0).column(last_column).border_widths = [0.5, 0.5, 0.5, 0.5]

  if extra_row_count > 0
    extra_rows = row((-1-extra_row_count)..-2)
    extra_rows.columns(0..5).borders = []
    extra_rows.column(4).font_style = :bold

    row(-1).columns(0..5).borders = []
    row(-1).column(4).font_style = :bold
  end
end
