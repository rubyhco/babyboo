require 'prawn/layout'

@font_face = Spree::PrintInvoice::Config[:print_invoice_font_face]

font @font_face
font_size 8

fill_color "000000"

render :partial => "spree/admin/shared/address"