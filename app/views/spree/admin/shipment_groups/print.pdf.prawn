require 'prawn/layout'

@font_face = Spree::PrintInvoice::Config[:print_invoice_font_face]

font @font_face
font_size 8

fill_color "000000"

# Address Stuff

def shipper_info
  if @hide_prices
    info = "Shipment ID #{@shipment_group.id}\n"
    info += "#{@dropship_name}\n"
    info += "#{@dropship_phone}"
  else
    info = "Shipment ID #{@shipment_group.id}\n"
    info += "BRANDEDBABY WHOLESELLER\n"
    info += "www.brandedbabys.com\n"
    info += "Tel. 087882905449\n"
    info += @order_or_invoice.class.name == "Spree::Order" ? "#{@order_or_invoice.completed_at.to_date}" : ""
    info.strip
  end
end

def address_info(address)
  info = %Q{
    #{address.first_name} #{address.last_name}
    #{address.address1}
  }
  info += "#{address.address2}\n" if address.address2.present?
  state = address.state ? address.state.abbr : ""
  info += "#{address.zipcode} #{address.city} #{state}\n"
  info += "#{address.country.name}\n"
  info += "#{address.phone}\n"
  info.strip
end

def notes_info
  info = "\n"
  @shipments.each do |shipment|
    if shipment.order.present?
      order = shipment.order
      next unless order.notes.present?
      info += "Notes Order #{order.number} #{order.dropship? ? "(Dropship)" : ""}: #{order.notes}\n"
    else
      invoice = shipment.invoice
      next unless invoice.note.present?
      info += "Notes INV #{invoice.id} #{invoice.dropship? ? "(Dropship)" : ""}: #{invoice.note}\n"
    end
  end
  info.strip
end


data = [
  ["INVOICE", Spree.t(:shipping_address)],
  [shipper_info + "\n\n Print Out Date : #{Time.now.to_date}\n\n" +
    notes_info, 
    address_info(@ship_address) + "\n\n via #{@shipment.get_shipping_method.name} (Weight: #{@shipment_group.weight} Kg)"]
]

move_down 10
table(data, :width => 500) do
  row(0).font_style = :bold
  row(0).padding_bottom = 0
  row(0).column(0).size = 14
  row(0).column(0).width = 230

  row(1).padding_top = 0

  # Billing address header
  row(0).column(0).borders = [:top, :right, :bottom, :left]
  row(0).column(0).border_widths = [0, 0, 0, 0]

  # Shipping address header
  row(0).column(1).borders = [:top, :right, :bottom, :left]
  row(0).column(1).border_widths = [0, 0, 0, 0]

  # Bill address information
  row(1).column(0).borders = [:top, :right, :bottom, :left]
  row(1).column(0).border_widths = [0, 0, 0, 0]

  # Ship address information
  row(1).column(1).borders = [:top, :right, :bottom, :left]
  row(1).column(1).border_widths = [0, 0, 0, 0]

end

data = []

if @hide_prices
  @column_widths = { 0 => 60, 1 => 40, 2 => 75, 3 => 110, 4 => 40, 5 => 80 }
  @align = { 0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :right, 5 => :right }
  data << ["ID","Date", Spree.t(:sku), Spree.t(:item_description), "Items per Pack", Spree.t(:qty)]
else
  @column_widths = { 0 => 50, 1 =>50,  2 => 75, 3 => 100, 4 => 50, 5 => 50, 6 => 50, 7 => 60 }
  @align = { 0 => :left, 1 => :left, 2 => :left, 3 => :left, 4 => :left, 5 => :right, 6 => :right, 7 => :right}
  data << ["ID","Date", Spree.t(:sku), Spree.t(:item_description), "Items per Pack", Spree.t(:price), "Pack Qty", Spree.t(:total)]
end

total_dp_invoice = 0
@shipments.each do |shipment|
  if !shipment.order.nil?
    order = shipment.order
    inventory_units = shipment.inventory_units
    inventory_units.each do |item|
      im = item.variant.display_image.attachment(:mini)
      row = [ shipment.order.number, shipment.order.created_at.to_date, item.variant.sku, item.variant.product.name]
      row << item.line_item.qty_per_pack
      row << item.line_item.single_display_amount.to_s unless @hide_prices
      row << item.line_item.quantity
      row << item.line_item.display_total.to_s unless @hide_prices
      data << row
    end
  else
    invoice = shipment.invoice
    inventory_units = shipment.inventory_units
    inventory_units.each do |item|
      invoice_item = item.invoice_item
      total_dp_invoice += invoice_item.quantity * item.line_item.down_payment_price.to_i
      im = item.variant.display_image.attachment(:mini)
      row = [ shipment.invoice.id,shipment.invoice.created_at.to_date, item.variant.sku, item.variant.product.name]
      row << item.line_item.qty_per_pack
      row << number_to_currency(invoice_item.single_price) unless @hide_prices
      row << item.line_item_or_iu_expected_shipped_in_pack
      row << number_to_currency(invoice_item.total_amount) unless @hide_prices
      data << row
    end
  end
end

extra_row_count = 0

unless @hide_prices
  extra_row_count += 1
  data << [""] * 7
  data << [nil,nil, nil, nil, nil, nil, Spree.t(:subtotal), number_to_currency(@shipment_group.subtotal)]
  if @shipment_group.total_adjustment < 0
    extra_row_count += 1
    data << [nil,nil, nil, nil, nil, nil, "Diskon", number_to_currency(@shipment_group.total_adjustment)]
  end
  extra_row_count += 1
  data << [nil,nil, nil, nil, nil, nil, "Shipping Cost", number_to_currency(@shipment_group.total_ongkir)]
  extra_row_count += 1
  data << [nil,nil, nil, nil, nil, nil, Spree.t(:total), number_to_currency(@shipment_group.total_order_invoice)]
  if total_dp_invoice > 0
    extra_row_count += 1
    data << [nil,nil, nil, nil, nil, nil, "Total DP Invoice", number_to_currency(total_dp_invoice)]
  end
end

move_down(25)
table(data, :width => @column_widths.values.compact.sum, :column_widths => @column_widths) do
  cells.border_width = 0.5

  row(0).borders = [:bottom]
  row(0).font_style = :bold

  last_column = data[0].length - 1
  row(0).columns(0..last_column).borders = [:top, :right, :bottom, :left]
  row(0).columns(0..last_column).border_widths = [0.5, 0, 0.5, 0.5]

  row(0).column(last_column).border_widths = [0.5, 0.5, 0.5, 0.5]

  if extra_row_count > 0
    extra_rows = row((-1-extra_row_count)..-2)
    extra_rows.columns(0..7).borders = []
    extra_rows.column(4).font_style = :bold

    row(-1).columns(0..7).borders = []
    row(-1).column(4).font_style = :bold
  end
end
