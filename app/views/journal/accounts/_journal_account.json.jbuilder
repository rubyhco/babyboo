json.extract! journal_account, :id, :name, :is_payment_account, :currency, :created_at, :updated_at
json.url journal_account_url(journal_account, format: :json)