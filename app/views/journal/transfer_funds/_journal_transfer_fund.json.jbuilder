json.extract! journal_transfer_fund, :id, :amount, :from, :to, :kurs, :created_at, :updated_at
json.url journal_transfer_fund_url(journal_transfer_fund, format: :json)