# require 'prawn/layout'

@font_face = Spree::PrintInvoice::Config[:print_invoice_font_face]

font @font_face
font_size 8

fill_color "000000"

# Address Stuff

def shipper_info
  info = "BRANDEDBABY WHOLESELLER\n"
  info += "www.brandedbabys.com\n"
  info += "Tel. 087882905449\n"
end

data = [
  ["Purchase Invoice #{@bill.number}"],
  [shipper_info]
]

move_down 10
table(data, :width => 500) do
  row(0).font_style = :bold
  row(0).padding_bottom = 0
  row(0).column(0).size = 14
  row(0).column(0).width = 230

  row(1).padding_top = 0

  # Billing address header
  row(0).column(0).borders = [:top, :right, :bottom, :left]
  row(0).column(0).border_widths = [0, 0, 0, 0]

  # Shipping address header
  row(0).column(1).borders = [:top, :right, :bottom, :left]
  row(0).column(1).border_widths = [0, 0, 0, 0]

  # Bill address information
  row(1).column(0).borders = [:top, :right, :bottom, :left]
  row(1).column(0).border_widths = [0, 0, 0, 0]

  # Ship address information
  row(1).column(1).borders = [:top, :right, :bottom, :left]
  row(1).column(1).border_widths = [0, 0, 0, 0]

end

data = []

@column_widths = { 0 =>75 , 1 => 50, 2 => 100, 3 => 50, 4 => 50, 5 => 75, 6 => 100}
@align = { 0 => :center, 1 => :left, 2 => :left, 3 => :left, 4 => :right, 5 => :right, 6 => :right}
data << ["Image", "PO ID", "SKU", "Qty Pcs", "Currency", "Price", "Total Price"]

@items.each do |item|
  if item.originator.variant.images.present?
    image = "#{Rails.root}/public"+item.originator.variant.display_image.attachment.url(:large, false)
    row = [{:image => image,:fit => [40, 50]}]
  else
    row = ["no image"]
  end
  row << item.originator.purchase_order_id
  row << item.originator.variant.sku
  row << item.quantity
  row << item.bill.currency
  row << number_to_delimited(item.amount)
  row << number_to_delimited(item.total)
  data << row
end

extra_row_count = 0

extra_row_count += 1
data << [""] * 5
data << [nil, nil, "Total Pcs:", @items.sum(:quantity), nil, Spree.t(:subtotal), number_to_delimited(@bill.total)]


move_down(25)

table(data, :width => @column_widths.values.compact.sum, :column_widths => @column_widths) do
  cells.border_width = 0.5

  row(0).borders = [:bottom]
  row(0).font_style = :bold

  last_column = data[0].length - 1
  row(0).columns(0..last_column).borders = [:top, :right, :bottom, :left]
  row(0).columns(0..last_column).border_widths = [0.5, 0, 0.5, 0.5]

  row(0).column(last_column).border_widths = [0.5, 0.5, 0.5, 0.5]

  if extra_row_count > 0
    extra_rows = row((-1-extra_row_count)..-2)
    extra_rows.columns(0..5).borders = []
    extra_rows.column(4).font_style = :bold

    row(-1).columns(0..5).borders = []
    row(-1).column(4).font_style = :bold
  end
end
