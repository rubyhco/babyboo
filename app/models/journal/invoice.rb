class Journal::Invoice < ApplicationRecord
  include Filterable
  has_many :items, class_name:"Journal::InvoiceItem", dependent: :destroy
  has_many :payments, class_name:"Journal::InvoicePayment", foreign_key: 'invoice_id'
  attr_accessor :category, :description
  belongs_to :transaction_id, class_name: "Journal::Transaction"
  belongs_to :user, class_name:"Spree::User", foreign_key: 'spree_user_id'
  
  scope :name_user, -> (vendor){where("vendor like ?", "%#{vendor}%")}
  scope :number, -> (number){where("number = ?", number)}
  scope :pending, -> {where(payment_state: "pending")}
  scope :id_is, -> (id){where(id: id)}
  scope :amount_is, -> (amount){where(total: amount)}
  scope :description_contain, -> (description){joins(:items).where("description like ?", "%#{description}%")}
  scope :payment_is, -> (status){where(payment_state: status)}
  scope :created_at_is, -> (date){where('created_at BETWEEN ? AND ?',date.first.to_datetime.beginning_of_day, date.first.to_datetime.end_of_day)}

  state_machine :payment_state, initial: :pending do
    event :pay do
      transition to: :paid, from: :pending
    end

    event :unpay do 
      transition :pending => :unpaid
    end
    after_transition :on => :unpay, :do => :after_unpay
  end
  
  validates :number, presence: true, uniqueness: { allow_blank: true }

  before_validation :generate_number, on: :create

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << ["Number", "Date", "Name", "Total", "Sisa Tagihan", "Payment State", "Description", "Due Date"]
      all.each do |inv|
        inv.items.each do |item|
          csv << [inv.number.nil? ? inv.id : inv.number, inv.created_at, inv.vendor, inv.total, inv.outstanding_balance, item.description, inv.due_date]
        end
      end
    end
  end

  def send_email_invoice
    Spree::BrandedBabysMailer.journal_invoice(self).deliver!
  end

  def send_sms_invoice
    sms = RajaSms.new
    phone = user.phone
    sms.send_sms(phone, "Credit #{self.id} menunggu pembayaran. Silahkan lakukan pembayaran sebesar 
      Rp#{self.total} untuk melunasi #{self.items.first.description.downcase}. Silahkan cek email Anda untuk info lengkapnya. -BrandedBabys")
  end

  def outstanding_balance
    self.total.to_i - (self.payments.paid.sum(:amount).to_i)
  end

  def update!
    if self.payments.paid.sum(:amount) >= self.total
      self.payment_state = "paid"
      self.save
    end
  end

  def outstanding_balance?
    outstanding_balance != 0
  end

  def add_payment_store_credit
    return if user.nil?
    return if user.total_available_store_credit.zero?

    remaining_total = outstanding_balance

    user.store_credits.order_by_priority.each do |credit|
      break if remaining_total.zero?
      next if credit.amount_remaining.zero?
      
      amount_to_take = [credit.amount_remaining, remaining_total].min

      journal_inv_payment = payments.create!(source_type: "Spree::StoreCredit", source_id: credit.id, payment_method_id: Spree::PaymentMethod::StoreCredit.first, amount: amount_to_take, state: 'paid', response_code: credit.generate_authorization_code)

      auth = credit.authorize(
            amount_to_take,
            "IDR",
            action_originator: journal_inv_payment
          )
          
      credit.capture(amount_to_take, auth, "IDR", action_originator: journal_inv_payment)

      remaining_total -= amount_to_take
    end
    self.update!
  end

  def generate_number(options = {})
    options[:length]  ||= 5
    options[:letters] ||= false
    options[:prefix]  ||= "CN"

    possible = (0..9).to_a
    possible += ('A'..'Z').to_a if options[:letters]

    self.number ||= loop do
      # Make a random number.
      random = "#{options[:prefix]}#{(0...options[:length]).map { possible.sample }.join}"
      # Use the random  number if no other order exists with it.
      if self.class.exists?(number: random)
        # If over half of all possible options are taken add another digit.
        options[:length] += 1 if self.class.count > (10**options[:length] / 2)
      else
        break random
      end
    end
  end

  def add_payment_store_credit_with_value(amount_sc)
    return if user.nil?
    return if user.total_available_store_credit.zero?

    if amount_sc > outstanding_balance
      remaining_total = outstanding_balance
    else
      remaining_total = amount_sc
    end

    user.store_credits.order_by_priority.each do |credit|
      break if remaining_total.zero?
      next if credit.amount_remaining.zero?
      
      amount_to_take = [credit.amount_remaining, remaining_total].min

      journal_inv_payment = payments.create!(source_type: "Spree::StoreCredit", source_id: credit.id, payment_method_id: Spree::PaymentMethod::StoreCredit.first, amount: amount_to_take, state: 'paid', response_code: credit.generate_authorization_code)

      auth = credit.authorize(
            amount_to_take,
            "IDR",
            action_originator: journal_inv_payment
          )
          
      credit.capture(amount_to_take, auth, "IDR", action_originator: journal_inv_payment)

      remaining_total -= amount_to_take
    end
    self.update!
  end

  def after_unpay
    debit_account_id = Journal::Account.find_by_code("6-60355").id
    credit_account_id = Journal::Account.find_by_code("1-10101").id
    Journal::Invoice.transaction do
      journal_entry_unpay = Journal::JournalEntry.create(description: "Bad Debt: #{number}", date: DateTime.now)
      # credit 
      Journal::JournalItem.create(account_id: credit_account_id, description: "Bad Debt: #{number}", credit: total, currency: "IDR", originator: self, journal_entry_id: journal_entry_unpay.id)
      # debit 
      Journal::JournalItem.create(account_id: debit_account_id, description: "Bad Debt: #{number}", debit: total, currency: "IDR", originator: self, journal_entry_id: journal_entry_unpay.id)
    end
  end
end
