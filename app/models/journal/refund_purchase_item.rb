class Journal::RefundPurchaseItem < ApplicationRecord
	belongs_to :refund_purchase, class_name: "Journal::RefundPurchase"
	belongs_to :originator, polymorphic: true
	belongs_to :bill_item, class_name: "Journal::BillItem"
	belongs_to :variant, class_name: "Spree::Variant"

	after_create :update_total
	after_update :update_total

	def update_total
		rp = self.refund_purchase
    rp.total = rp.items.sum(:total)
    rp.save
	end
end
