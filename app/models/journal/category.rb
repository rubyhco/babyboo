class Journal::Category < ApplicationRecord
  has_many :accounts1, class_name: "Journal::Account"
  has_many :accounts, -> (object) { where(is_payment_account: true, currency: "IDR")}, :class_name => 'Journal::Account'
  has_many :category_account, -> (object) { where(is_payment_account: false, currency: "IDR")}, :class_name => 'Journal::Account'
  has_many :transfer_to, -> (object) { where(is_payment_account: true, currency: "RMB")}, :class_name => 'Journal::Account'
  has_many :category_rmb, -> (object) { where(is_payment_account: false, currency: "RMB")}, :class_name => 'Journal::Account'
  
end


# Journal::Category.total_category_for "account"