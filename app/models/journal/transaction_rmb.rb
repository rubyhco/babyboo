class Journal::TransactionRmb < ApplicationRecord
  belongs_to :account, class_name: "Journal::Account"
  
  after_create :create_item

  def create_item
    Journal::JournalItem.transaction_rmb(self.amount, self.description, self.account_id, self.category_id, 
      self.is_expense, self.id, self.account.currency, "Journal::System")
  end
end
