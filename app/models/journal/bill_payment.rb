class Journal::BillPayment < ApplicationRecord
  include Filterable

  belongs_to :source, polymorphic: true
  belongs_to :bill, class_name: "Journal::Bill"
  belongs_to :journal_transaction, class_name: "Journal::Transaction"

  after_create :post_to_journal
  after_create :update_bill_payment_status
  after_create :create_payment_expense

  scope :unreconcile, -> {where(journal_transaction_id: nil)}
  scope :number, -> (number){joins(:bill).where("journal_bills.number = ?", number)}
  scope :name_user, -> (name_user){joins(:bill).where("journal_bills.vendor like ?", "%#{name_user}%")}
  scope :category_is, -> (category){joins(bill: :items).where("journal_bill_items.category = ?", category)}
  scope :amount_is, -> (amount_bill){where(amount: amount_bill)}

  def account_payable_journal_account_id
    if bill.currency == "IDR"
      @account_payable = Journal::Account.find_by_code("2-20101").id
    else
      @account_payable = Journal::Account.find_by_code("2-20104").id
    end
  end

  def create_payment_expense
    Journal::PaymentExpense.create(number: self.bill.number, account_id: self.source_id, description: "#{self.bill.vendor} : #{self.bill.items.first.description}", originator: self, amount: self.amount)
  end

  def debit_account_id
    if bill.expenses?
      bill.items.first.category
    else
      account_payable_journal_account_id
    end
  end

  def update_bill_payment_status
    if bill.amount_due > 0
      bill.partially_pay! unless bill.partially_paid?
    else
      bill.pay! unless bill.paid?
    end
  end

  def post_to_journal
    return if amount <= 0
    Journal::BillPayment.transaction do
      journal_entry_bill_payment = Journal::JournalEntry.create(description: "Payment: #{bill.number} #{bill.notes} - Account: #{self.source.name}", date: DateTime.now)
      # credit 
      Journal::JournalItem.create(account_id: source_id, description: "Payment: #{bill.number} #{bill.notes} - Account: #{self.source.name}", credit: amount, currency: bill.currency, originator: self, journal_entry_id: journal_entry_bill_payment.id)
      # debit 
      Journal::JournalItem.create(account_id: debit_account_id, description: "Payment: #{bill.number} #{bill.notes} - Account: #{self.source.name}", debit: amount, currency: bill.currency, originator: self, journal_entry_id: journal_entry_bill_payment.id)
      
      record_bill(source_id) if bill.currency == "RMB"
    end
  end

   def record_bill(account_id)
    total_idr_value = 0
    idr_value = 0
    bill.items.each do |item|
      total_item_price = item.total
      Journal::TransferFund.where(to: account_id).each do |tf|
        break if total_item_price.zero?
        next if tf.amount_remaining.zero?

        amount_to_take = [tf.amount_remaining, total_item_price].min

        used_amount = amount_to_take + tf.amount_used.to_i
        tf.update_columns(amount_used: used_amount)
        tf.variant_kurs.create!(variant_id: item.originator.variant_id)

        idr_value = amount_to_take * tf.kurs
        total_idr_value += idr_value
        total_item_price -= amount_to_take
      end
    end
    @supplier_deposit_idr = Journal::Account.find_by_code("1-10003").id
    @account_payable = Journal::Account.find_by_code("2-20101").id
    journal_entry_record_bill = Journal::JournalEntry.create(description: "Payment: #{bill.number} #{bill.notes}", date: DateTime.now)
    # credit 
    Journal::JournalItem.create(account_id:  @supplier_deposit_idr, description: "Payment: #{bill.number} #{bill.notes}", credit: total_idr_value, currency: "IDR", originator: self, journal_entry_id: journal_entry_record_bill.id)
    # debit 
    Journal::JournalItem.create(account_id: @account_payable, description: "Payment: #{bill.number} #{bill.notes}", debit: total_idr_value, currency: "IDR", originator: self, journal_entry_id: journal_entry_record_bill.id)
  end
end