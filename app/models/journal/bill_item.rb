class Journal::BillItem < ApplicationRecord
  belongs_to :bill, class_name: "Journal::Bill"
  belongs_to :originator, polymorphic: true
  belongs_to :spree_purchase_line_item, foreign_key: 'originator_id', foreign_type: "journal_bill_items.originator_type = 'Spree::PurchaseLineItem'", class_name: "Spree::PurchaseLineItem"  
  has_many :refund_items, class_name: "Journal::RefundPurchaseItem"

  after_commit :update_bill_total, if: :persisted?

  after_create :create_journal_transaction

  def update_bill_total
    return if bill.expenses?
    return if bill.nil?
    total = 0
    bill.items.each do |item|
      total+= item.total
    end
    bill.update_columns(total: total)
  end

  def outstanding_qty
    quantity - self.refund_items.sum(:qty)
  end

  def total
    amount * quantity.to_i
  end

  def create_journal_transaction
    return if bill.supplier.nil?
    post_to_journal
  end

  def post_to_journal
    if bill.currency == "RMB"
      bill_cat = Journal::Account.find_by_code("2-20104").id
    else
      bill_cat = Journal::Account.find_by_code("2-20101").id
    end
    journal_entry_bill = Journal::JournalEntry.create(description: description, date: DateTime.now)
    journal_credit = Journal::JournalItem.create(account_id: bill_cat, description: description, credit: total , currency: bill.currency, journal_entry_id: journal_entry_bill.id)
    journal_debit = Journal::JournalItem.create(account_id: category, description: description, debit: total , currency: bill.currency, journal_entry_id: journal_entry_bill.id)
  end

  def reverse_post_to_journal
    if bill.currency == "RMB"
      bill_cat = Journal::Account.find_by_code("2-20104").id
    else
      bill_cat = Journal::Account.find_by_code("2-20101").id
    end
    description_reverse = "Reverse Purchase Invoice " + description
    journal_entry_bill = Journal::JournalEntry.create(description: description_reverse, date: DateTime.now)
    journal_credit = Journal::JournalItem.create(account_id: bill_cat, description: description_reverse, debit: total , currency: bill.currency, journal_entry_id: journal_entry_bill.id)
    journal_debit = Journal::JournalItem.create(account_id: category, description: description_reverse, credit: total , currency: bill.currency, journal_entry_id: journal_entry_bill.id)
  end

  def update_selisih_po_item
    Journal::BillItem.transaction do
      po_item = self.originator
      selisih_pcs = quantity - po_item.outstanding_invoice_qty
      selisih_pack = selisih_pcs/ po_item.variant.product.total_items
      stock = po_item.variant.stock_items.first

      po_item.update_attributes(quantity: po_item.quantity + selisih_pack, outstanding_invoice_qty: po_item.outstanding_invoice_qty + selisih_pcs)
      stock.update_attributes(expected_count_on_hand: stock.expected_count_on_hand + selisih_pcs)
    end
  end
end
