class Journal::Account < ApplicationRecord
  include Filterable
  has_many :transactions, dependent: :destroy
  belongs_to :category, class_name: "Journal::Category", foreign_key: 'category_id'
  has_many :items, class_name: "Journal::JournalItem", foreign_key: 'account_id'

  validates_uniqueness_of :code, allow_bank: true

  scope :account_is, -> {where(is_payment_account: true)}
  scope :payment_account, -> {where(is_payment_account: true, currency: "IDR")}
  scope :category_is, -> {where(is_payment_account: false)}
  scope :currency_is, -> (currency){where("currency like ?", "%#{currency}%")}

  scope :suppliers, -> {where("is_payment_account = ? AND currency = ?", true, "RMB")}
  scope :cash_on_hand, -> {where("is_payment_account = ? AND currency = ?", true, "IDR")}
  scope :cash_and_bank, -> {joins(:category).where("journal_categories.prefix = ?", "1-100")}
  scope :from_transfer_fund, -> {joins(:category).where("journal_categories.prefix = ? OR journal_categories.prefix = ?", "1-100", "5-504")}

  scope :expense, -> {joins(:category).where("journal_categories.prefix = ? || journal_categories.prefix = ? || journal_categories.prefix = ?
    && journal_accounts.currency = ?", "5-504", "6-603", "9-900", "IDR")}
  scope :account_idr, -> {where(currency: "IDR")}
  scope :account_rmb, -> {where(currency: "RMB")}
  before_create :set_code

  def balance
    case prefix
    when 1
      items.sum(:debit) - items.sum(:credit)
    else
      items.sum(:credit) - items.sum(:debit)
    end
  end

  def balance_with_items(items_journal)
    journal_items = items_journal.where(account_id: self.id)
    case prefix
    when 1
      journal_items.sum(:debit) - journal_items.sum(:credit)
    else
      journal_items.sum(:credit) - journal_items.sum(:debit)
    end
  end

  def balance_statement
    transactions.sum(:amount)
  end

  def prefix
    a = code.split("-")
    a.first.to_i
  end

  private

  def set_code
    increment = Journal::Account.where(category_id: category_id).count
    i = 1
    self.code ||= loop do
      number = category.prefix + (increment + i).to_s.rjust(2, '0')
      if self.class.exists?(code: number)
        i += 1
      else
        break number
      end
    end
  end
end
