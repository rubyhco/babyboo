class Journal::TransferFund < ApplicationRecord
  include Filterable
  
  after_create :recalculate
  after_create :record_journal
  after_create :create_payment_expense

  has_many :variant_kurs, class_name: "Spree::VariantKur"
  belongs_to :income_transaction, class_name: "Journal::Transaction", foreign_key: "income_transaction_id"
  belongs_to :expense_transaction, class_name: "Journal::Transaction", foreign_key: "expense_transaction_id"
  validates :amount, :to, :from, :kurs, presence: true

  scope :unreconcile_income, -> {where(income_transaction_id: nil)}
  scope :unreconcile_expense, -> {where(expense_transaction_id: nil)}
  scope :number, -> (id){where id: id}
  scope :amount_is, -> (amount){where(total: amount)}
  scope :only_income_reconcile, -> {where.not(to: Journal::Account.where("name like ?", "%supplier%").pluck(:id))}
  scope :only_expense_reconcile, -> {where.not(from: Journal::Account.where("name like ?", "%supplier%").pluck(:id))}

  def record_journal
    supplier_deposit_idr = Journal::Account.find_by_code("1-10003")
    account_from = Journal::Account.find_by_id(from)
    account_to = Journal::Account.find_by_id(to)
    journal_entry = Journal::JournalEntry.create(description: "Transfer Fund #{id} [From: #{account_from.name}, To: #{account_to.name}, #{account_to.currency} #{amount}] ", date: DateTime.now)
    Journal::JournalItem.create(account_id: supplier_deposit_idr.id, description: "Transfer Fund #{id} [From: #{account_from.name}, To: #{account_to.name}, #{account_to.currency} #{amount}] ", debit: total, currency:supplier_deposit_idr.currency , originator: self, journal_entry_id: journal_entry.id)
    Journal::JournalItem.create(account_id: from, description: "Transfer Fund #{id} [From: #{account_from.name}, To: #{account_to.name}, #{account_to.currency} #{amount}]", credit: total, currency: Journal::Account.find_by_id(from).currency, originator: self, journal_entry_id: journal_entry.id)
    if kurs > 1
      transfer_from_idr = Journal::Account.find_by_code("4-40207")
      journal_entry_tf = Journal::JournalEntry.create(description: "Transfer Fund #{id} [From: #{account_from.name}, To: #{account_to.name}, #{account_to.currency} #{amount}] ", date: DateTime.now)
      Journal::JournalItem.create(account_id: to, description: "Transfer Fund #{id} [From: #{account_from.name}, To: #{account_to.name}, #{account_from.currency} #{total}] ", debit: amount, currency: Journal::Account.find_by_id(to).currency, originator: self, journal_entry_id: journal_entry_tf.id)
      Journal::JournalItem.create(account_id: transfer_from_idr.id, description: "Transfer Fund #{id} [From: #{account_from.name}, To: #{account_to.name}, #{account_from.currency} #{total}]", credit: amount, currency:transfer_from_idr.currency , originator: self, journal_entry_id: journal_entry_tf.id)
    end
  end

  def create_payment_expense
    Journal::PaymentExpense.create(number: self.id, account_id: self.from, description: "Transfer Funds From Account #{self.from_account} To Account #{self.to_account}", originator: self, amount: self.total)
  end

  def from_account
    Journal::Account.find(from).name
  end

  def to_account
    Journal::Account.find(to).name
  end

  def recalculate
  	total = kurs * amount
  	self.update_columns(total: total)
  end

  def amount_remaining
  	amount - amount_used.to_i
  end
end
