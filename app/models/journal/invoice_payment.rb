class Journal::InvoicePayment < ApplicationRecord
  include Filterable
  
  belongs_to :source, polymorphic: true
  belongs_to :invoice, class_name: "Journal::Invoice", foreign_key: 'invoice_id'
  belongs_to :journal_transaction, class_name: "Journal::Transaction"
  scope :created_at_is, -> (date){where('created_at BETWEEN ? AND ?',date.first.to_datetime.beginning_of_day, date.first.to_datetime.end_of_day)}
  scope :name_user, -> (name_user){joins(:invoice).where("vendor like ?", "%#{name_user}%")}
  scope :number, -> (number){joins(:invoice).where("journal_invoices.number = ?", number)}

  scope :paid, -> {where(state: "paid")}
  after_commit :post_to_journal

  def payment_method_is_store_credit?
    source_type == "Spree::StoreCredit"
  end

  def journal_transaction_account_id
    if journal_transaction.nil?
      @journal_transaction_account_id = Journal::Account.find_by_code("1-10001").id
    else
      @journal_transaction_account_id = journal_transaction.account_id
    end
    @journal_transaction_account_id
  end

  def store_credit_journal_account_id
    @store_credit = Journal::Account.find_by_code("2-20219").id
  end

  def account_receivable_journal_account_id
    @account_receivable = Journal::Account.find_by_code("1-10101").id
  end

  def post_to_journal
    return if state != "paid"
    return if amount <= 0
    Journal::InvoicePayment.transaction do
      if payment_method_is_store_credit?
        post_invoice_with_store_credit
      else
        post_invoice
      end
    end
  end

  def post_invoice_with_store_credit
    journal_entry = Journal::JournalEntry.create(description: "Payment: CN#{invoice_id} - #{invoice.vendor}", date: DateTime.now)
    # credit 
    Journal::JournalItem.create(account_id: account_receivable_journal_account_id, description: "Payment: CN#{invoice_id} - #{invoice.vendor}", credit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
    # debit 
    Journal::JournalItem.create(account_id: store_credit_journal_account_id, description: "Payment: CN#{invoice_id} - #{invoice.vendor}", debit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
  end

  def post_invoice
    journal_entry = Journal::JournalEntry.create(description: "Payment: CN#{invoice_id} - #{invoice.vendor}", date: DateTime.now)
    # credit 
    Journal::JournalItem.create(account_id: account_receivable_journal_account_id, description: "Payment: CN#{invoice_id} - #{invoice.vendor}", credit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
    # debit 
    Journal::JournalItem.create(account_id: journal_transaction_account_id, description: "Payment: CN#{invoice_id} - #{invoice.vendor}", debit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
  end
end