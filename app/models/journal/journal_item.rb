class Journal::JournalItem < ApplicationRecord
  include Filterable
  belongs_to :journal_entry, class_name: "Journal::JournalEntry", foreign_key: 'journal_entry_id'
  belongs_to :journal_transaction, class_name: "Journal::Transaction", foreign_key: 'transaction_id'
  belongs_to :account, class_name: "Journal::Account", foreign_key: 'account_id'
  belongs_to :originator, polymorphic: true

  scope :account_id_is, -> (account_id) { where account_id: account_id }
  scope :start_date, -> (start_date) { where("created_at >= ?", start_date) }
  scope :end_date, -> (end_date) { where("created_at <= ?", end_date) }
  scope :currency_is, -> (currency) { where currency: currency }
  scope :description_contain, -> (description) {where("description like ?", "%#{description}%")}
  scope :item_idr, -> {where(currency: "IDR")}


  def self.to_csv(options = {}, account, all_journal_item)
    CSV.generate(options) do |csv|
      csv << ["ID", "Date", "Account", "Description", "Debit", "Credit", "Currency", "Balance"]
      all.each do |item|
        csv << [item.id, item.created_at, item.account.present? ? item.account.name : "NULL", item.description, item.debit, item.credit, item.currency, item.balance_item]
      end
    end
  end

  def balance_item
    trans_before = Journal::JournalItem.where(account_id: account.id).where("id < ?", self.id)
    total_debit_before = trans_before.sum(:debit)
    total_credit_before = trans_before.sum(:credit)
    if account.prefix == 1
      total_balance_before = total_debit_before - total_credit_before
    else
      total_balance_before = total_credit_before - total_debit_before
    end
    balance_item = total_balance_before
    if account.prefix == 1
      balance_item -= credit.to_i
      balance_item += debit.to_i
    else
      balance_item += credit.to_i
      balance_item -= debit.to_i
    end
    return balance_item
  end

  def self.create_item(amount, description, account_id, category_id, is_expense, id, currency, originator_system)
    journal_entry = Journal::JournalEntry.create(description: description, date: DateTime.now)
    if is_expense == true
      item_expense_debit = Journal::JournalItem.new(account_id: category_id, description: description, debit: amount.abs, transaction_id: id,  currency: currency, originator_type: originator_system, journal_entry_id: journal_entry.id)
      item_expense_debit.save
      item_expense_credit = Journal::JournalItem.new(account_id: account_id, description: description, credit: amount.abs, transaction_id: id, currency: currency, originator_type: originator_system, journal_entry_id: journal_entry.id)
      item_expense_credit.save
    else 
      item_income_debit = Journal::JournalItem.new(account_id: account_id, description: description, debit: amount, transaction_id: id, currency: currency, originator_type: originator_system, journal_entry_id: journal_entry.id)
      item_income_debit.save
      item_income_credit = Journal::JournalItem.new(account_id: category_id, description: description, credit: amount, transaction_id: id, currency: currency, originator_type: originator_system, journal_entry_id: journal_entry.id)
      item_income_credit.save
    end

  end

  def self.transaction_rmb(amount, description, account_id, category_id, is_expense, id, currency, originator_system)
    journal_entry = Journal::JournalEntry.create(description: description, date: DateTime.now)
    if is_expense == true
      item_expense_debit = Journal::JournalItem.new(account_id: category_id, description: description, debit: amount,  currency: currency, originator_type: originator_system, journal_entry_id: journal_entry.id)
      item_expense_debit.save
      item_expense_credit = Journal::JournalItem.new(account_id: account_id, description: description, credit: amount, currency: currency, originator_type: originator_system, journal_entry_id: journal_entry.id)
      item_expense_credit.save
    else 
      item_income_debit = Journal::JournalItem.new(account_id: account_id, description: description, debit: amount, currency: currency, originator_type: originator_system, journal_entry_id: journal_entry.id)
      item_income_debit.save
      item_income_credit = Journal::JournalItem.new(account_id: category_id, description: description, credit: amount, currency: currency, originator_type: originator_system, journal_entry_id: journal_entry.id)
      item_income_credit.save
    end
  end

  def self.update_item(amount, description, account_id, category_id, id, currency)
    @journal_item_credit = Journal::JournalItem.where(transaction_id: id).last
    @journal_item_credit.update(description: description, account_id: account_id, credit: amount, currency: currency)
    @journal_item_debit = Journal::JournalItem.where(transaction_id: id).first
    @journal_item_debit.update(description: description, account_id: category_id, debit: amount, currency: currency)
  end

  def self.record_purchase(id,supplier_id, total, currency, transaction_id, originator_user)
    
  end

  def self.record_bills(vendor, bill_id, account_bill, desc, amount, currency, transaction_id, account_record, originator_user)
    
  end

  def self.record_transfer(id, amount, from, to, kurs, originator_user)

  end
end
