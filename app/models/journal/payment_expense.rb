class Journal::PaymentExpense < ApplicationRecord
  include Filterable

	belongs_to :originator, polymorphic: true
  belongs_to :journal_transaction, class_name: "Journal::Transaction", foreign_key: 'transaction_id'
  belongs_to :account, class_name: "Journal::Account"

  scope :pending, -> {where(state: "pending")}
  scope :name_user, -> (name_user){where("description like ?", "%#{name_user}%")}
  scope :category_is, -> (category){where(account_id: category)}
  scope :amount_is, -> (amount){where(amount: amount)}
  scope :number, -> (number){where(number: number)}

	state_machine :state, initial: :pending do
    event :reconcile do
      transition to: :reconciled, from: :pending
    end
    after_transition to: :reconciled, do: :after_reconciled
  end

  def after_reconciled
  	if originator_type == "Journal::TransferFund"
  		self.originator.update_columns(expense_transaction_id: self.transaction_id)
  	else
  		self.originator.update_columns(journal_transaction_id: self.transaction_id)
  	end
  end
end
