class Journal::Bill < ApplicationRecord
  include Filterable
  has_many :items, class_name:"Journal::BillItem", dependent: :destroy
  has_many :payments, class_name: "Journal::BillPayment", dependent: :destroy
  belongs_to :transaction_id, class_name: "Journal::Transaction"
  belongs_to :originator, polymorphic: true
  belongs_to :user, class_name: "Spree::User", foreign_key: :spree_user_id
  belongs_to :supplier, class_name: "Spree::Supplier"
  has_many :refund_purchases, class_name: "Journal::RefundPurchase", dependent: :destroy
  scope :created_at_is, -> (date){where('created_at BETWEEN ? AND ?',date.first.to_datetime.beginning_of_day, date.first.to_datetime.end_of_day)}
  scope :expenses, -> {where(expenses: true)}
  validates :number, presence: true, uniqueness: { allow_blank: true }

  before_validation :generate_number, on: :create

  attr_accessor :category, :description

  state_machine :payment_state, initial: :pending do
    event :partially_pay do
      transition to: :partially_paid, from: :pending
    end

    after_transition to: :partially_paid, do: :after_partially_paid

    event :pay do
      transition to: :paid, from: [:partially_paid, :pending]
    end

    after_transition to: :paid, do: :after_paid
  end

  state_machine :state, initial: :pending do
    event :draft do
      transition to: :draft, from: [:pending, :confirmed]
    end
    event :confirm do
      transition to: :confirmed, from: [:pending, :draft]
    end
    after_transition to: :confirmed, do: :after_confirm
  end

  scope :name_user, -> (name){where("vendor like ?", "%#{name}%")}
  scope :cash_out_id_is, -> (id){where(originator_id: id)}
  scope :pending, -> {where(payment_state: "pending")}
  scope :without_purchase_order, -> {where.not(originator_type: "Spree::PurchaseOrder")}
  scope :cash_out_request, -> {where(originator_type: "Spree::CashOutRequest")}
  scope :payment_is, -> (status){where(payment_state: status)}
  scope :description_contain, -> (description){joins(:items).where("description like ?", "%#{description}%")}
  scope :amount_is, -> (amount){where(total: amount)}

  def after_confirm
    return if supplier.nil?
    items.each do |item|
      item.update_selisih_po_item if item.quantity > item.originator.outstanding_invoice_qty
      item.post_to_journal
      item.originator.update_cost_price_variant

      qty = item.originator.outstanding_invoice_qty - item.quantity
      item.originator.update_columns(outstanding_invoice_qty: qty)
    end
  end

  def reverse_to_draft
    Journal::Bill.transaction do
      return if supplier.nil?
      items.each do |item|
        item.reverse_post_to_journal
        item.originator.update_cost_price_variant

        qty = item.originator.outstanding_invoice_qty + item.quantity
        item.originator.update_columns(outstanding_invoice_qty: qty)
      end
      self.draft!
    end
  end

  def total_refund
    return 0 if refund_purchases.empty?

    refund_purchases.sum(:total)
  end
  
  def prefix
    if !supplier_id.nil?
      "PURC"
    elsif originator_type == "Spree::CashOutRequest"
      "COR"
    elsif expenses? 
      "EX"
    else
      "AP"
    end
  end

  def generate_number(options = {})
    options[:length]  ||= 5
    options[:letters] ||= false
    options[:prefix]  ||= prefix

    possible = (0..9).to_a
    possible += ('A'..'Z').to_a if options[:letters]

    self.number ||= loop do
      # Make a random number.
      random = "#{options[:prefix]}#{(0...options[:length]).map { possible.sample }.join}"
      # Use the random  number if no other order exists with it.
      if self.class.exists?(number: random)
        # If over half of all possible options are taken add another digit.
        options[:length] += 1 if self.class.count > (10**options[:length] / 2)
      else
        break random
      end
    end
  end

  def amount_due
    total - payments.sum(:amount)
  end

  def after_partially_paid
    return if supplier_id.nil?
    items.each do |item|
      item.originator.partially_pay! unless item.originator.partially_paid?
    end
  end

  def after_paid
    return if supplier_id.nil?

    items.each do |item|
      all_bill_item = Journal::BillItem.joins(:bill).where("journal_bills.payment_state = ? AND journal_bill_items.originator_type = ? 
        AND journal_bill_items.originator_id = ?", "paid", item.originator_type, item.originator_id)
      if all_bill_item.sum(:quantity) >= item.originator.items
        item.originator.pay! unless item.originator.paid?
      else
        item.originator.partially_pay! unless item.originator.partially_paid?
      end
    end
  end

  private

  def purchase_order
    description = self.items.first.description
    po_id = description.split(" ").last
    po = Spree::PurchaseOrder.find po_id

    po
  end

end
