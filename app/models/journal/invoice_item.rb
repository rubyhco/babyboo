class Journal::InvoiceItem < ApplicationRecord
  belongs_to :invoice, class_name: "Journal::Invoice"

  after_create :post_journal_items

  def post_journal_items
    account_receivable_jurnal_id = Journal::Account.find_by_code("1-10101").id
    journal_entry = Journal::JournalEntry.create(description: "#{invoice.number}: #{description}", date: DateTime.now)
    journal_debit = Journal::JournalItem.create(account_id: account_receivable_jurnal_id, description: "#{invoice.number}: #{description}", debit: amount, currency: invoice.currency, journal_entry_id: journal_entry.id)
    journal_credit = Journal::JournalItem.create(account_id: category, description: "#{invoice.number}: #{description}", credit: amount, currency: invoice.currency, journal_entry_id: journal_entry.id)   
  end
end
