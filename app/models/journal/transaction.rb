class Journal::Transaction < ApplicationRecord
  include Filterable
  belongs_to :account
  belongs_to :category
  has_many :journal_items, class_name: "Journal::JournalItem"
  has_many :payments, class_name: "Spree::Payment", foreign_key: 'journal_transaction_id'
  has_many :invoice_payments, class_name: "Spree::InvoicePayment", foreign_key: 'journal_transaction_id'
  has_many :bills, class_name: "Journal::Bill", foreign_key: 'journal_transaction_id'
  has_many :journal_invoices, class_name: "Journal::Invoice", foreign_key: 'journal_transaction_id'
  has_many :containers, class_name: "Spree::Container", foreign_key: 'journal_transaction_id'
  has_many :journal_invoice_payments, class_name: "Journal::InvoicePayment", foreign_key: 'journal_transaction_id'
  has_many :bill_payments, class_name: "Journal::BillPayment", foreign_key: 'journal_transaction_id'
  has_many :histories, class_name: "Journal::TransactionHistory", foreign_key: 'journal_transaction_id'
  has_many :income_transfer_funds, class_name: "Journal::TransferFund", foreign_key: 'income_transaction_id'
  has_many :expense_transfer_funds, class_name: "Journal::TransferFund", foreign_key: 'expense_transaction_id'
  accepts_nested_attributes_for :journal_items, reject_if: :all_blank, allow_destroy: true

  after_create :create_item
  after_update :update_status
  after_create :update_status

  attr_accessor :account_counter_part
  scope :id_is, -> (id){where id: id}
  scope :account_is, -> (account){where(account_id: account)}
  scope :status_is, -> (status) {where status: status}
  scope :name_contain, -> (acc_name) {where("pemilik_rekening like ?", "%#{acc_name}%")}
  scope :description_contain, -> (desc) {where("description like ?", "%#{desc}%")}
  scope :amount_is, -> (amount) {where(amount: amount)}
  scope :processing, -> {where status: "processing"}
  scope :incomplete, -> {where.not(status: ["Complete", "Expense"])}
  scope :not_journal, -> {where(is_journal_item: false)}
  scope :start_date, -> (start_date) { where("created_at >= ?", start_date.to_date.beginning_of_day) }
  scope :end_date, -> (end_date) { where("created_at <= ?", end_date.to_date.end_of_day) }
  scope :not_expense, -> {where(is_expense: false)}

  def update_status
    if self.used_amount.to_i == self.amount.to_i.abs
      self.update_columns(status: "Complete")
    elsif self.used_amount.to_i == 0
      self.update_columns(status: "Unprocessable")
    else
      self.update_columns(status: "Processing")
    end
  end

  def create_item
    return if is_journal_item?
    Journal::JournalItem.create_item(self.amount, self.description, self.account_id, self.category_id, 
      self.is_expense, self.id, self.account.currency, "Journal::System")
  end

  def outstanding_balance
    if is_expense?
      (amount + used_amount.to_i) * -1
    else
      amount - used_amount.to_i
    end  
  end

  def process_counter_part(account_counter_part)
    Journal::Transaction.transaction do
      account_counterpart = Journal::Account.find_by_id(account_counter_part)
      journal_entry = Journal::JournalEntry.create(description: "Counter part from Transaction #{self.id}", date: DateTime.now)
      if amount >= 0
        # credit
        Journal::JournalItem.create(account_id: account_counterpart.id, description: "Counter part from Transaction #{self.id}", credit: amount.abs, currency: account_counterpart.currency, originator: self, journal_entry_id: journal_entry.id)
        # debit
        Journal::JournalItem.create(account_id: self.account_id, description: "Counter part from Transaction #{self.id}", debit: amount.abs, currency: self.account.currency, originator: self, journal_entry_id: journal_entry.id)
      else
        # debit
        Journal::JournalItem.create(account_id: account_counterpart.id, description: "Counter part from Transaction #{self.id}", debit: amount.abs, currency: account_counterpart.currency, originator: self, journal_entry_id: journal_entry.id)
        # credit
        Journal::JournalItem.create(account_id: self.account_id, description: "Counter part from Transaction #{self.id}", credit: amount.abs, currency: self.account.currency, originator: self, journal_entry_id: journal_entry.id)
      end
      self.update_columns(status: "Complete", used_amount: self.amount)
    end
  end

  def move_to_store_credit(user_id, outstanding_balance, whodunnit)
    sc = Spree::StoreCredit.create!(user_id: user_id, amount: outstanding_balance, category_id: 6, currency: "IDR", created_by: whodunnit, memo: "Overpaid From Transaction id #{id}", action_originator: self)
    store_credit_id = Journal::Account.find_by_code("2-20219").id
    self.histories.create(originator: sc, name: sc.user.name, description: "Store Credit #{sc.user.name} ID #{sc.id}")
    journal_entry = Journal::JournalEntry.create(description: "Overpaid from Transaction #{self.id}", date: DateTime.now)
    # credit
    Journal::JournalItem.create(account_id: store_credit_id, description: "Overpaid from Transaction #{self.id}", credit: outstanding_balance, currency: "IDR", originator: sc, journal_entry_id: journal_entry.id)
    # debit
    Journal::JournalItem.create(account_id: self.account_id, description: "Overpaid from Transaction #{self.id}", debit: outstanding_balance, currency: "IDR", originator: sc, journal_entry_id: journal_entry.id)
  end

  def record_purchase(purchase_order, whodunnit)
    
  end

  def self.import(file, account_id)
    CSV.foreach(file.path, headers: true) do |row|
      @transaction = Journal::Transaction.new()
      @transaction.description = row["description"]
      amount = row["amount"].to_i
      if !row["transaction_date"].nil?
        @transaction.transaction_date = row["transaction_date"].to_date
      end
      @transaction.amount = amount
      @transaction.account_id = account_id
      @transaction.pemilik_rekening = row["pemilik_rekening"]
      if row["amount"].to_i < 0
        @transaction.is_expense = true
      else
        @transaction.is_expense = false
      end
      @transaction.save!
    end
  end

end
