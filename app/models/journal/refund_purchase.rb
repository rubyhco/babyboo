class Journal::RefundPurchase < ApplicationRecord
  has_many :items, class_name: "Journal::RefundPurchaseItem", dependent: :destroy
  belongs_to :bill, class_name: "Journal::Bill"

  validates :number, presence: true, uniqueness: { allow_blank: true }

  before_validation :generate_number, on: :create

  def create_journal
    Journal::RefundPurchase.transaction do
      description = "Refund #{number} " + name
      accrued_inventory = Journal::Account.find_by_code('1-10201')
      if bill.currency == "IDR"
        supplier_deposit = Journal::Account.find_by_code('1-10003')
        journal_entry_idr = Journal::JournalEntry.create(description: description, date: DateTime.now)
        journal_credit = Journal::JournalItem.create(account_id: accrued_inventory.id, description: description, credit: total , currency: bill.currency, journal_entry_id: journal_entry_idr.id)
        journal_debit = Journal::JournalItem.create(account_id: supplier_deposit.id, description: description, debit: total , currency: bill.currency, journal_entry_id: journal_entry_idr.id)
      else
        supplier_deposit = Journal::Account.find_by_code('1-10011')
        accrued_inventory_rmb = Journal::Account.find_by_code('1-10202')
        total_idr = 0
        account_supplier_id = Spree::VariantKur.where(variant_id: items.first.variant_id).first.transfer_fund.to
        items.each do |item|
          ids = Spree::VariantKur.where(variant_id: item.variant_id).pluck(:transfer_fund_id)
          total_item = item.total
          Journal::TransferFund.where(id: ids).each do |transfund|
            break if total_item.zero?
            next if transfund.amount_used.zero?
            amount_used_rmb = transfund.amount_used
            amount_to_take = [total_item, amount_used_rmb].min

            adjustment = amount_to_take * transfund.kurs
            total_idr += adjustment
            transfund.update_attributes(amount_used: transfund.amount_used - amount_to_take)
            total_item -= amount_to_take
          end
        end
        journal_entry_rmb = Journal::JournalEntry.create(description: description, date: DateTime.now)
        journal_credit_rmb = Journal::JournalItem.create(account_id: accrued_inventory_rmb.id, description: description, credit: total , currency: "RMB", journal_entry_id: journal_entry_rmb.id)
        journal_debit_rmb = Journal::JournalItem.create(account_id: account_supplier_id, description: description, debit: total , currency: "RMB", journal_entry_id: journal_entry_rmb.id)
        
        journal_entry_idr = Journal::JournalEntry.create(description: description, date: DateTime.now)
        journal_credit_idr = Journal::JournalItem.create(account_id: accrued_inventory.id, description: description, credit: total_idr , currency: "IDR", journal_entry_id: journal_entry_idr.id)
        journal_debit_idr = Journal::JournalItem.create(account_id: supplier_deposit.id, description: description, debit: total_idr , currency: "IDR", journal_entry_id: journal_entry_idr.id)
      end
    end
  end

  def generate_number(options = {})
    options[:length]  ||= 5
    options[:letters] ||= false
    options[:prefix]  ||= "REF"

    possible = (0..9).to_a
    possible += ('A'..'Z').to_a if options[:letters]

    self.number ||= loop do
      # Make a random number.
      random = "#{options[:prefix]}#{(0...options[:length]).map { possible.sample }.join}"
      # Use the random  number if no other order exists with it.
      if self.class.exists?(number: random)
        # If over half of all possible options are taken add another digit.
        options[:length] += 1 if self.class.count > (10**options[:length] / 2)
      else
        break random
      end
    end
  end
end
