class Journal::TransactionHistory < ApplicationRecord
	belongs_to :journal_transaction, class_name: "Journal::Transaction"
	belongs_to :originator, polymorphic: true

	def amount_dp
		return 0 if self.originator_type != "Spree::Payment"
		if self.originator.order.preorder?
			self.originator.amount
		else
			return 0
		end
	end

	def amount_sales
		if self.originator_type == "Spree::Payment" 
			if self.originator.order.preorder?
				return 0
			else
				self.originator.amount
			end
		elsif self.originator_type == "Spree::InvoicePayment"
			self.originator.amount
    else
    	return 0
    end
	end

	def amount_cn

		if self.originator_type == "Journal::InvoicePayment"
			self.originator.amount
		elsif self.originator_type == "Journal::Invoice"
			self.originator.total
		else 
			0
		end

	end

	def amount_store_credit
		if self.originator_type == "Spree::StoreCredit"
			self.originator.amount
		else
			return 0
		end
	end

	def originator_description
		case originator_type
    when "Spree::Payment"
      "Payment Order #{originator.order.number}"
    when "Spree::InvoicePayment"
      "Payment Invoice #{originator.invoice.present? ? originator.invoice.id : "Tidak ada"}"
    when "Journal::InvoicePayment"
      "Payment Credit Note #{originator.invoice.number}"
    when "Journal::Invoice"
      "Payment Credit Note #{originator.number}"
    when "Spree::StoreCredit"
      "Store Credit: #{originator.memo}"
    else
    	description
    end
	end
end
