Spree::Money.class_eval do
  def to_s
    formatted = @money.format(@options)
    formatted.gsub(/.00$/, "")
  end

  def to_html(options = { :html => true })
    to_s
  end
end