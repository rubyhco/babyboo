Spree::OrderUpdater.class_eval do
  def update_item_total
    order.item_total = line_items.to_a.sum(&:amount)
    update_down_payment_total
    update_item_price
  end
  
  def update_item_price
    line_items.each do |line_item|
      break if line_item.order.completed?
      next if !line_item.inventory_units.empty? && line_item.inventory_units.first.canceled?
      line_item.set_pricing_attributes
      line_item.save
    end
    update_order_total
  end

  def update_down_payment_total
    return if !order.preorder?
    order.down_payment_total = 0
    line_items.each do |line_item|
      next if !line_item.inventory_units.empty? && line_item.inventory_units.first.canceled?
      order.down_payment_total += line_item.down_payment_price.to_i * line_item.quantity
    end
  end

  def update_payment_state
    last_state = order.payment_state
    if payments.present? && payments.valid.size == 0 && order.outstanding_balance != 0
      order.payment_state = 'failed'
    elsif order.state == 'canceled' && order.payment_total == 0
      order.payment_state = 'void'
    else
      order.payment_state = 'balance_due' if order.outstanding_balance > 0
      order.payment_state = 'credit_owed' if order.outstanding_balance < 0
      if !order.outstanding_balance?
        order.payment_state = 'paid' 
        if last_state != order.payment_state
          post_journal_inventory_from_order if !order.preorder?
          post_journal_revenue_and_shipping_from_order if !order.preorder?
          create_movement_warehouse_from_order unless order.preorder?
        end
      end
    end
    order.state_changed('payment') if last_state != order.payment_state
    order.payment_state
  end

  def send_sms
    sms = RajaSms.new
    phone = order.dropship? ? order.dropship_phone : order.ship_address.phone
    if order.preorder?
      sms.send_sms(phone, "Terimakasih. Pembayaran untuk pesanan #{order.number} sebesar #{order.down_payment_total} 
        telah kami terima. Pesanan Anda akan segera kami proses -BrandedBabys.com")
    else
      sms.send_sms(phone, "Terimakasih. Pembayaran untuk pesanan #{order.number} sebesar #{order.display_total.to_html} 
        telah kami terima. Pesanan Anda akan segera kami proses -BrandedBabys.com")
    end
  end

  def post_journal_inventory_from_order
    @inventory = Journal::Account.find_by_code("1-10203").id
    @cost_of_sales = Journal::Account.find_by_code("5-50401").id

    order.line_items.each do |item|
      amount_cost_price = item.real_cost_price
      journal_entry = Journal::JournalEntry.create(description: "Order [Inventory]: #{order.number} - #{item.quantity} X #{item.variant.sku}", date: DateTime.now)
      # credit inventory
      Journal::JournalItem.create(account_id: @inventory, description: "Order [Inventory]: #{order.number} - #{item.quantity} X #{item.variant.sku}", credit: amount_cost_price, currency: order.currency, originator: item, journal_entry_id: journal_entry.id)
      # debit cost of sales
      Journal::JournalItem.create(account_id: @cost_of_sales, description: "Order [Inventory]: #{order.number} - #{item.quantity} X #{item.variant.sku}", debit: amount_cost_price, currency: order.currency, originator: item, journal_entry_id: journal_entry.id)
    end
  end

  def create_movement_warehouse_from_order
    order.line_items.each do |item|
      item.variant.stock_items.first.stock_warehouse_movements.create(quantity: item.total_line_items * -1, description: "Shipment from Order #{order.number} - #{order.user.present? ? order.user.name : "Cannot Display Cust Name"}", originator: order)
    end
  end

  def post_journal_revenue_and_shipping_from_order
    transisi_revenue_id = Journal::Account.find_by_code("4-40208").id
    revenue_id = Journal::Account.find_by_code("4-40201").id
    shipment_account_id = order.account_journal_shipping.id
    journal_entry = Journal::JournalEntry.create(description: "Order Paid: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", date: DateTime.now)
    #debit
    Journal::JournalItem.create(account_id: transisi_revenue_id, description: "Order Paid: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", debit: order.total, currency: order.currency, originator: order, journal_entry_id: journal_entry.id)
    # credit
    Journal::JournalItem.create(account_id: revenue_id, description: "Order Paid: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", credit: order.item_total, currency: order.currency, originator: order, journal_entry_id: journal_entry.id)
    # credit
    Journal::JournalItem.create(account_id: shipment_account_id, description: "Order Paid: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", credit: order.shipping_cost, currency: order.currency, originator: order, journal_entry_id: journal_entry.id)
  end
end