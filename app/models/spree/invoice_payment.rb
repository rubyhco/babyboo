class Spree::InvoicePayment < Spree::Base
  
  include Filterable

  belongs_to :invoice, class_name: "Spree::Invoice"
  belongs_to :payment_confirmation, class_name: "Spree::PaymentConfirmation"
  belongs_to :journal_transaction, class_name: "Journal::Transaction"
  belongs_to :user, class_name: "Spree::User"
  scope :created_at_is, -> (date){where('created_at BETWEEN ? AND ?',date.first.to_datetime.beginning_of_day, date.first.to_datetime.end_of_day)}
  
  scope :name_user, -> (name_user){joins(invoice: :user).where("spree_users.name like ?", "%#{name_user}%")}
  scope :number, -> (number){joins(:invoice).where("spree_invoices.id = ?", number)}

  scope :valid, -> {where(state: "checkout")}
  scope :paid, -> {where(state: "paid")}

  after_commit :post_to_journal

  def payment_method_is_store_credit?
    payment_method_id == Spree::PaymentMethod::StoreCredit.first.id
  end

  def customer_down_payment_journal_account_id
    @customer_down_payment = Journal::Account.find_by_code("2-20218").id
  end

  def store_credit_journal_account_id
    @store_credit = Journal::Account.find_by_code("2-20219").id
  end

  def transisi_revenue_journal_account_id
    @transisi_revenue = Journal::Account.find_by_code("4-40208").id
  end

  def journal_transaction_account_id
    if journal_transaction.nil?
      @journal_transaction_account_id = Journal::Account.find_by_code("1-10001").id
    else
      @journal_transaction_account_id = journal_transaction.account_id
    end
    @journal_transaction_account_id
  end

  def post_to_journal
    return if is_cn?
    return if state != "paid"
    return if amount <= 0
    Spree::InvoicePayment.transaction do
      if payment_method_is_store_credit?
        post_invoice_with_store_credit
      else
        post_invoice
      end
    end
  end

  def post_invoice_with_store_credit
    journal_entry = Journal::JournalEntry.create(description: "Payment [Store Credit]: #{invoice.number} - #{invoice.user.name}", date: DateTime.now)
    # credit DP
    Journal::JournalItem.create(account_id: transisi_revenue_journal_account_id, description: "Payment [Store Credit]: #{invoice.number} - #{invoice.user.name}", credit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
    # debit Store Credit
    Journal::JournalItem.create(account_id: store_credit_journal_account_id, description: "Payment [Store Credit]: #{invoice.number} - #{invoice.user.name}", debit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
  end

  def post_invoice
    journal_entry = Journal::JournalEntry.create(description: "Payment: #{invoice.number} - #{invoice.user.name}", date: DateTime.now)
    # credit DP
    Journal::JournalItem.create(account_id: transisi_revenue_journal_account_id, description: "Payment: #{invoice.number} - #{invoice.user.name}", credit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
    # debit Store Credit
    Journal::JournalItem.create(account_id: journal_transaction_account_id, description: "Payment: #{invoice.number} - #{invoice.user.name}", debit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
  end
end
