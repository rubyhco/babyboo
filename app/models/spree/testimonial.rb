module Spree
  class Testimonial < Spree::Base
    scope :published, -> {where(publish: true)}
  end
end
