Spree::OrderInventory.class_eval do
	def verify(shipment = nil)
    if order.completed? || shipment.present?
      if inventory_units.size < line_item.quantity
        quantity = line_item.quantity - inventory_units.size

        shipment = determine_target_shipment unless shipment
        add_to_shipment(shipment, quantity)
      elsif inventory_units.size > line_item.quantity
        remove(inventory_units, shipment)
      end
    end
  end

  private

  def add_to_shipment(shipment, quantity)
    if variant.should_track_inventory?
      on_hand, back_order = shipment.stock_location.fill_status(variant, quantity)

      on_hand.times { shipment.set_up_inventory('on_hand', variant, order, line_item) }
      back_order.times { shipment.set_up_inventory('backordered', variant, order, line_item) }
    else
      quantity.times { shipment.set_up_inventory('on_hand', variant, order, line_item) }
    end
    # adding to this shipment, and removing from stock_location
    if order.completed? && order.approver_id.nil?
      shipment.stock_location.unstock(variant, quantity, shipment)
    end

    quantity
  end
end
