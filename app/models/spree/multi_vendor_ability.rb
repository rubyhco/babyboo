module Spree
  class MultiVendorAbility
    include CanCan::Ability

    def initialize(user)
      user ||= Spree::User.new # guest user (not logged in)
      if user.admin?
        can :admin, :all
        puts "IS admin"
      elsif user.sales?
        # ORDER Tabs
        can :admin, Spree::Order
        can :manage, Spree::Order
        can :manage, Spree::LineItem
        can :manage, Spree::Variant
        can :read, Spree::OptionValuesVariant
        can :read, Spree::OptionValue
        can :read, Spree::OptionType
        can :read, Spree::Price
        can :read, Spree::Image
        can :read, Spree::StockItem
        can :read, Spree::StockLocation
        can :read, Spree::VariantPropertyRule
        can :read, Spree::Product
        can :admin, Spree::Adjustment
        can :manage, Spree::Adjustment
        can :admin, Spree::Payment
        can :manage, Spree::Payment
        can :admin, Spree::PaymentMethod
        can :manage, Spree::PaymentMethod
        can :admin, Spree::ReturnAuthorization
        can :manage, Spree::ReturnAuthorization
        can :admin, Spree::CustomerReturn
        can :manage, Spree::CustomerReturn
        can :admin, Spree::OrderCancellations
        can :manage, Spree::OrderCancellations

        #address User
        # can :read, Spree::User
        # can :read, Spree::Address
        # can :read, Spree::State
        # can :read, Spree::City
        # can :read, Spree::Country

        # Promotion Tabs
        can :admin, Spree::Promotion
        can :manage, Spree::Promotion
        # Stock Item Tabs
        can :admin, Spree::StockItem
        can :manage, Spree::StockItem
        can :admin, Spree::StockTransfer
        can :manage, Spree::StockTransfer
        # Invoice Tabs
        can :admin, Spree::Invoice
        can :manage, Spree::Invoice
        # Shipment Tabs
        can :admin, Spree::Shipment
        can :manage, Spree::Shipment
      elsif user.products?
        # All Tabs Product

        # Bagian Product aja
        can :admin, Spree::Product
        can :manage, Spree::Product
        can :manage, Spree::ProductOptionType
        can :manage, Spree::OptionType
        can :manage, Spree::ProductProperty
        can :manage, Spree::Variant
        can :manage, Spree::OptionValuesVariant
        can :manage, Spree::PromotionRule
        can :manage, Spree::ProductPromotionRule
        can :manage, Spree::TaxCategory
        can :manage, Spree::ShippingCategory

        # view product
        can :admin, Spree::Image
        can :manage, Spree::Image
        can :admin, Spree::Price
        can :manage, Spree::Price
        can :admin, Spree::PreorderPrice
        can :manage, Spree::PreorderPrice
        can :admin, Spree::StockItem
        can :manage, Spree::StockItem

        can :admin, Spree::OptionType
        can :manage, Spree::OptionType
        can :admin, Spree::Property
        can :manage, Spree::Property
        can :admin, Spree::Taxonomy
        can :manage, Spree::Taxonomy
        can :admin, Spree::Taxon
        can :manage, Spree::Taxon
        can :admin, Spree::ProductImport
        can :manage, Spree::ProductImport
        # tabs display order tambahan
        can :manage, Spree::PromotionRuleTaxon
        can [:admin, :index, :read, :update, :edit], :taxons
      elsif user.warehouse?
        # Stock Item Tabs
        can :admin, Spree::StockItem
        can :manage, Spree::StockItem
        can :admin, Spree::StockTransfer
        can :manage, Spree::StockTransfer

        # Shipment Tabs
        can :admin, Spree::Shipment
        can :manage, Spree::Shipment
      elsif user.finance?
        # Order Tabs
        can :admin, Spree::Order
        can :manage, Spree::Order
        can :manage, Spree::LineItem
        can :manage, Spree::Variant
        can :read, Spree::Product
        can :admin, Journal::Transaction
      elsif user.operations?
        can :manage, :all            
      else
        can :read, :all
        puts "NOT admin"
      end
    end
  end
end