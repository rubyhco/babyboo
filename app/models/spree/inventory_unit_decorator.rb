Spree::InventoryUnit.class_eval do
  include Filterable
  scope :ready_to_invoice, -> { Spree::InventoryUnit.on_hand.joins(:order).where("spree_orders.preorder=? && 
    spree_orders.payment_state = ?", true, "paid").where.not(id: Spree::InvoiceItem.pluck(:inventory_unit_id)) }

  scope :shipped, -> {where(state: "shipped")}
  scope :preorder, -> { Spree::InventoryUnit.on_hand.joins(:order).where("spree_orders.preorder=? && 
    spree_orders.payment_state = ?", true, "paid")}
  scope :canceled, -> {where(state: "canceled")}
  scope :dp_hangus, -> { Spree::InventoryUnit.canceled.joins(:order, :line_item).where("spree_orders.preorder=? && 
    spree_orders.payment_state = ? && spree_line_items.down_payment_price > ?", true, "paid", 0)}
  scope :start_date, -> (start_date) { where("spree_inventory_units.created_at >= ?", start_date) }
  scope :end_date, -> (end_date) { where("spree_inventory_units.created_at <= ?", end_date) }
  scope :order_number_contain, -> (number) {Spree::InventoryUnit.joins(:order).where("spree_orders.number like ?", "%#{number}%")}
  scope :not_split, ->{where(split_invoice: false)}
  scope :split, ->{where(split_invoice: true)}
  scope :pending_po, -> {Spree::InventoryUnit.backordered.joins(:order).where("spree_orders.state = ? && spree_orders.preorder = ? && 
    spree_orders.payment_state != ?","complete", true, "void")}

  has_one :invoice_item, class_name: "Spree::InvoiceItem", foreign_key: "inventory_unit_id"
  self.whitelisted_ransackable_associations = %w[order line_item variant shipment]
  self.whitelisted_ransackable_attributes = %w[id state variant_id order_id line_item_id]


  def order_payment_state_human
    case order.payment_state
    when "paid"
      if (order.preorder? && state == "on_hand") || (order.preorder? && state == "backordered")
        if invoice_item.present?
          if invoice_item.invoice.status == "pending_payment"
            "Pending on Invoice"
          elsif invoice_item.invoice.status == "canceled"
            "Invoice Canceled"
          else
            "Invoice Paid"
          end
        else
          "Dp Confirmed"
        end
      elsif state == "canceled"
        "Cancel Item"
      elsif state == "shipped"
        "Shipped Item"
      else
        "Paid"
      end
    when "balance_due"
      if !order.payment_confirmations.empty?
        "waiting for confirmation"
      else
        if order.preorder?
          "Pending on DP"
        else
          "Pending"
        end
      end
    when "void"
      "Cancel Order"
    when "credit_owed"
      "Overpayment"
    end
  end

  def state_to_human
    case state
    when "on_hand"
      "Item Ready"
    when "backordered"
      "Item Pending"
    when "canceled"
      "Item Canceled"
    when "returned"
      "Item Returned"
    when "shipped"
      "Item Shipped"
    end
  end

  def update_free_stock(value)
    stock_item = self.variant.stock_items.first
    stock_item.update_columns(free_stock: stock_item.free_stock + value)
  end

  def outstanding_unit
    total_pieces = expected_shipped > 0 ? expected_shipped : line_item.total_pieces
    total_pieces - shipped.to_i
  end

  def expected_shipped_in_pack
    expected_shipped / line_item.qty_per_pack
  end

  def total_pieces
    if split_invoice?
      expected_shipped
    else
      line_item.total_line_items
    end
  end

  def line_item_or_iu_expected_shipped_in_pack
    if split_invoice?
      expected_shipped_in_pack
    else
      line_item.quantity
    end
  end
end