Spree::StockItem.class_eval do

  self.whitelisted_ransackable_associations = %w[variant]

  scope :ready_stock, -> {where("count_on_hand > ?", 0)}
  has_many :stock_warehouse_movements, class_name: 'Spree::StockWarehouseMovement', foreign_key: 'stock_item_id'

  def last_transaction_line
    Spree::LineItem.joins(:order).where("spree_orders.payment_state = ? && variant_id = ?", "paid", variant_id).last
  end

  def self.update_stock_query(data_json, field_desc="count_on_hand")
    status = []
    data_json.each do |data|
      stock = Spree::StockItem.joins(:variant).where("spree_variants.sku = ?", data[:sku]).last
      if stock && field_desc
        if field_desc == "count_on_hand"
          stock.update_columns(count_on_hand: data[:count_on_hand].to_i)
        else 
          stock.update_columns(stock_warehouse: data[:stock_warehouse].to_i)
        end
        status.push(sku: data[:sku], message: "berhasil")
      else
        status.push(sku: data[:sku], message: "gagal")
      end
    end
    puts status
  end

  def can_supply_qty?(qty)
    return false if count_on_hand >= free_stock
    return real_qty_free_stock >= qty
  end

  def real_qty_free_stock
    free_stock - count_on_hand
  end

  def console_move
    admin = Spree::User.find_by_email("tiaraitalyana@gmail.com")
    stock_movements.create(quantity: count_on_hand.abs, originator: admin)
  end

  def paid_backordered_inventory_units
    backordered_inventory_units.includes(:order).where("spree_orders.payment_state = ?", "paid")
  end

  def adjust_free_stock(value)
    self.free_stock = free_stock + value
    save!
  end

  def adjust_stock_warehouse(value)
    self.stock_warehouse = stock_warehouse + value

    save!
  end

  def process_backorders(number)
    if number > 0
      paid_backordered_inventory_units.each do |o|
        break if number == 0
        qty_pcs_iu = o.line_item.quantity * o.line_item.qty_per_pack
        if qty_pcs_iu <= number
          o.fill_backorder
          number -= qty_pcs_iu
        end
      end
      if number > 0
        backordered_inventory_units.each do |o|
          break if number == 0
          qty_pcs_iu = o.line_item.quantity * o.line_item.qty_per_pack
          if qty_pcs_iu <= number
            o.fill_backorder
            number -= qty_pcs_iu
          end
        end
      end
    end
  end
end