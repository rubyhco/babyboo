 module Spree
  class VariantKur < Spree::Base
    belongs_to :variant, class_name: "Spree::Variant"
    belongs_to :transfer_fund, class_name: "Journal::TransferFund"
  end
end
