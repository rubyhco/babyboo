Spree::StoreCredit.class_eval do
  include Filterable
  DEFAULT_CREATED_BY_EMAIL = "system@brandedbabys.com"
  scope :start_date, -> (start_date) { where("created_at >= ?", start_date) }
  scope :end_date, -> (end_date) { where("created_at <= ?", end_date) }
  scope :name_user, -> (name){joins(:user).where("name like ?", "%#{name}%")}
  scope :created_at_is, -> (date){where('created_at BETWEEN ? AND ?',date.first.to_datetime.beginning_of_day, date.first.to_datetime.end_of_day)}
  scope :number, -> (number) {where("id = ?", number)}
  after_create :record_to_journal

  self.whitelisted_ransackable_attributes = %w[category_id created_at updated_at created_by_id]

  class << self
    def default_created_by
      Spree.user_class.find_by(email: DEFAULT_CREATED_BY_EMAIL)
    end
  end

  private

  def record_to_journal
    return if created_by.email == DEFAULT_CREATED_BY_EMAIL

    store_credit_journal_account_id = Journal::Account.find_by_code("2-20219").id
    journal_entry = Journal::JournalEntry.create(description: "Kategori: #{category.present? ? category.name : "Default"} - #{memo}", date: DateTime.now)
    # credit store credit
    Journal::JournalItem.create(account_id: store_credit_journal_account_id, description: "Kategori: #{category.present? ? category.name : "Default"} - #{memo}", credit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
    # debit sales sales_discount_id
    Journal::JournalItem.create(account_id: category.journal_account_id, description: "Kategori: #{category.present? ? category.name : "Default"} - #{memo}", debit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
  end
end