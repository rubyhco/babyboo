module Spree
  class PurchaseLineItem < Spree::Base
    include Filterable
    belongs_to :purchase_order, class_name: "Spree::PurchaseOrder"
    belongs_to :variant, class_name: "Spree::Variant"
    has_many :container_items, class_name: "Spree::ContainerItem"

    has_many :journal_bill_items, as: :originator, class_name: "Journal::BillItem"

    before_update :total_adjustment
    after_update :po_adjustment

    scope :shipped, -> {where(shipment_status: "shipped")}
    scope :in_completed, -> {where(shipment_status: "in_completed")}
    scope :completed, -> {where(shipment_status: "completed")}
    scope :sku_contain, -> (sku) {joins(:variant).where("spree_variants.sku like ?", "%#{sku}%")}
    scope :name_contain, -> (name_con) {joins(variant: :product).where("spree_products.name like ?", "%#{name_con}%")}
    scope :zero_cost_price, -> (zero) {where(cost_price: 0)}

    scope :pending_payment, -> {where(payment_state: ['pending_payment', 'partially_paid'])}
    scope :cart_payment, -> {where(payment_state: 'cart')}

    self.whitelisted_ransackable_associations = %w[variant purchase_order]
    self.whitelisted_ransackable_attributes = %w[payment_state shipment_status]
    

    state_machine :shipment_status, initial: :pending do
      event :ship do  
        transition to: :shipped, from: [:in_completed, :pending]
      end

      event :in_complete do
        transition to: :in_completed, from: [:shipped, :pending], if: :is_incompleted?
      end

      event :complete do
        transition to: :completed, from: [:pending, :in_completed, :shipped], if: :is_completed?
      end

      after_transition to: :completed, do: :after_completed

      after_transition any => any, :do => :update_purchase_order
    end

    state_machine :payment_state, initial: :cart do
      event :pending_payment do  
        transition to: :pending_payment, from: :cart
      end

      event :partially_pay do  
        transition to: :partially_paid, from: :pending_payment
      end

      event :pay do
        transition to: :paid, from: [:pending_payment, :partially_paid]
      end

      after_transition to: :paid, do: :after_paid
    end


    def is_incompleted?
      return false if container_items.nil?

      self.container_items.sum(:qty_received) < items
    end

    def is_completed?
      return false if container_items.nil?
      return true if !completed_by.nil?

      self.container_items.sum(:qty_received) >= items
    end

    def update_purchase_order
      purchase_order.update_shipment_state!
    end

    def total_received
      container_items.sum(:qty_received)
    end

    def outstanding_pieces
      items - total_received
    end

    def mark_completed(user)
      update_expected_coh
      self.completed_by = user.id
      self.save
      self.pay! unless self.paid?
      self.complete! unless self.completed?
    end

    def update_expected_coh
      qty_adjust = container_items.sum(:qty_received) - items
      stock = variant.stock_items.first
      total_update = stock.expected_count_on_hand + qty_adjust
      stock.update_attributes(expected_count_on_hand: total_update) unless total_update < 0
    end

    def update_cost_price_variant
      bill_item_variants = Journal::BillItem.where(originator_type: "Spree::PurchaseLineItem", originator_id: id).joins(:bill).where("journal_bills.state = ?", "confirmed")
      if bill_item_variants.present?
        total_cost_price = 0
        bill_item_variants.each do |item|
          total_cost_price += item.amount * item.quantity
        end
        average_cost_price = total_cost_price/bill_item_variants.sum(:quantity)
      else
        average_cost_price = self.cost_price
      end
      variant.update_columns(cost_price: average_cost_price)
    end

    def excess_shipping?
      items < container_items.sum(:qty_received)
    end

    def shortage?
      container_items.sum(:qty_received) < items 
    end

    def update_after_change_item
      items = quantity * variant.product.total_items
      total = items * cost_price
      outstanding_invoice_qty = quantity * variant.product.total_items
      self.save
    end

    private

    def after_completed
      # container_items.collect{|x|x.}
    end

    def after_paid
      if Spree::PurchaseLineItem.where(purchase_order: purchase_order_id, payment_state: "paid").count == Spree::PurchaseLineItem.where(purchase_order: purchase_order_id).count
        purchase_order.pay!
      end
      variant.update_columns(cost_price: variant.cost_price * variant.real_kurs, cost_currency: "IDR") unless variant.cost_currency == "IDR" && completed_by.present?
    end

    def total_adjustment
      qty = self.quantity.nil? ? 0 : self.quantity
      self.items = qty * self.variant.product.total_items
      self.total = self.items * self.cost_price
    end

    def po_adjustment
      po = self.purchase_order
      po.total = po.items.to_a.sum(&:total)
      po.save
    end
  end
end