class Spree::PaymentConfirmationItem < ApplicationRecord
  belongs_to :payment_confirmation, class_name: "Spree::PaymentConfirmation"
  belongs_to :order, class_name: "Spree::Order"
  belongs_to :invoice, class_name: "Spree::Invoice"

  after_create :create_payment

  private

  def create_payment
    payment_method = Spree::PaymentMethod.where(type: "Spree::PaymentMethod::BankTransfer").first
    create_order_payment(payment_method)
    create_invoice_payment(payment_method)
  end

  def create_order_payment(payment_method)
    return if self.order.nil?
    self.order.payments.create!(
      payment_method: payment_method,
      amount: self.order.preorder? ? self.order.down_payment_total : self.order.total,
      state: 'checkout',
      bank_name: self.payment_confirmation.bank_sender,
      account_name: self.payment_confirmation.sender_name,
      deposited_on: self.payment_confirmation.transaction_date
    )
  end

  def create_invoice_payment(payment_method)
    return if self.invoice.nil?
    self.invoice.payments.create!(
      payment_method_id: payment_method.id,
      amount: self.invoice.total,
      state: 'checkout',
      payment_confirmation_id: self.payment_confirmation.id
    )
  end
end
