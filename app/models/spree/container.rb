module Spree
  class Container < Spree::Base
    include Filterable
    validates :name, :delivery_orders, :delivery_date, :sack, :cbm, presence: true
    validates :name, :delivery_orders, uniqueness: true

    has_many :items, class_name: "Spree::ContainerItem", dependent: :destroy
    belongs_to :transaction_id, class_name: "Journal::Transaction"
    belongs_to :supplier, class_name: "Spree::Supplier"

    scope :name_user, -> (name){where("name like ?", "%#{name}%")}
    scope :id_is, -> (id_value){where(id: id_value)}
    scope :sku_contain, -> (sku) {joins(items: :variant).where("spree_variants.sku like ?", "%#{sku}%")}

    self.whitelisted_ransackable_attributes = %w[id delivery_orders name]


    state_machine :status, initial: :pending do
      event :receive do
        transition to: :received
      end
      after_transition to: :received, do: :after_received

      event :complete do
        transition to: :completed, from: :received
      end
    end

    state_machine :payment_state, initial: :pending do
      event :pay do
        transition to: :paid, from: :pending
      end
    end

    def total_order_qty
      items.sum(:total_items)
    end

    def total_order_received_qty
      items.sum(:qty_received)
    end

    def excess_shipping?
      total_order_qty < total_order_received_qty
    end

    def shortage?
      total_order_received_qty < total_order_qty
    end

    def biaya_per_pcs
      return 0 if biaya_import <= 0 || total_order_received_qty <= 0
      (biaya_import/total_order_received_qty).ceil
    end

    private

    def after_received
      if total_order_qty == total_order_received_qty
        complete!
      end
    end
  end
end
