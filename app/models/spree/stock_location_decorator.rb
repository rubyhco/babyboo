Spree::StockLocation.class_eval do

  def restock_backordered(variant, quantity, total_pieces, _originator = nil)
    item = stock_item_or_create(variant)
    item.update_columns(
      count_on_hand: item.count_on_hand + total_pieces,
      free_stock: item.free_stock + total_pieces,
      updated_at: Time.current
    )
    # move(variant, quantity, _originator)
  end 
end