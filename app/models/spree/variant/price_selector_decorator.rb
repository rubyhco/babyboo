Spree::Variant::PriceSelector.class_eval do
  def price_for_with_leveling(price_options, price_level=nil)
    prices = variant.currently_valid_prices.detect do |price|
        ( price.country_iso == price_options.desired_attributes[:country_iso] ||
          price.country_iso.nil?
        ) && price.currency == price_options.desired_attributes[:currency]
      end

    if price_level == "gold"
      prices.try!(:money_gold)
    elsif price_level == "silver"
      prices.try!(:money_silver)
    elsif price_level == "sale"
      prices.try!(:money_sale)
    else
      return price_for_without_leveling(price_options)
    end
  end

  alias_method_chain :price_for, :leveling
end