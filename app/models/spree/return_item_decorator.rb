Spree::ReturnItem.class_eval do
  scope :awaiting, -> {where(reception_status: "awaiting")}

  def self.from_inventory_unit(inventory_unit)
    valid.where(inventory_unit: inventory_unit).last ||
      new(inventory_unit: inventory_unit).tap(&:set_default_amount)
  end

  private

  def validate_no_other_completed_return_items
    false
  end

  def cancel_others

  end

  def process_inventory_unit!
    inventory_unit.return! unless inventory_unit.returned?

    if customer_return
      customer_return.stock_location.restock(inventory_unit.variant, quantity, customer_return) if should_restock?
      if !resellable?
        lost_item = Spree::LostItem.create(description: "Return Item Order #{inventory_unit.order.number} Not Resellable")
        # harusnya gausah dikurangin lagi stocknya
        # customer_return.stock_location.move(inventory_unit.variant, (quantity * -1), lost_item)
      end
      post_journal_inventory_from_return_item
      customer_return.process_return!
    end
  end

  def post_journal_inventory_from_return_item
    if !resellable
      #goods reject account IDR
      @inventory_return = Journal::Account.find_by_code("6-60352").id
    else
      #inventory account IDR
      @inventory_return = Journal::Account.find_by_code("1-10203").id
    end
    @cost_of_sales = Journal::Account.find_by_code("5-50401").id
    amount_cost_price = quantity * inventory_unit.variant.cost_price.to_i
    journal_entry = Journal::JournalEntry.create(description: "Return Item Order: #{inventory_unit.order.number} - #{quantity} pcs X #{inventory_unit.variant.sku}", date: DateTime.now)
    # credit cost of sales
    Journal::JournalItem.create(account_id: @cost_of_sales, description: "Return Item Order: #{inventory_unit.order.number} - #{quantity} pcs X #{inventory_unit.variant.sku}", credit: amount_cost_price, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
    # debit inventory
    Journal::JournalItem.create(account_id: @inventory_return, description: "Return item Order: #{inventory_unit.order.number} - #{quantity} pcs X #{inventory_unit.variant.sku}", debit: amount_cost_price, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
  end
end