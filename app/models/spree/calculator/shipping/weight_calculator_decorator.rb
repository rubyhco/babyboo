Spree::Calculator::Shipping::WeightCalculator.class_eval do
  def shipping_rate(object)
    if object.shipment.invoice.nil?
      # ready stock
      city = Spree::City.find_by_name(object.shipment.order.ship_address.city)
    else
      # preorder
      city = Spree::City.find_by_name(Spree::InvoiceItem.where(shipment_id: object.shipment.id).first.ship_address.city)
    end
    found = self.calculable.shipping_rates_per_methods.find_by_city_id(city.id)
    return found
  end

  def total_weight(contents, min_weight)
    weight = 0
    contents.each do |item|
      item_weight = item.variant.weight
      quantity = item.inventory_unit.expected_shipped > 0 ? (item.inventory_unit.expected_shipped/item.line_item.qty_per_pack) : item.inventory_unit.line_item.quantity
      weight += quantity * item_weight
    end
    weight = weight > min_weight ? weight : min_weight
    weight.ceil
  end
end