Spree::Calculator::Shipping::CargoCalculator.class_eval do
  def compute(order=nil)
    begin
      content_items = order.contents
      # total_weight = total_weight(content_items)
      shipping_price = shipping_price(order)
      # total_cost = (total_weight * shipping_price) + adm_fee(order)

      total_cost = (shipping_price) + adm_fee(order)

      puts "=============>>>>> #{self.calculable.name}: #{total_cost} - #{adm_fee(order)}"

      return total_cost
    rescue => e
      false
    end
  end

  def shipping_rate(order)
    weight = total_weight(order.contents)
    city = Spree::City.find_by_name(order.shipment.get_shipping_address.city)

    cargo_method = self.calculable.cargo_shipping_rates_per_methods.find_by_city_id(city.id)

    return false if cargo_method.nil?

    min_weight = cargo_method.cargo_detail_shipping_rates_per_methods.order(weight: :asc).first.weight
    if weight < min_weight
      weight = min_weight
    end

    found = cargo_method.cargo_detail_shipping_rates_per_methods.find_by_weight(weight)
    return found
  end

  def total_weight(contents)
    weight = 0
    contents.each do |item|
      item_weight = item.variant.weight > 0.0 ? item.variant.weight : 1
      quantity = item.inventory_unit.expected_shipped > 0 ? (item.inventory_unit.expected_shipped/item.line_item.qty_per_pack) : item.line_item.quantity
      weight += quantity * item_weight
    end
    weight.ceil
  end

  def selected_cargo_method(order)
    city = Spree::City.find_by_name(order.shipment.get_shipping_address.city)
    cargo_method = self.calculable.cargo_shipping_rates_per_methods.find_by_city_id(city.id)
    return cargo_method
  end
end