module Spree
  class PurchaseOrder < Spree::Base
    include Filterable
    has_many :items, class_name: "Spree::PurchaseLineItem", dependent: :destroy
    has_many :container_items, class_name: "Spree::ContainerItem"
    accepts_nested_attributes_for :items, reject_if: :all_blank, allow_destroy: true
    belongs_to :supplier, class_name: "Spree::Supplier"
    belongs_to :user

    validates :supplier_id, presence: true
    # validates :items, presence: { message: "from this Supplier have not been ordered by customer(s)" }
    scope :start_date, -> (start_date) { where("created_at >= ?", start_date) }
    scope :end_date, -> (end_date) { where("created_at <= ?", end_date) }
    scope :supplier_id_is, -> (supplier_id){where(supplier_id: supplier_id)}
    scope :id_is, -> (id_po){where(id: id_po)}
    scope :shipment_is, -> (shipment){where(shipping_state: shipment)}
    scope :payment_is, -> (payment){where(payment_state: payment)}
    scope :sku_contain, -> (sku) {joins(items: :variant).where("spree_variants.sku like ?", "%#{sku}%")}

    after_create :build_items
    scope :shipment_pending, -> {where(shipping_state: "pending")}
    self.whitelisted_ransackable_attributes = %w(supplier_id total payment_state shipping_state created_at updated_at)

    state_machine :shipping_state, initial: :pending do
      event :partial_ship do
        transition to: :partial_shipped
      end

      event :ship do
        transition to: :shipped
      end

      event :complete do
        transition to: :completed
      end
    end

    state_machine :payment_state, initial: :cart do
      event :pending do
        transition to: :pending, from: :cart
      end
      after_transition to: :pending, do: :after_pending

      event :confirm do
        transition to: :confirmed, from: :pending
      end
      after_transition to: :confirmed, do: :post_to_journal

      event :pay do
        transition to: :paid, from: :confirmed
      end
    end

    def after_pending
      items.each(&:pending_payment!)   
    end

    def items_present?
      !self.items.blank?
    end

    def pending_payment?
      payment_state == "pending" || payment_state == "cart"
    end

    def build_items
      purchase_items = Spree::PurchaseLineItem.joins(:purchase_order).where("spree_purchase_line_items.quantity > ? AND 
        spree_purchase_orders.supplier_id = ?", 0, supplier_id).select(:variant_id).distinct
      order_line_items = Spree::LineItem.joins(:order, :inventory_units).where("spree_orders.preorder = ? && spree_orders.state = 
        ? && spree_orders.shipment_state = ? && supplier_id = ? && spree_inventory_units.state = ?", true, "complete", "backorder", 
        supplier_id, 'backordered').where.not(variant_id: purchase_items.pluck(:variant_id))

      return if order_line_items.empty?

      order_line_items.each do |line_item|
        item = self.items.where(variant_id: line_item.variant_id).first
        next if item
        total_items_product = line_item.variant.product.total_items
        dp_confirm = order_line_items.where(variant_id: line_item.variant_id).dp_confirm.sum(:quantity)
        item = Spree::PurchaseLineItem.new(variant_id: line_item.variant_id, quantity: 0, 
          items: total_items_product, cost_price: line_item.variant.cost_price.to_i, 
          currency: line_item.variant.cost_currency, purchase_order_id: self.id, shipment_status: "pending", 
          dp_confirm: dp_confirm)
        update_total_items(item)
        update_total_item_price(item)
        item.save
      end
      update_po
    end

    def create_item(variant)
      order_line_items = Spree::LineItem.joins(:order).where("spree_orders.preorder = ? && 
        spree_orders.state = ? && spree_orders.shipment_state = ? && supplier_id = ?
        ", true, "complete", "backorder", self.supplier_id).where(variant_id: variant.id)
      dp_confirm = order_line_items.where(variant_id: variant.id).sum(:quantity)
      item = Spree::PurchaseLineItem.new(variant_id: variant.id, quantity: order_line_items.count > 1 ? order_line_items.count : 1, 
        items: variant.product.total_items, cost_price: variant.cost_price.to_i, 
        currency: variant.cost_currency, purchase_order_id: self.id, shipment_status: "pending", dp_confirm: dp_confirm)
      update_total_items(item)
      update_total_item_price(item)
      item.save
      update_po
    end

    def update_expected_stock
      self.items.each do |item|
        stock = item.variant.stock_items.first
        stock.update_attributes(expected_count_on_hand: stock.expected_count_on_hand + item.items)
      end
    end

    def update_shipment_state!
      total_line_item = items.count
      complete_shipment = items.completed.count
      shipped = items.shipped.count
      if complete_shipment == total_line_item
        self.complete!
      elsif shipped == total_line_item
        self.ship!
      elsif shipped < total_line_item && shipped > 0
        self.partial_ship! if !self.partial_shipped?
      end
    end

    def recalculate
      Spree::PurchaseLineItem.where("quantity = 0 OR quantity is NULL").delete_all
      update_po
    end

    def update_po
      self.update_columns(total: self.items.to_a.sum(&:total))
    end

    def post_to_journal
      recalculate

      Spree::PurchaseOrder.transaction do
        check_items_payment_state
        update_outstanding_invoice_qty
        update_expected_stock
      end
    end

    def check_items_payment_state
      items.cart_payment.each(&:pending_payment!)
    end

    def update_outstanding_invoice_qty
      items.each do |item|
        item.update_columns(outstanding_invoice_qty: item.items)
      end
    end

    def reverse_po_to_pending
      Spree::PurchaseOrder.transaction do
        reverse_outstanding_qty
        reverse_expected_stock

        self.update_columns(payment_state: "pending")
      end
    end

    def reverse_outstanding_qty
      items.update_all(outstanding_invoice_qty: 0)
    end

    def reverse_expected_stock
      self.items.each do |item|
        stock = item.variant.stock_items.first
        stock.update_attributes(expected_count_on_hand: stock.expected_count_on_hand - item.items)
      end
    end

    private

    def update_item(item)
      item.quantity += item.quantity
      update_total_items(item)
    end

    def update_total_items(item)
      item.items = item.quantity * item.items
      update_total_item_price(item)
    end

    def update_total_item_price(item)
      item.total = item.items * item.cost_price if item.cost_price
    end

    def after_paid
      items.each do |item|
        item.variant.kurs = kurs_rmb
        if item.variant.cost_currency == "RMB"
          item.variant.hpp = kurs_rmb * item.variant.cost_price.to_i
        else
          item.variant.hpp = item.variant.cost_price.to_i
        end
        item.variant.save
      end
    end

    def kurs_rmb
      total_tf = Journal::TransferFund.sum(:amount)
      account_tf = Journal::Account.where(name: "Transfer Fund", currency: "IDR").first.id
      total_journal_tf = Journal::JournalItem.where(account_id: account_tf).sum(:debit)
      kurs_rmb = total_journal_tf/total_tf
      return kurs_rmb
    end
  end
end