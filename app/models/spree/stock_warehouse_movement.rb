class Spree::StockWarehouseMovement < ApplicationRecord
  belongs_to :originator, polymorphic: true
  belongs_to :stock_item, class_name: 'Spree::StockItem'

  after_create :update_stock_warehouse_quantity

  def balance_stock
  	movement_before = Spree::StockWarehouseMovement.where(stock_item_id: self.stock_item_id).where("id <= ?", self.id)
    balance_quantity_before = movement_before.sum(:quantity)
    balance_item = balance_quantity_before
    return balance_item
  end

  def update_stock_warehouse_quantity
    stock_item.adjust_stock_warehouse quantity
  end
end
