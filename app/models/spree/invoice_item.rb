module Spree
  class InvoiceItem < Spree::Base
    belongs_to :invoice, class_name: "Spree::Invoice"
    belongs_to :inventory_unit, class_name: "Spree::InventoryUnit"
    belongs_to :order, class_name: "Spree::Order"
    belongs_to :variant, class_name: "Spree::Variant"
    belongs_to :shipping_method, class_name: "Spree::ShippingMethod"
    belongs_to :shipment, class_name: "Spree::Shipment"
    belongs_to :ship_address, foreign_key: :ship_address_id, class_name: 'Spree::Address'

    after_create :import_line_items
    after_update :recalculate, if: ->(item){ item.adjustment_total.present? and item.adjustment_total_changed? }

    def recalculate
      total_amount = self.total_amount_per_item + self.adjustment_total
      self.update_columns(total_amount: total_amount)
      self.invoice.recalculate_plus_adjustment
    end

    def total_pieces
      quantity * self.inventory_unit.line_item.qty_per_pack
    end

    def single_price
      total_amount / total_pieces
    end

    def item_cost_price
      total_pieces * variant.cost_price.to_i
    end

    # Total quantity semua line item untuk invoice yang ordernya dipecah
    def total_quantity_line_items(line_item_id)
      Spree::InvoiceItem.joins(:inventory_unit).where("spree_inventory_units.line_item_id = ?", line_item_id).sum(:quantity)
    end

    def total_quantity_line_items_in_pcs(line_item_id)
      Spree::InventoryUnit.where(line_item_id: line_item_id).sum(:expected_shipped)
    end

    def total_amount_per_item
      inventory_unit.line_item.price * (quantity * inventory_unit.line_item.qty_per_pack)
    end

    def total_weight_item
      self.variant.weight * quantity
    end

    private

    def import_line_items
      self.update_attributes(variant_id: self.inventory_unit.variant_id, total_amount: 
        self.total_amount_per_item, order_id: self.inventory_unit.order_id, shipping_method_id: self.inventory_unit.order.shipments.first.shipping_method.id,
        ship_address_id: self.inventory_unit.order.ship_address_id)

      create_iu     
    end

    def create_iu
      return if total_quantity_line_items(inventory_unit.line_item_id) == inventory_unit.line_item.quantity

      expected_shipped = quantity * inventory_unit.line_item.qty_per_pack
      inventory_unit.update_columns(expected_shipped: expected_shipped, split_invoice: true)

      return if total_quantity_line_items_in_pcs(inventory_unit.line_item_id) == inventory_unit.line_item.total_line_items
      
      inv_unit_new = Spree::InventoryUnit.new(state: inventory_unit.state, variant_id: inventory_unit.variant_id, 
        order_id: inventory_unit.order_id, shipment_id: inventory_unit.shipment_id, pending: inventory_unit.pending, 
        line_item_id: inventory_unit.line_item_id, carton_id: inventory_unit.carton_id, shipped: 0, 
        expected_shipped: inventory_unit.line_item.total_line_items - total_quantity_line_items_in_pcs(inventory_unit.line_item_id),
        split_invoice: true)
      inv_unit_new.save(validate: false)
    end
  end
end
