Spree::Payment.class_eval do
  include Filterable

  scope :bank_transfers, -> {joins(:payment_method).where("spree_payment_methods.type = ?", "Spree::PaymentMethod::BankTransfer")}
  scope :void, -> {where(state: "void")}
  belongs_to :journal_transaction, class_name: "Journal::Transaction"
  belongs_to :user, class_name: "Spree::User"
  scope :created_at_is, -> (date){where('created_at BETWEEN ? AND ?',date.first.to_datetime.beginning_of_day, date.first.to_datetime.end_of_day)}
  scope :name_user, -> (name_user){joins(order: :user).where("spree_users.name like ?", "%#{name_user}%")}
  scope :number, -> (number){joins(:order).where("spree_orders.number = ?", number)}
  after_commit :post_to_journal

  def store_type_is_store_credit?
    source_type == "Spree::StoreCredit"
  end

  def customer_down_payment_journal_account_id
    @customer_down_payment = Journal::Account.find_by_code("2-20218").id
  end

  def store_credit_journal_account_id
    @store_credit = Journal::Account.find_by_code("2-20219").id
  end

  def transisi_revenue_journal_account_id
    @transisi_revenue = Journal::Account.find_by_code("4-40208").id
  end

  def journal_transaction_account_id
    if journal_transaction.nil?
      @journal_transaction_account_id = Journal::Account.find_by_code("1-10001").id
    else
      @journal_transaction_account_id = journal_transaction.account_id
    end
    @journal_transaction_account_id
  end

  def post_to_journal
    return if is_cn?
    return if state != "completed"  
    return if amount <= 0
    Journal::JournalItem.transaction do
      if order.preorder?
        if store_type_is_store_credit?
          post_preorder_with_store_credit
        else
          post_preorder
        end
      else
        if store_type_is_store_credit?
          post_ready_stock_with_store_credit
        else
          post_ready_stock
        end
      end
    end
  end

  def post_preorder_with_store_credit
    journal_entry = Journal::JournalEntry.create(description: "Payment Preorder [Store Credit]: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", date: DateTime.now)
    # credit DP
    Journal::JournalItem.create(account_id: customer_down_payment_journal_account_id, description: "Payment Preorder [Store Credit]: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", credit: amount, currency: order.currency, originator: self, journal_entry_id: journal_entry.id)
    # debit Store Credit
    Journal::JournalItem.create(account_id: store_credit_journal_account_id, description: "Payment Preorder [Store Credit]: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", debit: amount, currency: order.currency, originator: self, journal_entry_id: journal_entry.id)
  end

  def post_preorder
    journal_entry = Journal::JournalEntry.create(description: "Payment Preorder: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", date: DateTime.now)
    # credit DP
    Journal::JournalItem.create(account_id: customer_down_payment_journal_account_id, description: "Payment Preorder: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", credit: amount, currency: order.currency, originator: self, journal_entry_id: journal_entry.id)
    # debit Store Credit
    Journal::JournalItem.create(account_id: journal_transaction_account_id, description: "Payment Preorder: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", debit: amount, currency: order.currency, originator: self, journal_entry_id: journal_entry.id)
  end

  def post_ready_stock_with_store_credit
    journal_entry = Journal::JournalEntry.create(description: "Payment [Store Credit]: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", date: DateTime.now)
    # credit DP
    Journal::JournalItem.create(account_id: transisi_revenue_journal_account_id, description: "Payment [Store Credit]: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", credit: amount, currency: order.currency, originator: self, journal_entry_id: journal_entry.id)
    # debit Store Credit
    Journal::JournalItem.create(account_id: store_credit_journal_account_id, description: "Payment [Store Credit]: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", debit: amount, currency: order.currency, originator: self, journal_entry_id: journal_entry.id)
  end

  def post_ready_stock
    journal_entry = Journal::JournalEntry.create(description: "Payment: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", date: DateTime.now)
    # credit DP
    Journal::JournalItem.create(account_id: transisi_revenue_journal_account_id, description: "Payment: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", credit: amount, currency: order.currency, originator: self, journal_entry_id: journal_entry.id)
    # debit Store Credit
    Journal::JournalItem.create(account_id: journal_transaction_account_id, description: "Payment: #{order.number} - #{order.user.nil? ? order.billing_address.firstname : order.user.name}", debit: amount, currency: order.currency, originator: self, journal_entry_id: journal_entry.id)
  end
end