module Spree
  class Invoice < Spree::Base
    include Filterable
    belongs_to :user, class_name: Spree::UserClassHandle.new
    has_many :items, class_name: "Spree::InvoiceItem", dependent: :destroy
    has_many :payments, class_name: "Spree::InvoicePayment", dependent: :destroy
    has_many :payment_confirmations, class_name: "Spree::PaymentConfirmationItem"
    belongs_to :ship_address, foreign_key: :ship_address_id, class_name: 'Spree::Address'
    has_many :shipments, dependent: :destroy, inverse_of: :invoice do
      def states
        pluck(:state).uniq
      end
    end
    scope :name_user, -> (name){joins(:user).where("name like ?", "%#{name}%")}
    scope :number, -> (number){where("id = ?", number)}
    scope :start_date, -> (start_date) { where("spree_invoices.created_at >= ?", start_date.to_date.beginning_of_day) }
    scope :end_date, -> (end_date) { where("spree_invoices.created_at <= ?", end_date.to_date.end_of_day) }
    scope :payment_is, -> (payment){ if payment == "waiting_confirmation" then Spree::Invoice.valid.pending_payment.joins(:payment_confirmations).where("spree_payment_confirmation_items.invoice_id IS NOT NULL") elsif payment == "paid" then Spree::Invoice.valid.paid else
      where(status: payment) end}
    scope :id_is, -> (id){where(id: id)}
    scope :name_contain, -> (name){joins(:user).where("name like ?", "%#{name}%")}
    scope :valid, -> {where("total is not null")}
    scope :sku_contain, -> (sku) {joins(items: :variant).where("spree_variants.sku like ?", "%#{sku}%")}
    scope :waiting_confirmation, -> { Spree::Invoice.valid.pending_payment.joins(:payment_confirmations).where("spree_payment_confirmation_items.invoice_id IS NOT NULL").uniq}
    scope :lock_is, -> (status){where(lock: status)}


    validates :user_id, presence: true

    after_create :set_initial_states

    scope :paid, -> {where(status: ["paid", "Partially Shipped", "Completed"])}
    scope :valid, -> {where("total is not null")}
    scope :pending_payment, -> {where(status: "pending_payment")}

    attr_accessor :courier

    def self.to_csv(options = {})
      total_hpp = 0
      total_invoice = 0
      CSV.generate(options) do |csv|
        csv << ["number", "code item", "name", "qty", "harga satuan", "tanggal bayar", "harga jual", "harga beli", "gross profit", "HPP(pcs)"]
        all.each do |inv|
          total_invoice += inv.items.sum(:total_amount) + inv.items.sum(:adjustment_total)
          inv.items.each do |item|
            total_hpp += item.total_pieces * item.variant.cost_price.to_i
            harga_jual = item.total_amount - item.adjustment_total.to_i
            harga_beli = item.total_pieces * item.variant.cost_price.to_i
            gross_profit = harga_jual - harga_beli
            csv << [inv.number, item.variant.sku, item.variant.name, item.total_pieces, (item.total_amount - item.adjustment_total.to_i)/item.total_pieces, inv.payments.empty? ? "tidak ada" : inv.payments.last.updated_at.to_date, harga_jual, harga_beli, gross_profit, item.variant.cost_price.to_i]
          end
        end
        net_income = total_invoice - total_hpp
        csv << ["AMOUNT JUAL", total_invoice]
        csv << ["HARGA BELI TOTAL", total_hpp]
        csv << ["NET INCOME ", net_income]
      end
    end

    def amount_item_total
      total - shipping_cost
    end

    def shipping_cost
      shipments.sum(:cost)
    end

    def account_journal_shipping
      items.first.shipping_method.account
    end

    def process_lock!
      if lock?
        self.update_columns(lock: false)
      else
        self.update_columns(lock: true)
      end
    end
    
    def create_shipment(shipping_method_id)
      if status != "pending_payment"
        return errors.add(:base, 'Tidak dapat mengubah pesanan karena pesanan telah dibayar')
      end

      items.each do |x|
        next if x.shipment_id.nil?
        Spree::Shipment.where(invoice_id: x.invoice_id).delete_all
      end

      items.update_all(shipment_id: nil)

      addresses = self.items.collect{|x|x.ship_address_id}.uniq
      addresses.each do |ship_address_id|
        shipment = self.shipments.create!(stock_location_id: 1)
        items = self.items.where(ship_address_id: ship_address_id)
        update_inventory_unit_shipment_id(items, shipment)
        if refresh_rates(items, shipment, shipping_method_id)
          update_total_invoice(items, shipment)
        else
          self.errors.add(:shipping_method_id, "is not available for this city")
          return false
        end
      end
      items.update_all(shipping_method_id: shipping_method_id)
      send_sms_shipment_ready 
      send_email_shipment_ready
    end

    def recalculate_plus_adjustment
      return if self.total.nil?
      self.total = items.sum(:total_amount) + shipments.sum(:cost)
      self.save
    end

    def reminder_sms
      sms = RajaSms.new
      order = items.first.order
      phone = order.dropship? ? order.dropship_phone : order.ship_address.phone
      sms.send_sms(phone, "Pesanan #{number} menunggu pembayaran. Silahkan lakukan pembayaran sebesar 
        Rp#{outstanding_balance}. Silahkan cek email Anda untuk info lengkapnya. -BrandedBabys")
    end

    def number
      "INV#{id}"
    end

    def update!
      update_payment_state
      self.save
    end

    def send_sms_shipment_ready
      sms = RajaSms.new
      order = items.first.order
      phone = order.dropship? ? order.dropship_phone : order.ship_address.phone
      sms.send_sms(phone, "PO Anda sudah ready. #{number}: Rp. #{outstanding_balance}. Detail invoice di my account. Batas waktu tt 3x24 jam. Reply to 087882905449. Brandedbabys.com")
    end

    def send_email_shipment_ready
      Spree::OrderMailer.preorder_is_ready(self).deliver_later
    end

    # TO DO
    def outstanding_balance
      # If reimbursement has happened add it back to total to prevent balance_due payment state
      # See: https://github.com/spree/spree/issues/6229
      self.total - (self.payments.paid.sum(:amount) + dp_paid)
    end

    def dp_paid
      # ada order sblmnya yg seharusnya tdk disable dp
      # return 0 if user.disable_dp?

      dp = 0
      self.items.each do |i|
        dp += i.quantity * i.inventory_unit.line_item.down_payment_price.to_i
      end
      return dp
    end

    def outstanding_balance?
      outstanding_balance != 0
    end

    def update_payment_state
      if self.payments.paid.sum(:amount) >= self.total - dp_paid
        self.status = "paid"
        update_shipments
        post_to_journal_move_customer_down_payment
        post_journal_revenue_and_shipping_from_invoice
        send_sms
        post_journal_inventory_from_invoice
        create_movement_warehouse_from_invoice
      end
    end

    def create_movement_warehouse_from_invoice
      self.items.each do |inv_item|
        inv_item.variant.stock_items.first.stock_warehouse_movements.create(quantity: inv_item.total_pieces * -1, description: "Shipment from invoice #{id} - #{self.user.present? ? self.user.name : "Cannot Display Cust Name"}", originator: inv_item)
      end
    end

    def post_journal_inventory_from_invoice
      @inventory = Journal::Account.find_by_code("1-10203").id
      @cost_of_sales = Journal::Account.find_by_code("5-50401").id

      self.items.each do |item|
        amount_cost_price = item.item_cost_price
        journal_entry = Journal::JournalEntry.create(description: "Invoice: #{id} - #{item.quantity} X #{item.variant.sku}", date: DateTime.now)
        # credit inventory
        Journal::JournalItem.create(account_id: @inventory, description: "Invoice: #{id} - #{item.quantity} X #{item.variant.sku}", credit: amount_cost_price, currency: "IDR", originator: item, journal_entry_id: journal_entry.id)
        # debit cost of sales
        Journal::JournalItem.create(account_id: @cost_of_sales, description: "Invoice: #{id} - #{item.quantity} X #{item.variant.sku}", debit: amount_cost_price, currency: "IDR", originator: item, journal_entry_id: journal_entry.id)
      end
    end

    def post_journal_revenue_and_shipping_from_invoice
      revenue_journal_account_id = Journal::Account.find_by_code("4-40201").id
      transisi_revenue_account_id = Journal::Account.find_by_code("4-40208").id
      journal_entry = Journal::JournalEntry.create(description: "Invoice Paid: #{number} - #{user.name}", date: DateTime.now)
      # debit transisi revenue
      Journal::JournalItem.create(account_id: transisi_revenue_account_id, description: "Invoice Paid: #{number} - #{user.name}", debit: total, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
      # credit amount
      Journal::JournalItem.create(account_id: revenue_journal_account_id, description: "Invoice Paid: #{number} - #{user.name}", credit: amount_item_total, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
      # credit shipping
      Journal::JournalItem.create(account_id: account_journal_shipping.id, description: "Invoice Paid: #{number} - #{user.name}", credit: shipping_cost, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
    end

    def send_sms
      sms = RajaSms.new
      order = items.first.order
      phone = order.preorder? ? order.dropship_phone : items.first.ship_address.phone
      sms.send_sms(phone, "Terimakasih. Pembayaran untuk pesanan #{id} sebesar Rp#{outstanding_balance} telah kami terima. Pesanan Anda akan segera
        kami proses -BrandedBabys.com")
    end

    def update_shipments
      shipments.each do |shipment|
        shipment.update_attributes(state: "ready", updated_at: Time.now)
      end
    end

    def subtotal
      items.sum(:total_amount)
    end

    def can_ship?
      true
    end

    def shipping
      @shipping ||= Spree::OrderShipping.new(self)
    end

    def pending_payment?
      status == "pending_payment"
    end

    def paid?
      status == "paid"
    end

    def update_status!
      # shipments = Spree::Shipment.where(invoice_id: self.id)
      # if shipments.count == shipments.where(state: "shipped").count
      #   self.status = "Completed"
      # else
      #   self.status = "Partially Shipped"
      # end
      self.status = "Completed"
      self.save
    end

    def send_email_payment_confirmation(journal_transaction_id)
      Spree::OrderMailer.invoice_payment_confirmed(self, journal_transaction_id).deliver_later
    end

    def cancel!(whodunnit)
      iu = Spree::InventoryUnit.transaction do
        self.update_columns(status: "canceled")
        self.items.first.shipment.cancel!
        # record_cancellation_to_journal(whodunnit)
      end

      iu
    end

    def self.generate_invoice
      Spree::Order.po_ready.select(:user_id).distinct.each do |order|
        puts "user: #{order.user.email}"
        user = order.user
        invoice = Spree::Invoice.create(user_id: user.id)

        # # INSERT LINE ITEMS
        Spree::InventoryUnit.ready_to_invoice.where("spree_orders.user_id = ?", user.id).each do |unit|
          puts "unit id: #{unit.id}"
          invoice.items.create(inventory_unit_id: unit.id)
        end

        if !invoice.items.empty?
          original_shipping_method_id = invoice.items.first.shipping_method_id
          invoice.create_shipment(original_shipping_method_id)
        end

        # CHEAT FIX
        Spree::Invoice.where("total is null").delete_all
      end
    end

    def add_store_credit_payments
      return if user.nil?
      return if user.total_available_store_credit.zero?

      remaining_total = outstanding_balance

      if user.store_credits.any?
        payment_method = Spree::PaymentMethod::StoreCredit.first

        user.store_credits.order_by_priority.each do |credit|
          break if remaining_total.zero?
          next if credit.amount_remaining.zero?

          amount_to_take = [credit.amount_remaining, remaining_total].min

          inv_payment = payments.create!(payment_method_id: payment_method.id,
                           amount: amount_to_take,
                           state: 'paid',
                           )

          auth = credit.authorize(
            amount_to_take,
            "IDR",
            action_originator: inv_payment
          )
          
          credit.capture(amount_to_take, auth, "IDR", action_originator: inv_payment)

          remaining_total -= amount_to_take
        end
      end

      payments.reset
      update!
    end

    def add_store_credit_payment_with_value(amount_sc)
      return if user.nil?
      return if user.total_available_store_credit.zero?

      if amount_sc > outstanding_balance
        remaining_total = outstanding_balance
      else
        remaining_total = amount_sc
      end

      if user.store_credits.any?
        payment_method = Spree::PaymentMethod::StoreCredit.first

        user.store_credits.order_by_priority.each do |credit|
          break if remaining_total.zero?
          next if credit.amount_remaining.zero?

          amount_to_take = [credit.amount_remaining, remaining_total].min

          inv_payment = payments.create!(payment_method_id: payment_method.id,
                           amount: amount_to_take,
                           state: 'paid',
                           )

          auth = credit.authorize(
            amount_to_take,
            "IDR",
            action_originator: inv_payment
          )
          
          credit.capture(amount_to_take, auth, "IDR", action_originator: inv_payment)

          remaining_total -= amount_to_take
        end
      end

      payments.reset
      update!
    end

    def post_to_journal_move_customer_down_payment
      return if dp_paid <= 0
      revenue_journal_account_id = Journal::Account.find_by_code("4-40201").id
      customer_down_payment_journal_account_id = Journal::Account.find_by_code("2-20218").id
      Spree::Invoice.transaction do
        journal_entry = Journal::JournalEntry.create(description: "Payment [Customer Deposit] : #{number} - #{user.name}", date: DateTime.now)
        # credit DP
        Journal::JournalItem.create(account_id: revenue_journal_account_id, description: "Payment [Customer Deposit] : #{number} - #{user.name}", credit: dp_paid, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
        # debit Store Credit
        Journal::JournalItem.create(account_id: customer_down_payment_journal_account_id, description: "Payment [Customer Deposit] : #{number} - #{user.name}", debit: dp_paid, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
      end
    end

    private

    def record_cancellation_to_journal(whodunnit)
      
    end

    def restock!(item, unit_cancel)
      stock = item.variant.stock_items.first
      stock.stock_movements.create!(quantity: item.quantity, originator: unit_cancel)
    end

    def set_initial_states
      self.status = "pending_payment"
      self.save
    end

    def refresh_rates(items, shipment, shipping_method_id)
      # StockEstimator.new assigment below will replace the current shipping_method
      # original_shipping_method_id = items.first.shipping_method_id
      shipping_method  = Spree::ShippingMethod.find_by_id shipping_method_id
      package = to_package(shipment)
      shipping_rates = shipping_method.calculator.compute(package)

      return false if shipping_rates == false

      shipment.cost = shipping_rates
      shipment.save!
    end

    def update_inventory_unit_shipment_id(items, shipment)
      items.update_all(shipment_id: shipment.id)
      items.each do |invoice_item|
        invoice_item.inventory_unit.shipment_id = shipment.id
        invoice_item.inventory_unit.save!
      end
    end

    def update_total_invoice(items, shipment)
      self.total = 0
      self.total += items.sum(:total_amount) + shipment.cost
      self.save
    end

    def to_package(shipment)
      package = Stock::Package.new(1)
      package.shipment = shipment
      shipment.inventory_units.includes(:variant).group_by(&:state).each do |state, state_inventory_units|
        package.add_multiple state_inventory_units, state.to_sym
      end
      package
    end
  end
end