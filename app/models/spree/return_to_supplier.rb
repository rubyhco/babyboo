module Spree
  class ReturnToSupplier < Spree::Base
    belongs_to :supplier, class_name: "Spree::Supplier"
    has_many :items, class_name: "Spree::ReturnToSupplierItem", dependent: :destroy
    self.whitelisted_ransackable_attributes = %w(supplier_id id return_at status total created_at updated_at)
    self.whitelisted_ransackable_associations = %w[items supplier]


    state_machine :status, initial: :pending do
      event :confirm do
        transition to: :confirmed, from: :pending
      end
      after_transition to: :confirmed, do: :after_confirmed

      event :complete do
        transition to: :completed, from: :confirmed
      end
    end

    private

    def after_confirmed
      total = 0
      items.all.each do |item|
        total += (item.quantity * item.cost_price)
      end 
      self.total = total
      self.currency = items.first.cost_currency
      self.save
      create_stock_movement
    end

    def create_stock_movement
      items.each do |item|
        stock = item.variant.stock_items.first
        stock.stock_movements.create!(quantity: (item.quantity * -1), originator: item)
      end
    end
  end
end
