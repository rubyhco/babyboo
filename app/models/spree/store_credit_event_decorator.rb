Spree::StoreCreditEvent.class_eval do
  after_commit :store_to_user_credit_history
  has_many :user_store_credit_histories, class_name: "Spree::UserStoreCreditHistory"

  self.whitelisted_ransackable_associations = %w[store_credit]
  self.whitelisted_ransackable_attributes = %w[id created_at updated_at amount originator_type originator_id]

  def capture_history(user)
    amount = -1 * self.amount
    accumulative = user.store_credit_histories.joins(:store_credit_event).where("spree_store_credit_events.
      originator_type = ? && DATE(spree_store_credit_events.created_at) = DATE(?) && spree_store_credit_events.action = ?", self.originator_type, Time.now, 
      self.action)
    if self.originator_type == "Spree::Payment"
      if accumulative.empty?
        memo = "Payment - ##{originator.order.number}"
        event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: 8)
      else
        old = accumulative.last
        if old.store_credit_event.originator.order_id == originator.order_id
          old.update_columns(amount: (old.amount - self.amount))
        else
          memo = "Payment - ##{originator.order.number}"
          event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: 8)
        end
      end
    elsif self.originator_type == "Spree::InvoicePayment"
      if accumulative.empty?
        memo = "Invoice Payment - ##{originator.invoice.id}"
        event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: 9)
      else
        old = accumulative.last
        if old.store_credit_event.originator.invoice_id == originator.invoice_id
          old.update_columns(amount: (old.amount - self.amount))
        else
          memo = "Invoice Payment - ##{originator.invoice.id}"
          event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: 9)
        end
      end
    elsif self.originator_type == "Journal::InvoicePayment"
      if accumulative.empty?
        memo = "Credit Payment - ##{originator.invoice.number}"
        event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: 10)
      else
        old = accumulative.last
        if old.store_credit_event.originator.invoice_id == originator.invoice_id
          old.update_columns(amount: (old.amount - self.amount))
        else
          memo = "Credit Payment - ##{originator.invoice.number}"
          event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: 10)
        end
      end
    elsif self.originator_type == "Spree::CashOutRequest"
      if accumulative.empty?
        memo = "Cash out request - ##{originator.id}"
        event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: 11)
      else
        old = accumulative.last
        if old.store_credit_event.originator.id == originator.id
          old.update_columns(amount: (old.amount - self.amount))
        else
          memo = "Cash out request - ##{originator.id}"
          event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: 11)
        end
      end
    else
      memo = store_credit.memo
      event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: self.store_credit.category_id)
    end

    event
  end

  def credit_history(user)
    amount = self.amount
    accumulative = user.store_credit_histories.where("DATE(created_at) = DATE(?) && action = ?", Time.now, self.action)
    if self.originator.nil?
      if accumulative.empty?
        memo = "Cancel Payment - #{Spree::StoreCreditEvent.where(action: "capture", authorization_code: authorization_code).first.originator.order.number}"
        event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: self.store_credit.category_id)
      else
        old = accumulative.last
        if Spree::StoreCreditEvent.where(action: "capture", authorization_code: old.store_credit_event.authorization_code).first.originator.order.number ==
          Spree::StoreCreditEvent.where(action: "capture", authorization_code: authorization_code).first.originator.order.number
          old.update_columns(amount: (old.amount + self.amount))
        else
          memo = "Cancel Payment - #{Spree::StoreCreditEvent.where(action: "capture", authorization_code: 
          authorization_code).first.originator.order.number}"
          event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: self.store_credit.category_id)
        end
      end
    else
      if accumulative.empty?
        memo = "Refund - ##{originator.payment.order.number}"
        event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: self.store_credit.category_id)
      else
        old = accumulative.last
        if old.store_credit_event.originator.payment.order_id == originator&.payment&.order_id
          old.update_columns(amount: (old.amount + self.amount))
        else
          memo = "Refund - ##{originator.payment.order.number}"
          event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: self.store_credit.category_id)
        end
      end
    end
    event
  end

  def store_to_user_credit_history
    return if !%w(capture credit allocation adjustment invalidate).include?(self.action)
    return if self.originator.nil? && %w(capture).include?(self.action)

    user = self.store_credit.user

    if self.action == "capture"
      event = capture_history(user)
    elsif self.action == "credit"
      event = credit_history(user)
    elsif self.action == "invalidate"
      amount = -1 * self.amount
      memo = self.update_reason.name
      event = user_store_credit_histories.create!(amount: amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: 1)
    elsif self.action == "adjustment"
      memo = self.update_reason.name
      event = user_store_credit_histories.create!(amount: self.amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: self.store_credit.category_id)
    else
      memo = store_credit.memo
      event = user_store_credit_histories.create!(amount: self.amount, user_id: self.store_credit.user_id, memo: memo, action: self.action, category_id: self.store_credit.category_id)
    end
    balance = user.store_credit_histories.sum(&:amount)
    event.update_columns(balance: balance) if event
  end
end