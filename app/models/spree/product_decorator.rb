Spree::Product.class_eval do

  after_validation :update_supplier_line_item

  MORE_PRICE_ATTRIBUTES = [
    :silver_price, :gold_price, :sale_price, :display_silver_amount, 
    :display_gold_amount, :display_sale_amount, :display_gold_price, :display_silver_price,
    :display_sale_price
  ]

  MORE_PRICE_ATTRIBUTES.each do |attr|
    delegate :"#{attr}", :"#{attr}=", to: :find_or_build_master
  end

  default_scope {order("DATE(spree_products.created_at) DESC, spree_products.description ASC")}

  belongs_to :supplier, class_name: "Spree::Supplier"

  scope :out_of_stock, -> { joins(:stock_items).where("spree_stock_items.count_on_hand = 0") }
  scope :preorder_pending, -> { joins(:stock_items).where("spree_stock_items.count_on_hand < 0") }
  scope :product_types_for, -> (product_type) { where(:is_preorder => product_type) }
  scope :ready_stock, -> {where(is_preorder: false)}
  scope :hidden_product, -> {where(available_on: nil)}
  self.whitelisted_ransackable_attributes = %w[created_at]

  validates :total_items, presence: true, on: :update

  def self.export_model_csv(options = {})
    CSV.generate(options) do |csv|
      csv << ["SKU", "Name", "Status", "Last Transaction Date", "Total Items", "Count On Hand", "Stock Pending", "Stock Gudang", "Master Price", "Silver Price", "Gold Price", "Sale Price", "HPP", "Total stock value", "Publish?", "Biaya Import", "Supplier"]
      Spree::Product.all.each do |prod|
        csv << [prod.sku, prod.name, prod.preorder? ? "Preorder" : "Ready Stock", prod.line_items.present? ? prod.line_items.last.created_at : nil, prod.total_items, prod.total_on_hand, prod.stock_pending, prod.stock_gudang, prod.price, prod.silver_price, prod.gold_price, prod.sale_price, prod.cost_price.to_i, prod.stock_gudang * prod.cost_price.to_i, prod.available_on.present? ? "Yes" : "No", prod.master.biaya_import, prod&.supplier&.name]
      end
    end
  end

  def stock_gudang
    if self.stock_items.first.present?
      self.stock_items.first.stock_warehouse
    else
      0
    end
  end

  def stock_pending
    if preorder?
      stock = self.line_items.joins(:inventory_units, :order).where("spree_orders.payment_state = ?", "balance_due").sum(&:total_line_items)
    else
      a = self.line_items.joins(:inventory_units, :order).where("spree_orders.payment_state = ? && spree_inventory_units.state = ?", "balance_due", "on_hand").sum(&:total_line_items)
      b = Spree::InvoiceItem.joins(:invoice).where("spree_invoice_items.variant_id = ?", self.master.id).where("spree_invoices.status = ?", "pending_payment").sum(&:total_pieces)
      stock = a + b
    end
    stock
  end

  def update_supplier_line_item
    return if !supplier_id_changed?
    line_items.update_all(supplier_id: supplier_id)
  end

  def sale?
  	if !sale_price.blank?
      if sale_price > 0
        return true
      end
    end
    return false
  end

  def product_type
    if is_preorder?
      "Preorder"
    else
      "Ready Stock"
    end
  end

end