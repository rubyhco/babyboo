Spree::Stock::Quantifier.class_eval do

  def expected_count_on_hand_leftover
    stock_item = @stock_items.first
    stock_item.expected_count_on_hand + stock_item.count_on_hand
  end

  def can_supply?(required, preorder=false)
    if preorder
      expected_count_on_hand = @stock_items.first.expected_count_on_hand
      if expected_count_on_hand != 0
        expected_count_on_hand_leftover >= required
      else
        backorderable?
      end
    else
      total_on_hand >= required || backorderable?
      # total_on_hand >= required
    end
  end
end