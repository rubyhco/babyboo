Spree::Stock::AvailabilityValidator.class_eval do

  private

  def ensure_in_stock(line_item, quantity, stock_location = nil)
    quantifier = Spree::Stock::Quantifier.new(line_item.variant, stock_location)
    unless quantifier.can_supply?(quantity, line_item.variant.product.preorder?)
      variant = line_item.variant
      display_name = variant.name.to_s
      display_name += %{ (#{variant.options_text})} unless variant.options_text.blank?

      line_item.errors[:quantity] << Spree.t(
        :selected_quantity_not_available,
        item: display_name.inspect
      )
    end
  end

end