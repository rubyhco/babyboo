Spree::Order.class_eval do
  ORDER_NUMBER_LENGTH  = 5
  ORDER_NUMBER_LETTERS = false
  ORDER_NUMBER_PREFIX  = 'R'

  include Filterable
  scope :po_ready, -> {Spree::Order.payment_state_is('paid').joins(:inventory_units).where("spree_orders.preorder = ? && spree_inventory_units.state = ? && spree_inventory_units.id not in (?)", true, "on_hand", Spree::InvoiceItem.pluck(:inventory_unit_id))}
  scope :payment_state_is, -> (payment_state){where(payment_state: payment_state)}
  scope :ready_stock, -> {where(preorder: false)}
  scope :preorder, -> {where(preorder: true)}
  scope :paid, -> {where(payment_state: "paid")}
  scope :ready_to_ship, -> {where("shipment_state = ?", "ready").where(lock: false)}
  scope :name_user, -> (name){joins(:user).where("name like ?", "%#{name}%")}
  scope :number, -> (number){where(number: number)}
  scope :disable_dp, -> {where(disable_dp: true)}
  scope :start_date, -> (start_date) { where("spree_orders.completed_at >= ?", start_date.to_date.beginning_of_day) }
  scope :end_date, -> (end_date) { where("spree_orders.completed_at <= ?", end_date.to_date.end_of_day) }
  scope :sku_contain, -> (sku) {joins(line_items: :variant).where("spree_variants.sku like ?", "%#{sku}%")}

  has_many :payment_confirmations, class_name: "Spree::PaymentConfirmationItem"

  money_methods :order_total_after_store_credit_without_shipping_fee, :outstanding_down_payment, :down_payment_total, :subtotal_preorder

  self.whitelisted_ransackable_associations = %w[payment_confirmations shipments user promotions bill_address ship_address line_items]
  
  self.whitelisted_ransackable_attributes = %w[completed_at created_at email number state payment_state shipment_state total store_id preorder item_total down_payment_total lock]

  def self.export_po_with_state(email= "tiara@rubyh.co", state= "backordered", options = {})
    a = CSV.generate(options) do |csv|
      csv << ["Code Item", "Order Number", "Member ID", "Member Name", "Member Email", "Qty PO (pack)", "Qty PO (pcs)", "DP Amount", "Harga", "Status Payment"]
      Spree::InventoryUnit.pending_po.each do |iu|
        csv << [iu.variant.sku, iu.order.number, iu&.order&.user&.id, iu&.order&.user&.name, iu&.order&.user&.email, iu&.line_item&.quantity, iu&.line_item&.total_line_items, iu&.line_item&.total_down_payment, iu.line_item.amount, iu.order.payment_state_human]
      end
    end
    user = Spree::User.find_by_email(email)
    Spree::OrderMailer.export_results(user, a).deliver
  end

  def self.to_csv_rs(options = {})
    total_hpp = 0
    total_amount_jual = 0
    CSV.generate(options) do |csv|
      csv << ["number", "code item", "name", "qty", "harga satuan", "tanggal bayar", "harga jual", "harga beli", "gross profit", "HPP(pcs)"]
      all.each do |order|
        order.line_items.each do |a|
          total_hpp += a.real_cost_price.to_i
          total_amount_jual += a.amount
          csv << [order.number, a.variant.sku, a.variant.name, a.total_line_items, a.price, order.payments.last.updated_at, a.amount, a.real_cost_price.to_i, a.amount - a.real_cost_price.to_i, a.variant.cost_price.to_i ]
        end
      end
      net_income = total_amount_jual - total_hpp
      csv << ["AMOUNT JUAL", total_amount_jual]
      csv << ["HPP TOTAL", total_hpp]
      csv << ["NET INCOME ", net_income]
    end
  end

  def self.to_csv_po(options = {})
    CSV.generate(options) do |csv|
      csv << ["number", "code item", "name", "total DP", "tanggal bayar"]
      all.each do |order|
        csv << [order.number, order.variants.first.sku, order.variants.first.sku, order.down_payment_total, order.payments.last.updated_at]
      end
      csv << ["Total DP", all.sum(:payment_total)]
    end
  end

  def shipping_cost
    shipments.sum(:cost)
  end

  def account_journal_shipping
    shipments.first.shipping_method.account
  end

  def payment_state_human
    case payment_state
    when "paid"
      if preorder?
        "dp confirmed"
      else
        "paid"
      end
    when "balance_due"
      if !payment_confirmations.empty?
        "waiting for confirmation"
      else
        if preorder?
          "pending on dp"
        else
          "pending"
        end
      end
    when "void"
      "cancel"
    when "credit_owed"
      "overpayment"
    end
  end

  def order_state_human
    case payment_state
    when "paid"
      if preorder?
        "dp confirmed - #{shipment_state == "backorder" ? "shipment pending" : shipment_state}"
      else
        "paid - #{shipment_state == "backorder" ? "shipment pending" : shipment_state}"
      end
    when "balance_due"
      if !payment_confirmations.empty?
        "waiting for confirmation"
      else
        if preorder?
          "pending on dp"
        else
          "pending"
        end
      end
    when "void"
      "cancel"
    when "credit_owed"
      "overpayment"
    end
  end

  def subtotal_preorder
    total - shipment_total
  end

  def process_lock!
    if lock?
      self.update_columns(lock: false)
    else
      self.update_columns(lock: true)
    end
  end

  def order_total_after_store_credit_without_shipping_fee
    order_total_after_store_credit - shipment_total
  end

  def order_total_after_store_credit
    total - total_store_credit_used
  end

  def outstanding_down_payment
    down_payment_total - total_store_credit_used
  end

  def total_store_credit_used
    payments.store_credits.completed.sum(:amount)
  end

  def store_credit_preorder
    if self.total_store_credit_used > dp
      amount = total_store_credit_used - dp
    end
  end

  def total_weight
    weight = 0
    line_items.each do |line_item|
      weight += line_item.total_weight
    end
    weight
  end

  def mark_as_paid
    puts "================>>>>>>>>>>>>>>>>> Order ID: #{id}"
    Spree::Order.transaction do
      payment = payments.create({ :amount => outstanding_balance, :payment_method => Spree::PaymentMethod::BankTransfer.first})
      if payment
        payment.started_processing!
        payment.pend!
        payment.complete!
        update!
      end
    end
  end

  # add credit store
  def after_cancel
    Spree::Order.transaction do
      shipments.each(&:cancel!)
      add_credit_store
      payments.completed.each(&:cancel!)
      payments.store_credits.pending.each(&:void_transaction!)
      send_cancel_email
      update!
    end
  end

  def add_store_credit_payments_from_admin
    return if user.nil?

    return if payments.store_credits.checkout.empty? && user.total_available_store_credit.zero?

    payments.store_credits.checkout.each(&:invalidate!)

    use_store_credit_balance

    process_payments! 
  end

  # bayar dengan store credit dari my account user
  def add_store_credit_payments_from_user(amount_sc)
    return if user.nil?

    return if payments.store_credits.checkout.empty? && user.total_available_store_credit.zero?

    payments.store_credits.checkout.each(&:invalidate!)

    use_store_credit_balance_with_value(amount_sc)

    process_payments! 
  end

  # add exception for preorder
  def add_store_credit_payments
    return if !use_store_credit?
    return if user.nil?
    return if payments.store_credits.checkout.empty? && user.total_available_store_credit.zero?


    payments.store_credits.checkout.each(&:invalidate!)

    use_store_credit_balance
  end

  def send_email_payment_confirmation(journal_transaction_id)
    Spree::OrderMailer.payment_confirmed(self, journal_transaction_id).deliver_later
  end

  def add_credit_store
    return if payments.bank_transfers.completed.count <= 0
    amount = 0
    payments.bank_transfers.completed.each do |payment|
      amount += payment.amount
    end
    if amount > 0
      user.store_credits.create!(amount: amount, category_id: 2, currency: currency, created_by: 
        Spree::StoreCredit.default_created_by, memo: "Cancelled order #{number}", action_originator: self)
    end
  end

  def generate_order_number(options = {})
    options[:length]  ||= ORDER_NUMBER_LENGTH
    options[:letters] ||= ORDER_NUMBER_LETTERS
    options[:prefix]  ||= ORDER_NUMBER_PREFIX

    possible = (0..9).to_a
    possible += ('A'..'Z').to_a if options[:letters]

    if preorder? && !number.nil? && number.first == "R" 
      self.number = loop do
        # Make a random number.
        random = "#{options[:prefix]}#{(0...options[:length]).map { possible.sample }.join}"
        # Use the random  number if no other order exists with it.
        if self.class.exists?(number: random)
          # If over half of all possible options are taken add another digit.
          options[:length] += 1 if self.class.count > (10**options[:length] / 2)
        else
          break random
        end
      end
    elsif !preorder? && !number.nil? && number.first == "P"
      self.number = loop do
        # Make a random number.
        random = "#{options[:prefix]}#{(0...options[:length]).map { possible.sample }.join}"
        # Use the random  number if no other order exists with it.
        if self.class.exists?(number: random)
          # If over half of all possible options are taken add another digit.
          options[:length] += 1 if self.class.count > (10**options[:length] / 2)
        else
          break random
        end
      end
    else
      self.number ||= loop do
        # Make a random number.
        random = "#{options[:prefix]}#{(0...options[:length]).map { possible.sample }.join}"
        # Use the random  number if no other order exists with it.
        if self.class.exists?(number: random)
          # If over half of all possible options are taken add another digit.
          options[:length] += 1 if self.class.count > (10**options[:length] / 2)
        else
          break random
        end
      end
    end
  end

  def use_store_credit_balance
    # this can happen when multiple payments are present, auto_capture is
    # turned off, and one of the payments fails when the user tries to
    # complete the order, which sends the order back to the 'payment' state.
    authorized_total = payments.pending.sum(:amount)

    if preorder?
      remaining_total = outstanding_down_payment - authorized_total 
    else
      remaining_total = outstanding_balance - authorized_total
    end

    return if remaining_total <= 0

    if user.store_credits.any?
      payment_method = Spree::PaymentMethod::StoreCredit.first

      user.store_credits.order_by_priority.each do |credit|
        break if remaining_total.zero?
        next if credit.amount_remaining.zero?

        amount_to_take = [credit.amount_remaining, remaining_total].min
        payments.create!(source: credit,
                         payment_method: payment_method,
                         amount: amount_to_take,
                         state: 'checkout',
                         response_code: credit.generate_authorization_code)
        remaining_total -= amount_to_take
      end
    end

    other_payments = payments.checkout.not_store_credits
    if remaining_total.zero?
      other_payments.each(&:invalidate!)
    elsif other_payments.size == 1
      other_payments.first.update_attributes!(amount: remaining_total)
    end

    payments.reset

    if !preorder?
      if payments.where(state: %w(checkout pending)).sum(:amount) != total
        errors.add(:base, Spree.t("store_credit.errors.unable_to_fund")) && (return false)
      end
    end
  end

  # bayar order pake store credit dengan jumlah yg ditentukan
  def use_store_credit_balance_with_value(amount_sc)
    # this can happen when multiple payments are present, auto_capture is
    # turned off, and one of the payments fails when the user tries to
    # complete the order, which sends the order back to the 'payment' state.
    authorized_total = payments.pending.sum(:amount)

    if preorder?
      if amount_sc > outstanding_down_payment - authorized_total
        remaining_total = outstanding_down_payment - authorized_total
      else
        remaining_total = amount_sc
      end
    else
      if amount_sc > outstanding_balance - authorized_total
        remaining_total = outstanding_balance - authorized_total
      else
        remaining_total = amount_sc
      end
    end

    return if remaining_total <= 0

    if user.store_credits.any?
      payment_method = Spree::PaymentMethod::StoreCredit.first

      user.store_credits.order_by_priority.each do |credit|
        break if remaining_total.zero?
        next if credit.amount_remaining.zero?

        amount_to_take = [credit.amount_remaining, remaining_total].min
        payments.create!(source: credit,
                         payment_method: payment_method,
                         amount: amount_to_take,
                         state: 'checkout',
                         response_code: credit.generate_authorization_code)
        remaining_total -= amount_to_take
      end
    end

    other_payments = payments.checkout.not_store_credits
    if remaining_total.zero?
      other_payments.each(&:invalidate!)
    elsif other_payments.size == 1
      other_payments.first.update_attributes!(amount: remaining_total)
    end

    payments.reset

    if !preorder?
      if payments.where(state: %w(checkout pending)).sum(:amount) != total
        errors.add(:base, Spree.t("store_credit.errors.unable_to_fund")) && (return false)
      end
    end
  end

  def validate_line_item_availability
    checking_order_type
    availability_validator = Spree::Stock::AvailabilityValidator.new
    raise Spree::Order::InsufficientStock unless line_items.all? { |line_item| availability_validator.validate(line_item) }
  end

  def checking_order_type
    line_items.all.each do |line_item|
      if line_item.variant.preorder? != preorder?
        return errors.add(:base, 'Anda tidak diperkenankan mempunyai pesanan Preorder & 
          Ready Stock di dalam 1 keranjang belanja yang sama. Periksa kembali keranjang belanja Anda. ')
      end
    end
  end
end