Spree::OrderCancellations.class_eval do
  def short_ship(inventory_units, whodunnit:nil)
    if inventory_units.map(&:order_id).uniq != [@order.id]
      raise ArgumentError, "Not all inventory units belong to this order"
    end

    unit_cancels = []

    Spree::Order.transaction do
      Spree::OrderMutex.with_lock!(@order) do
        Spree::InventoryUnit.transaction do
          inventory_units.each do |iu|
            unit_cancels << short_ship_unit(iu, whodunnit: whodunnit)
          end

          update_shipped_shipments(inventory_units)
          Spree::OrderMailer.inventory_cancellation_email(@order, inventory_units.to_a).deliver_later if Spree::OrderCancellations.send_cancellation_mailer
        end

        @order.update!

        refund_to_store_credit(inventory_units)

        @order.update!


        if short_ship_tax_notifier
          short_ship_tax_notifier.call(unit_cancels)
        end
      end
    end

    if @order.inventory_units.not_canceled.count < 1 
      @order.shipments.update_all(state: "canceled")
      @order.update_columns(state: "canceled", shipment_state: "canceled", payment_state: "void")
    end

    unit_cancels
  end

  private

  def restock(inventory_unit, unit_cancel)
    if inventory_unit.on_hand?
      inventory_unit.shipment.stock_location.restock inventory_unit.variant, inventory_unit.line_item.quantity, unit_cancel
    end

    if inventory_unit.backordered?
      inventory_unit.shipment.stock_location.restock_backordered inventory_unit.variant, inventory_unit.line_item.quantity, 
      inventory_unit.outstanding_unit, unit_cancel
    end
  end

  def short_ship_unit(inventory_unit, whodunnit:nil)
    unit_cancel = Spree::UnitCancel.create!(
      inventory_unit: inventory_unit,
      reason: Spree::UnitCancel::DEFAULT_REASON,
      created_by: whodunnit
    )
    unit_cancel.adjust!
    restock(inventory_unit, unit_cancel)
    if !inventory_unit.canceled?
      inventory_unit.cancel!
      post_journal_inventory_from_cancel_item(inventory_unit) if @order.paid? && !@order.preorder?
    end
    unit_cancel
  end

  def post_journal_inventory_from_cancel_item(inventory_unit)
    @inventory = Journal::Account.find_by_code("1-10203").id
    @cost_of_sales = Journal::Account.find_by_code("5-50401").id
    qty_cancel = inventory_unit.split_invoice? ? inventory_unit.expected_shipped : inventory_unit.line_item.quantity*inventory_unit.line_item.qty_per_pack
    amount_cost_price = qty_cancel * inventory_unit.variant.cost_price.to_i
    journal_entry = Journal::JournalEntry.create(description: "Cancel Order: #{inventory_unit.order.number} - #{qty_cancel} pcs X #{inventory_unit.variant.sku}", date: DateTime.now)
    # credit cost of sales
    Journal::JournalItem.create(account_id: @cost_of_sales, description: "Cancel Order: #{inventory_unit.order.number} - #{qty_cancel} pcs X #{inventory_unit.variant.sku}", credit: amount_cost_price, currency: "IDR", originator: inventory_unit, journal_entry_id: journal_entry.id)
    # debit inventory
    Journal::JournalItem.create(account_id: @inventory, description: "Cancel Order: #{inventory_unit.order.number} - #{qty_cancel} pcs X #{inventory_unit.variant.sku}", debit: amount_cost_price, currency: "IDR", originator: inventory_unit, journal_entry_id: journal_entry.id)
  end

  def refund_to_store_credit(inventory_units)
    return if !inventory_units.first.order.paid?
    order = inventory_units.first.order

    order.payments.store_credits.pending.each(&:void_transaction!)
    if order.payments.bank_transfers.completed.count > 0
      amount = 0
      order.payments.bank_transfers.completed.each do |payment|
        amount += payment.amount
      end
      if amount > 0
        order.user.store_credits.create!(amount: amount, category_id: 2, currency: "IDR", created_by: 
          Spree::StoreCredit.default_created_by, memo: "Cancelled product item in order ID #{order.number}", action_originator: order)
      end
    end

    order.payments.completed.each(&:cancel!)

    order.update!
    order.reload

    order.add_store_credit_payments_from_admin
    order.update!
  end
end