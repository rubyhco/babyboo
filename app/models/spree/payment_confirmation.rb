module Spree
  class PaymentConfirmation < Spree::Base
  	include Filterable
    has_many :contents, class_name: "Spree::PaymentConfirmationItem", dependent: :destroy
    belongs_to :user, class_name: "Spree::User"
    after_create :set_status

    scope :sender_name_contain, -> (sender){where("sender_name like ?", "%#{sender}%")}
    scope :member_contain, -> (member){joins(:user).where("name like ?", "%#{member}%")}
    scope :amount_is, -> (amount_is){where(amount: amount_is)}
    scope :invoice_is, -> (id_inv){joins(:contents).where("spree_payment_confirmation_items.invoice_id = ?", id_inv)}
    scope :order_is, -> (id_order){joins(contents: :order).where("spree_orders.number = ?", id_order)}
    scope :status_is, -> (status){where(status: status)}
    
    def confirmed!
      self.status = "confirmed"
      self.save
    end

    def waiting?
      status == "waiting_for_confirmation"
    end

    def send_sms
      sms = RajaSms.new
      if self.contents.empty?
        return if self.user.nil?
        return if self.user.phone.nil?
        phone = self.user.phone
      else
        content = self.contents.where("order_id IS NOT NULL OR invoice_id IS NOT NULL").last
        if content.invoice_id.nil?
          phone = content.order.dropship? ? content.order.dropship_phone : content.order.ship_address.phone
        else
          order_last = content.invoice.items.last.order
          phone = order_last.dropship? ? order_last.dropship_phone : order_last.ship_address.phone
        end
      end
      sms.send_sms(phone, "Terimakasih. Pembayaran untuk pesanan anda sebesar #{amount} 
          telah kami konfirmasi. Pesanan Anda akan segera kami proses -BrandedBabys.com")
    end

    private

    def set_status
      self.status = "waiting_for_confirmation"
      self.save
    end
  end
end
