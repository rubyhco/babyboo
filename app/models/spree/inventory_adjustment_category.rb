module Spree
  class InventoryAdjustmentCategory < Spree::Base
    belongs_to :account, class_name: "Journal::Account", foreign_key: 'journal_account_id'
    belongs_to :account_rmb, class_name: "Journal::Account", foreign_key: 'journal_account_rmb_id'
  	validates :name, :journal_account_id, :journal_account_rmb_id, presence: true
  end
end