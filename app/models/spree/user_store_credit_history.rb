module Spree
  class UserStoreCreditHistory < Spree::Base
  	include Filterable
    belongs_to :user, class_name: "Spree::User"
    belongs_to :store_credit_event, class_name: "Spree::StoreCreditEvent"
    belongs_to :category, class_name: "Spree::StoreCreditCategory"


    scope :start_date, -> (start_date) { where("created_at >= ?", start_date) }
	  scope :end_date, -> (end_date) { where("created_at <= ?", end_date) }
	  scope :name_user, -> (name){joins(:user).where("name like ?", "%#{name}%")}

	  self.whitelisted_ransackable_associations = %w[user store_credit_event category]
  	self.whitelisted_ransackable_attributes = %w[id created_at updated_at memo balance category_id]
  end
end