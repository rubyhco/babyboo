Spree::ShippingMethod.class_eval do
  belongs_to :account, class_name: "Journal::Account", foreign_key: 'account_id'

  validates :account, presence: true

  WEIGHTCITY= "Spree::Calculator::Shipping::WeightCalculator"
  CARGO="Spree::Calculator::Shipping::CargoCalculator"

  def export_rates_by_calc(email= "tiara@rubyh.co", calc_type= WEIGHTCITY, options = {})
    # return false if calc_type != WEIGHTCITY || calc_type != CARGO
    if calc_type == WEIGHTCITY
      a = CSV.generate(options) do |csv|
        csv << ["shipping method", "country", "state", "city", "price", "min weight", "admin price", "etd"]
        self.shipping_rates_per_methods.each do |rt|
          csv << [rt.shipping_method.name, rt.country_name, rt.state_name, rt.city_name, rt.price.to_s, rt.min_weight, rt.adm_price.to_s, rt.etd]
        end
      end
    elsif calc_type == CARGO
      a = CSV.generate(options) do |csv|
        csv << ["shipping method", "country", "state", "city", "admin price", "etd"] + self.cargo_shipping_rates_per_methods.first.cargo_detail_shipping_rates_per_methods.sort.pluck(:weight)
        self.cargo_shipping_rates_per_methods.each do |rt|
          f = [rt.shipping_method.name, rt.country_name, rt.state_name, rt.city_name, rt.adm_price.to_s, rt.etd]
          l = rt.cargo_detail_shipping_rates_per_methods.sort.pluck(:price.to_s)
          csv << f + l
        end
      end
    end
    user = Spree::User.find_by_email(email)
    Spree::OrderMailer.export_shipping_method(user, a).deliver
  end
end