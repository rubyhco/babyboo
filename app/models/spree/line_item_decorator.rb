Spree::LineItem.class_eval do
  belongs_to :invoice, class_name: "Spree::Invoice"
  scope :dp_confirm, -> { Spree::LineItem.joins(:order, :inventory_units).where("spree_orders.preorder=? && spree_orders.payment_state = ? && spree_inventory_units.state != ?", true, "paid", "canceled")}
  scope :pending_on_dp, -> { Spree::LineItem.joins(:order).where("spree_orders.preorder=? && 
    spree_orders.payment_state = ?", true, "balance_due")}
  
  def order_payment_state_human
    case order.payment_state
    when "paid"
      "Paid"
    when "balance_due"
      if order.preorder?
        "Pending on DP"
      else
        "Pending"
      end
    when "void"
      "Cancel"
    when "credit_owed"
      "Overpayment"
    end
  end

  def amount
    price * ( quantity * qty_per_pack)
  end

  def total_items
    qty_per_pack
  end

  def total_line_items
    quantity * qty_per_pack
  end

  def total_weight
    self.variant.weight * quantity
  end

  def real_cost_price
    real_cost_price = total_line_items * variant.cost_price.to_i
  end

  def options=(options = {})
    return unless options.present?

    assign_attributes options

    # When price is part of the options we are not going to fetch
    # it from the variant. Please note that this always allows to set
    # a price for this line item, even if there is no existing price
    # for the associated line item in the order currency.
    unless options.key?(:price) || options.key?('price')
      set_price_level
    end

    if options.key?(:qty_per_pack) || options.key?('qty_per_pack')
      update_stock_movement_and_stock_items
    end
  end
  
  def set_pricing_attributes
    return handle_copy_price_override if respond_to?(:copy_price)
    self.currency ||= order.currency
    self.cost_price ||= variant.cost_price
    set_price_level if order.approver.nil? 
    set_supplier_id
    set_qty_per_pack unless order.complete?
    true
  end

  def discount_amount
    self.variant.price - self.price
  end

  def total_pieces
    self.quantity * self.qty_per_pack
  end

  def sufficient_stock?
    Spree::Stock::Quantifier.new(variant).can_supply?(total_line_items, self.order.preorder?)
  end

  def total_down_payment
    self.quantity * self.down_payment_price
  end

  private

  def set_price_level
    if variant.product.sale?
      self.money_price = variant.price_for(pricing_options, 'sale')
      if variant.preorder?
        if order.user.nil?
          self.down_payment_price = variant.preorder_price_for(pricing_options).try(:to_d)
        else
          if order.user.disable_dp?
            self.down_payment_price = 0
          else
            self.down_payment_price = variant.preorder_price_for(pricing_options).try(:to_d)
          end
        end
      else
        self.down_payment_price = 0
      end
    else
      price_level = Spree::PriceLevel.last
      qty = self.order.item_count
      member_type = self.order.user.nil? ? "Default" : self.order.user.member_type
      if variant.preorder?
        if order.user.nil?
          self.down_payment_price = variant.preorder_price_for(pricing_options).try(:to_d)
        else
          if order.user.disable_dp?
            self.down_payment_price = 0
          else
            self.down_payment_price = variant.preorder_price_for(pricing_options).try(:to_d)
          end
        end
      else
        self.down_payment_price = 0
      end

      if member_type == "Gold"
        self.money_price = variant.price_for(pricing_options, "gold")
      elsif member_type == "Silver"
        if qty < price_level.gold_level
          self.money_price = variant.price_for(pricing_options, "silver")
        else
          self.money_price = variant.price_for(pricing_options, "gold")
        end
      else
        if qty < price_level.silver_level
          self.money_price = variant.price_for(pricing_options)
        elsif qty < price_level.gold_level
          self.money_price = variant.price_for(pricing_options, "silver")
        else
          self.money_price = variant.price_for(pricing_options, "gold")
        end
      end
    end
  end

  def update_stock_movement_and_stock_items
    stock_movement = Spree::StockMovement.where(stock_item_id: self.variant.stock_items.first.id, originator: self.inventory_units.first.shipment).first
    if stock_movement.quantity.abs != self.total_pieces
      selisih = stock_movement.quantity.abs - self.total_pieces
      
      # update stock movement quantity
      qty_update_movement = stock_movement.quantity + selisih
      stock_movement.update_columns(quantity: qty_update_movement)
      
      # update stock item coh
      qty_update_stock = stock_movement.stock_item.count_on_hand + selisih
      stock_movement.stock_item.update_columns(count_on_hand: qty_update_stock)
    end
  end

  def set_supplier_id
    self.supplier_id = variant.product.supplier_id
  end

  def set_qty_per_pack
    self.qty_per_pack = variant.product.total_items
  end
end