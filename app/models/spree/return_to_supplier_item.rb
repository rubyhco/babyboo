module Spree
  class ReturnToSupplierItem < Spree::Base
  	belongs_to :variant, class_name: "Spree::Variant"
  	belongs_to :return_to_supplier, class_name: "Spree::ReturnToSupplier"

    self.whitelisted_ransackable_associations = %w[variant]
  end
end
