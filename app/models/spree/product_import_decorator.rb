Spree::ProductImport.class_eval do
  def open_import_data_csv
    if Rails.env.production?
      open(ENV["SITE_URL"] + self.data_file.url,"r:#{encoding_csv}").read.encode('utf-8')
    else
      open(self.data_file.path,"r:#{encoding_csv}").read.encode('utf-8')
    end
  end

  def csv_file
    if Rails.env.production?  
      CSV.parse(open(ENV["SITE_URL"] + self.data_file.url).read, :col_sep => separatorChar)
    else
      CSV.parse(open(self.data_file.path).read, :col_sep => separatorChar)
    end
  end
end