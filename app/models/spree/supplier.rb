module Spree
  class Supplier < Spree::Base
    has_many :purchase_orders, class_name: "Spree::PurchaseOrder"
    has_many :products, class_name: "Spree::Product"

    scope :available_to_ship, -> {joins(:purchase_orders).where("spree_purchase_orders.shipping_state != ? 
      AND spree_purchase_orders.shipping_state != ?", "shipped", "completed")}
  end
end

