Spree::StoreCreditCategory.class_eval do
  belongs_to :account, class_name: "Journal::Account", foreign_key: 'journal_account_id'

  validates :name, :journal_account_id, presence: true
end