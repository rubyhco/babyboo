Spree::Price.class_eval do
  money_methods :silver_amount, :gold_amount, :sale_amount

  alias_method :money_silver, :display_silver_amount
  alias_method :money_gold, :display_gold_amount
  alias_method :money_sale, :display_sale_amount

  def sale_price
    sale_amount
  end

  def silver_price
    silver_amount
  end

  def gold_price
    gold_amount
  end

  def silver_price=(price)
  	self[:silver_amount] = Spree::LocalizedNumber.parse(price)
  end

  def gold_price=(price)
  	self[:gold_amount] = Spree::LocalizedNumber.parse(price)
  end

  def sale_price=(price)
    self[:sale_amount] = Spree::LocalizedNumber.parse(price)
  end
end