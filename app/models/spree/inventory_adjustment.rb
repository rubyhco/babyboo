module Spree
  class InventoryAdjustment < Spree::Base
    has_many :items, class_name:"Spree::InventoryAdjustmentItem", dependent: :destroy

    self.whitelisted_ransackable_attributes = %w[id name state inventory_date created_at updated_at]

    state_machine :state, initial: :draft do
      event :in_progress do
        transition to: :in_progress, from: :draft
      end

      event :validate do
        transition to: :validated, from: :in_progress
      end

      before_transition to: :validated, do: :before_validated
    end

    def before_validated
      Spree::InventoryAdjustment.transaction do
        items.each do |item|
          if item.selisih == 0
            item.destroy!
          else
            item.variant.stock_items.first.stock_movements.create!(quantity: item.selisih, originator: self)
            item.create_journal
          end
        end
      end
    end

  end
end
