module Spree
  require 'rake'
  class ShipmentGroup < Spree::Base
    include Filterable
    has_many :shipments, class_name: "Spree::Shipment"
    belongs_to :user, class_name: "Spree::User"
    belongs_to :shipping_method, class_name: "Spree::ShippingMethod"

    state_machine :status, initial: :pending do
      event :ready_to_pack do
        transition to: :ready_to_pack, from: :pending
      end

      event :processing do
        transition to: :processing, from: :ready_to_pack
      end

      event :packed do
        transition to: :packed, from: :processing
      end

      event :ship do
        transition to: :shipped, from: :packed
      end
      after_transition to: :shipped, do: :after_ship

      event :complete do
        transition to: :completed, from: :shipped
      end
      after_transition to: :completed, do: :after_complete
    end

    scope :status_contain, -> (status) { where status: status }
    scope :ekspedisi_is, -> (ekspedisi) { where shipping_method_id: ekspedisi}
    scope :actual_cost, -> (actual_cost) {where("actual_cost #{actual_cost}")}
    scope :user_is, -> (user) {where user_id: user}
    scope :name_contain, -> (name_contain) { joins(:user).where("spree_users.name like?", "%#{name_contain}%")}
    scope :ship_id_is, -> (ship) { where(id: ship)}
    scope :can_be_deleted, -> {where("status = ? || status = ?", "ready_to_pack", "pending")}

    def is_pending?
      status == "pending" || status == "ready_to_pack"
    end

    def self.generate_shipment_group
      # QUICK CHEAT TO PREVENT DUPLICATE
      Spree::ShipmentGroup.can_be_deleted.delete_all

      # FOR READY STOCK
      orders = Spree::Order.ready_stock.ready_to_ship
      preorders = Spree::InvoiceItem.joins(:shipment).joins(:invoice).where("spree_shipments.state != ? && spree_shipments.state != ? && spree_shipments.state != ? && spree_invoices.lock = ?", 
        "shipped", "canceled", "pending", false)

      ship_address_ids_1 = orders.pluck(:ship_address_id).uniq
      ship_address_ids_2 = preorders.pluck(:ship_address_id).uniq
      ship_address_ids = (ship_address_ids_1 + ship_address_ids_2).uniq

      shipping_method_ids = Spree::ShippingMethod.all.pluck(:id)

      ship_address_ids.each do |id|
        shipping_method_ids.each do |sid|
          group = Spree::ShipmentGroup.create(shipping_method_id: sid)
          orders.where(ship_address_id: id).each do |order|
            order.shipments.each do |shipment|
              next if shipment.shipping_method.id != sid
              if !shipment.shipment_group.nil?
                next if !shipment.shipment_group.is_pending?
              end
              shipment.update_attributes(shipment_group_id: group.id)
            end
          end
          preorders.where(ship_address_id: id).each do |preorder|
            next if preorder.shipping_method_id != sid
            if !preorder.shipment.shipment_group.nil?
              next if !preorder.shipment.shipment_group.is_pending?
            end
            preorder.shipment.update_attributes(shipment_group_id: group.id)
          end
          if !group.shipments.empty?
            user_id = group.shipments.first.order_or_invoice.user_id
            group.update_attributes(shipping_method_id: sid, user_id: user_id)
            group.update!
          else
            group.delete
          end
        end
      end
      Spree::ShipmentGroup.can_be_deleted.each do |x|
        if x.shipments.empty?
          x.delete
        end
      end
      Spree::ShipmentGroup.where("total is null").delete_all
    end
    
    def cek_ongkir
      return if actual_cost.nil?
      if actual_cost < total
        amount = total - actual_cost
        add_store_credit(amount, "kelebihan bayar ongkir - #{id}")
      elsif actual_cost > total
        amount = actual_cost - total

        shipping_account_id = self.shipping_method.account_id
        invoice = Journal::Invoice.create(vendor: user.name, currency: "IDR", due_date: created_at+3.days, total: amount, payment_state: "pending", spree_user_id: user.id)
        invoice.items.create(category: shipping_account_id, amount: amount, description: "Kurang Ongkir Shipment #{id}", invoice_id: invoice.id)
        Spree::BrandedBabysMailer.kurang_ongkir(amount, user, id).deliver!
      end
    end

    def add_store_credit(amount, memo)
      user.store_credits.create!(amount: amount, category_id: 3, currency: "IDR", created_by: 
        Spree::StoreCredit.default_created_by, memo: memo, action_originator: self)

      # record_to_journal
      shipping_account_id = self.shipping_method.account_id
      store_credit_journal_account_id = Journal::Account.find_by_code("2-20219").id
      journal_entry = Journal::JournalEntry.create(description: "Kelebihan Ongkir: SH#{id} - #{user.name}", date: DateTime.now)
      # credit DP
      Journal::JournalItem.create(account_id: store_credit_journal_account_id, description: "Kelebihan Ongkir: SH#{id} - #{user.name}", credit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
      # debit Store Credit
      Journal::JournalItem.create(account_id: shipping_account_id, description: "Kelebihan Ongkir: SH#{id} - #{user.name}", debit: amount, currency: "IDR", originator: self, journal_entry_id: journal_entry.id)
    end

    def update!
      total_cost = shipments.sum(:cost)
      self.update_attributes(total: total_cost)
    end

    def after_ship
      Spree::ShipmentGroup.transaction do
        shipments.update_all(tracking: resi)
        shipments.each do |shipment|
          shipment.ship! unless shipment.shipped?
        end
        # create_movement_warehouse
        selisih_barang
        send_sms
      end
    end

    def create_movement_warehouse
      shipments.each do |shipment|
        shipment.inventory_units.each do |inventory_unit|
          inventory_unit.variant.stock_items.first.stock_warehouse_movements.create(quantity: inventory_unit.shipped * -1, description: "Shipment from #{shipment.order_or_invoice.number} - #{shipment.order_or_invoice.user.present? ? shipment.order_or_invoice.user.name : "Cannot Display Cust Name"}", originator: shipment)
        end
      end
    end

    def after_complete
      cek_ongkir
    end

    def selisih?
      return false if shipments.blank?
      selisih = 0
      shipments.each do |shipment|
        shipment.inventory_units.each do |inventory_unit|
          if inventory_unit.outstanding_unit > 0
            selisih += 1
          end
        end
      end
      if selisih > 0
        return true
      else
        return false
      end 
    end

    def selisih_barang
      shipments.each do |shipment|
        if shipment.order.nil?
          shipment.inventory_units.each do |inventory_unit|
            outstanding_unit = inventory_unit.outstanding_unit
            if outstanding_unit > 0
              Spree::ShipmentGroup.transaction do
                amount = outstanding_unit * inventory_unit.invoice_item.single_price
                user.store_credits.create!(amount: amount, category_id: 4, currency: "IDR", created_by: Spree::StoreCredit.default_created_by, memo: "Refund no stock: Invoice ##{shipment.invoice_id}:#{inventory_unit.variant.sku}", action_originator: inventory_unit.invoice_item)
                
                # record_to_journal
                record_no_stock_to_journal(amount, 
                  "No Stock: #{shipment.invoice.number} - #{inventory_unit.variant.sku} - #{user.name}", shipment.invoice)
                
                cost_price_amount = outstanding_unit * inventory_unit.variant.cost_price.to_i
                record_no_stock_inventory(cost_price_amount, 
                  "No Stock: #{shipment.invoice.number} - #{outstanding_unit} pieces X #{inventory_unit.variant.sku} - #{user.name}", shipment.invoice)
                
                Spree::BrandedBabysMailer.add_store_credit_from_no_stock(user, inventory_unit, amount).deliver!
                lost_item = Spree::LostItem.create(description: "No Stock Invoice ##{shipment.invoice_id}: #{outstanding_unit} pieces X #{inventory_unit.variant.sku}")
                if inventory_unit.variant.stock_items.first.count_on_hand > 0
                  inventory_unit.variant.stock_items.first.stock_movements.create!(quantity: outstanding_unit, originator: lost_item)
                end
              end
            end
          end
        else
          shipment.inventory_units.each do |inventory_unit|
            outstanding_unit = inventory_unit.outstanding_unit
            if outstanding_unit > 0
              Spree::ShipmentGroup.transaction do
                amount = outstanding_unit * inventory_unit.line_item.price
                user.store_credits.create!(amount: amount, category_id: 4, currency: "IDR", created_by: Spree::StoreCredit.default_created_by, memo: "Refund no stock: Order ##{shipment.order.number}:#{inventory_unit.variant.sku}", action_originator: inventory_unit.line_item)
                
                # record_to_journal
                record_no_stock_to_journal(amount, 
                  "No stock: #{shipment.order.number} - #{inventory_unit.variant.sku}", shipment.order)

                cost_price_amount = outstanding_unit * inventory_unit.variant.cost_price.to_i

                record_no_stock_inventory(cost_price_amount, 
                  "No stock: #{shipment.order.number} - #{outstanding_unit} pieces X #{inventory_unit.variant.sku}", shipment.order)

                Spree::BrandedBabysMailer.add_store_credit_from_no_stock(user, inventory_unit, amount).deliver!
                lost_item = Spree::LostItem.create(description: "No Stock Order ##{shipment.order.number}: #{outstanding_unit} pieces X #{inventory_unit.variant.sku}")
                if inventory_unit.variant.stock_items.first.count_on_hand > 0
                  inventory_unit.variant.stock_items.first.stock_movements.create!(quantity: outstanding_unit, originator: lost_item)
                end
              end
            end
          end
        end
      end
    end

    def record_no_stock_inventory(amount, description, originator)
      
      lost_on_inventory_id = Journal::Account.find_by_code("1-10205").id
      cost_of_sales_id = Journal::Account.find_by_code("5-50401").id
      journal_entry = Journal::JournalEntry.create(description: description, date: DateTime.now)
      # credit store credit
      Journal::JournalItem.create(account_id: cost_of_sales_id, description: description, credit: amount, currency: "IDR", originator: originator, journal_entry_id: journal_entry.id)
      # debit sales return
      Journal::JournalItem.create(account_id: lost_on_inventory_id, description: description, debit: amount, currency: "IDR", originator: originator, journal_entry_id: journal_entry.id)


    end

    def record_no_stock_to_journal(amount, description, originator)
      sales_return_id = Journal::Account.find_by_code("4-40206").id
      store_credit_journal_account_id = Journal::Account.find_by_code("2-20219").id
      journal_entry = Journal::JournalEntry.create(description: description, date: DateTime.now)
      # credit store credit
      Journal::JournalItem.create(account_id: store_credit_journal_account_id, description: description, credit: amount, currency: "IDR", originator: originator, journal_entry_id: journal_entry.id)
      # debit sales return
      Journal::JournalItem.create(account_id: sales_return_id, description: description, debit: amount, currency: "IDR", originator: originator, journal_entry_id: journal_entry.id)
    end

    def send_sms
      sms = RajaSms.new
      if !shipments.first.order.nil?
        order = shipments.first.order
      else
        order = shipments.first.invoice.items.first.order
      end
      phone = order.dropship? ? order.dropship_phone : order.ship_address.phone
      if order.dropship?
        # sms.send_sms(phone, "Paket anda sudah dikirim. No order: #{shipment_orders_and_invoices_numbers}. No resi: #{shipping_method.name}-#{resi}. Thank you for your order :)")
      else
        # sms.send_sms(phone, "Paket anda sudah dikirim. No order: #{shipment_orders_and_invoices_numbers}. No resi: #{shipping_method.name}-#{resi}. Thank you for your order :) Brandedbabys.com")
      end
    end

    def shipment_orders_and_invoices_numbers
      shipments.collect {|x|x.order_or_invoice.number}.to_sentence
    end

    def total_ongkir
      shipments.sum(:cost)
    end

    def subtotal
      amount = 0

      order_ids = shipments.collect{|x| x.order_id}.uniq.compact

      if order_ids.any?
        order_ids.each do |order_id|
          amount += Spree::Order.find_by_id(order_id).item_total
        end
      end

      invoice_ids = shipments.collect{|x| x.invoice_id}.uniq.compact

      if invoice_ids.any?
        invoice_ids.each do |invoice_id|
          amount += Spree::Invoice.find_by_id(invoice_id).subtotal
        end
      end

      return amount
    end

    def total_order_invoice
      amount = 0

      order_ids = shipments.collect{|x| x.order_id}.uniq.compact

      if order_ids.any?
        order_ids.each do |order_id|
          amount += Spree::Order.find_by_id(order_id).total
        end
      end

      invoice_ids = shipments.collect{|x| x.invoice_id}.uniq.compact

      if invoice_ids.any?
        invoice_ids.each do |invoice_id|
          amount += Spree::Invoice.find_by_id(invoice_id).total
        end
      end

      return amount
    end

    def total_adjustment
      total_order_invoice - total_ongkir - subtotal
    end
  end
end