Spree::Variant.class_eval do    
  has_many :transfer_kurs, class_name: "Spree::VariantKur", foreign_key: 'variant_id'
  has_many :container_items, class_name: "Spree::ContainerItem"
  has_many :inventory_units, class_name: "Spree::InventoryUnit"
  has_many :refund_items, class_name: "Journal::RefundPurchaseItem"

  def real_hpp
    if cost_currency == "RMB"
      real_kurs * cost_price
    else
      cost_price.to_i
    end
  end

  def biaya_import
    container_ids = self.container_items.pluck(:container_id).uniq
    if container_ids.present?
      a = Spree::Container.where(id: container_ids).sum(&:biaya_per_pcs)/container_ids.count
    else
      a = 0
    end
    return a.to_i
  end

  def real_kurs
    return 1 if cost_currency == "IDR"
    tfund_ids = transfer_kurs.collect(&:transfer_fund_id).uniq
    transfer_funds = Journal::TransferFund.where(id: tfund_ids)
    if transfer_funds.present?
      real_kurs = transfer_funds.sum(:kurs)/transfer_funds.count
      return real_kurs
    else
      return Spree::Store.find_by_id(1).default_kurs.to_i
    end
  end

  def kurs_rmb
    tfund_ids = transfer_kurs.collect(&:transfer_fund_id).uniq
    transfer_funds = Journal::TransferFund.where(id: tfund_ids)
    if transfer_funds.present?
      real_kurs = transfer_funds.sum(:kurs)/transfer_funds.count
      return real_kurs
    else
      return Spree::Store.find_by_id(1).default_kurs.to_i
    end
  end

  def can_supply?(quantity = 1)
    Spree::Stock::Quantifier.new(self).can_supply?(quantity, product.preorder?)
  end

  def cost_price_pack
    cost_price.to_i * self.product.total_items
  end

  def self.cek_stock(options = {})
    CSV.generate(options) do |csv|
      csv << ["id", "sku", "total order","count on hand", "movement", "dp confirmed", "pending on dp", "po supplier", "override"]
      all.each do |variant|
        csv << [variant.id, variant.sku, Spree::LineItem.joins(:order).where("spree_orders.state = ? AND spree_line_items.variant_id = ?", "complete", variant.id).sum(:quantity) * variant.product.total_items, variant.stock_items.first.count_on_hand, variant.stock_movements.sum(:quantity),Spree::LineItem.where(variant_id: variant.id).dp_confirm.sum(:quantity)*variant.product.total_items, Spree::LineItem.where(variant_id: variant.id).pending_on_dp.sum(:quantity)*variant.product.total_items, variant.stock_items.first.expected_count_on_hand, variant.stock_movements.where(originator_type: "Spree::User").sum(:quantity)]
      end
    end
  end
end