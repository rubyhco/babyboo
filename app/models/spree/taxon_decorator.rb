Spree::Taxon.class_eval do
  scope :open_po, -> {where("is_open = ?", true)}
  scope :po_close, -> {where("is_open = ?", false)}
  scope :permalink_contain, -> (permalink){where("permalink like ?", "%#{permalink}%")}

  def show_all_product_album
		Spree::Taxon.permalink_contain("preorder/album/").each do |taxon|
			taxon.products.collect{|x|x.update_columns(available_on: Time.now)}
		end
	end
end