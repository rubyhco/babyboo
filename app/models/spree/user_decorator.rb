Spree::User.class_eval do
  belongs_to :bank, class_name: "Spree::Bank"
  has_many :store_credit_histories, class_name: "Spree::UserStoreCreditHistory"
  has_many :cash_out_requests, class_name: "Spree::CashOutRequest"
  has_many :invoices, class_name: "Spree::Invoice"

  after_create :set_bank
  scope :active, -> {where(active_user: true)}
  scope :not_gold, -> {where.not(member_type: "Gold")}
  validates_uniqueness_of :phone, on: :create
  after_create :subscribe_user_to_mailing_list

  self.whitelisted_ransackable_attributes = %w[member_type phone email active_user created_at]

  def total_transaction
    old_transaction_total + self.orders.paid.ready_stock.sum(:total) + self.invoices.paid.sum(:total)
  end

  def self.export_model_csv(options = {})
    CSV.generate(options) do |csv|
      csv << ["Registered At", "Email", "Full Name", "Member Type", "Last Transaction Date", "Total Old Transaction", "Total Ready Stock Order", "Total Invoice", "Total Store Credit", "Last Login At", "Status", "Telp"]
      Spree::User.all.each do |user|
        csv << [user.created_at, user.email, user.name, user.member_type, user.orders.present? ? user.orders.last.created_at : "None", user.old_transaction_total, user.orders.paid.ready_stock.sum(:total), user.invoices.paid.sum(:total), user.total_available_store_credit, user.last_sign_in_at, user.active_user? ? "Active" : "Inactive", user.phone.present? ? "0" + user.phone : ""]
      end
    end
  end

  private

  def set_bank
  	bank = Spree::Bank.where(active: true).sample(1).first
  	self.bank_id = bank.id
  	self.save
  end

  def subscribe_user_to_mailing_list
    mailchimp_api_key = Rails.application.secrets.mailchimp_api_key
    mailchimp_list_id = Rails.application.secrets.mailchimp_list_key

    if mailchimp_api_key.present? && mailchimp_list_id.present?
      begin
        gibbon = Gibbon::Request.new(api_key: mailchimp_api_key, debug: true)
        gibbon.lists(mailchimp_list_id).members.create(body: {email_address: self.email, status: "subscribed", merge_fields: {FNAME: self.name}})
      rescue Gibbon::MailChimpError => e
        puts "Mailchimp error: #{e.message} - #{e.raw_body}"
      end
    end
  end
end