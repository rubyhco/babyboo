module Spree
  class CashOutRequest < Spree::Base
    validates :amount, :bank_name, :bank_account_name, :bank_account_no, presence: true

    after_create :begin_operation

    belongs_to :user, class_name: "Spree::User"

    private

    def begin_operation
      Spree::CashOutRequest.transaction do
        authorized_store_credit
        record_to_journal
        store_user_bank_account
      end
    end

    def store_user_bank_account
      user.update_attributes(bank_account_no: bank_account_no, bank_branch: bank_branch, bank_name: bank_name,
        bank_account_name: bank_account_name)
    end

    def authorized_store_credit
      remaining = amount
      user.store_credits.order_by_priority.each do |credit|
        break if remaining.zero?
        next if credit.amount_remaining.zero?

        amount_to_take = [remaining, credit.amount_remaining].min
        auth = credit.authorize(
          amount_to_take,
          "IDR",
          action_originator: self
        )
        credit.capture(amount_to_take, auth, "IDR", action_originator: self)
        remaining -= amount_to_take
      end
    end

    def record_to_journal
      store_credit = Journal::Account.find_by_code("2-20219").id
      bill = Journal::Bill.create(vendor: user.name, currency: "IDR", due_date: Date.today+3.days, total: amount, payment_state: "pending", spree_user_id: user.id, originator: self)
      bill.items.create(category: store_credit, quantity: 1, amount: amount, description: "#{bill.number}: #{user.name} Bank Account: #{bank_name}(#{bank_account_no}) - #{bank_account_name}")
      bill.confirm!
    end
  end
end
