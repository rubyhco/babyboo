Spree::Shipment.class_eval do
  belongs_to :invoice, class_name: "Spree::Invoice", inverse_of: :shipments
  belongs_to :shipment_group, class_name: "Spree::ShipmentGroup"

  def after_cancel
    if invoice.present?
      inventory_units.each {|iu| manifest_restock_invoice(iu)}
    else
      manifest.each { |item| manifest_restock(item) }
    end
    inventory_units.each {|iu| cancel_unit(iu)}
  end


  def cancel_unit(inventory_unit, reason: Spree::UnitCancel::DEFAULT_REASON, whodunnit:nil)
    unit_cancel = nil

    unit_cancel = Spree::UnitCancel.create!(
      inventory_unit: inventory_unit,
      reason: reason,
      created_by: whodunnit
    )

    if !inventory_unit.canceled?
      inventory_unit.cancel!
      post_journal_inventory_from_cancel_order(inventory_unit) if inventory_unit.order.paid? && !inventory_unit.order.preorder?
    end

    unit_cancel
  end

  def post_journal_inventory_from_cancel_order(inventory_unit)
    @inventory = Journal::Account.find_by_code("1-10203").id
    @cost_of_sales = Journal::Account.find_by_code("5-50401").id
    qty_cancel = inventory_unit.split_invoice? ? inventory_unit.expected_shipped : inventory_unit.line_item.quantity*inventory_unit.line_item.qty_per_pack
    amount_cost_price = qty_cancel * inventory_unit.variant.cost_price.to_i
    journal_entry = Journal::JournalEntry.create(description: "Cancel Order: #{inventory_unit.order.number} - #{qty_cancel} pcs X #{inventory_unit.variant.sku}", date: DateTime.now)
    # credit cost of sales
    Journal::JournalItem.create(account_id: @cost_of_sales, description: "Cancel Order: #{inventory_unit.order.number} - #{qty_cancel} pcs X #{inventory_unit.variant.sku}", credit: amount_cost_price, currency: "IDR", originator: inventory_unit, journal_entry_id: journal_entry.id)
    # debit inventory
    Journal::JournalItem.create(account_id: @inventory, description: "Cancel Order: #{inventory_unit.order.number} - #{qty_cancel} pcs X #{inventory_unit.variant.sku}", debit: amount_cost_price, currency: "IDR", originator: inventory_unit, journal_entry_id: journal_entry.id)
  end

  def manifest_restock(item)
    if item.states["on_hand"].to_i > 0
     stock_location.restock item.variant, item.line_item.quantity, self
    end

    if item.states["backordered"].to_i > 0
      stock_location.restock_backordered item.variant, item.line_item.quantity, item.line_item.total_line_items, self
    end
  end

  def manifest_restock_invoice(iu)
    quantity = iu.expected_shipped > 0 ? (iu.expected_shipped/iu.line_item.qty_per_pack) :  iu.line_item.quantity
    if iu.state == "on_hand"
     stock_location.restock iu.variant, quantity, invoice
    end
  end

  def is_shipped?
    self.state == "shipped"
  end

  def get_shipping_address
    if self.order.nil?
      self.invoice.items.first.ship_address
    else
      self.order.ship_address
    end
  end

  def get_shipping_method
    if self.order.nil?
      original_shipping_method_id = self.invoice.items.first.shipping_method_id
      Spree::ShippingMethod.find_by_id original_shipping_method_id
    else
      self.shipping_method
    end
  end

  def order_or_invoice
    if order.nil?
      invoice
    else
      order
    end
  end

  def order_or_invoice_items
    if order.nil?
      invoice.items
    else
      order.line_items
    end
  end

  def dropship?
    if !self.order.nil?
      order.dropship?
    else
      invoice.dropship? || invoice.items.first.order.dropship?
    end
  end

  private

  def after_ship
    order = self.invoice.nil? ? self.order : self.invoice
    order.shipping.ship_shipment(self, suppress_mailer: false)
  end
end