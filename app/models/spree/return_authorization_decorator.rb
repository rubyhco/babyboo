Spree::ReturnAuthorization.class_eval do
  self.whitelisted_ransackable_attributes = %w[created_at]


  private

  def must_have_shipped_units
    false
  end

  def no_previously_exchanged_inventory_units
    false
  end
end