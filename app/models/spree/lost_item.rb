module Spree
  class LostItem < Spree::Base
    before_create :generate_number

    private

    def generate_number
      self.number ||= loop do
        random = "LI#{Array.new(9){ rand(9) }.join}"
        break random unless self.class.exists?(number: random)
      end
    end
  end
end
