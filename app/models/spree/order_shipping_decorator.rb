Spree::OrderShipping.class_eval do
  def ship_shipment(shipment, external_number: nil, tracking_number: nil, suppress_mailer: false)
    ship(
      inventory_units: shipment.inventory_units.shippable,
      stock_location: shipment.stock_location,
      address: shipment.get_shipping_address,
      shipping_method: shipment.get_shipping_method,
      shipped_at: Time.current,
      external_number: external_number,
      # TODO: Remove the `|| shipment.tracking` once Shipment#ship! is called by
      # OrderShipping#ship rather than vice versa
      tracking_number: tracking_number || shipment.tracking,
      suppress_mailer: suppress_mailer
    )
  end

  def ship(inventory_units:, stock_location:, address:, shipping_method:,
           shipped_at: Time.current, external_number: nil, tracking_number: nil, suppress_mailer: false)

    carton = nil

    Spree::InventoryUnit.transaction do
      inventory_units.each(&:ship!)

      carton = Spree::Carton.create!(
        stock_location: stock_location,
        address: address,
        shipping_method: shipping_method,
        inventory_units: inventory_units,
        shipped_at: shipped_at,
        external_number: external_number,
        tracking: tracking_number
      )
    end

    inventory_units.map(&:shipment).uniq.each do |shipment|
      # Temporarily propagate the tracking number to the shipment as well
      # TODO: Remove tracking numbers from shipments.
      shipment.update_attributes!(tracking: tracking_number)

      next unless shipment.inventory_units.reload.all? { |iu| iu.shipped? || iu.canceled? }
      # TODO: make OrderShipping#ship_shipment call Shipment#ship! rather than
      # having Shipment#ship! call OrderShipping#ship_shipment. We only really
      # need this `update_columns` for the specs, until we make that change.
      shipment.update_columns(state: 'shipped', shipped_at: Time.current)
    end

    send_shipment_emails(carton) if stock_location.fulfillable? && !suppress_mailer # e.g. digital gift cards that aren't actually shipped
    fulfill_order_stock_locations(stock_location)
    if @order.class.name == "Spree::Invoice"
      @order.update_status!
    else
      @order.update!
    end
    carton
  end
end

