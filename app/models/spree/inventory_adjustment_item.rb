module Spree
  class InventoryAdjustmentItem < Spree::Base
    belongs_to :inventory_adjustment, class_name: "Spree::InventoryAdjustment"
    belongs_to :variant, class_name: "Spree::Variant"
    belongs_to :category, class_name: "Spree::InventoryAdjustmentCategory", foreign_key: "category_id"

    def selisih
      real_qty - theoritical_qty
    end

    def create_journal
      return if selisih == 0
      amount = selisih.abs * variant.real_hpp
      description = "Inventory Adjustment #{inventory_adjustment_id} #{selisih} x #{variant.sku}"
      acc_inventory_adjustment = category.account
      inventory_idr = Journal::Account.find_by_code("1-10203")
      journal_entry = Journal::JournalEntry.create(description: description, date: DateTime.now)
      if selisih > 0
        journal_debit = Journal::JournalItem.create(account_id: inventory_idr.id, description: description, debit: amount , currency: inventory_idr.currency, originator: self.inventory_adjustment, journal_entry_id: journal_entry.id)
        journal_credit = Journal::JournalItem.create(account_id: acc_inventory_adjustment.id, description: description, credit: amount , currency: acc_inventory_adjustment.currency, originator: self.inventory_adjustment, journal_entry_id: journal_entry.id)
        
        create_journal_rmb if variant.cost_currency == "RMB"
      else
        journal_debit = Journal::JournalItem.create(account_id: acc_inventory_adjustment.id, description: description, debit: amount , currency: acc_inventory_adjustment.currency, originator: self.inventory_adjustment, journal_entry_id: journal_entry.id)
        journal_credit = Journal::JournalItem.create(account_id: inventory_idr.id, description: description, credit: amount , currency: inventory_idr.currency, originator: self.inventory_adjustment, journal_entry_id: journal_entry.id)
        
        create_journal_rmb if variant.cost_currency == "RMB"
      end
    end

    def create_journal_rmb
      # TO DO SESUAIKAN ACCOUNT
      description = "Inventory Adjustment #{inventory_adjustment_id} #{selisih} x #{variant.sku}"
      acc_inventory_adjustment_rmb = category.account_rmb
      inventory_rmb = Journal::Account.find_by_code("1-10204")
      amount_rmb = selisih.abs * variant.cost_price
      journal_entry = Journal::JournalEntry.create(description: description, date: DateTime.now)
      if selisih > 0
        journal_credit = Journal::JournalItem.create(account_id: acc_inventory_adjustment_rmb.id, description: description, credit: amount_rmb , currency: acc_inventory_adjustment_rmb.currency, originator: self.inventory_adjustment, journal_entry_id: journal_entry.id)
        journal_debit = Journal::JournalItem.create(account_id: inventory_rmb.id, description: description, debit: amount_rmb , currency: inventory_rmb.currency, originator: self.inventory_adjustment)
      else
        journal_debit = Journal::JournalItem.create(account_id: acc_inventory_adjustment_rmb.id, description: description, debit: amount_rmb , currency: acc_inventory_adjustment_rmb.currency, originator: self.inventory_adjustment, journal_entry_id: journal_entry.id)
        journal_credit = Journal::JournalItem.create(account_id: inventory_rmb.id, description: description, credit: amount_rmb , currency: inventory_rmb.currency, originator: self.inventory_adjustment)
      end
    end
  end
end
