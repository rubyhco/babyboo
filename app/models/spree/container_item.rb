module Spree
  class ContainerItem < Spree::Base
    include Filterable
    belongs_to :variant, class_name: "Spree::Variant"
    belongs_to :container, class_name: "Spree::Container"
    belongs_to :purchase_order, class_name: "Spree::PurchaseOrder"
    belongs_to :purchase_line_item, class_name: "Spree::PurchaseLineItem"

    after_create :copy_line_items

    scope :shipped, -> {where(status: "shipped")}
    scope :received, -> {where(status: "received")}
    scope :completed, -> {where(status: "completed")}
    scope :sku_contain, -> (sku) {joins(:variant).where("spree_variants.sku like ?", "%#{sku}%")}

    self.whitelisted_ransackable_associations = %w[variant container]

    state_machine :status, initial: :pending do
      event :ship do
        transition to: :shipped, from: :pending
      end
      after_transition to: :shipped, do: :after_shipped

      event :receive do
        transition to: :received, from: :shipped
      end
      after_transition to: :received, do: :after_received
      
      event :complete do
        transition to: :completed, from: :received
      end
      after_transition to: :completed, do: :after_complete
    end

    def after_shipped
      purchase_line_item.ship! unless purchase_line_item.shipped?
    end

    def delete_item
      Spree::ContainerItem.transaction do
        purchase_line_item.update_columns(shipment_status: "pending")
        self.destroy
      end

    end

    def after_received
      qty_received = Spree::ContainerItem.where(purchase_line_item_id: self.purchase_line_item_id).sum(:qty_received)

      # remove from preorder album
      variant.product.classifications.joins(:taxon).where("spree_taxons.taxonomy_id = ?", 1).delete_all
      
      update_stock_item
      update_weight

      variant_total_items = purchase_line_item.items
      
      if variant.stock_items.first.count_on_hand >= 0
        update_preorder_status
      end
      if qty_received < variant_total_items
        purchase_line_item.in_complete! unless purchase_line_item.in_completed?
      else
        if qty_received >= purchase_line_item.items
          purchase_line_item.complete! unless purchase_line_item.completed?
        else
          purchase_line_item.in_complete! unless purchase_line_item.in_completed?
        end
        complete!
      end
      
      post_journal_inventory_from_container
    end

    def accrued_inventory_account_id(currency)
      if currency == "RMB"
        @accrued = Journal::Account.find_by_code("1-10202").id
      else
        @accrued = Journal::Account.find_by_code("1-10201").id
      end
    end

    def inventory_account_id(currency)
      if currency == "RMB"
        @accrued = Journal::Account.find_by_code("1-10204").id
      else
        @accrued = Journal::Account.find_by_code("1-10203").id
      end
    end

    def post_journal_inventory_from_container
      description = "Received Container: #{container.id} - #{qty_received} pcs X #{variant.sku}"
      amount = qty_received * variant.cost_price.to_i
      journal_entry = Journal::JournalEntry.create(description: description, date: DateTime.now)
      journal_credit = Journal::JournalItem.create(account_id: accrued_inventory_account_id(variant.cost_currency), description: description, credit: amount , currency: variant.cost_currency, originator: self, journal_entry_id: journal_entry.id)
      journal_debit = Journal::JournalItem.create(account_id: inventory_account_id(variant.cost_currency), description: description, debit: amount , currency: variant.cost_currency, originator: self, journal_entry_id: journal_entry.id)
      if variant.cost_currency == "RMB"
        kurs = variant.real_kurs
        amount_idr = amount * kurs
        journal_entry_inv = Journal::JournalEntry.create(description: description, date: DateTime.now)
        journal_credit = Journal::JournalItem.create(account_id: accrued_inventory_account_id("IDR"), description: description, credit: amount_idr , currency: "IDR", originator: self, journal_entry_id: journal_entry_inv.id)
        journal_debit = Journal::JournalItem.create(account_id: inventory_account_id("IDR"), description: description, debit: amount_idr , currency: "IDR", originator: self, journal_entry_id: journal_entry_inv.id)
      end
    end

    def update_preorder_status
      variant.product.update_attributes(po_status: "READY", is_preorder: false, available_on: nil, created_at: DateTime.now) 
      variant.stock_items.first.update_columns(backorderable: false)
    end

    def after_complete
      if container.items.count == container.items.completed.count
        container.receive! unless container.completed?
      end
    end

    def update_weight
      return if variant.weight > 0 
      variant.update_attributes(weight: weight)
    end

    def update_stock_item
      stock = self.variant.stock_items.first
      stock.stock_movements.create!(quantity: qty_received, originator: self)
    end

    def is_ready?
      if !self.qty_received.blank?
        if self.qty_received > 0
          return true
        end
      end
      return false
    end

    def outstanding_item
      total_items - qty_received
    end

    def excess_shipping?
      total_items < qty_received
    end

    def shortage?
      qty_received < total_items
    end

    def manual_completed(whodunnit)
      Spree::ContainerItem.transaction do
        if excess_shipping?

        elsif shortage?
          if purchase_line_item.journal_bill_items.present?
            # jika purchase invoice telah di confirmed
            hpp_idr = variant.real_hpp * outstanding_item
            hpp_rmb = variant.cost_currency == "IDR" ? (variant.cost_price/variant.kurs_rmb)*outstanding_item : variant.cost_price*outstanding_item
            bill = purchase_line_item.journal_bill_items.last.bill
            if bill.currency == "RMB"
              ap_account = Journal::Account.find_by_code("2-20104")
              ac_inventory = Journal::Account.find_by_code("1-10202")
              total_outstanding_item = hpp_rmb
            else
              ap_account = Journal::Account.find_by_code("2-20101")
              ac_inventory = Journal::Account.find_by_code("1-10201")
              total_outstanding_item = hpp_idr
            end
            journal_entry = Journal::JournalEntry.create(description: "Mark as complete selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", date: DateTime.now)
            journal_credit = Journal::JournalItem.create(account_id: ac_inventory.id, description: "Mark as complete selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", credit: total_outstanding_item, currency: ap_account.currency, originator: self, journal_entry_id: journal_entry.id)
            journal_debit = Journal::JournalItem.create(account_id: ap_account.id, description: "Mark as complete selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", debit: total_outstanding_item, currency: ac_inventory.currency, originator: self, journal_entry_id: journal_entry.id)
          end
        end
        self.completed_by = whodunnit.id
        self.save
        self.complete! unless completed?
      end
    end

    def add_my_credit(whodunnit)
      Spree::ContainerItem.transaction do 
        hpp_idr = variant.real_hpp * outstanding_item
        hpp_rmb = variant.cost_currency == "IDR" ? (variant.cost_price/variant.kurs_rmb)*outstanding_item : variant.cost_price*outstanding_item

        supplier_deposit = Journal::Account.cash_and_bank.where("journal_accounts.name like ?", container.supplier.name).first
        if supplier_deposit.present?
          account_credit_rmb = Journal::Account.find_by_code("1-10202")
          account_credit_idr = Journal::Account.find_by_code("1-10201")
          journal_entry_credit = Journal::JournalEntry.create(description: "Credit selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", date: DateTime.now)
          if supplier_deposit.currency == "RMB"
            journal_debit = Journal::JournalItem.create(account_id: supplier_deposit.id, description: "Credit selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", debit: hpp_rmb, currency: supplier_deposit.currency, originator: self, journal_entry_id: journal_entry_credit.id)
            journal_credit = Journal::JournalItem.create(account_id: account_credit_rmb.id, description: "Credit selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", credit: hpp_rmb, currency: account_credit_rmb.currency, originator: self, journal_entry_id: journal_entry_credit.id)
          else
            journal_debit = Journal::JournalItem.create(account_id: supplier_deposit.id, description: "Credit selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", debit: hpp_idr, currency: supplier_deposit.currency, originator: self, journal_entry_id: journal_entry_credit.id)
            journal_credit = Journal::JournalItem.create(account_id: account_credit_idr.id, description: "Credit selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", credit: hpp_idr, currency: account_credit_idr.currency, originator: self, journal_entry_id: journal_entry_credit.id)
          end
        end
        self.completed_by = whodunnit.id
        self.save
        self.complete! unless completed?
      end
    end

    def treat_as_expense(whodunnit)
      Spree::ContainerItem.transaction do 
        hpp_idr = variant.real_hpp * outstanding_item
        hpp_rmb = variant.cost_currency == "IDR" ? (variant.cost_price/variant.kurs_rmb)*outstanding_item : variant.cost_price*outstanding_item

        account_credit_rmb = Journal::Account.find_by_code("1-10202") 
        account_credit_idr = Journal::Account.find_by_code("1-10201")
        account_expense_idr = Journal::Account.find_by_code("6-60332")
        account_expense_rmb = Journal::Account.find_by_code("6-60356")
        journal_entry_exp = Journal::JournalEntry.create(description: "Expense selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item} RMB", date: DateTime.now)
        if variant.currency == "RMB"
          journal_debit = Journal::JournalItem.create(account_id: account_expense_rmb.id, description: "Expense selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item} RMB", debit: hpp_rmb, currency: account_expense_rmb.currency, originator: self, journal_entry_id: journal_entry_exp.id)
          journal_credit = Journal::JournalItem.create(account_id: account_credit_rmb.id, description: "Expense selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item} RMB", credit: hpp_rmb, currency: account_credit_rmb.currency, originator: self, journal_entry_id: journal_entry_exp.id)
        else
          journal_debit = Journal::JournalItem.create(account_id: account_expense_idr.id, description: "Expense selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", debit: hpp_idr, currency: account_expense_idr.currency, originator: self, journal_entry_id: journal_entry_exp.id)
          journal_credit = Journal::JournalItem.create(account_id: account_credit_idr.id, description: "Expense selisih barang container #{container.supplier.name} ID #{self.id} - #{variant.sku} x #{outstanding_item}", credit: hpp_idr, currency: account_credit_idr.currency, originator: self, journal_entry_id: journal_entry_exp.id)
        end
        self.completed_by = whodunnit.id
        self.save
        self.complete! unless completed?
      end
    end

    def generate_purchase_order
      purchase_order = Spree::PurchaseOrder.create!(supplier_id: container.supplier_id)
      # QUICK CHEAT FOR SKIPPING CALLBACK AFTER CREATE
      purchase_order.items.delete_all
      if qty_received > total_items
        exceed_items = qty_received - total_items
        cost_price = variant.cost_price
        total_amount = cost_price * exceed_items
        purchase_order.items.create(variant_id: variant_id, quantity: -1, items: exceed_items, 
          cost_price: cost_price, currency: variant.cost_currency, total: total_amount,
          shipment_status: "completed")
      end
      purchase_order.update_po
      purchase_order.save
      purchase_order.confirm!
      complete!
    end

    def reverse_to_shipped
      Spree::ContainerItem.transaction do 
        reverse_journal
        reverse_stock_movement
        self.update_columns(status: "shipped", qty_received: 0)
        reverse_line_status
        reverse_container_status
      end
    end

    def reverse_line_status
      qty_received = Spree::ContainerItem.where(variant_id: self.variant_id).sum(:qty_received)
      if qty_received == 0 
        purchase_line_item.update_columns(shipment_status: "shipped")
      else
        purchase_line_item.update_columns(shipment_status: "in_completed")
      end
    end

    def reverse_container_status
      if container.items.received.count + container.items.completed.count > 0
        container.update_columns(status: 'received')
      else
        container.update_columns(status: 'pending')
      end
    end

    def reverse_stock_movement
      stock = self.variant.stock_items.first
      if stock.update_columns(backorderable: true)
        qty_reverse = qty_received * -1
        stock.stock_movements.create!(quantity: qty_reverse, originator: self)
      end
    end

    def reverse_journal
      description = "Reverse - Container: #{container.id} - #{qty_received} pcs X #{variant.sku}"
      amount = qty_received * variant.cost_price.to_i
      journal_entry_rvs = Journal::JournalEntry.create(description: description, date: DateTime.now)
      journal_debit = Journal::JournalItem.create(account_id: accrued_inventory_account_id(variant.cost_currency), description: description, debit: amount , currency: variant.cost_currency, originator: self, journal_entry_id: journal_entry_rvs.id)
      journal_credit = Journal::JournalItem.create(account_id: inventory_account_id(variant.cost_currency), description: description, credit: amount , currency: variant.cost_currency, originator: self, journal_entry_id: journal_entry_rvs.id)
    end

    private

    def copy_line_items
      line = Spree::PurchaseLineItem.find_by_id self.purchase_line_item_id
      self.purchase_order_id = line.purchase_order.id
      self.purchase_line_item_id = line.id
      self.quantity = line.quantity
      self.items = line.variant.product.total_items
      self.total_items = self.quantity * self.items

      self.save
    end
  end
end
