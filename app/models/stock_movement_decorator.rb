Spree::StockMovement.class_eval do
  include Filterable
  before_save :override_qty, unless: :originator_is_admin?
  after_create :record_to_journal, if: Proc.new {originator_type == "Spree::CustomerReturn" || originator_type == "Spree::LostItem"}
  after_create :create_movement_warehouse, if: :warehouse_originator?

  scope :lost_item, -> {where(originator_type: "Spree::LostItem")}
  scope :start_date, -> (start_date) { where("created_at >= ?", start_date) }
  scope :end_date, -> (end_date) { where("created_at <= ?", end_date) }

  def override_qty
    if self.originator_type == "Spree::Shipment" && self.originator.present?
      variant_id = self.stock_item.variant_id
      line_item = self.originator.order.line_items.where(variant_id: variant_id).first
      if quantity < 0
        self.quantity = line_item.quantity * line_item.qty_per_pack * -1
      else
        self.quantity = quantity * line_item.qty_per_pack
      end
    else
      self.quantity = quantity * self.stock_item.variant.product.total_items
    end
  end

  def balance_stock
    movement_before = Spree::StockMovement.where(stock_item_id: self.stock_item_id).where("id <= ?", self.id)
    balance_quantity_before = movement_before.sum(:quantity)
    balance_item = balance_quantity_before
    return balance_item
  end

  def originator_is_admin?
    originator_type == "Spree::User" || originator_type == "Spree::ContainerItem" || originator_type == "Spree::ReturnToSupplierItem" || 
    originator_type == "Spree::CustomerReturn" || originator_type == "Spree::LostItem" || originator_type == "Spree::InventoryAdjustment"
  end

  def create_movement_warehouse
    stock_item.stock_warehouse_movements.create(quantity: quantity, description: originator_to_human, originator: originator)
  end

  def warehouse_originator?
    originator_type == "Spree::ContainerItem" || originator_type == "Spree::ReturnToSupplierItem" || originator_type == "Spree::CustomerReturn" || 
    originator_type == "Spree::InventoryAdjustment" || originator_type == "Spree::User"
  end

  def originator_to_human
    case originator_type
    when "Spree::User"
      "Override by #{originator.email}"
    when "Spree::Shipment"
      if originator.get_shipping_address
        "#{originator.order_or_invoice.number} - #{originator.get_shipping_address.firstname} #{originator.get_shipping_address.lastname}"
      else
        "Shipment: #{originator.number}"
      end
    when "Spree::ContainerItem"
      if quantity < 0
        "Reverse from Container #{originator.container_id}"
      else
        "Received from Container #{originator.container_id}"
      end
    when "Spree::CustomerReturn"
      "Customer Return - #{originator.number}"
    when "Spree::LostItem"
      if originator.description.blank?
        "Lost Item - #{originator.number}"
      else
        "#{originator.description}"
      end
    when "Spree::UnitCancel"
      "Order - #{originator.inventory_unit.order.number} - #{originator.inventory_unit.order.user.name} "
    when "Spree::Invoice"
      "Invoice - #{originator.id} - #{originator.user.name} "
    when "Spree::InventoryAdjustment"
      "Inventory Adjustment ##{originator.id} - #{originator.name}"
    end

  end

  private

  def update_stock_item_quantity
    return unless stock_item.should_track_inventory?
    stock_item.adjust_count_on_hand quantity
    stock_item.adjust_free_stock quantity
  end

  def record_to_journal
    
  end
end