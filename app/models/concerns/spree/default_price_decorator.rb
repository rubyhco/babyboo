Spree::DefaultPrice.class_eval do 
  delegate :silver_price, :gold_price, :sale_price, :display_silver_amount, :display_gold_amount, :display_sale_amount , :currency, to: :find_or_build_default_price
  delegate :silver_price=, :gold_price=, :sale_price=, to: :find_or_build_default_price
end