module Filterable
  extend ActiveSupport::Concern

  module ClassMethods
    def filter(filtering_params)
      results = self.where(nil)
      params = []
      filtering_params.each do |key, value|
        if value.present?
          if value[0].nil? || !value[0].empty?
            results = results.public_send(key, value) 
            params << key
          end
        end
      end
      results
    end
  end
end