// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require spree/frontend/jquery.cookie
//= require jquery_ujs
//= require spree/frontend
//= require_tree .
//= require spree/frontend/solidus_shipping_module

//= require spree/frontend/solidus_veritrans_gateway
//= require spree/frontend/solidus_banner
//= require spree/frontend/solidus_page
//= require spree/frontend/jquery.carouFredSel-5.6.1
//= require spree/frontend/jquery.easing.1.3
//= require spree/frontend/jquery.mousewheel

/* Set the width of the side navigation to 250px */
function openNav() {
	if(document.getElementById("mySidenav").style.width == "200px"){
    	closeNav();
    } else {
    	document.getElementById("mySidenav").style.width = "200px";
    }
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}//= require spree/frontend/solidus_export_csv
