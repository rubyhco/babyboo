jQuery ->
  cities_shipping_rate = $('#shipping-rate .select-city').html()
  $('#shipping-rate .select-state').change ->
    state = $('option:selected', this).text()
    escaped_state = state.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(cities_shipping_rate).filter("optgroup[label=" + escaped_state + "]").html()
    if options
      $('#shipping-rate .select-city').html(options)
    else
      $('#shipping-rate .select-city').empty()

  cities = $('.select-city').html()
  $('.select-state').change ->
    state = $('option:selected', this).text()
    escaped_state = state.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(cities).filter("optgroup[label=" + escaped_state + "]").html()
    if options
      $('.select-city').html(options)
    else
      $('.select-city').empty()