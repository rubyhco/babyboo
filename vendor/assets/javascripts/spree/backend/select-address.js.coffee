jQuery ->
  provinces = $('.select-state').html()
  $('.select-country').change ->
    country = $('option:selected', this).text()
    escaped_country = country.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(provinces).filter("optgroup[label=" + escaped_country + "]").html()
    if options
      $('.select-state').html(options)
    else
      $('.select-state').empty()


  cities = $('.select-city').html()
  $('.select-state').change ->
    state = $('option:selected', this).text()
    escaped_state = state.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(cities).filter("optgroup[label=" + escaped_state + "]").html()
    if options
      $('.select-city').html(options)
    else
      $('.select-city').empty()