// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require spree/backend
//= require_tree .
//= require spree/backend/solidus_shipping_module
//= require spree/backend/solidus_veritrans_gateway
//= require spree/backend/solidus_import_products
//= require spree/backend/solidus_banner
//= require spree/backend/solidus_page
//= require ckeditor/init
//= require spree/backend/solidus_export_csv
