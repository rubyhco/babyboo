require 'test_helper'

class Journal::AccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @journal_account = journal_accounts(:one)
  end

  test "should get index" do
    get journal_accounts_url
    assert_response :success
  end

  test "should get new" do
    get new_journal_account_url
    assert_response :success
  end

  test "should create journal_account" do
    assert_difference('Journal::Account.count') do
      post journal_accounts_url, params: { journal_account: { currency: @journal_account.currency, is_payment_account: @journal_account.is_payment_account, name: @journal_account.name } }
    end

    assert_redirected_to journal_account_url(Journal::Account.last)
  end

  test "should show journal_account" do
    get journal_account_url(@journal_account)
    assert_response :success
  end

  test "should get edit" do
    get edit_journal_account_url(@journal_account)
    assert_response :success
  end

  test "should update journal_account" do
    patch journal_account_url(@journal_account), params: { journal_account: { currency: @journal_account.currency, is_payment_account: @journal_account.is_payment_account, name: @journal_account.name } }
    assert_redirected_to journal_account_url(@journal_account)
  end

  test "should destroy journal_account" do
    assert_difference('Journal::Account.count', -1) do
      delete journal_account_url(@journal_account)
    end

    assert_redirected_to journal_accounts_url
  end
end
