require 'test_helper'

class Journal::TransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @journal_transaction = journal_transactions(:one)
  end

  test "should get index" do
    get journal_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_journal_transaction_url
    assert_response :success
  end

  test "should create journal_transaction" do
    assert_difference('Journal::Transaction.count') do
      post journal_transactions_url, params: { journal_transaction: { account_id: @journal_transaction.account_id, amount: @journal_transaction.amount, category_id: @journal_transaction.category_id, description: @journal_transaction.description, is_expense: @journal_transaction.is_expense, is_verified: @journal_transaction.is_verified } }
    end

    assert_redirected_to journal_transaction_url(Journal::Transaction.last)
  end

  test "should show journal_transaction" do
    get journal_transaction_url(@journal_transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_journal_transaction_url(@journal_transaction)
    assert_response :success
  end

  test "should update journal_transaction" do
    patch journal_transaction_url(@journal_transaction), params: { journal_transaction: { account_id: @journal_transaction.account_id, amount: @journal_transaction.amount, category_id: @journal_transaction.category_id, description: @journal_transaction.description, is_expense: @journal_transaction.is_expense, is_verified: @journal_transaction.is_verified } }
    assert_redirected_to journal_transaction_url(@journal_transaction)
  end

  test "should destroy journal_transaction" do
    assert_difference('Journal::Transaction.count', -1) do
      delete journal_transaction_url(@journal_transaction)
    end

    assert_redirected_to journal_transactions_url
  end
end
