namespace :brandedbabys do
  desc "Chart of Account - Category"
  task create_coa_category: :environment do
    Journal::Category.create(name: "Account Receivable (A/R)", prefix: "1-101")
    Journal::Category.create(name: "Other Current Assets", prefix: "1-103")
    Journal::Category.create(name: "Cash & Bank", prefix: "1-100")
    Journal::Category.create(name: "Inventory", prefix: "1-102")
    Journal::Category.create(name: "Fixed Assets", prefix: "1-107")
    Journal::Category.create(name: "Other Assets", prefix: "1-108")
    Journal::Category.create(name: "Depreciation & Amortization", prefix: "1-107")
    Journal::Category.create(name: "Account Payable (A/P)", prefix: "2-201")
    Journal::Category.create(name: "Credit Card", prefix: "2-21")
    Journal::Category.create(name: "Other Current Liabilities", prefix: "2-202")
    Journal::Category.create(name: "Long Term Liabilities", prefix: "2-207")
    Journal::Category.create(name: "Equity", prefix: "3-302")
    Journal::Category.create(name: "Income", prefix: "4-402")
    Journal::Category.create(name: "Other Income", prefix: "7-700")
    Journal::Category.create(name: "Cost of Sales", prefix: "5-504")
    Journal::Category.create(name: "Expenses", prefix: "6-603")
    Journal::Category.create(name: "Other Expenses", prefix: "9-900")
  end

  desc "Chart of Account"
  task create_coa: :environment do

    # IDR
    Journal::Account.create(name: "Cash", currency: "IDR", category_id: 3)
    Journal::Account.create(name: "Bank Account", currency: "IDR", category_id: 3)
    Journal::Account.create(name: "Supplier Deposit", currency: "IDR", category_id: 3)
    Journal::Account.create(name: "Account Receivable", currency: "IDR", category_id: 1)
    Journal::Account.create(name: "Unbilled Accounts Receivable", currency: "IDR", category_id: 1)
    Journal::Account.create(name: "Inventory", currency: "IDR", category_id: 4)
    Journal::Account.create(name: "Others Receivables", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "Employee Receivables", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "Undeposited Funds", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "Other current assets", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "Prepaid expenses", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "Advances", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "VAT In", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "Prepaid Income Tax - PPh 22", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "Prepaid Income Tax - PPh 23", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "Prepaid Income Tax - PPh 25", currency: "IDR", category_id: 2)
    Journal::Account.create(name: "Fixed Assets - Land", currency: "IDR", category_id: 5)
    Journal::Account.create(name: "Fixed Assets - Building", currency: "IDR", category_id: 5)
    Journal::Account.create(name: "Fixed Assets - Building Improvements", currency: "IDR", category_id: 5)
    Journal::Account.create(name: "Fixed Assets - Vehicles", currency: "IDR", category_id: 5)
    Journal::Account.create(name: "Fixed Assets - Machinery & Equipment", currency: "IDR", category_id: 5)
    Journal::Account.create(name: "Fixed Assets - Office Equipment", currency: "IDR", category_id: 5)
    Journal::Account.create(name: "Fixed Assets - Leased Asset", currency: "IDR", category_id: 5)
    Journal::Account.create(name: "Intangible Assets", currency: "IDR", category_id: 5)
    Journal::Account.create(name: "Accumulated Depreciation - Building", currency: "IDR", category_id: 7)
    Journal::Account.create(name: "Accumulated Depreciation - Building Improvements", currency: "IDR", category_id: 7)
    Journal::Account.create(name: "Accumulated Depreciation - Vehicles", currency: "IDR", category_id: 7)
    Journal::Account.create(name: "Accumulated Depreciation - Machinery & Equipment", currency: "IDR", category_id: 7)
    Journal::Account.create(name: "Accumulated Depreciation - Office Equipment", currency: "IDR", category_id: 7)
    Journal::Account.create(name: "Accumulated Depreciation - Leased Asset", currency: "IDR", category_id: 7)
    Journal::Account.create(name: "Accumulated Amortization", currency: "IDR", category_id: 7)
    Journal::Account.create(name: "Investments", currency: "IDR", category_id: 6)
    Journal::Account.create(name: "Account Payable", currency: "IDR", category_id: 8)
    Journal::Account.create(name: "Trade Payable", currency: "IDR", category_id: 8)
    Journal::Account.create(name: "Unbilled Accounts Payable", currency: "IDR", category_id: 8)
    Journal::Account.create(name: "Other Payables", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Salaries Payable", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Dividends Payable", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Unearned Revenue", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Payable - Others", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Accrued Utilities", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Accrued Interest", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Other Accrued Expenses", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Bank Loans", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "VAT Out", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Tax Payable - PPh 21", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Tax Payable - PPh 22", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Tax Payable - PPh 23", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Tax Payable - PPh 29", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Other Taxes Payable", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Loan from Shareholders", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Other Current Liabilities", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Customer Deposit", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Store Credit", currency: "IDR", category_id: 10)
    Journal::Account.create(name: "Employee Benefit Liabilities", currency: "IDR", category_id: 11)
    Journal::Account.create(name: "Paid In Capital", currency: "IDR", category_id: 12)
    Journal::Account.create(name: "Additional Paid In Capital", currency: "IDR", category_id: 12)
    Journal::Account.create(name: "Retained Earnings", currency: "IDR", category_id: 12)
    Journal::Account.create(name: "Dividends", currency: "IDR", category_id: 12)
    Journal::Account.create(name: "Other Comprehensive Income", currency: "IDR", category_id: 12)
    Journal::Account.create(name: "Opening Balance Equity", currency: "IDR", category_id: 12)
    Journal::Account.create(name: "Revenues", currency: "IDR", category_id: 13)
    Journal::Account.create(name: "Sales Discount", currency: "IDR", category_id: 13)
    Journal::Account.create(name: "Sales Return", currency: "IDR", category_id: 13)
    Journal::Account.create(name: "Unbilled Revenues", currency: "IDR", category_id: 13)
    Journal::Account.create(name: "Sales Return", currency: "IDR", category_id: 13)
    Journal::Account.create(name: "Sales Return - No Stock", currency: "IDR", category_id: 13)
    Journal::Account.create(name: "Cost of Sales", currency: "IDR", category_id: 15)
    Journal::Account.create(name: "Purchase Discounts", currency: "IDR", category_id: 15)
    Journal::Account.create(name: "Purchase Return", currency: "IDR", category_id: 15)
    Journal::Account.create(name: "Shipping/Freight & Delivery", currency: "IDR", category_id: 15)
    Journal::Account.create(name: "Import Charges", currency: "IDR", category_id: 15)
    Journal::Account.create(name: "Cost of Production", currency: "IDR", category_id: 15)
    Journal::Account.create(name: "Selling Expenses", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Advertising & Promotion", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Commission & Fees", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Fuel, Toll and Parking - Sales", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Travelling - Sales", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Communication - Sales", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Other Marketing", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "General & Administrative Expenses", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Salaries", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Wages", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Meals and Transport", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Overtime", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Medical", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "THR and Bonus", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Jamsostek", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Incentive", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Severance", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Other Benefit and Allowances", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Donations", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Entertainment", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Fuel, Toll and Parking - General", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Repair and Maintenance", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Travelling - General", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Meals", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Communication - General", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Dues & Subscription", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Insurance", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Legal & Professional Fees", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Employee Benefit Expense", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Utilities Expense", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Training & Developments", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Bad Debt Expense", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Taxes and Licenses", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Penalties", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Office Expense", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Stationery & Printing", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Stamp & Duty", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Securities and Cleaning", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Supplies and Materials", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Subcontractors", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Rental Expense - Building", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Rental Expense - Vehicle", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Rental Expense - Operating Lease", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Rental Expense - Others", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Depreciation - Building", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Depreciation - Building Improvements", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Depreciation - Vehicle", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Depreciation - Machinery & Equipment", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Depreciation - Office Equipment", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Depreciation - Leased Asset", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Waste Goods Expense", currency: "IDR", category_id: 16)
    Journal::Account.create(name: "Interest Income - Bank", currency: "IDR", category_id: 14)
    Journal::Account.create(name: "Interest Income - Time deposit", currency: "IDR", category_id: 14)
    Journal::Account.create(name: "Other Income", currency: "IDR", category_id: 14)
    Journal::Account.create(name: "Interest Expense", currency: "IDR", category_id: 17)
    Journal::Account.create(name: "Provision", currency: "IDR", category_id: 17)
    Journal::Account.create(name: "(Gain)/Loss on Disposal of Fixed Assets", currency: "IDR", category_id: 17)
    Journal::Account.create(name: "Inventory Adjustments", currency: "IDR", category_id: 17)
    Journal::Account.create(name: "Other Miscellaneous Expense", currency: "IDR", category_id: 17)
    Journal::Account.create(name: "Income Taxes - Current", currency: "IDR", category_id: 17)
    Journal::Account.create(name: "Income Taxes - Deferred", currency: "IDR", category_id: 17)

     # RMB
    Journal::Account.create(name: "Inventory", currency: "RMB", category_id: 4)
    Journal::Account.create(name: "Account Payable", currency: "RMB", category_id: 8)
  end

  desc "If stock less than total items, change to retail"
  task update_stock: :environment do
    Spree::Product.all.each do |p|
      next if p.total_on_hand <= 0
      next if p.total_on_hand >= p.total_items
      p.update_attributes(total_items: 1)
    end
  end

  desc "Generate Invoice"
  task generate_invoice: :environment do
    Spree::Invoice.generate_invoice
  end

  desc "Set Member Type"
  task set_member_type: :environment do
    Spree::User.not_gold.each do |user|
      total_order_from_ready_stock = user.orders.paid.ready_stock.sum(:total)
      total_order_from_invoice = user.invoices.paid.sum(:total)
      total = total_order_from_ready_stock + total_order_from_invoice + user.old_transaction_total
      price_level = Spree::PriceLevel.first

      if total < price_level.silver_member
        user.member_type = "Reguler"
      elsif total < price_level.gold_member
        user.member_type = "Silver"
      else
        user.member_type = "Gold"
      end

      user.save
    end
  end

  desc "Grouping shipments"
  task generate_shipment_group: :environment do 
    Spree::ShipmentGroup.generate_shipment_group
  end

  desc "payment reminder to customer"
  task send_payment_reminder: :environment do
    # Spree::Order.where("payment_state != ? && created_at <= ?", "paid", DateTime.now - 3).each do |order|
    #   sms = RajaSms.new
    #   phone = order.dropship? ? order.dropship_phone : order.ship_address.phone
    #   if order.preorder?
    #     sms.send_sms(phone, "Pesanan #{order.number} menunggu pembayaran. Silahkan lakukan pembayaran sebesar 
    #       Rp#{order.down_payment_total}. Silahkan cek email Anda untuk info lengkapnya. -BrandedBabys")
    #   else
    #     sms.send_sms(phone, "Pesanan #{order.number} menunggu pembayaran. Silahkan lakukan pembayaran sebesar 
    #       #{order.display_total.to_html}. Silahkan cek email Anda untuk info lengkapnya. -BrandedBabys")
    #   end
    # end
    datetime_start = (DateTime.now - 3).beginning_of_day
    datetime_end = (DateTime.now - 3).end_of_day
    Spree::Invoice.valid.pending_payment.where('created_at BETWEEN ? AND ?',datetime_start, datetime_end).each do |inv|
      sms = RajaSms.new
      order = inv.items.first.order
      phone = order.dropship? ? order.dropship_phone : inv.items.first.ship_address.phone
      sms.send_sms(phone, "Pesanan #{inv.id} menunggu pembayaran. Silahkan lakukan pembayaran sebesar 
        Rp#{inv.outstanding_balance}. Silahkan cek email Anda untuk info lengkapnya. -BrandedBabys")
      inv.send_email_shipment_ready
    end
  end

  desc "set preorder album open flag"
  task set_preorder_album_open_flag: :environment do
    Spree::Taxon.open_po.each do |taxon|
      total_out_of_stock = 0
      taxon.products.each do |product|
        product.variants.each do |variant|
          if variant.stock_items.first.expected_count_on_hand != 0
            if variant.stock_items.first.expected_count_on_hand + variant.stock_items.first.count_on_hand <= 0
              total_out_of_stock += 1
            end
          end
        end
        if total_out_of_stock == product.variants.count
          puts taxon.name
          taxon.update_attributes(is_open: false)
        end
      end
    end
  end

  desc "delete duplicate record in spree inventory units"
  task remove_duplicate_inventory_units: :environment do
    ids = Spree::InventoryUnit.all.select("MIN(id) as id").group(:variant_id, :order_id).collect(&:id)
    split_ids = Spree::InventoryUnit.split.collect(&:id)
    ids += split_ids
    Spree::InventoryUnit.where.not(id: ids.uniq).delete_all
  end

  desc "set active user in user"
  task set_active_user: :environment do
    order = Spree::Order.where("state = ? && created_at >= ?", "complete", DateTime.now - 180)
    Spree::User.all.each do |user|
      active_user = order.where(user_id: user.id)
      if !active_user.empty?
        user.update_columns(active_user: true)
      else
        user.update_columns(active_user: false)
      end
    end
  end

  desc "set address"
  task set_address: :environment do
    addresses = Spree::Address.all
    addresses.each do |a|
      next if Spree::Address.find_by_id(a.id).nil?
      duplicate = Spree::Address.where(address1: a.address1, city: a.city)
      puts "============================================>>>>>>>>>"
      puts "============================================>>>>>>>>>"
      puts "============================================>>>>>>>>>"
      puts "id: #{a.id} / name: #{a.firstname} / address1: #{a.address1}"
      duplicate.each do |address|
        next if address.id == a.id
        puts "duplicate: #{address.firstname} / address1: #{address.address1}"
        Spree::Order.where(ship_address_id: address.id).update_all(ship_address_id: a.id)
        Spree::Order.where(bill_address_id: address.id).update_all(bill_address_id: a.id)
        Spree::InvoiceItem.where(ship_address_id: address.id).update_all(ship_address_id: a.id)
        Spree::User.where(ship_address_id: address.id).update_all(ship_address_id: a.id)
        Spree::User.where(bill_address_id: address.id).update_all(bill_address_id: a.id)
      end
      
      duplicate.where.not(id: a.id).delete_all
      user = Spree::User.find_by_ship_address_id(a.id)
      next if user.nil?
      Spree::UserAddress.where(default: false).where(user_id: user.id).delete_all
      Spree::UserAddress.where(default: true).where(user_id: user.id).update_all(address_id: a.id)
    end
  end

  task update_shipment_preorder: :environment do
    Spree::Order.where(preorder: true).where("shipment_state != ?", "shipped").each do |o|
      order_count = o.inventory_units.count
      shipped_count = o.inventory_units.where(state: "shipped").count
      if order_count == shipped_count
        puts "=================>>>>>>>>>>>>>>>> #{o.number}"
        o.update_columns(shipment_state: "shipped")
        o.shipments.collect {|x|x.update_columns(state: "shipped")}
      end
    end
  end

  desc "automatic cancel order/invoice by system"
  task auto_cancel: :environment do

    # for order ready_stock auto cancel
    ready_stocks = Spree::Order.where(state: "complete").ready_stock.payment_state_is("balance_due")
    ready_stocks.each do |order|
      next if order.total.to_i != order.outstanding_balance
      if !order.payment_confirmations.empty?
        # untuk cancel order yg waiting confirmed tapi sudah lebih dari 7 hari
        due_date_waiting_confirmed = order.completed_at + 7.days
        order.cancel! if Time.now > due_date_waiting_confirmed 
      else
        due_date = order.completed_at + 1.days
        order.cancel! if Time.now > due_date
      end
    end

    # for order po auto cancel
    preorders = Spree::Order.where(state: "complete").complete.preorder.payment_state_is("balance_due")
    preorders.each do |order|
      next if order.down_payment_total.to_i != order.outstanding_balance.to_i
      if !order.payment_confirmations.empty?
        # untuk cancel order yg waiting confirmed tapi sudah lebih dari 7 hari
        due_date_waiting_confirmed = order.completed_at + 7.days
        order.cancel! if Time.now > due_date_waiting_confirmed 
      else
        due_date = order.completed_at + 2.days
        order.cancel!  if Time.now > due_date
      end
    end

    # for invoice auto cancel
    # invoices = Spree::Invoice.valid.pending_payment
    # user_system = Spree::User.find_by_email("system@brandedbabys.com")
    # invoices.each do |inv|
    #   next if !inv.payment_confirmations.empty?
    #   due_date = inv.created_at + 3.days
    #   if Time.now > due_date
    #     inv.cancel!(user_system) 
    #   end
    # end
  end

  desc "store credit for invoice"
  task store_credit_cn: :environment do
    Journal::Invoice.all.each do |x|
      user = Spree::User.find_by_id x.spree_user_id
      remaining = x.total

      user.store_credits.order_by_priority.each do |credit|
        break if remaining.zero?
        next if credit.amount_remaining.zero?
        amount_to_take = [remaining, credit.amount_remaining].min
        auth = credit.authorize(
          amount_to_take,
          "IDR",
          action_originator: Spree::User.find_by_id(3)
        )
        credit.capture(amount_to_take, auth, "IDR", action_originator: Spree::User.find_by_id(3))
        remaining -= amount_to_take
      end
      if remaining <= 0
        x.destroy
      else
        x.update_attributes(total: remaining)
        x.items.first.update_attributes(amount: remaining)
      end
    end  
  end

  desc "set complete shipment"
  task set_complete_shipment: :environment do
    Spree::ShipmentGroup.status_contain("shipped").each do |s|
      if !s.resi.blank? && !s.weight.blank? && !s.actual_cost.blank?
        s.complete!
      end
    end
  end

  desc "hide out of stock product"
  task hide_products: :environment do
    Spree::StockItem.where("count_on_hand = ? AND backorderable = ?", 0, false).collect {|x|x.variant.product.update_columns(available_on: nil)}
    Spree::StockItem.where("count_on_hand != ? AND expected_count_on_hand > ?", 0, 0).each do |x|
      a = x.expected_count_on_hand + x.count_on_hand
      next if a > 0
      next if x.variant.product.available_on.blank?
      x.variant.product.update_columns(available_on: nil)
    end

    # untuk product preorder yg po_closed_at melebihi Today.now
    # Spree::Product.where(is_preorder: true).where("po_closed_at < ?",Time.now).where("available_on IS NOT NULL").collect{|product|product.update_columns(available_on: nil)}
  end

  desc "new hide out of stock product"
  task new_hide_products: :environment do
    #untuk ready stock 1 minggu setelah sold auto hide
    Spree::Product.joins(:stock_items).where("available_on is not ? AND is_preorder = ? AND 
      spree_stock_items.count_on_hand <= ?", nil, false, 0).collect {|x| x.update_columns(available_on: nil) if x.stock_items.first.updated_at + 7.days < Time.now}

    Spree::Product.where("available_on is not ? AND is_preorder = ?", nil, true).each do |x|
      next if x.stock_items.first.count_on_hand < 0 && x.stock_items.first.expected_count_on_hand == 0
      x.update_columns(available_on: nil) if x.po_closed_at + 5.days < Time.now && x.stock_items.first.expected_count_on_hand + x.stock_items.first.count_on_hand <= 0
    end
  end

  desc "auto delete album if all product hide"
  task auto_delete_album: :environment do
    Spree::Taxon.permalink_contain("preorder/album/").where("closed_at IS NOT NULL").where("closed_at < ?",Time.now).each do |taxon|
      total_product_taxon = taxon.products.count
      total_product_hide = taxon.products.where(available_on: nil).count
      if total_product_taxon == total_product_hide
        taxon.destroy
      end
    end
  end

  desc "remove stock movement"
  task normalize_stock_movement_karena_discount_line_item: :environment do
    variant_ids = Spree::LineItem.joins(:order).where("spree_line_items.quantity > ?", 1).where("spree_orders.approver_id IS NOT NULL").where("spree_orders.state = ?","complete").where("spree_orders.approved_at <= ?", "2017-09-05 23:59:59").pluck(:variant_id)
    variant_ids.each do |x|
        puts "=================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> #{x}"
        ids = Spree::StockMovement.where(stock_item_id: x).select("MIN(id) as id").group(:stock_item_id, :originator_id).collect(&:id)
        Spree::StockMovement.where(stock_item_id: x).where.not(id: ids).each do |y|
            next if y.quantity > 0
            puts "=================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> #{y.id}: #{y.stock_item.count_on_hand}"
            y.stock_item.update_columns(count_on_hand: y.stock_item.count_on_hand + y.quantity.abs)
            puts "=================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> #{y.id} UPDATED: #{y.stock_item.count_on_hand}"
            y.delete 
        end
    end
  end

  desc "normalize inventory unit status"
  task normalize_inventory_unit: :environment do
    Spree::InventoryUnit.where(state: "on_hand").each do |x|
      if Spree::ContainerItem.where(variant_id: x.variant_id).count < 1 && Spree::StockMovement.where(stock_item_id: x.variant_id, originator_type: "Spree::User").where("quantity > 0").count < 1
        x.shipment.update_columns(state: "pending")
        x.order.update_columns(shipment_state: "backorder")
        x.update_columns(state: "backordered") 
     end
    end
  end

  desc "update cost price variant rmb"
  task update_cost_price_variant_rmb: :environment do
    variant_rmb_1 = [[1090,71225],[1091,67375],[785,71225],[786,71225],[787,67375],[788,75075],[789,75075],[790,73150],[791,71225],[792,73150],[793,67375],[794,77000],[440,96250],[441,92400],[442,73150],[443,67375],[444,69300],[445,96250],[446,86625],[447,86625],[448,86625],[449,73150],[450,73150],[969,92160],[1269,92160],[970,92160],[1267,92160],[971,105600],[972,57600],[973,90240],[974,90240],[1266,90240],[975,90240],[976,67200],[977,67200],[1268,67200],[978,72960],[979,72960],[1265,72960],[980,67200],[1264,67200],[808,86625],[809,71225],[810,67375],[811,86625],[812,71225],[813,63525],[814,92400],[815,92400],[816,94325],[817,73150],[818,86625],[819,63525],[820,63525],[293,82775],[451,67375],[452,67375],[453,63525],[454,63525],[455,90475],[456,71225],[457,67375],[458,73150],[459,90475],[460,82775],[197,49500],[198,49500],[461,65450],[462,65450],[463,65450],[464,67375],[465,65450],[466,65450],[467,67375],[468,65450],[469,65450],[470,67375],[471,63525],[472,71225],[473,59675],[474,59675],[795,71225],[904,71225],[796,71225],[797,71225],[905,71225],[798,71225],[906,71225],[799,71225],[800,71225],[907,71225],[801,71225],[908,71225],[802,71225],[909,71225],[769,71225],[910,71225],[803,71225],[911,71225],[804,71225],[912,71225],[805,71225],[913,71225],[806,71225],[914,71225],[807,71225],[1092,61600],[1108,61600],[1107,61600],[1093,61600],[1094,61600],[1095,61600],[1096,61600],[1106,61600],[1097,38500],[1104,38500],[1105,38500],[1103,38500],[1098,38500],[1099,38500],[1100,38500],[1102,38500],[1101,38500],[646,40425],[647,40425],[648,25025],[649,25025],[650,25025],[136,72580],[126,61120],[184,72580],[127,61120],[166,61120],[185,72580],[128,61120],[167,61120],[137,72580],[129,61120],[168,61120],[138,72580],[130,61120],[169,61120],[139,72580],[172,72580],[131,61120],[140,72580],[132,61120],[141,72580],[133,61120],[186,72580],[134,61120],[170,61120],[142,72580],[173,72580],[135,61120],[171,61120],[174,78310],[175,91680],[149,19100],[176,72580],[177,72580],[148,84040],[143,72580],[178,72580],[144,72580],[145,72580],[179,72580],[182,84040],[146,72580],[187,84040],[147,72580],[180,72580],[183,84040],[181,72580],[306,88090],[294,76600],[307,88090],[295,76600],[308,88090],[296,76600],[309,88090],[297,76600],[310,88090],[298,76600],[311,88090],[299,76600],[312,88090],[300,76600],[301,76600],[302,76600],[303,76600],[313,88090],[304,76600],[305,76600],[419,101495],[434,101495],[488,101495],[429,90005],[430,90005],[481,90005],[420,101495],[435,101495],[489,101495],[410,90005],[482,90005],[421,101495],[436,101495],[490,101495],[411,90005],[483,90005],[422,101495],[437,101495],[491,101495],[412,90005],[423,101495],[492,101495],[413,90005],[431,90005],[484,90005],[424,101495],[438,101495],[493,101495],[414,90005],[432,90005],[425,101495],[439,101495],[494,101495],[415,90005],[426,101495],[416,90005],[433,90005],[485,90005],[427,101495],[417,90005],[486,90005],[428,101495],[495,101495],[418,90005],[487,90005],[561,82345],[573,93835],[562,82345],[574,93835],[563,82345],[575,93835],[564,82345],[576,93835],[565,82345],[577,93835],[566,82345],[567,63195],[578,72770],[568,63195],[569,63195],[579,72770],[570,63195],[580,93835],[571,82345],[581,93835],[572,82345],[701,60295],[689,48625],[690,48625],[702,60295],[691,48625],[692,48625],[703,60295],[693,48625],[704,60295],[694,48625],[695,48625],[705,60295],[696,48625],[706,60295],[697,48625],[707,60295],[698,48625],[699,48625],[700,48625],[708,42790],[709,42790],[710,46680],[711,46680],[720,60295],[712,50570],[721,60295],[713,50570],[722,62240],[715,50570],[723,62240],[714,50570],[724,62240],[716,50570],[725,62240],[717,50570],[718,50570],[726,62240],[719,50570],[737,87525],[727,75855],[728,75855],[738,87525],[729,75855],[730,75855],[739,87525],[731,75855],[732,75855],[733,75855],[740,87525],[734,75855],[735,75855],[736,75855],[751,81690],[741,70020],[742,70020],[743,70020],[744,70020],[752,81690],[745,70020],[753,81690],[746,70020],[754,81690],[747,70020],[755,81690],[748,70020],[756,81690],[749,70020],[750,70020],[893,21065],[894,21065],[895,21065],[896,21065],[897,21065],[898,21065],[899,21065],[900,21065],[1118,21065],[901,21065],[902,40215],[1112,40215],[1111,40215],[1110,40215],[1109,40215],[1117,38300],[1113,38300],[1114,38300],[1115,38300],[903,38300],[1116,38300],[475,90475],[476,90475],[477,77000],[478,77000],[479,71225],[480,71225],[770,67375],[771,67375],[772,73150],[773,73150],[774,71225],[775,71225],[776,71225],[777,75075],[778,75075],[779,75075],[780,73150],[781,73150],[782,67375],[783,77000],[784,75075],[1132,71040],[1133,76800],[1134,76800],[1135,76800],[1136,78720],[1137,78720],[1138,78720],[1139,72960],[1140,72960],[1141,74880],[1142,74880],[1143,78720],[1144,78720],[1145,74880],[1146,74880],[509,47750],[502,38200],[503,38200],[504,38200],[510,47750],[511,47750],[505,38200],[512,47750],[506,38200],[507,38200],[508,38200],[225,36575],[496,36575],[497,36575],[498,36575],[499,36575],[500,36575],[501,36575],[950,63525],[634,96250],[632,84700],[635,96250],[633,84700],[636,57750],[637,57750],[638,57750],[639,57750],[640,57750],[641,57750],[644,92400],[642,77000],[645,92400],[643,77000],[963,67375],[964,67375],[965,63525],[966,63525],[967,63525],[968,63525],[199,79540],[620,73150],[621,73150],[622,69300],[623,69300],[624,71225],[625,71225],[626,71225],[627,75075],[628,75075],[629,73150],[630,71225],[631,71225],[1119,71040],[1120,71040],[1121,71040],[1122,72960],[1123,74880],[1124,74880],[1125,74880],[1126,92160],[1127,78720],[1128,67200],[1129,78720],[1130,78720],[1131,71040],[594,53592],[582,42108],[595,53592],[583,42108],[596,53592],[584,42108],[597,53592],[585,42108],[598,53592],[586,42108],[599,53592],[587,42108],[600,53592],[588,42108],[601,53592],[589,42108],[590,42108],[602,53592],[591,42108],[603,53592],[592,42108],[604,53592],[593,42108],[284,98175],[314,98175],[285,92400],[315,92400],[286,92400],[316,92400],[287,92400],[288,98175],[317,98175],[289,98175],[318,90475],[319,92400],[290,92400],[320,92400],[291,92400],[292,96250],[321,96250],[605,63525],[606,90475],[607,90475],[608,77000],[609,78925],[610,77000],[611,90475],[612,86625],[613,90475],[614,73150],[615,90475],[616,90475],[617,77000],[618,77000],[619,67375],[981,92400],[982,96250],[983,98175],[984,94325],[985,96250],[986,92400],[987,102025],[988,98175],[989,94325],[990,98175],[991,84700],[992,96250],[993,67375],[994,63525]]
    variant_rmb = [[81,68000],[82,68000],[83,68000],[84,68000],[188,32000],[821,53488],[822,82302],[823,82302],[824,82302],[825,63162],[826,82302],[827,72732],[851,50570],[852,50570],[853,50570],[854,50570],[855,50570],[856,50570],[857,50570],[858,50570],[859,50570],[860,50570],[861,50570],[862,50570],[863,62240],[864,62240],[865,62240],[866,62240],[867,62240],[868,62240],[869,62240],[870,62240],[871,62240],[872,62240],[873,62240],[915,62000],[916,62000],[917,62000],[918,72000],[919,72000],[920,72000],[921,72000],[922,72000],[952,62000],[953,62000],[954,62000],[955,62000],[956,62000],[957,72000],[958,72000],[15312,46000],[15313,46000],[15314,46000],[15315,46000],[15316,46000],[15317,46000],[15318,46000],[15319,46000],[15320,46000],[15321,46000],[15322,46000],[17545,60000],[17546,60000],[17547,110000],[17550,130000],[17551,85000],[17552,85000],[17554,60000],[17555,85000],[17556,70000],[17563,100000],[17564,60000],[17566,80000],[17571,75000],[17572,85000],[17573,70000],[17602,75000],[17603,95000],[17604,70000]]
    
    variant_rmb.each do |key,value|
      puts "=================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> #{key}"
      item = Spree::PurchaseLineItem.find_by_id(key)
      item.update_columns(cost_price: value)
      item.total = item.items * item.cost_price
      item.save
      po = item.purchase_order
      po.total = po.items.to_a.sum(&:total)
      po.save
    end
  end 

  desc "update required coa"
  task update_required_coa: :environment do
    # Cash And Bank
    # Journal::Account.find_by_code("1-10001").update_columns(required: true)
    # Journal::Account.find_by_code("1-10002").update_columns(required: true)
    # Journal::Account.find_by_code("1-10003").update_columns(required: true)
    # Journal::Account.find_by_code("1-10004").update_columns(required: true)
    # Journal::Account.find_by_code("1-10005").update_columns(required: true)
    # Journal::Account.find_by_code("1-10006").update_columns(required: true)
    # Journal::Account.find_by_code("1-10007").update_columns(required: true)
    # Journal::Account.find_by_code("1-10008").update_columns(required: true)
    # Journal::Account.find_by_code("1-10009").update_columns(required: true)
    # Journal::Account.find_by_code("1-10010").update_columns(required: true)
    # Journal::Account.find_by_code("1-10011").update_columns(required: true)
    # Journal::Account.find_by_code("1-10012").update_columns(required: true)
    # Journal::Account.find_by_code("1-10013").update_columns(required: true)

    
    Journal::Account.find_by_code("1-10101").update_columns(required: true)
    Journal::Account.find_by_code("1-10201").update_columns(required: true)
    Journal::Account.find_by_code("1-10202").update_columns(required: true)
    Journal::Account.find_by_code("1-10203").update_columns(required: true)
    Journal::Account.find_by_code("1-10204").update_columns(required: true)
    Journal::Account.find_by_code("1-10205").update_columns(required: true)
    Journal::Account.find_by_code("2-20101").update_columns(required: true)
    Journal::Account.find_by_code("2-20104").update_columns(required: true)
    Journal::Account.find_by_code("2-20218").update_columns(required: true)
    Journal::Account.find_by_code("2-20219").update_columns(required: true)
    Journal::Account.find_by_code("4-40201").update_columns(required: true)
    Journal::Account.find_by_code("4-40202").update_columns(required: true)
    Journal::Account.find_by_code("4-40205").update_columns(required: true)
    Journal::Account.find_by_code("4-40206").update_columns(required: true)
    Journal::Account.find_by_code("4-40207").update_columns(required: true)
    Journal::Account.find_by_code("5-50401").update_columns(required: true)
    Journal::Account.find_by_code("6-60352").update_columns(required: true)
  end

  desc "create category store credit"
  task create_category_store_credit: :environment do
    Spree::StoreCreditCategory.create(name: "Cancel Order", journal_account_id: 63)
    Spree::StoreCreditCategory.create(name: "Kelebihan Ongkir", journal_account_id: 63)
    Spree::StoreCreditCategory.create(name: "Refund No Stock", journal_account_id: 63)
    Spree::StoreCreditCategory.create(name: "Refund From Return", journal_account_id: 63)
    Spree::StoreCreditCategory.create(name: "Outstanding From Transaction", journal_account_id: 63)
  end

  desc "tambahan create category store credit"
  task new_create_category_store_credit: :environment do
    Spree::StoreCreditCategory.create(name: "Payment Order", journal_account_id: 63)
    Spree::StoreCreditCategory.create(name: "Payment Invoice", journal_account_id: 63)
    Spree::StoreCreditCategory.create(name: "Payment Credit Note", journal_account_id: 63)
    Spree::StoreCreditCategory.create(name: "Cash Out Request", journal_account_id: 63)
  end

  desc "generate category id"
  task generate_category_id_store_credit_history: :environment do 
    Spree::UserStoreCreditHistory.all.each do |hstr|
      cat_id = nil
      if hstr.amount < 0 && hstr.store_credit_event.present?
        if hstr.store_credit_event.originator_type == "Spree::Payment"
          cat_id = 8
        elsif hstr.store_credit_event.originator_type == "Spree::InvoicePayment"
          cat_id = 9
        elsif hstr.store_credit_event.originator_type == "Journal::InvoicePayment"
          cat_id = 10
        elsif hstr.store_credit_event.originator_type == "Spree::CashOutRequest"
          cat_id = 11
        else
          cat_id = 1
        end
      else
        cat_id = hstr.store_credit_event.present? ? hstr.store_credit_event.store_credit.category_id : 1
      end
      hstr.update_columns(category_id: cat_id)
    end
  end

  desc "fill qty_per_pack line item"
  task fill_qty_per_pack_line_item: :environment do
    Spree::LineItem.all.collect{|x|x.update_columns(qty_per_pack: x.variant.product.total_items)}
  end

  desc "update created at ready stock"
  task update_created_at_ready_stock: :environment do
    Spree::Product.ready_stock.collect{|x|x.update_columns(created_at: x.updated_at)}
  end

  desc "create transaction histories"
  task create_transaction_histories: :environment do
    Journal::Transaction.all.each do |trans|
      # payment
      trans.payments.each do |payment|
        trans.histories.create(originator: payment, name: payment.order.user.name, created_at: payment.created_at, description: "Payment Order #{payment.order.number}")
      end

      # invoice_payment
      trans.invoice_payments.each do |inv_payment|
        trans.histories.create(originator: inv_payment, name: inv_payment.invoice.present? ? inv_payment.invoice.user.name : "No Name", created_at: inv_payment.created_at, description: "Payment Invoice ID #{inv_payment.invoice.present? ? inv_payment.invoice.id : "NULL"}")
      end
      
      # journal_invoice
      trans.journal_invoices.each do |invoice|
        trans.histories.create(originator: invoice, name: invoice.vendor, created_at: invoice.created_at, description: "Credit Note #{invoice.number}")
      end

      # invoice payment
      trans.journal_invoice_payments.each do |inv_payment|
        trans.histories.create(originator: inv_payment, name: inv_payment.invoice.vendor, created_at: inv_payment.created_at, description: "Credit Note #{inv_payment.invoice.number}")
      end

      # outstanding amount
      Spree::StoreCredit.where("memo like ?", "%amount from transaction id #{trans.id}%").each do |sc|
        trans.histories.create(originator: sc, name: sc.user.name, created_at: sc.created_at, description: "Store Credit #{sc.user.name} ID #{sc.id}")
      end

    end
  end

  desc "fix transfer fund"
  task fix_transfer_fund: :environment do
    Spree::VariantKur.all.each do |variant_kurs|
      variant_kurs.update_columns(variant_id: Spree::PurchaseLineItem.find_by_id(variant_kurs.variant_id).variant_id)
    end
  end

  desc "fix variant cost price & currency variant kurs"
  task fix_cost_price_variant_kurs: :environment do
    variant_ids = Spree::VariantKur.pluck(:variant_id).uniq
    Spree::Variant.where(id: variant_ids).each do |variant|
      variant.update_columns(cost_price: variant.cost_price * variant.real_kurs, cost_currency: "IDR")
    end
  end

  desc "Fix stock movement and coh stock item"
  task fix_stock_movement_and_stock_item: :environment do
    Spree::StockMovement.where("quantity < ?", 0).where(originator_type: "Spree::Shipment").each do |movement|
      next unless movement.originator.present?
      order = movement.originator.order.present? ? movement.originator.order : movement.originator.inventory_units.first.order
      next unless order.present?
      line_item = order.line_items.where(variant_id: movement.stock_item.variant_id).first
      next unless line_item.present?
      qty_mv_from_line_item = line_item.total_line_items * -1
      if movement.quantity != qty_mv_from_line_item
        selisih = (movement.quantity - qty_mv_from_line_item) * -1
        movement.update_columns(quantity: qty_mv_from_line_item)
        movement.stock_item.update_columns(count_on_hand: movement.stock_item.count_on_hand + selisih)
      end
    end
  end

  desc "auto publish product when stock available"
  task auto_publish_product: :environment do
    # untuk ready stock
    Spree::Product.joins(:stock_items).where("available_on is ? AND is_preorder = ? AND 
      spree_stock_items.count_on_hand > ?", nil, false, 0).collect {|x| x.update_columns(available_on: DateTime.now)}
    # untuk preorder
    Spree::Product.joins(:stock_items).where("available_on is ? AND is_preorder = ? AND 
      spree_stock_items.count_on_hand + spree_stock_items.expected_count_on_hand > ?", nil, true, 0).collect {|x| x.update_columns(available_on: DateTime.now)}
  end

  desc "Fill stock warehouse on hand and create stock movement gudang"
  task fill_stock_warehouse_and_create_movement: :environment do
    Spree::StockItem.all.each do |stock|
      next if stock.count_on_hand < 0
      if stock.variant.line_items.present?
        stock_order_on_hand = stock.variant.line_items.joins(:inventory_units).where("spree_inventory_units.state = ?", "on_hand").sum(&:total_line_items)
      else
        stock_order_on_hand = 0
      end
      if stock.count_on_hand > 0
        stock.update_columns(stock_warehouse: stock.count_on_hand + stock_order_on_hand)
        stock.stock_warehouse_movements.create(quantity: stock.count_on_hand + stock_order_on_hand, description: "initial stock warehouse", originator: stock)
      end
    end
  end

  desc "Fill free stock count on hand"
  task fill_free_stock: :environment do
    Spree::StockItem.all.collect{|x|x.update_columns(free_stock: x.count_on_hand)}
  end

  desc "generate journal entry to journal items"
  task generate_journal_entry_to_journal_items: :environment do
    Journal::JournalItem.all.group_by{|item| [item.description, item.originator_type, item.originator_id]}.each do |group, items|
      journal_entry = Journal::JournalEntry.create(description: group.first, date: items.first.created_at)
      items.collect{|item| item.update_columns(journal_entry_id: journal_entry.id)}
    end
  end

  desc "generate journal payment expense"
  task generate_journal_payment_expense: :environment do
    Journal::BillPayment.unreconcile.collect{|bill_payment| bill_payment.create_payment_expense}
    Journal::TransferFund.only_expense_reconcile.unreconcile_expense.collect{|transfer_fund| transfer_fund.create_payment_expense}
  end

  task normalize_stock_warehouse: :environment do
    Spree::StockItem.all.collect{|stock|stock.update_columns(stock_warehouse: stock.stock_warehouse_movements.sum(:quantity))}
  end

  task normalize_stock_warehouse_duplicate: :environment do
    all_shipment_order = Spree::StockWarehouseMovement.where("description like ? && created_at > ?", "%shipment from order%", Spree::StockWarehouseMovement.find(24911).created_at)
    ids = all_shipment_order.select("MIN(id) as id").group(:stock_item_id, :quantity, :description, :originator_type, :originator_id).collect(&:id)
    all_shipment_order.where.not(id: ids).each do |swm|
      a = swm.stock_item.update_columns(stock_warehouse: swm.stock_item.stock_warehouse - (swm.quantity))
      if a
        swm.destroy
      end
    end
  end

  task normalize_movement_warehouse_duplicate_without_update_stock: :environment do
    all_shipment_order = Spree::StockWarehouseMovement.all
    ids = all_shipment_order.select("MIN(id) as id").group(:stock_item_id, :quantity, :description, :originator_type, :originator_id).collect(&:id)
    all_shipment_order.where.not(id: ids).each do |swm|
      swm.destroy
    end
  end
end
